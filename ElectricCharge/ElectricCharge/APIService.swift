// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct IRegisterUser: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - password
  ///   - email
  ///   - phone
  public init(name: Swift.Optional<String?> = nil, password: Swift.Optional<String?> = nil, email: Swift.Optional<String?> = nil, phone: Swift.Optional<String?> = nil) {
    graphQLMap = ["name": name, "password": password, "email": email, "phone": phone]
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var password: Swift.Optional<String?> {
    get {
      return graphQLMap["password"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "password")
    }
  }

  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var phone: Swift.Optional<String?> {
    get {
      return graphQLMap["phone"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phone")
    }
  }
}

public struct IPagination: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - page
  ///   - limit
  public init(page: Int, limit: Int) {
    graphQLMap = ["page": page, "limit": limit]
  }

  public var page: Int {
    get {
      return graphQLMap["page"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "page")
    }
  }

  public var limit: Int {
    get {
      return graphQLMap["limit"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "limit")
    }
  }
}

public struct ISearchFilter: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - query
  ///   - available
  ///   - chargeType
  ///   - plugsType
  ///   - powerRating
  ///   - withIn
  ///   - surrounding
  public init(query: Swift.Optional<String?> = nil, available: Swift.Optional<Bool?> = nil, chargeType: Swift.Optional<[ChargerType?]?> = nil, plugsType: Swift.Optional<[PlugType?]?> = nil, powerRating: Swift.Optional<[Double?]?> = nil, withIn: Swift.Optional<ISearchWithIn?> = nil, surrounding: Swift.Optional<ISearchSurrounding?> = nil) {
    graphQLMap = ["query": query, "available": available, "chargeType": chargeType, "plugsType": plugsType, "powerRating": powerRating, "withIn": withIn, "surrounding": surrounding]
  }

  public var query: Swift.Optional<String?> {
    get {
      return graphQLMap["query"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "query")
    }
  }

  public var available: Swift.Optional<Bool?> {
    get {
      return graphQLMap["available"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "available")
    }
  }

  public var chargeType: Swift.Optional<[ChargerType?]?> {
    get {
      return graphQLMap["chargeType"] as? Swift.Optional<[ChargerType?]?> ?? Swift.Optional<[ChargerType?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargeType")
    }
  }

  public var plugsType: Swift.Optional<[PlugType?]?> {
    get {
      return graphQLMap["plugsType"] as? Swift.Optional<[PlugType?]?> ?? Swift.Optional<[PlugType?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plugsType")
    }
  }

  public var powerRating: Swift.Optional<[Double?]?> {
    get {
      return graphQLMap["powerRating"] as? Swift.Optional<[Double?]?> ?? Swift.Optional<[Double?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "powerRating")
    }
  }

  public var withIn: Swift.Optional<ISearchWithIn?> {
    get {
      return graphQLMap["withIn"] as? Swift.Optional<ISearchWithIn?> ?? Swift.Optional<ISearchWithIn?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "withIn")
    }
  }

  public var surrounding: Swift.Optional<ISearchSurrounding?> {
    get {
      return graphQLMap["surrounding"] as? Swift.Optional<ISearchSurrounding?> ?? Swift.Optional<ISearchSurrounding?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "surrounding")
    }
  }
}

public enum ChargerType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case ac
  case dc
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "AC": self = .ac
      case "DC": self = .dc
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .ac: return "AC"
      case .dc: return "DC"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ChargerType, rhs: ChargerType) -> Bool {
    switch (lhs, rhs) {
      case (.ac, .ac): return true
      case (.dc, .dc): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ChargerType] {
    return [
      .ac,
      .dc,
    ]
  }
}

public enum PlugType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case ccs2
  case gbt
  case chademo
  case tesla
  case type1
  case type2
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "CCS2": self = .ccs2
      case "GBT": self = .gbt
      case "CHADEMO": self = .chademo
      case "TESLA": self = .tesla
      case "TYPE1": self = .type1
      case "TYPE2": self = .type2
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .ccs2: return "CCS2"
      case .gbt: return "GBT"
      case .chademo: return "CHADEMO"
      case .tesla: return "TESLA"
      case .type1: return "TYPE1"
      case .type2: return "TYPE2"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: PlugType, rhs: PlugType) -> Bool {
    switch (lhs, rhs) {
      case (.ccs2, .ccs2): return true
      case (.gbt, .gbt): return true
      case (.chademo, .chademo): return true
      case (.tesla, .tesla): return true
      case (.type1, .type1): return true
      case (.type2, .type2): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [PlugType] {
    return [
      .ccs2,
      .gbt,
      .chademo,
      .tesla,
      .type1,
      .type2,
    ]
  }
}

public struct ISearchWithIn: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - topRightLatitude
  ///   - topRightLongitude
  ///   - bottomLeftLatitude
  ///   - bottomLeftLongitude
  public init(topRightLatitude: Swift.Optional<Double?> = nil, topRightLongitude: Swift.Optional<Double?> = nil, bottomLeftLatitude: Swift.Optional<Double?> = nil, bottomLeftLongitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["topRightLatitude": topRightLatitude, "topRightLongitude": topRightLongitude, "bottomLeftLatitude": bottomLeftLatitude, "bottomLeftLongitude": bottomLeftLongitude]
  }

  public var topRightLatitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["topRightLatitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topRightLatitude")
    }
  }

  public var topRightLongitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["topRightLongitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topRightLongitude")
    }
  }

  public var bottomLeftLatitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["bottomLeftLatitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "bottomLeftLatitude")
    }
  }

  public var bottomLeftLongitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["bottomLeftLongitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "bottomLeftLongitude")
    }
  }
}

public struct ISearchSurrounding: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - radius
  ///   - latitude
  ///   - longitude
  public init(radius: Swift.Optional<Double?> = nil, latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["radius": radius, "latitude": latitude, "longitude": longitude]
  }

  public var radius: Swift.Optional<Double?> {
    get {
      return graphQLMap["radius"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "radius")
    }
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public enum SearchSortOption: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case nearest
  case cheapestAc
  case cheapestDc
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "NEAREST": self = .nearest
      case "CHEAPEST_AC": self = .cheapestAc
      case "CHEAPEST_DC": self = .cheapestDc
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .nearest: return "NEAREST"
      case .cheapestAc: return "CHEAPEST_AC"
      case .cheapestDc: return "CHEAPEST_DC"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: SearchSortOption, rhs: SearchSortOption) -> Bool {
    switch (lhs, rhs) {
      case (.nearest, .nearest): return true
      case (.cheapestAc, .cheapestAc): return true
      case (.cheapestDc, .cheapestDc): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [SearchSortOption] {
    return [
      .nearest,
      .cheapestAc,
      .cheapestDc,
    ]
  }
}

public enum StationStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: StationStatus, rhs: StationStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [StationStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public enum ChargerStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ChargerStatus, rhs: ChargerStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ChargerStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public enum PlugStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: PlugStatus, rhs: PlugStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [PlugStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public final class ForgetPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ForgetPassword($username: String!) {
      forgetPassword(username: $username) {
        __typename
        success
        message
        data {
          __typename
          id
          resetToken
        }
        error
      }
    }
    """

  public let operationName: String = "ForgetPassword"

  public var username: String

  public init(username: String) {
    self.username = username
  }

  public var variables: GraphQLMap? {
    return ["username": username]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("forgetPassword", arguments: ["username": GraphQLVariable("username")], type: .object(ForgetPassword.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(forgetPassword: ForgetPassword? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "forgetPassword": forgetPassword.flatMap { (value: ForgetPassword) -> ResultMap in value.resultMap }])
    }

    public var forgetPassword: ForgetPassword? {
      get {
        return (resultMap["forgetPassword"] as? ResultMap).flatMap { ForgetPassword(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "forgetPassword")
      }
    }

    public struct ForgetPassword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ForgetPasswordResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ForgetPasswordResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ForgetPasswordResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("resetToken", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, resetToken: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "ForgetPasswordResponseData", "id": id, "resetToken": resetToken])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var resetToken: String? {
          get {
            return resultMap["resetToken"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "resetToken")
          }
        }
      }
    }
  }
}

public final class LoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation Login($username: String!, $password: String!) {
      loginUser(username: $username, password: $password) {
        __typename
        success
        message
        data {
          __typename
          token
        }
        error
      }
    }
    """

  public let operationName: String = "Login"

  public var username: String
  public var password: String

  public init(username: String, password: String) {
    self.username = username
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["username": username, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("loginUser", arguments: ["username": GraphQLVariable("username"), "password": GraphQLVariable("password")], type: .object(LoginUser.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(loginUser: LoginUser? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "loginUser": loginUser.flatMap { (value: LoginUser) -> ResultMap in value.resultMap }])
    }

    public var loginUser: LoginUser? {
      get {
        return (resultMap["loginUser"] as? ResultMap).flatMap { LoginUser(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "loginUser")
      }
    }

    public struct LoginUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUserResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "LoginUserResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["LoginUserResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "LoginUserResponseData", "token": token])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }
      }
    }
  }
}

public final class RefreshTokenMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RefreshToken {
      refreshToken {
        __typename
        success
        message
        data {
          __typename
          token
        }
        error
      }
    }
    """

  public let operationName: String = "RefreshToken"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("refreshToken", type: .object(RefreshToken.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(refreshToken: RefreshToken? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "refreshToken": refreshToken.flatMap { (value: RefreshToken) -> ResultMap in value.resultMap }])
    }

    public var refreshToken: RefreshToken? {
      get {
        return (resultMap["refreshToken"] as? ResultMap).flatMap { RefreshToken(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "refreshToken")
      }
    }

    public struct RefreshToken: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUserResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "LoginUserResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["LoginUserResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "LoginUserResponseData", "token": token])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }
      }
    }
  }
}

public final class RegisterUserMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RegisterUser($input: IRegisterUser!) {
      registerUser(input: $input) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "RegisterUser"

  public var input: IRegisterUser

  public init(input: IRegisterUser) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("registerUser", arguments: ["input": GraphQLVariable("input")], type: .object(RegisterUser.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(registerUser: RegisterUser? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "registerUser": registerUser.flatMap { (value: RegisterUser) -> ResultMap in value.resultMap }])
    }

    public var registerUser: RegisterUser? {
      get {
        return (resultMap["registerUser"] as? ResultMap).flatMap { RegisterUser(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "registerUser")
      }
    }

    public struct RegisterUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class ResendRegistrationOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ResendRegistrationOTP($userId: String!) {
      resendRegistrationOTP(userId: $userId) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "ResendRegistrationOTP"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("resendRegistrationOTP", arguments: ["userId": GraphQLVariable("userId")], type: .object(ResendRegistrationOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resendRegistrationOtp: ResendRegistrationOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "resendRegistrationOTP": resendRegistrationOtp.flatMap { (value: ResendRegistrationOtp) -> ResultMap in value.resultMap }])
    }

    public var resendRegistrationOtp: ResendRegistrationOtp? {
      get {
        return (resultMap["resendRegistrationOTP"] as? ResultMap).flatMap { ResendRegistrationOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "resendRegistrationOTP")
      }
    }

    public struct ResendRegistrationOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class SearchQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Search($pagination: IPagination, $filter: ISearchFilter, $sort: SearchSortOption) {
      Search(pagination: $pagination, filter: $filter, sort: $sort) {
        __typename
        docs {
          __typename
          station {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            amenities {
              __typename
              name
              value
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            acUnitPriceOverride {
              __typename
              power
              perUnitPrice
            }
            dcUnitPriceOverride {
              __typename
              power
              perUnitPrice
            }
            flag
            images
            chargers {
              __typename
              id
              name
              status
              power
              type
              plugs {
                __typename
                id
                name
                status
                supportedPorts
              }
            }
            cpo {
              __typename
              id
              name
              revenuePlan
              address {
                __typename
                line1
                line2
                city
                state
                zip
                phone
              }
            }
            location
          }
          calculatedDistance
        }
        pagination {
          __typename
          totalDocs
          limit
          hasPrevPage
          hasNextPage
          page
          totalPages
          prevPage
          nextPage
        }
      }
    }
    """

  public let operationName: String = "Search"

  public var pagination: IPagination?
  public var filter: ISearchFilter?
  public var sort: SearchSortOption?

  public init(pagination: IPagination? = nil, filter: ISearchFilter? = nil, sort: SearchSortOption? = nil) {
    self.pagination = pagination
    self.filter = filter
    self.sort = sort
  }

  public var variables: GraphQLMap? {
    return ["pagination": pagination, "filter": filter, "sort": sort]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("Search", arguments: ["pagination": GraphQLVariable("pagination"), "filter": GraphQLVariable("filter"), "sort": GraphQLVariable("sort")], type: .object(Search.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(search: Search? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Search": search.flatMap { (value: Search) -> ResultMap in value.resultMap }])
    }

    public var search: Search? {
      get {
        return (resultMap["Search"] as? ResultMap).flatMap { Search(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Search")
      }
    }

    public struct Search: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SearchResults"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("docs", type: .list(.object(Doc.selections))),
          GraphQLField("pagination", type: .object(Pagination.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(docs: [Doc?]? = nil, pagination: Pagination? = nil) {
        self.init(unsafeResultMap: ["__typename": "SearchResults", "docs": docs.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, "pagination": pagination.flatMap { (value: Pagination) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var docs: [Doc?]? {
        get {
          return (resultMap["docs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Doc?] in value.map { (value: ResultMap?) -> Doc? in value.flatMap { (value: ResultMap) -> Doc in Doc(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, forKey: "docs")
        }
      }

      public var pagination: Pagination? {
        get {
          return (resultMap["pagination"] as? ResultMap).flatMap { Pagination(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "pagination")
        }
      }

      public struct Doc: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SearchResult"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("calculatedDistance", type: .scalar(Double.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(station: Station? = nil, calculatedDistance: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "SearchResult", "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "calculatedDistance": calculatedDistance])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var calculatedDistance: Double? {
          get {
            return resultMap["calculatedDistance"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "calculatedDistance")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("amenities", type: .list(.object(Amenity.selections))),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("acUnitPriceOverride", type: .list(.object(AcUnitPriceOverride.selections))),
              GraphQLField("dcUnitPriceOverride", type: .list(.object(DcUnitPriceOverride.selections))),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("chargers", type: .list(.object(Charger.selections))),
              GraphQLField("cpo", type: .object(Cpo.selections)),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, amenities: [Amenity?]? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, acUnitPriceOverride: [AcUnitPriceOverride?]? = nil, dcUnitPriceOverride: [DcUnitPriceOverride?]? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, cpo: Cpo? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "amenities": amenities.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "acUnitPriceOverride": acUnitPriceOverride.flatMap { (value: [AcUnitPriceOverride?]) -> [ResultMap?] in value.map { (value: AcUnitPriceOverride?) -> ResultMap? in value.flatMap { (value: AcUnitPriceOverride) -> ResultMap in value.resultMap } } }, "dcUnitPriceOverride": dcUnitPriceOverride.flatMap { (value: [DcUnitPriceOverride?]) -> [ResultMap?] in value.map { (value: DcUnitPriceOverride?) -> ResultMap? in value.flatMap { (value: DcUnitPriceOverride) -> ResultMap in value.resultMap } } }, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "cpo": cpo.flatMap { (value: Cpo) -> ResultMap in value.resultMap }, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var amenities: [Amenity?]? {
            get {
              return (resultMap["amenities"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Amenity?] in value.map { (value: ResultMap?) -> Amenity? in value.flatMap { (value: ResultMap) -> Amenity in Amenity(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, forKey: "amenities")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var acUnitPriceOverride: [AcUnitPriceOverride?]? {
            get {
              return (resultMap["acUnitPriceOverride"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AcUnitPriceOverride?] in value.map { (value: ResultMap?) -> AcUnitPriceOverride? in value.flatMap { (value: ResultMap) -> AcUnitPriceOverride in AcUnitPriceOverride(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [AcUnitPriceOverride?]) -> [ResultMap?] in value.map { (value: AcUnitPriceOverride?) -> ResultMap? in value.flatMap { (value: AcUnitPriceOverride) -> ResultMap in value.resultMap } } }, forKey: "acUnitPriceOverride")
            }
          }

          public var dcUnitPriceOverride: [DcUnitPriceOverride?]? {
            get {
              return (resultMap["dcUnitPriceOverride"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DcUnitPriceOverride?] in value.map { (value: ResultMap?) -> DcUnitPriceOverride? in value.flatMap { (value: ResultMap) -> DcUnitPriceOverride in DcUnitPriceOverride(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [DcUnitPriceOverride?]) -> [ResultMap?] in value.map { (value: DcUnitPriceOverride?) -> ResultMap? in value.flatMap { (value: DcUnitPriceOverride) -> ResultMap in value.resultMap } } }, forKey: "dcUnitPriceOverride")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var chargers: [Charger?]? {
            get {
              return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
            }
          }

          public var cpo: Cpo? {
            get {
              return (resultMap["cpo"] as? ResultMap).flatMap { Cpo(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "cpo")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }

          public struct Amenity: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Amenities"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("value", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(name: String? = nil, value: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Amenities", "name": name, "value": value])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var value: String? {
              get {
                return resultMap["value"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "value")
              }
            }
          }

          public struct AcUnitPriceOverride: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["PriceOverride"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("power", type: .scalar(Int.self)),
                GraphQLField("perUnitPrice", type: .scalar(Double.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(power: Int? = nil, perUnitPrice: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "PriceOverride", "power": power, "perUnitPrice": perUnitPrice])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var power: Int? {
              get {
                return resultMap["power"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var perUnitPrice: Double? {
              get {
                return resultMap["perUnitPrice"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitPrice")
              }
            }
          }

          public struct DcUnitPriceOverride: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["PriceOverride"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("power", type: .scalar(Int.self)),
                GraphQLField("perUnitPrice", type: .scalar(Double.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(power: Int? = nil, perUnitPrice: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "PriceOverride", "power": power, "perUnitPrice": perUnitPrice])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var power: Int? {
              get {
                return resultMap["power"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var perUnitPrice: Double? {
              get {
                return resultMap["perUnitPrice"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitPrice")
              }
            }
          }

          public struct Charger: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Charger"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(ChargerStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("type", type: .scalar(ChargerType.self)),
                GraphQLField("plugs", type: .list(.object(Plug.selections))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: ChargerType? = nil, plugs: [Plug?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: ChargerStatus? {
              get {
                return resultMap["status"] as? ChargerStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var type: ChargerType? {
              get {
                return resultMap["type"] as? ChargerType
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var plugs: [Plug?]? {
              get {
                return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
              }
              set {
                resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
              }
            }

            public struct Plug: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Plug"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(String.self)),
                  GraphQLField("name", type: .scalar(String.self)),
                  GraphQLField("status", type: .scalar(PlugStatus.self)),
                  GraphQLField("supportedPorts", type: .list(.scalar(PlugType.self))),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPorts: [PlugType?]? = nil) {
                self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPorts": supportedPorts])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: String? {
                get {
                  return resultMap["id"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var name: String? {
                get {
                  return resultMap["name"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "name")
                }
              }

              public var status: PlugStatus? {
                get {
                  return resultMap["status"] as? PlugStatus
                }
                set {
                  resultMap.updateValue(newValue, forKey: "status")
                }
              }

              public var supportedPorts: [PlugType?]? {
                get {
                  return resultMap["supportedPorts"] as? [PlugType?]
                }
                set {
                  resultMap.updateValue(newValue, forKey: "supportedPorts")
                }
              }
            }
          }

          public struct Cpo: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["CPO"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("revenuePlan", type: .scalar(String.self)),
                GraphQLField("address", type: .object(Address.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, revenuePlan: String? = nil, address: Address? = nil) {
              self.init(unsafeResultMap: ["__typename": "CPO", "id": id, "name": name, "revenuePlan": revenuePlan, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var revenuePlan: String? {
              get {
                return resultMap["revenuePlan"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "revenuePlan")
              }
            }

            public var address: Address? {
              get {
                return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "address")
              }
            }

            public struct Address: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Address"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("line1", type: .scalar(String.self)),
                  GraphQLField("line2", type: .scalar(String.self)),
                  GraphQLField("city", type: .scalar(String.self)),
                  GraphQLField("state", type: .scalar(String.self)),
                  GraphQLField("zip", type: .scalar(String.self)),
                  GraphQLField("phone", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var line1: String? {
                get {
                  return resultMap["line1"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line1")
                }
              }

              public var line2: String? {
                get {
                  return resultMap["line2"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line2")
                }
              }

              public var city: String? {
                get {
                  return resultMap["city"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "city")
                }
              }

              public var state: String? {
                get {
                  return resultMap["state"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "state")
                }
              }

              public var zip: String? {
                get {
                  return resultMap["zip"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "zip")
                }
              }

              public var phone: String? {
                get {
                  return resultMap["phone"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "phone")
                }
              }
            }
          }
        }
      }

      public struct Pagination: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Pagination"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("totalDocs", type: .scalar(Int.self)),
            GraphQLField("limit", type: .scalar(Int.self)),
            GraphQLField("hasPrevPage", type: .scalar(Int.self)),
            GraphQLField("hasNextPage", type: .scalar(Int.self)),
            GraphQLField("page", type: .scalar(Int.self)),
            GraphQLField("totalPages", type: .scalar(Int.self)),
            GraphQLField("prevPage", type: .scalar(Int.self)),
            GraphQLField("nextPage", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDocs: Int? = nil, limit: Int? = nil, hasPrevPage: Int? = nil, hasNextPage: Int? = nil, page: Int? = nil, totalPages: Int? = nil, prevPage: Int? = nil, nextPage: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "Pagination", "totalDocs": totalDocs, "limit": limit, "hasPrevPage": hasPrevPage, "hasNextPage": hasNextPage, "page": page, "totalPages": totalPages, "prevPage": prevPage, "nextPage": nextPage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var totalDocs: Int? {
          get {
            return resultMap["totalDocs"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDocs")
          }
        }

        public var limit: Int? {
          get {
            return resultMap["limit"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "limit")
          }
        }

        public var hasPrevPage: Int? {
          get {
            return resultMap["hasPrevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasPrevPage")
          }
        }

        public var hasNextPage: Int? {
          get {
            return resultMap["hasNextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasNextPage")
          }
        }

        public var page: Int? {
          get {
            return resultMap["page"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "page")
          }
        }

        public var totalPages: Int? {
          get {
            return resultMap["totalPages"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalPages")
          }
        }

        public var prevPage: Int? {
          get {
            return resultMap["prevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "prevPage")
          }
        }

        public var nextPage: Int? {
          get {
            return resultMap["nextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "nextPage")
          }
        }
      }
    }
  }
}

public final class VerifyRegistrationOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation VerifyRegistrationOTP($userId: String!, $otp: String!) {
      verifyRegistrationOTP(userId: $userId, otp: $otp) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "VerifyRegistrationOTP"

  public var userId: String
  public var otp: String

  public init(userId: String, otp: String) {
    self.userId = userId
    self.otp = otp
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "otp": otp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("verifyRegistrationOTP", arguments: ["userId": GraphQLVariable("userId"), "otp": GraphQLVariable("otp")], type: .object(VerifyRegistrationOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(verifyRegistrationOtp: VerifyRegistrationOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "verifyRegistrationOTP": verifyRegistrationOtp.flatMap { (value: VerifyRegistrationOtp) -> ResultMap in value.resultMap }])
    }

    public var verifyRegistrationOtp: VerifyRegistrationOtp? {
      get {
        return (resultMap["verifyRegistrationOTP"] as? ResultMap).flatMap { VerifyRegistrationOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "verifyRegistrationOTP")
      }
    }

    public struct VerifyRegistrationOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}
