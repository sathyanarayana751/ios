//
//  GooglePlacesManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 24/06/22.
//

import Foundation
import GooglePlaces
import GoogleMaps
import Lottie

class GooglePlace:NSObject {
    var query:String?
    var id: String?
    var fullAddress: NSAttributedString?
    var primaryAddress: NSAttributedString?
    var secondaryAddress: NSAttributedString?
    
    init(query:String, prediction:GMSAutocompletePrediction) {
        self.id = prediction.placeID
        self.fullAddress = prediction.attributedFullText
        
        let regularFont = UIFont.systemFont(ofSize: UIFont.labelFontSize)
        let boldFont = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)

        if let boldedText:NSMutableAttributedString = self.fullAddress?.mutableCopy() as? NSMutableAttributedString {
            
            boldedText.enumerateAttributes(in: NSRange(location: 0, length: boldedText.length),
                                           options: .longestEffectiveRangeNotRequired) { value, range, stop in
                
                let font = value.count == 0 ? regularFont : boldFont
                boldedText.addAttribute(.font, value: font, range: range)
            }
            
            self.fullAddress = boldedText;
        }
        
        self.primaryAddress = prediction.attributedPrimaryText
        if let boldedText:NSMutableAttributedString = self.primaryAddress?.mutableCopy() as? NSMutableAttributedString {
            
            boldedText.enumerateAttributes(in: NSRange(location: 0, length: boldedText.length),
                                           options: .longestEffectiveRangeNotRequired) { value, range, stop in
                
                let font = value.count == 0 ? regularFont : boldFont
                boldedText.addAttribute(.font, value: font, range: range)
            }
            
            self.primaryAddress = boldedText;
        }

        if let secondaryText = prediction.attributedSecondaryText {
            self.secondaryAddress = secondaryText
        }
    }
}

class GooglePlaceDetails:NSObject {
    var formattedAddress: String?
    var placeId: String?
    var name: String?
    
    var latitude: Double?
    var longitude: Double?
    var locationCoordinate:CLLocationCoordinate2D?

    init(placeDetails:GMSPlace) {
        self.placeId = placeDetails.placeID
        self.formattedAddress = placeDetails.formattedAddress
        self.name = placeDetails.name
        self.latitude = placeDetails.coordinate.latitude
        self.longitude = placeDetails.coordinate.longitude
        self.locationCoordinate = placeDetails.coordinate
    }
}


class GooglePlacesManager:NSObject {

    static var shared = GooglePlacesManager()
    let placesClient = GMSPlacesClient.shared()
    let token = GMSAutocompleteSessionToken.init()
    var filter = GMSAutocompleteFilter()
    
    var radius = 0
    var strictBounds = false
    let locationManager = LocationManager.shared
    var countryCode: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
    }
    var previousSearchedAddresses:[String:Any] = [:]
    var previousSearchedAddressDetails:[String:Any] = [:]

    func getPlaces(with query: String, completion: @escaping ([GooglePlace]) -> Void) {
        
        if let cachedAddress = previousSearchedAddresses[query] as? [GMSAutocompletePrediction] {
            completion(cachedAddress.map { GooglePlace(query:query, prediction: $0) })
        } else {
            // Create a type filter.
            var locationBounds = GMSCoordinateBounds()
            locationBounds = locationBounds.includingCoordinate(locationManager.currentLocation().coordinate)
            filter.origin = locationManager.currentLocation()
            filter.type = .establishment
            filter.country = countryCode
            filter.locationBias = GMSPlaceRectangularLocationOption( locationBounds.northEast, locationBounds.southWest)
            
            placesClient.findAutocompletePredictions(fromQuery: query,
                                                     filter: filter,
                                                     sessionToken: token) {results, error in
                
                if let error = error {
                      print("Autocomplete error: \(error)")
                      return
                }
                if let results = results {
                    self.previousSearchedAddresses[query] = results
                    completion(results.map { GooglePlace(query: query, prediction: $0) })
                }
            }
        }
    }
    
    func getPlaceDetails(id: String, completion: @escaping (GooglePlaceDetails?) -> Void) {
        
        if let cachedAddressDetails = previousSearchedAddressDetails[id] as? GMSPlace {
            completion(GooglePlaceDetails(placeDetails: cachedAddressDetails))
        } else {
        
            // Specify the place data types to return.
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.all.rawValue))
            
            placesClient.fetchPlace(fromPlaceID: id, placeFields: fields, sessionToken: token) { place, error in
                
                if let error = error {
                    print("An error occurred: \(error.localizedDescription)")
                    return
                  }
                  if let place = place {
                      print("The selected place is: \(String(describing: place.name))")
                      self.previousSearchedAddressDetails[id] = place
                      completion(GooglePlaceDetails(placeDetails: place))
                  }
            }
        }
    }
}
