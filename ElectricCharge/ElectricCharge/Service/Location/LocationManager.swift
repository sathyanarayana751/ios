//
//  LocationManager.swift
//  ElectricCharge
//
//  Created by iosdev on 2/16/22.
//

import Foundation
import CoreLocation

public enum LocationAuthorizationStatus : Int32 {

    
    // User has not yet made a choice with regards to this application
    case notDetermined = 0

    
    // This application is not authorized to use location services.  Due
    // to active restrictions on location services, the user cannot change
    // this status, and may not have personally denied authorization
    case restricted = 1

    
    // User has explicitly denied authorization for this application, or
    // location services are disabled in Settings.
    case denied = 2

    
    // User has granted authorization to use their location at any
    // time.  Your app may be launched into the background by
    // monitoring APIs such as visit monitoring, region monitoring,
    // and significant location change monitoring.
    //
    // This value should be used on iOS, tvOS and watchOS.  It is available on
    // MacOS, but kCLAuthorizationStatusAuthorized is synonymous and preferred.
    @available(iOS 8.0, *)
    case authorizedAlways = 3

    
    // User has granted authorization to use their location only while
    // they are using your app.  Note: You can reflect the user's
    // continued engagement with your app using
    // -allowsBackgroundLocationUpdates.
    //
    // This value is not available on MacOS.  It should be used on iOS, tvOS and
    // watchOS.
    @available(iOS 8.0, *)
    case authorizedWhenInUse = 4
}

protocol LocationManagerProtocol {
    func locationManagerDidChangeAuthorization(status:LocationAuthorizationStatus)
}

class LocationManager:NSObject {
    
    var cllocation = CLLocationManager()
    var currentLat = 0.0
    var currentLong = 0.0
    public static var shared = LocationManager()
    var delegate:LocationManagerProtocol? = nil
    
    override init() {
        cllocation.requestWhenInUseAuthorization()
    }
    
    func startUpdatingLocation() {
        
        cllocation.delegate = self
//        cllocation.startUpdatingLocation()
        if CLLocationManager.significantLocationChangeMonitoringAvailable() {
                // The device does not support this service.
            cllocation.startMonitoringSignificantLocationChanges()
        }
    }
    
    func currentLocation() -> CLLocation {
        
        return CLLocation(latitude: currentLat, longitude: currentLong)
    }
    
    func stopUpdatingLocation() {
        
        cllocation.stopUpdatingLocation()
    }
    
    func locationAuthorizationStatus() -> LocationAuthorizationStatus {
        
        if #available(iOS 14.0, *) {
            return LocationAuthorizationStatus(rawValue: cllocation.authorizationStatus.rawValue) ?? .notDetermined
        } else {
            // Fallback on earlier versions
            return LocationAuthorizationStatus(rawValue: CLLocationManager.authorizationStatus().rawValue) ?? .notDetermined
        }
    }
    
    func calculateDistance(loc1Latitude:Double,
                           loc1Longitude:Double,
                           loc2Latitude:Double,
                           loc2Longitude:Double) -> Double {
        
        let location1 = CLLocation(latitude: loc1Latitude, longitude: loc1Longitude)
        let location2 = CLLocation(latitude: loc2Latitude, longitude: loc2Longitude)

        //Measuring distance in km
        let distance = location1.distance(from: location2) / 1000

        //Display the result in km
        print(String(format: "The distance is %.01f km", distance))
        return distance
    }
}

extension LocationManager:  CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        currentLat  = coord.latitude
        currentLong = coord.longitude
        print("Current Location = \(coord)")
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
        if let locDelegate = self.delegate {
                        
            if #available(iOS 14.0, *) {
                locDelegate.locationManagerDidChangeAuthorization(status: LocationAuthorizationStatus(rawValue: manager.authorizationStatus.rawValue) ?? .notDetermined)
            } else {
                // Fallback on earlier versions
                locDelegate.locationManagerDidChangeAuthorization(status: LocationAuthorizationStatus(rawValue: CLLocationManager.authorizationStatus().rawValue) ?? .notDetermined)
            }
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        
    }
}

