//
//  MapBoxGeocodeManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 18/06/22.
//

import Foundation

/*let geocoder = Geocoder(accessToken: "pk.eyJ1IjoicmFnaGF2ZW5kcmF2ZW5rYXRlc2g2NDc2IiwiYSI6ImNsNGp1dHpnMzA4dnIzZm8xZzk4ejJkcnIifQ.QXP-oRQhGZYjYE2VM1Cffg")

class MapBoxGeocodeManager:NSObject {

    public static var shared = MapBoxGeocodeManager.init()
                
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
    
    var countryCode: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
    }
    
    var previousSearchedAddresses:[String:Any] = ["test":[
        [
          "description": "Paris, France",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
          "reference": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "France",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "France" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, TX, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJmysnFgZYSoYRSfPTL2YJuck",
          "reference": "ChIJmysnFgZYSoYRSfPTL2YJuck",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "TX, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris"],
              [ "offset": 7, "value": "TX" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, TN, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJ4zHP-Sije4gRBDEsVxunOWg",
          "reference": "ChIJ4zHP-Sije4gRBDEsVxunOWg",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "TN, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "TN" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, Brant, ON, Canada",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJsamfQbVtLIgR-X18G75Hyi0",
          "reference": "ChIJsamfQbVtLIgR-X18G75Hyi0",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "Brant, ON, Canada",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "Brant" ],
              [ "offset": 14, "value": "ON" ],
              [ "offset": 18, "value": "Canada" ],
            ],
          "types": ["neighborhood", "political", "geocode"],
        ],
        [
          "description": "Paris, KY, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJsU7_xMfKQ4gReI89RJn0-RQ",
          "reference": "ChIJsU7_xMfKQ4gReI89RJn0-RQ",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "KY, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "KY" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
      ]]
    var previousSearchedAddressDetails:[String:Any] = [:]

    var placeType:PlaceType = .address
    var radius = 0
    var strictBounds = false
    let locationManager = LocationManager.shared
    
    func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": Constants.Maps.MapboxAccessToken
        ]
        
        params["location"] = "\(locationManager.currentLat),\(locationManager.currentLong)"
        
        if radius > 0 {
            params["radius"] = "\(radius)"
        }
        
        if strictBounds {
            params["strictbounds"] = "true"
        }
        
        return params
    }
    
    func doRequest(_ query: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        let options = ForwardGeocodeOptions(query: query)

        // To refine the search, you can set various properties on the options object.
        if let countryCode = countryCode {
            options.allowedISOCountryCodes = [countryCode]
        } else {
            options.allowedISOCountryCodes = ["IN"]
        }
        options.focalLocation = CLLocation(latitude: locationManager.currentLat, longitude: locationManager.currentLong)
        options.allowedScopes = [.address, .pointOfInterest]

        let task = geocoder.geocode(options) { (placemarks, attribution, error) in
            guard let placemark = placemarks?.first else {
                return
            }
            
            print(placemark.name)
                // 200 Queen St
            
            if let qualifiedName = placemark.qualifiedName {
                print(qualifiedName)
                    // 200 Queen St, Saint John, New Brunswick E2L 2X1, Canada
            }

            if let coordinate = placemark.location?.coordinate {
                print("\(coordinate.latitude), \(coordinate.longitude)")
            }
        }
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        
        if let searchText = parameters["input"] {
            
            var params = parameters
            if let deviceLanguage = deviceLanguage {
                params["language"] = deviceLanguage
            }
            
            if let cachedAddress = previousSearchedAddresses[searchText] as? [[String:Any]] {
                completion(cachedAddress.map { Place(prediction: $0) })
            } else {
                self.doRequest(
                    "https://maps.googleapis.com/maps/api/place/autocomplete/json",
                    params: params,
                    completion: {
                        guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                        self.previousSearchedAddresses[searchText] = predictions
                        completion(predictions.map { Place(prediction: $0) })
                    }
                )
            }
        }
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        
        if let cachedAddressDetails = previousSearchedAddressDetails[id] as? [String:Any] {
            completion(PlaceDetails(json: cachedAddressDetails))
        } else {
            self.doRequest(
                "https://maps.googleapis.com/maps/api/place/details/json",
                params: parameters,
                completion: {
                    self.previousSearchedAddressDetails[id] = $0 as? [String: Any] ?? [:]
                    completion(PlaceDetails(json: $0 as? [String: Any] ?? [:]))
                }
            )
        }
    }

}*/
