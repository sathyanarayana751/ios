//
//  GoogleGeocodeManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 18/06/22.
//

import Foundation
import GooglePlaces

public enum PlaceType: String {
    case all = ""
    case geocode
    case address
    case establishment
    case regions = "(regions)"
    case cities = "(cities)"
}

open class Place: NSObject {
    public let id: String
    public let mainAddress: String
    public let secondaryAddress: String
    
    override open var description: String {
        get { return "\(mainAddress), \(secondaryAddress)" }
    }
    
    public init(id: String, mainAddress: String, secondaryAddress: String) {
        self.id = id
        self.mainAddress = mainAddress
        self.secondaryAddress = secondaryAddress
    }
    
    convenience public init(prediction: [String: Any]) {
        let structuredFormatting = prediction["structured_formatting"] as? [String: Any]
        
        self.init(
            id: prediction["place_id"] as? String ?? "",
            mainAddress: structuredFormatting?["main_text"] as? String ?? "",
            secondaryAddress: structuredFormatting?["secondary_text"] as? String ?? ""
        )
    }
}

open class PlaceDetails: CustomStringConvertible {
    public let formattedAddress: String
    public let placeId: String
    open var name: String? = nil

    open var streetNumber: String? = nil
    open var route: String? = nil
    open var postalCode: String? = nil
    open var country: String? = nil
    open var countryCode: String? = nil

    open var locality: String? = nil
    open var subLocality: String? = nil
    open var administrativeArea: String? = nil
    open var administrativeAreaCode: String? = nil
    open var subAdministrativeArea: String? = nil
    
    open var latitude: Double? = nil
    open var longitude: Double? = nil

    init?(json: [String: Any]) {
        guard let result = json["result"] as? [String: Any],
            let formattedAddress = result["formatted_address"] as? String
            else { return nil }
        self.formattedAddress = formattedAddress
        
        guard let placeId = result["place_id"] as? String else { return nil }
        self.placeId = placeId
        
        self.name = result["name"] as? String
        
        if let addressComponents = result["address_components"] as? [[String: Any]] {
            streetNumber = get("street_number", from: addressComponents, ofType: .short)
            route = get("route", from: addressComponents, ofType: .short)
            postalCode = get("postal_code", from: addressComponents, ofType: .long)
            country = get("country", from: addressComponents, ofType: .long)
            countryCode = get("country", from: addressComponents, ofType: .short)
            
            locality = get("locality", from: addressComponents, ofType: .long)
            subLocality = get("sublocality", from: addressComponents, ofType: .long)
            administrativeArea = get("administrative_area_level_1", from: addressComponents, ofType: .long)
            administrativeAreaCode = get("administrative_area_level_1", from: addressComponents, ofType: .short)
            subAdministrativeArea = get("administrative_area_level_2", from: addressComponents, ofType: .long)
        }
        
        if let geometry = result["geometry"] as? [String: Any],
            let location = geometry["location"] as? [String: Any],
            let lat = location["lat"] as? Double,
            let long = location["lng"] as? Double {
            latitude = lat
            longitude = long
        }
    }
    
    open var description: String {
        return "\nAddress: \(formattedAddress)\ncoordinate: (\(latitude ?? 0), \(longitude ?? 0))\n"
    }
}

private extension PlaceDetails {
    
    enum ComponentType: String {
        case short = "short_name"
        case long = "long_name"
    }
    
    /// Parses the element value with the specified type from the array or components.
    /// Example: `{ "long_name" : "90", "short_name" : "90", "types" : [ "street_number" ] }`
    ///
    /// - Parameters:
    ///   - component: The name of the element.
    ///   - array: The root component array to search from.
    ///   - ofType: The type of element to extract the value from.
    func get(_ component: String, from array: [[String: Any]], ofType: ComponentType) -> String? {
        return (array.first { ($0["types"] as? [String])?.contains(component) == true })?[ofType.rawValue] as? String
    }
}



class GoogleGeocodeManager:NSObject {

    public static var shared = GoogleGeocodeManager()
    let placesClient = GMSPlacesClient.shared()
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
    
    var previousSearchedAddresses:[String:Any] = ["test":[
        [
          "description": "Paris, France",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
          "reference": "ChIJD7fiBh9u5kcRYJSMaMOCCwQ",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "France",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "France" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, TX, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJmysnFgZYSoYRSfPTL2YJuck",
          "reference": "ChIJmysnFgZYSoYRSfPTL2YJuck",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "TX, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris"],
              [ "offset": 7, "value": "TX" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, TN, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJ4zHP-Sije4gRBDEsVxunOWg",
          "reference": "ChIJ4zHP-Sije4gRBDEsVxunOWg",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "TN, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "TN" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
        [
          "description": "Paris, Brant, ON, Canada",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJsamfQbVtLIgR-X18G75Hyi0",
          "reference": "ChIJsamfQbVtLIgR-X18G75Hyi0",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "Brant, ON, Canada",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "Brant" ],
              [ "offset": 14, "value": "ON" ],
              [ "offset": 18, "value": "Canada" ],
            ],
          "types": ["neighborhood", "political", "geocode"],
        ],
        [
          "description": "Paris, KY, USA",
          "matched_substrings": [[ "length": 5, "offset": 0 ]],
          "place_id": "ChIJsU7_xMfKQ4gReI89RJn0-RQ",
          "reference": "ChIJsU7_xMfKQ4gReI89RJn0-RQ",
          "structured_formatting":
            [
              "main_text": "Paris",
              "main_text_matched_substrings": [[ "length": 5, "offset": 0 ]],
              "secondary_text": "KY, USA",
            ],
          "terms":
            [
              [ "offset": 0, "value": "Paris" ],
              [ "offset": 7, "value": "KY" ],
              [ "offset": 11, "value": "USA" ],
            ],
          "types": ["locality", "political", "geocode"],
        ],
      ]]
    var previousSearchedAddressDetails:[String:Any] = [:]

    var placeType:PlaceType = .geocode
    var radius = 0
    var strictBounds = false
    let locationManager = LocationManager.shared


    func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": Constants.Keys.Search.Google
        ]
        
        params["location"] = "\(locationManager.currentLat),\(locationManager.currentLong)"
        
        if radius > 0 {
            params["radius"] = "\(radius)"
        }
        
        if strictBounds {
            params["strictbounds"] = "true"
        }
        
        return params
    }
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        
        if let searchText = parameters["input"] {
            
            var params = parameters
            if let deviceLanguage = deviceLanguage {
                params["language"] = deviceLanguage
            }
            
            if let cachedAddress = previousSearchedAddresses[searchText] as? [[String:Any]] {
                completion(cachedAddress.map { Place(prediction: $0) })
            } else {
                self.doRequest(
                    "https://maps.googleapis.com/maps/api/place/autocomplete/json",
                    params: params,
                    completion: {
                        guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                        self.previousSearchedAddresses[searchText] = predictions
                        completion(predictions.map { Place(prediction: $0) })
                    }
                )
            }
        }
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        
        if let cachedAddressDetails = previousSearchedAddressDetails[id] as? [String:Any] {
            completion(PlaceDetails(json: cachedAddressDetails))
        } else {
            self.doRequest(
                "https://maps.googleapis.com/maps/api/place/details/json",
                params: parameters,
                completion: {
                    self.previousSearchedAddressDetails[id] = $0 as? [String: Any] ?? [:]
                    completion(PlaceDetails(json: $0 as? [String: Any] ?? [:]))
                }
            )
        }
    }
}

