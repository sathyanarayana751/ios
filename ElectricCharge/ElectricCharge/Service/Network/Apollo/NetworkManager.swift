//
//  APIManager.swift
//  ElectricCharge
//
//  Created by iosdev on 2/22/22.
//

import Foundation
import Apollo
import UIKit

struct UrlComponents {
    
    static let baseURL = "https://api.quikplugs.com"
}

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private(set) lazy var apollo: ApolloClient = {
        let url = URL(string: UrlComponents.baseURL)!
        
        let store = ApolloStore(cache: InMemoryNormalizedCache())
        let provider = DefaultInterceptorProvider(store: store)
        let transport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                     endpointURL: url)
        return ApolloClient(networkTransport: transport, store: store)
    }()
    
//    private lazy var networkTransport: RequestChainNetworkTransport = {
//
//        RequestChainNetworkTransport(interceptorProvider: InterceptorProvider, endpointURL: <#T##URL#>)
//            let transport = NetworkTransport(url: URL(string: "https://example.com/graphql")!)
//            transport.delegate = self
//
//            return transport
//        }()
//
//        private(set) lazy var apollo = ApolloClient(networkTransport: self.networkTransport)
//    }
    
    
    func searchAPICall(searchRequest: SearchRequestModel,
                       completionHandler: @escaping (Bool, SearchQuery.Data.Search?, Error?) -> ()) {
        
        var searchQuery:SearchQuery
        
        var arrayOfChargeTypes:[PowerType] = []
        var arrayOfPlugTypes:[String] = []
        var sortFilter = SearchSortOption.nearest

        for eachChargeType in searchRequest.chargeType {
            if let chargeType = PowerType(rawValue: eachChargeType) {
                arrayOfChargeTypes.append(chargeType)
            }
        }
        
        for eachPlugType in searchRequest.plugType {
                        
            if let plugTypeDetails = Constants.Filters.PortTypes.first(where: { $0["name"] as? String == eachPlugType }), let plugTypeId = plugTypeDetails["id"] as? String {
                arrayOfPlugTypes.append(plugTypeId)
            }
        }
        
        if searchRequest.sortBy == "Cheapest", let chargeType = searchRequest.chargeType.first {
            
            if chargeType == "AC" {
                sortFilter = .cheapestAc
            } else {
                sortFilter = .cheapestDc
            }
        }
                
        if searchRequest.isForWithIn {
            
            if searchRequest.withIn.count > 0 {

                searchQuery = SearchQuery(pagination: IPagination(page: 1, limit: 10),
                                          filter: ISearchFilter(query: nil,
                                                                available: true,
                                                                chargeType: arrayOfChargeTypes,
                                                                plugsType: arrayOfPlugTypes,
                                                                powerRating: nil,
                                                                withIn: ISearchWithIn(topRightLatitude: searchRequest.withIn["topRightLatitude"] as? Double,
                                                                                      topRightLongitude: searchRequest.withIn["topRightLongitude"] as? Double,
                                                                                      bottomLeftLatitude:  searchRequest.withIn["bottomLeftLatitude"] as? Double,
                                                                                      bottomLeftLongitude: searchRequest.withIn["bottomLeftLongitude"] as? Double),
                                                                surrounding: nil),
                                          sort: sortFilter)
            } else {
                searchQuery = SearchQuery(pagination: IPagination(page: 1, limit: 10), filter: nil, sort: .nearest)
            }
        } else {
            
            if searchRequest.surrounding.count > 0 {
                
                searchQuery = SearchQuery(pagination: IPagination(page: 1, limit: 10),
                                          filter: ISearchFilter(query: nil,
                                                                available: true,
                                                                chargeType: arrayOfChargeTypes,
                                                                plugsType: arrayOfPlugTypes,
                                                                powerRating: nil,
                                                                withIn: nil,
                                                                surrounding: ISearchSurrounding(radius: searchRequest.surrounding["radius"] as? Double,
                                                                                                latitude: searchRequest.surrounding["latitude"] as? Double,
                                                                                                longitude: searchRequest.surrounding["longitude"] as? Double)),
                                          sort: sortFilter)
                
            } else {
                searchQuery = SearchQuery(pagination: IPagination(page: 1, limit: 10),
                                          filter: nil,
                                          sort: .nearest)
            }
        }
        
        print("Called Search Query : \(searchQuery)")
            
        _ = apollo.watch(query: searchQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.search, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func getUserDetailsAPICall(needToFetchFromServer:Bool,
                               completionHandler: @escaping (Bool, UserQuery.Data.User?, Error?) -> ()) {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        let userQuery = UserQuery(userId: userId)
        
        print("Called User Query : UserId = \(userQuery.userId)")
        
        var cachePlicy:CachePolicy = .returnCacheDataElseFetch
        if needToFetchFromServer {
            cachePlicy = .fetchIgnoringCacheData
        }
        
        _ = apollo.watch(query: userQuery, cachePolicy: cachePlicy, callbackQueue: .main) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.user, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func favouriteApiCall(stationDetailsModel:StationDetailsModel,
                                 completion: @escaping(Bool, UpdateUserPreferenceMutation.Data.UpdateUserPreference?, Error?) ->())  {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        
        var arrayOfFavStations:[String?] = []
        if let favStations = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStations) as? [String] {
            arrayOfFavStations = favStations
        }
        arrayOfFavStations.append(stationDetailsModel.id)

        let updateUserPreferenceQuery = UpdateUserPreferenceMutation(userId: userId, input: IUserPreference(plug: "", stations: arrayOfFavStations))
        
        print("Called Favourite Mutation : UserId = \(updateUserPreferenceQuery.userId)")

        _ = apollo.perform(mutation: updateUserPreferenceQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completion(true, graphQLResult.data?.updateUserPreference, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completion(false, nil, error)
          }
        }
    }
    
    
    func loginApiCall(userEmail: String,
                      password: String,
                      completion: @escaping(Bool, LoginMutation.Data.LoginUser?, Error?) ->())  {
        
        let loginMutation = LoginMutation(username: userEmail,
                                          password: password)
        
        print("Called Login Mutation : Email/UserName = \(loginMutation.username)")

        _ = apollo.perform(mutation: loginMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.loginUser, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func registerApiCall(name: String,
                         phone: String,
                         email: String,
                         password: String,
                         completion: @escaping(Bool, RegisterUserMutation.Data.RegisterUser?, Error?) ->())  {
        
        let registerUser = IRegisterUser(name: name,
                                         password: password,
                                         email: email,
                                         phone: phone)
        let registerUserMutation = RegisterUserMutation(input: registerUser)
        print("Called Register Mutation : UserName = \(String(describing: registerUser.name))")
        _ = apollo.perform(mutation: registerUserMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.registerUser, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func socialLoginApiCall(token: String,
                      completion: @escaping(Bool, SocialLoginWithGoogleTokenMutation.Data.SocialLoginWithGoogleToken?, Error?) ->())  {
        
        let socialLoginMutation = SocialLoginWithGoogleTokenMutation(token: token)
        
        print("Called Social Login Mutation : Token = \(socialLoginMutation.token)")

        _ = apollo.perform(mutation: socialLoginMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.socialLoginWithGoogleToken, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func socialLoginUpdateUserDetailsApiCall(token:String,
                                             name: String,
                                             phone: String,
                                             email: String,
                                             password: String,
                         completion: @escaping(Bool, SocialLoginUpdateUserDetailsMutation.Data.SocialLoginUpdateUserDetail?, Error?) ->())  {
        
        let registerUserMutation = SocialLoginUpdateUserDetailsMutation(token: token,
                                                                        input:IRegisterUser(name: name,
                                                                             password: password,
                                                                             email: email,
                                                                             phone: phone))
        print("Called Social Login Update User Details Mutation : Token = \(registerUserMutation.token)")

        _ = apollo.perform(mutation: registerUserMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.socialLoginUpdateUserDetails, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func forgetPasswordApiCall(userName: String,
                      completion: @escaping(Bool, ForgetPasswordMutation.Data.ForgetPassword?, Error?) ->())  {
        
        let forgetPasswordMutation = ForgetPasswordMutation(username: userName)
        print("Called Forget Password Mutation : UserName = \(forgetPasswordMutation.username)")

        _ = apollo.perform(mutation: forgetPasswordMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.forgetPassword, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func setNewPasswordApiCall(userId:String,
                               password: String,
                               completion: @escaping(Bool, ResetUserPasswordMutation.Data.ResetUserPassword?, Error?) ->())  {
        
        let forgetPasswordMutation = ResetUserPasswordMutation(userId: userId, password: password)
        print("Called Set New Password Mutation : UserId = \(forgetPasswordMutation.userId)")

        _ = apollo.perform(mutation: forgetPasswordMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.resetUserPassword, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func sendRegisterOTPCall(userId: String,
                             completion: @escaping(Bool, ResendRegistrationOtpMutation.Data.ResendRegistrationOtp?, Error?) ->())  {
        
        let resendRegistrationOtpMutation = ResendRegistrationOtpMutation(userId: userId)
        print("Called Resend Registration OTP Mutation : UserId = \(resendRegistrationOtpMutation.userId)")

        _ = apollo.perform(mutation: resendRegistrationOtpMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.resendRegistrationOtp, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func resendLoginOTPCall(userId: String,
                            completion: @escaping(Bool, ResendLoginOtpMutation.Data.ResendLoginOtp?, Error?) ->())  {
        
        let resendLoginOtpMutation = ResendLoginOtpMutation(id: userId)
        print("Called Resend Login OTP Mutation : UserId = \(resendLoginOtpMutation.id)")

        _ = apollo.perform(mutation: resendLoginOtpMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.resendLoginOtp, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func verifyRegisterOTPCall(userId: String,
                               otp: String,
                               completion: @escaping(Bool, VerifyRegistrationOtpMutation.Data.VerifyRegistrationOtp?, Error?) ->())  {
        
        let verifyRegistrationOtpMutation = VerifyRegistrationOtpMutation(userId: userId,
                                                                          otp: otp)
        print("Called Verify Register OTP Mutation : UserId = \(verifyRegistrationOtpMutation.userId)")

        _ = apollo.perform(mutation: verifyRegistrationOtpMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.verifyRegistrationOtp, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func verifyLoginOTPCall(userId: String,
                            otp: String,
                            completion: @escaping(Bool, VerifyLoginOtpMutation.Data.VerifyLoginOtp?, Error?) ->())  {
        
        let verifyLoginOTPMutation = VerifyLoginOtpMutation(id: userId, otp: otp)
        print("Called Verify Login OTP Mutation : UserId = \(verifyLoginOTPMutation.id)")

        _ = apollo.perform(mutation: verifyLoginOTPMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.verifyLoginOtp, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
           }
        }
    }
    
    func availableSlotAPICall(availableSlotRequest: AvailableSlotRequestModel,
                       completionHandler: @escaping (Bool, AvailabilitySlotQuery.Data.AvailabilitySlot?, Error?) -> ()) {
        
        let availableSlotQuery = AvailabilitySlotQuery(input: IAvailabilitySlot(stationId: availableSlotRequest.stationId,
                                                                                chargeType:PowerType(rawValue: availableSlotRequest.chargeType),
                                                                                kw: availableSlotRequest.powerValue,
                                                                                plugType:  availableSlotRequest.plugType,
                                                                                startTime: availableSlotRequest.startTime,
                                                                                endTime: availableSlotRequest.endTime,
                                                                                durationInMin: availableSlotRequest.durationInMin))
        print("Called Availability Slot Query : StationId = \(availableSlotRequest.stationId)")

        _ = apollo.watch(query: availableSlotQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.availabilitySlot, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func reserveSlotApiCall(reserveSlotRequestModel:ReserveSlotRequestModel,
                      completion: @escaping(Bool, ReserveMutation.Data.Reserve?, Error?) ->())  {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        
        let reserveMutation = ReserveMutation(userId: userId,
                                            input: IReservation(stationId: reserveSlotRequestModel.stationId,
                                                                plugType:  reserveSlotRequestModel.plugType,
                                                                chargeType: PowerType(rawValue: reserveSlotRequestModel.chargeType)!,
                                                                                powerRating: reserveSlotRequestModel.powerValue,
                                                                                startTime: reserveSlotRequestModel.startTime,
                                                                                endTime: reserveSlotRequestModel.endTime,
                                                                                chargerId: reserveSlotRequestModel.chargerId,
                                                                                plugId: reserveSlotRequestModel.plugId))
        print("Called Reserve Slot Mutation : StationId = \(reserveSlotRequestModel.stationId), UserId = \(reserveMutation.userId)")

        _ = apollo.perform(mutation: reserveMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.reserve, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func startChargingOTPApiCall(reservationId:String,
                      completion: @escaping(Bool, GenerateOtpMutation.Data.GenerateOtp?, Error?) ->())  {
        
        let generateOTPMutation = GenerateOtpMutation(reservationId: reservationId)
        print("Called Start Charging OTP Mutation : ReservationId = \(generateOTPMutation.reservationId)")

        _ = apollo.perform(mutation: generateOTPMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.generateOtp, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func startChargingApiCall(reservationId:String,
                      completion: @escaping(Bool, StartChargingMutation.Data.StartCharging?, Error?) ->())  {
        
        let startChargingMutation = StartChargingMutation(reservationId: reservationId)
        print("Called Start Charging Mutation : ReservationId = \(startChargingMutation.reservationId)")
        _ = apollo.perform(mutation: startChargingMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.startCharging, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func stopChargingApiCall(reservationId:String,
                      completion: @escaping(Bool, StopChargingMutation.Data.StopCharging?, Error?) ->())  {
        
        let stopChargingMutation = StopChargingMutation(reservationId: reservationId)
        print("Called Stop Charging Mutation : ReservationId = \(stopChargingMutation.reservationId)")

        _ = apollo.perform(mutation: stopChargingMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.stopCharging, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func reservationsListApiCall(page:Int,
                                 limit:Int,
                                 status:[ReservationStatus],
                                 needToFetchFromServer:Bool,
                                 completion: @escaping(Bool, ReservationsListQuery.Data.Reservation?, Error?) ->())  {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        let reservationsListQuery = ReservationsListQuery(pagination: IPagination(page: page, limit: limit), filter: IReservationFilter(startAfter: nil, createdAfter: nil, createdBefore: nil, status: status, userId: userId))
        
        print("Called Reservations List Query : UserId = \(userId)")

        var cachePlicy:CachePolicy = .returnCacheDataElseFetch
        if needToFetchFromServer {
            cachePlicy = .fetchIgnoringCacheData
        }
        
        _ = apollo.watch(query: reservationsListQuery, cachePolicy: cachePlicy, callbackQueue: .main) { result in
            switch result {
              case .success(let graphQLResult):
                completion(true, graphQLResult.data?.reservations, nil)
              case .failure(let error):
                print("Error while fetching query: \(error.localizedDescription)")
                completion(false, nil, error)
            }
            
        }
//        _ = apollo.watch(query: reservationsListQuery) { result in
//
//        }
    }
    
    func upiPaymentApiCall(reservationId:String,
                           UPIId:String,
                           completion: @escaping(Bool, PayForReservationFromUpiMutation.Data.PayForReservationFromUpi?, Error?) ->())  {
        
        let upiPaymentMutation = PayForReservationFromUpiMutation(reservationId: reservationId, upi: UPIId)
        print("Pay For Reservation From UPI Mutation : ReservationId = \(upiPaymentMutation.reservationId),  UPI = \(upiPaymentMutation.upi)")
        _ = apollo.perform(mutation: upiPaymentMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.payForReservationFromUpi, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func walletPaymentApiCall(reservationId:String,
                              completion: @escaping(Bool, PayForReservationFromWalletMutation.Data.PayForReservationFromWallet?, Error?) ->())  {
        
        let walletPaymentMutation = PayForReservationFromWalletMutation(reservationId: reservationId)
        print("Pay For Reservation From Wallet Mutation : ReservationId = \(walletPaymentMutation.reservationId)")
        _ = apollo.perform(mutation: walletPaymentMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.payForReservationFromWallet, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func walletDetailsApiCall(needToFetchFromServer:Bool,
                              completion: @escaping(Bool, WalletQuery.Data.Wallet?, Error?) ->())  {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        let walletQuery = WalletQuery(userId: userId)
        print("Wallet Query : userId = \(userId)")
        
        var cachePlicy:CachePolicy = .returnCacheDataElseFetch
        if needToFetchFromServer {
            cachePlicy = .fetchIgnoringCacheData
        }
        
        _ = apollo.watch(query: walletQuery, cachePolicy: cachePlicy, callbackQueue: .main) { result in
          switch result {
            case .success(let graphQLResult):
              completion(true, graphQLResult.data?.wallet, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completion(false, nil, error)
          }
        }
    }
    
    func topupWalletApiCall(amount:Int,
                            completion: @escaping(Bool, TopUpWalletMutation.Data.TopUpWallet?, Error?) ->())  {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        let topupWalletMutation = TopUpWalletMutation(userId: userId,amount: amount)
        print("Topup Wallet Mutation : userId = \(userId), amount = \(amount)")
        _ = apollo.perform(mutation: topupWalletMutation) { result in
            
            switch result {
                case .success(let graphQLResult):
                    completion(true, graphQLResult.data?.topUpWallet, nil)
                case .failure(let error):
                    print("Error while fetching query: \(error.localizedDescription)")
                    completion(false, nil, error)
          }
        }
    }
    
    func getPortTypesApiCall(completionHandler: @escaping (Bool, [PortTypesQuery.Data.PortType?]?, Error?) -> ()) {
        
        var userId = ""
        if let userIdValue = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            userId = userIdValue
        }
        let portTypesQuery = PortTypesQuery()
        
        print("Called User Query : UserId = \(userId)")
        
        _ = apollo.watch(query: portTypesQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.portTypes, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func getResolveChargeApiCall(chargerId:String,
                                 plugId:String,
                                 completionHandler: @escaping (Bool, ResolveChargeQuery.Data.ResolveCharge?, Error?) -> ()) {
        
        let resolveChargeQuery = ResolveChargeQuery(chargerId: chargerId, plugId: plugId)
        
        print("Called Resolve Charge Query : chargerId = \(chargerId), plugId = \(plugId)")
        
        _ = apollo.watch(query: resolveChargeQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.resolveCharge, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func getVehicleManufacturerApiCall(completionHandler: @escaping (Bool, [VehicleManufacturerQuery.Data.VehicleManufacturer?]?, Error?) -> ()) {
    
        let vehicleManufacturerQuery = VehicleManufacturerQuery()
        
        print("Called Vehicle Manufacturer Query")
        
        _ = apollo.watch(query: vehicleManufacturerQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.vehicleManufacturer, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
    
    func getVehicleModelsApiCall(arrayOfManufactureIds:[String],
                                 arrayOfTypes:[String],
                                 completionHandler: @escaping (Bool, VehicleModelsQuery.Data.VehicleModel?, Error?) -> ()) {
        
        var vehicleModelFilter = IVehicleModelFilter()
        vehicleModelFilter.manufacturerIds = arrayOfManufactureIds
        vehicleModelFilter.type = arrayOfTypes
        
        let vehicleModelsQuery = VehicleModelsQuery(pagination: IPagination(page: 0, limit: 100),
                                                    filter: vehicleModelFilter)
        
        print("Called Vehicle Models Query, manufacturerIds = \(arrayOfManufactureIds), types = \(arrayOfTypes)")
        
        _ = apollo.watch(query: vehicleModelsQuery) { result in
          switch result {
            case .success(let graphQLResult):
              completionHandler(true, graphQLResult.data?.vehicleModels, nil)
            case .failure(let error):
              print("Error while fetching query: \(error.localizedDescription)")
              completionHandler(false, nil, error)
          }
        }
    }
}

//extension NetworkManager: HTTPNetworkTransportPreflightDelegate {
//    func networkTransport(_ networkTransport: HTTPNetworkTransport, shouldSend request: URLRequest) -> Bool {
//        return true
//    }
//
//    func networkTransport(_ networkTransport: HTTPNetworkTransport, willSend request: inout URLRequest) {
//        var headers = request.allHTTPHeaderFields ?? [String: String]()
//        headers["Authorization"] = "Bearer \(YOUR_TOKEN)"
//
//        request.allHTTPHeaderFields = headers
//    }
//}
