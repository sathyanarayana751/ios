// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct IAvailabilitySlot: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stationId
  ///   - chargeType
  ///   - kw
  ///   - plugType
  ///   - startTime
  ///   - endTime
  ///   - durationInMin
  public init(stationId: Swift.Optional<String?> = nil, chargeType: Swift.Optional<PowerType?> = nil, kw: Swift.Optional<Double?> = nil, plugType: Swift.Optional<String?> = nil, startTime: Swift.Optional<String?> = nil, endTime: Swift.Optional<String?> = nil, durationInMin: Swift.Optional<Int?> = nil) {
    graphQLMap = ["stationId": stationId, "chargeType": chargeType, "kw": kw, "plugType": plugType, "startTime": startTime, "endTime": endTime, "durationInMin": durationInMin]
  }

  public var stationId: Swift.Optional<String?> {
    get {
      return graphQLMap["stationId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stationId")
    }
  }

  public var chargeType: Swift.Optional<PowerType?> {
    get {
      return graphQLMap["chargeType"] as? Swift.Optional<PowerType?> ?? Swift.Optional<PowerType?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargeType")
    }
  }

  public var kw: Swift.Optional<Double?> {
    get {
      return graphQLMap["kw"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "kw")
    }
  }

  public var plugType: Swift.Optional<String?> {
    get {
      return graphQLMap["plugType"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plugType")
    }
  }

  public var startTime: Swift.Optional<String?> {
    get {
      return graphQLMap["startTime"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startTime")
    }
  }

  public var endTime: Swift.Optional<String?> {
    get {
      return graphQLMap["endTime"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endTime")
    }
  }

  public var durationInMin: Swift.Optional<Int?> {
    get {
      return graphQLMap["durationInMin"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "durationInMin")
    }
  }
}

public enum PowerType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case ac
  case dc
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "AC": self = .ac
      case "DC": self = .dc
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .ac: return "AC"
      case .dc: return "DC"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: PowerType, rhs: PowerType) -> Bool {
    switch (lhs, rhs) {
      case (.ac, .ac): return true
      case (.dc, .dc): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [PowerType] {
    return [
      .ac,
      .dc,
    ]
  }
}

public enum ReservationStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case hold
  case reserved
  case completed
  case running
  case canceled
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "HOLD": self = .hold
      case "RESERVED": self = .reserved
      case "COMPLETED": self = .completed
      case "RUNNING": self = .running
      case "CANCELED": self = .canceled
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .hold: return "HOLD"
      case .reserved: return "RESERVED"
      case .completed: return "COMPLETED"
      case .running: return "RUNNING"
      case .canceled: return "CANCELED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ReservationStatus, rhs: ReservationStatus) -> Bool {
    switch (lhs, rhs) {
      case (.hold, .hold): return true
      case (.reserved, .reserved): return true
      case (.completed, .completed): return true
      case (.running, .running): return true
      case (.canceled, .canceled): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ReservationStatus] {
    return [
      .hold,
      .reserved,
      .completed,
      .running,
      .canceled,
    ]
  }
}

public enum UserRole: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case owner
  case manager
  case guest
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "OWNER": self = .owner
      case "MANAGER": self = .manager
      case "GUEST": self = .guest
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .owner: return "OWNER"
      case .manager: return "MANAGER"
      case .guest: return "GUEST"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: UserRole, rhs: UserRole) -> Bool {
    switch (lhs, rhs) {
      case (.owner, .owner): return true
      case (.manager, .manager): return true
      case (.guest, .guest): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [UserRole] {
    return [
      .owner,
      .manager,
      .guest,
    ]
  }
}

public enum UserStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inactive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "INACTIVE": self = .inactive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inactive: return "INACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: UserStatus, rhs: UserStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inactive, .inactive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [UserStatus] {
    return [
      .active,
      .inactive,
    ]
  }
}

public enum StationStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: StationStatus, rhs: StationStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [StationStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public enum ChargerStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ChargerStatus, rhs: ChargerStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ChargerStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public enum PlugStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case active
  case inActive
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ACTIVE": self = .active
      case "IN_ACTIVE": self = .inActive
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .active: return "ACTIVE"
      case .inActive: return "IN_ACTIVE"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: PlugStatus, rhs: PlugStatus) -> Bool {
    switch (lhs, rhs) {
      case (.active, .active): return true
      case (.inActive, .inActive): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [PlugStatus] {
    return [
      .active,
      .inActive,
    ]
  }
}

public enum OCPPVersion: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case v_2
  case v_1_6
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "V_2": self = .v_2
      case "V_1_6": self = .v_1_6
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .v_2: return "V_2"
      case .v_1_6: return "V_1_6"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: OCPPVersion, rhs: OCPPVersion) -> Bool {
    switch (lhs, rhs) {
      case (.v_2, .v_2): return true
      case (.v_1_6, .v_1_6): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [OCPPVersion] {
    return [
      .v_2,
      .v_1_6,
    ]
  }
}

public enum ReservationAmountSource: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case upi
  case wallet
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "UPI": self = .upi
      case "WALLET": self = .wallet
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .upi: return "UPI"
      case .wallet: return "WALLET"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ReservationAmountSource, rhs: ReservationAmountSource) -> Bool {
    switch (lhs, rhs) {
      case (.upi, .upi): return true
      case (.wallet, .wallet): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ReservationAmountSource] {
    return [
      .upi,
      .wallet,
    ]
  }
}

public enum CPOKind: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case cpo
  case iso
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "CPO": self = .cpo
      case "ISO": self = .iso
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .cpo: return "CPO"
      case .iso: return "ISO"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: CPOKind, rhs: CPOKind) -> Bool {
    switch (lhs, rhs) {
      case (.cpo, .cpo): return true
      case (.iso, .iso): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [CPOKind] {
    return [
      .cpo,
      .iso,
    ]
  }
}

public struct IRegisterUser: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - password
  ///   - email
  ///   - phone
  public init(name: Swift.Optional<String?> = nil, password: Swift.Optional<String?> = nil, email: Swift.Optional<String?> = nil, phone: Swift.Optional<String?> = nil) {
    graphQLMap = ["name": name, "password": password, "email": email, "phone": phone]
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var password: Swift.Optional<String?> {
    get {
      return graphQLMap["password"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "password")
    }
  }

  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var phone: Swift.Optional<String?> {
    get {
      return graphQLMap["phone"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phone")
    }
  }
}

public struct IPagination: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - page
  ///   - limit
  public init(page: Int, limit: Int) {
    graphQLMap = ["page": page, "limit": limit]
  }

  public var page: Int {
    get {
      return graphQLMap["page"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "page")
    }
  }

  public var limit: Int {
    get {
      return graphQLMap["limit"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "limit")
    }
  }
}

public struct IReservationFilter: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - startAfter
  ///   - createdAfter
  ///   - createdBefore
  ///   - status
  ///   - userId
  ///   - cpoId
  ///   - stationId
  ///   - settlement
  ///   - payoutId
  public init(startAfter: Swift.Optional<String?> = nil, createdAfter: Swift.Optional<String?> = nil, createdBefore: Swift.Optional<String?> = nil, status: Swift.Optional<[ReservationStatus?]?> = nil, userId: Swift.Optional<String?> = nil, cpoId: Swift.Optional<String?> = nil, stationId: Swift.Optional<String?> = nil, settlement: Swift.Optional<ReservationSettlementStatus?> = nil, payoutId: Swift.Optional<String?> = nil) {
    graphQLMap = ["startAfter": startAfter, "createdAfter": createdAfter, "createdBefore": createdBefore, "status": status, "userId": userId, "cpoId": cpoId, "stationId": stationId, "settlement": settlement, "payoutId": payoutId]
  }

  public var startAfter: Swift.Optional<String?> {
    get {
      return graphQLMap["startAfter"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startAfter")
    }
  }

  public var createdAfter: Swift.Optional<String?> {
    get {
      return graphQLMap["createdAfter"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAfter")
    }
  }

  public var createdBefore: Swift.Optional<String?> {
    get {
      return graphQLMap["createdBefore"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdBefore")
    }
  }

  public var status: Swift.Optional<[ReservationStatus?]?> {
    get {
      return graphQLMap["status"] as? Swift.Optional<[ReservationStatus?]?> ?? Swift.Optional<[ReservationStatus?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "status")
    }
  }

  public var userId: Swift.Optional<String?> {
    get {
      return graphQLMap["userId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var cpoId: Swift.Optional<String?> {
    get {
      return graphQLMap["cpoId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cpoId")
    }
  }

  public var stationId: Swift.Optional<String?> {
    get {
      return graphQLMap["stationId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stationId")
    }
  }

  public var settlement: Swift.Optional<ReservationSettlementStatus?> {
    get {
      return graphQLMap["settlement"] as? Swift.Optional<ReservationSettlementStatus?> ?? Swift.Optional<ReservationSettlementStatus?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "settlement")
    }
  }

  public var payoutId: Swift.Optional<String?> {
    get {
      return graphQLMap["payoutId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "payoutId")
    }
  }
}

public enum ReservationSettlementStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case notReady
  case unsettled
  case settled
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "NOT_READY": self = .notReady
      case "UNSETTLED": self = .unsettled
      case "SETTLED": self = .settled
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .notReady: return "NOT_READY"
      case .unsettled: return "UNSETTLED"
      case .settled: return "SETTLED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: ReservationSettlementStatus, rhs: ReservationSettlementStatus) -> Bool {
    switch (lhs, rhs) {
      case (.notReady, .notReady): return true
      case (.unsettled, .unsettled): return true
      case (.settled, .settled): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [ReservationSettlementStatus] {
    return [
      .notReady,
      .unsettled,
      .settled,
    ]
  }
}

public struct IReservation: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - stationId
  ///   - plugType
  ///   - chargeType
  ///   - powerRating
  ///   - startTime
  ///   - endTime
  ///   - chargerId
  ///   - plugId
  public init(stationId: String, plugType: String, chargeType: PowerType, powerRating: Double, startTime: String, endTime: String, chargerId: String, plugId: String) {
    graphQLMap = ["stationId": stationId, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "startTime": startTime, "endTime": endTime, "chargerId": chargerId, "plugId": plugId]
  }

  public var stationId: String {
    get {
      return graphQLMap["stationId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stationId")
    }
  }

  public var plugType: String {
    get {
      return graphQLMap["plugType"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plugType")
    }
  }

  public var chargeType: PowerType {
    get {
      return graphQLMap["chargeType"] as! PowerType
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargeType")
    }
  }

  public var powerRating: Double {
    get {
      return graphQLMap["powerRating"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "powerRating")
    }
  }

  public var startTime: String {
    get {
      return graphQLMap["startTime"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startTime")
    }
  }

  public var endTime: String {
    get {
      return graphQLMap["endTime"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endTime")
    }
  }

  public var chargerId: String {
    get {
      return graphQLMap["chargerId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargerId")
    }
  }

  public var plugId: String {
    get {
      return graphQLMap["plugId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plugId")
    }
  }
}

public struct ISearchFilter: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - query
  ///   - available
  ///   - chargeType
  ///   - plugsType
  ///   - powerRating
  ///   - withIn
  ///   - surrounding
  public init(query: Swift.Optional<String?> = nil, available: Swift.Optional<Bool?> = nil, chargeType: Swift.Optional<[PowerType?]?> = nil, plugsType: Swift.Optional<[String?]?> = nil, powerRating: Swift.Optional<[Double?]?> = nil, withIn: Swift.Optional<ISearchWithIn?> = nil, surrounding: Swift.Optional<ISearchSurrounding?> = nil) {
    graphQLMap = ["query": query, "available": available, "chargeType": chargeType, "plugsType": plugsType, "powerRating": powerRating, "withIn": withIn, "surrounding": surrounding]
  }

  public var query: Swift.Optional<String?> {
    get {
      return graphQLMap["query"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "query")
    }
  }

  public var available: Swift.Optional<Bool?> {
    get {
      return graphQLMap["available"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "available")
    }
  }

  public var chargeType: Swift.Optional<[PowerType?]?> {
    get {
      return graphQLMap["chargeType"] as? Swift.Optional<[PowerType?]?> ?? Swift.Optional<[PowerType?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargeType")
    }
  }

  public var plugsType: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["plugsType"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plugsType")
    }
  }

  public var powerRating: Swift.Optional<[Double?]?> {
    get {
      return graphQLMap["powerRating"] as? Swift.Optional<[Double?]?> ?? Swift.Optional<[Double?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "powerRating")
    }
  }

  public var withIn: Swift.Optional<ISearchWithIn?> {
    get {
      return graphQLMap["withIn"] as? Swift.Optional<ISearchWithIn?> ?? Swift.Optional<ISearchWithIn?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "withIn")
    }
  }

  public var surrounding: Swift.Optional<ISearchSurrounding?> {
    get {
      return graphQLMap["surrounding"] as? Swift.Optional<ISearchSurrounding?> ?? Swift.Optional<ISearchSurrounding?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "surrounding")
    }
  }
}

public struct ISearchWithIn: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - topRightLatitude
  ///   - topRightLongitude
  ///   - bottomLeftLatitude
  ///   - bottomLeftLongitude
  public init(topRightLatitude: Swift.Optional<Double?> = nil, topRightLongitude: Swift.Optional<Double?> = nil, bottomLeftLatitude: Swift.Optional<Double?> = nil, bottomLeftLongitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["topRightLatitude": topRightLatitude, "topRightLongitude": topRightLongitude, "bottomLeftLatitude": bottomLeftLatitude, "bottomLeftLongitude": bottomLeftLongitude]
  }

  public var topRightLatitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["topRightLatitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topRightLatitude")
    }
  }

  public var topRightLongitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["topRightLongitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "topRightLongitude")
    }
  }

  public var bottomLeftLatitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["bottomLeftLatitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "bottomLeftLatitude")
    }
  }

  public var bottomLeftLongitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["bottomLeftLongitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "bottomLeftLongitude")
    }
  }
}

public struct ISearchSurrounding: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - radius
  ///   - latitude
  ///   - longitude
  public init(radius: Swift.Optional<Double?> = nil, latitude: Swift.Optional<Double?> = nil, longitude: Swift.Optional<Double?> = nil) {
    graphQLMap = ["radius": radius, "latitude": latitude, "longitude": longitude]
  }

  public var radius: Swift.Optional<Double?> {
    get {
      return graphQLMap["radius"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "radius")
    }
  }

  public var latitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["latitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Swift.Optional<Double?> {
    get {
      return graphQLMap["longitude"] as? Swift.Optional<Double?> ?? Swift.Optional<Double?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }
}

public enum SearchSortOption: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case nearest
  case cheapestAc
  case cheapestDc
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "NEAREST": self = .nearest
      case "CHEAPEST_AC": self = .cheapestAc
      case "CHEAPEST_DC": self = .cheapestDc
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .nearest: return "NEAREST"
      case .cheapestAc: return "CHEAPEST_AC"
      case .cheapestDc: return "CHEAPEST_DC"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: SearchSortOption, rhs: SearchSortOption) -> Bool {
    switch (lhs, rhs) {
      case (.nearest, .nearest): return true
      case (.cheapestAc, .cheapestAc): return true
      case (.cheapestDc, .cheapestDc): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [SearchSortOption] {
    return [
      .nearest,
      .cheapestAc,
      .cheapestDc,
    ]
  }
}

public struct IUserPreference: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - plug
  ///   - stations
  public init(plug: Swift.Optional<String?> = nil, stations: Swift.Optional<[String?]?> = nil) {
    graphQLMap = ["plug": plug, "stations": stations]
  }

  public var plug: Swift.Optional<String?> {
    get {
      return graphQLMap["plug"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "plug")
    }
  }

  public var stations: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["stations"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stations")
    }
  }
}

public struct IVehicleManufacturerFilter: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - query
  public init(query: Swift.Optional<String?> = nil) {
    graphQLMap = ["query": query]
  }

  public var query: Swift.Optional<String?> {
    get {
      return graphQLMap["query"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "query")
    }
  }
}

public struct IVehicleModelFilter: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - query
  ///   - manufacturerIds
  ///   - type
  ///   - supportedPorts
  ///   - powerType
  public init(query: Swift.Optional<String?> = nil, manufacturerIds: Swift.Optional<[String?]?> = nil, type: Swift.Optional<[String?]?> = nil, supportedPorts: Swift.Optional<[String?]?> = nil, powerType: Swift.Optional<[PowerType?]?> = nil) {
    graphQLMap = ["query": query, "manufacturerIds": manufacturerIds, "type": type, "supportedPorts": supportedPorts, "powerType": powerType]
  }

  public var query: Swift.Optional<String?> {
    get {
      return graphQLMap["query"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "query")
    }
  }

  public var manufacturerIds: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["manufacturerIds"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "manufacturerIds")
    }
  }

  public var type: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["type"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }

  public var supportedPorts: Swift.Optional<[String?]?> {
    get {
      return graphQLMap["supportedPorts"] as? Swift.Optional<[String?]?> ?? Swift.Optional<[String?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "supportedPorts")
    }
  }

  public var powerType: Swift.Optional<[PowerType?]?> {
    get {
      return graphQLMap["powerType"] as? Swift.Optional<[PowerType?]?> ?? Swift.Optional<[PowerType?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "powerType")
    }
  }
}

public enum WalletTransactionType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case credit
  case debit
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "CREDIT": self = .credit
      case "DEBIT": self = .debit
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .credit: return "CREDIT"
      case .debit: return "DEBIT"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: WalletTransactionType, rhs: WalletTransactionType) -> Bool {
    switch (lhs, rhs) {
      case (.credit, .credit): return true
      case (.debit, .debit): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [WalletTransactionType] {
    return [
      .credit,
      .debit,
    ]
  }
}

public enum WalletTransactionStatus: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case pending
  case completed
  case cancelled
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "PENDING": self = .pending
      case "COMPLETED": self = .completed
      case "CANCELLED": self = .cancelled
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .pending: return "PENDING"
      case .completed: return "COMPLETED"
      case .cancelled: return "CANCELLED"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: WalletTransactionStatus, rhs: WalletTransactionStatus) -> Bool {
    switch (lhs, rhs) {
      case (.pending, .pending): return true
      case (.completed, .completed): return true
      case (.cancelled, .cancelled): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [WalletTransactionStatus] {
    return [
      .pending,
      .completed,
      .cancelled,
    ]
  }
}

public final class AvailabilitySlotQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query AvailabilitySlot($input: IAvailabilitySlot!) {
      AvailabilitySlot(input: $input) {
        __typename
        success
        data {
          __typename
          plug {
            __typename
            id
            name
          }
          station {
            __typename
            id
            name
          }
          charger {
            __typename
            id
            name
          }
          occupiedSlots {
            __typename
            startTime
            endTime
          }
          formattedSlots {
            __typename
            startTime
            endTime
            reserved
          }
        }
        error
        message
      }
    }
    """

  public let operationName: String = "AvailabilitySlot"

  public var input: IAvailabilitySlot

  public init(input: IAvailabilitySlot) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("AvailabilitySlot", arguments: ["input": GraphQLVariable("input")], type: .object(AvailabilitySlot.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(availabilitySlot: AvailabilitySlot? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "AvailabilitySlot": availabilitySlot.flatMap { (value: AvailabilitySlot) -> ResultMap in value.resultMap }])
    }

    public var availabilitySlot: AvailabilitySlot? {
      get {
        return (resultMap["AvailabilitySlot"] as? ResultMap).flatMap { AvailabilitySlot(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "AvailabilitySlot")
      }
    }

    public struct AvailabilitySlot: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AvailabilitySlotResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, data: Datum? = nil, error: String? = nil, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "AvailabilitySlotResponse", "success": success, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["AvailabilitySlot"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("occupiedSlots", type: .list(.object(OccupiedSlot.selections))),
            GraphQLField("formattedSlots", type: .list(.object(FormattedSlot.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(plug: Plug? = nil, station: Station? = nil, charger: Charger? = nil, occupiedSlots: [OccupiedSlot?]? = nil, formattedSlots: [FormattedSlot?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "AvailabilitySlot", "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "occupiedSlots": occupiedSlots.flatMap { (value: [OccupiedSlot?]) -> [ResultMap?] in value.map { (value: OccupiedSlot?) -> ResultMap? in value.flatMap { (value: OccupiedSlot) -> ResultMap in value.resultMap } } }, "formattedSlots": formattedSlots.flatMap { (value: [FormattedSlot?]) -> [ResultMap?] in value.map { (value: FormattedSlot?) -> ResultMap? in value.flatMap { (value: FormattedSlot) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var occupiedSlots: [OccupiedSlot?]? {
          get {
            return (resultMap["occupiedSlots"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [OccupiedSlot?] in value.map { (value: ResultMap?) -> OccupiedSlot? in value.flatMap { (value: ResultMap) -> OccupiedSlot in OccupiedSlot(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [OccupiedSlot?]) -> [ResultMap?] in value.map { (value: OccupiedSlot?) -> ResultMap? in value.flatMap { (value: OccupiedSlot) -> ResultMap in value.resultMap } } }, forKey: "occupiedSlots")
          }
        }

        public var formattedSlots: [FormattedSlot?]? {
          get {
            return (resultMap["formattedSlots"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FormattedSlot?] in value.map { (value: ResultMap?) -> FormattedSlot? in value.flatMap { (value: ResultMap) -> FormattedSlot in FormattedSlot(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [FormattedSlot?]) -> [ResultMap?] in value.map { (value: FormattedSlot?) -> ResultMap? in value.flatMap { (value: FormattedSlot) -> ResultMap in value.resultMap } } }, forKey: "formattedSlots")
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct OccupiedSlot: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["AvailabilityOccupiedSlot"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("startTime", type: .scalar(String.self)),
              GraphQLField("endTime", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(startTime: String? = nil, endTime: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "AvailabilityOccupiedSlot", "startTime": startTime, "endTime": endTime])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var startTime: String? {
            get {
              return resultMap["startTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "startTime")
            }
          }

          public var endTime: String? {
            get {
              return resultMap["endTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "endTime")
            }
          }
        }

        public struct FormattedSlot: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["AvailabilityFormattedSlot"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("startTime", type: .scalar(String.self)),
              GraphQLField("endTime", type: .scalar(String.self)),
              GraphQLField("reserved", type: .scalar(Bool.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(startTime: String? = nil, endTime: String? = nil, reserved: Bool? = nil) {
            self.init(unsafeResultMap: ["__typename": "AvailabilityFormattedSlot", "startTime": startTime, "endTime": endTime, "reserved": reserved])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var startTime: String? {
            get {
              return resultMap["startTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "startTime")
            }
          }

          public var endTime: String? {
            get {
              return resultMap["endTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "endTime")
            }
          }

          public var reserved: Bool? {
            get {
              return resultMap["reserved"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "reserved")
            }
          }
        }
      }
    }
  }
}

public final class ForgetPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ForgetPassword($username: String!) {
      forgetPassword(username: $username) {
        __typename
        success
        message
        data {
          __typename
          id
          resetToken
        }
        error
      }
    }
    """

  public let operationName: String = "ForgetPassword"

  public var username: String

  public init(username: String) {
    self.username = username
  }

  public var variables: GraphQLMap? {
    return ["username": username]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("forgetPassword", arguments: ["username": GraphQLVariable("username")], type: .object(ForgetPassword.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(forgetPassword: ForgetPassword? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "forgetPassword": forgetPassword.flatMap { (value: ForgetPassword) -> ResultMap in value.resultMap }])
    }

    public var forgetPassword: ForgetPassword? {
      get {
        return (resultMap["forgetPassword"] as? ResultMap).flatMap { ForgetPassword(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "forgetPassword")
      }
    }

    public struct ForgetPassword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ForgetPasswordResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ForgetPasswordResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ForgetPasswordResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("resetToken", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, resetToken: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "ForgetPasswordResponseData", "id": id, "resetToken": resetToken])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var resetToken: String? {
          get {
            return resultMap["resetToken"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "resetToken")
          }
        }
      }
    }
  }
}

public final class GenerateOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation GenerateOTP($reservationId: String!) {
      generateOTP(reservationId: $reservationId) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
          }
          charger {
            __typename
            id
            name
          }
          plug {
            __typename
            id
            name
          }
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          unitsConsumed
          plugActivationCode
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            id
            name
          }
        }
        error
      }
    }
    """

  public let operationName: String = "GenerateOTP"

  public var reservationId: String

  public init(reservationId: String) {
    self.reservationId = reservationId
  }

  public var variables: GraphQLMap? {
    return ["reservationId": reservationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("generateOTP", arguments: ["reservationId": GraphQLVariable("reservationId")], type: .object(GenerateOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(generateOtp: GenerateOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "generateOTP": generateOtp.flatMap { (value: GenerateOtp) -> ResultMap in value.resultMap }])
    }

    public var generateOtp: GenerateOtp? {
      get {
        return (resultMap["generateOTP"] as? ResultMap).flatMap { GenerateOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "generateOTP")
      }
    }

    public struct GenerateOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("plugActivationCode", type: .scalar(String.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, unitsConsumed: Double? = nil, plugActivationCode: String? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "unitsConsumed": unitsConsumed, "plugActivationCode": plugActivationCode, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var plugActivationCode: String? {
          get {
            return resultMap["plugActivationCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugActivationCode")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class LoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation Login($username: String!, $password: String!) {
      loginUser(username: $username, password: $password) {
        __typename
        success
        message
        data {
          __typename
          user {
            __typename
            id
            name
            email
            phone
            role
            status
            preference {
              __typename
              plug
              stationIds
            }
          }
          token
        }
        error
      }
    }
    """

  public let operationName: String = "Login"

  public var username: String
  public var password: String

  public init(username: String, password: String) {
    self.username = username
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["username": username, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("loginUser", arguments: ["username": GraphQLVariable("username"), "password": GraphQLVariable("password")], type: .object(LoginUser.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(loginUser: LoginUser? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "loginUser": loginUser.flatMap { (value: LoginUser) -> ResultMap in value.resultMap }])
    }

    public var loginUser: LoginUser? {
      get {
        return (resultMap["loginUser"] as? ResultMap).flatMap { LoginUser(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "loginUser")
      }
    }

    public struct LoginUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUserResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "LoginUserResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["LoginUserResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("user", type: .object(User.selections)),
            GraphQLField("token", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(user: User? = nil, token: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "LoginUserResponseData", "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }, "token": token])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("role", type: .scalar(UserRole.self)),
              GraphQLField("status", type: .scalar(UserStatus.self)),
              GraphQLField("preference", type: .object(Preference.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, email: String? = nil, phone: String? = nil, role: UserRole? = nil, status: UserStatus? = nil, preference: Preference? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name, "email": email, "phone": phone, "role": role, "status": status, "preference": preference.flatMap { (value: Preference) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var role: UserRole? {
            get {
              return resultMap["role"] as? UserRole
            }
            set {
              resultMap.updateValue(newValue, forKey: "role")
            }
          }

          public var status: UserStatus? {
            get {
              return resultMap["status"] as? UserStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var preference: Preference? {
            get {
              return (resultMap["preference"] as? ResultMap).flatMap { Preference(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "preference")
            }
          }

          public struct Preference: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["UserPreference"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("plug", type: .scalar(String.self)),
                GraphQLField("stationIds", type: .list(.scalar(String.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(plug: String? = nil, stationIds: [String?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "UserPreference", "plug": plug, "stationIds": stationIds])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var plug: String? {
              get {
                return resultMap["plug"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "plug")
              }
            }

            public var stationIds: [String?]? {
              get {
                return resultMap["stationIds"] as? [String?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "stationIds")
              }
            }
          }
        }
      }
    }
  }
}

public final class PayForReservationFromUpiMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation PayForReservationFromUPI($reservationId: String!, $upi: String!) {
      payForReservationFromUPI(reservationId: $reservationId, upi: $upi) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            flag
            images
            location
          }
          charger {
            __typename
            id
            name
            status
            power
            type
            plugs {
              __typename
              id
              name
              status
              supportedPort
              power
              perUnitAcCharge
              perUnitDcCharge
            }
            ocppVersion
            machineId
          }
          plug {
            __typename
            id
            name
            status
            supportedPorts
          }
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          plugActivationCode
          unitsConsumed
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            _id
            id
            name
            role
            email
            phone
            status
          }
          holdExpiry
          reservedAmount
          reserveAmountSource
          acceptStartChargingAt
          cfPaymentStatus
          autoCancelReservationAt
        }
        error
      }
    }
    """

  public let operationName: String = "PayForReservationFromUPI"

  public var reservationId: String
  public var upi: String

  public init(reservationId: String, upi: String) {
    self.reservationId = reservationId
    self.upi = upi
  }

  public var variables: GraphQLMap? {
    return ["reservationId": reservationId, "upi": upi]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("payForReservationFromUPI", arguments: ["reservationId": GraphQLVariable("reservationId"), "upi": GraphQLVariable("upi")], type: .object(PayForReservationFromUpi.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(payForReservationFromUpi: PayForReservationFromUpi? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "payForReservationFromUPI": payForReservationFromUpi.flatMap { (value: PayForReservationFromUpi) -> ResultMap in value.resultMap }])
    }

    public var payForReservationFromUpi: PayForReservationFromUpi? {
      get {
        return (resultMap["payForReservationFromUPI"] as? ResultMap).flatMap { PayForReservationFromUpi(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "payForReservationFromUPI")
      }
    }

    public struct PayForReservationFromUpi: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("plugActivationCode", type: .scalar(String.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
            GraphQLField("holdExpiry", type: .scalar(String.self)),
            GraphQLField("reservedAmount", type: .scalar(Int.self)),
            GraphQLField("reserveAmountSource", type: .scalar(ReservationAmountSource.self)),
            GraphQLField("acceptStartChargingAt", type: .scalar(String.self)),
            GraphQLField("cfPaymentStatus", type: .scalar(String.self)),
            GraphQLField("autoCancelReservationAt", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, plugActivationCode: String? = nil, unitsConsumed: Double? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil, holdExpiry: String? = nil, reservedAmount: Int? = nil, reserveAmountSource: ReservationAmountSource? = nil, acceptStartChargingAt: String? = nil, cfPaymentStatus: String? = nil, autoCancelReservationAt: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "plugActivationCode": plugActivationCode, "unitsConsumed": unitsConsumed, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }, "holdExpiry": holdExpiry, "reservedAmount": reservedAmount, "reserveAmountSource": reserveAmountSource, "acceptStartChargingAt": acceptStartChargingAt, "cfPaymentStatus": cfPaymentStatus, "autoCancelReservationAt": autoCancelReservationAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var plugActivationCode: String? {
          get {
            return resultMap["plugActivationCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugActivationCode")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public var holdExpiry: String? {
          get {
            return resultMap["holdExpiry"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "holdExpiry")
          }
        }

        public var reservedAmount: Int? {
          get {
            return resultMap["reservedAmount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "reservedAmount")
          }
        }

        public var reserveAmountSource: ReservationAmountSource? {
          get {
            return resultMap["reserveAmountSource"] as? ReservationAmountSource
          }
          set {
            resultMap.updateValue(newValue, forKey: "reserveAmountSource")
          }
        }

        public var acceptStartChargingAt: String? {
          get {
            return resultMap["acceptStartChargingAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "acceptStartChargingAt")
          }
        }

        public var cfPaymentStatus: String? {
          get {
            return resultMap["cfPaymentStatus"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "cfPaymentStatus")
          }
        }

        public var autoCancelReservationAt: String? {
          get {
            return resultMap["autoCancelReservationAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "autoCancelReservationAt")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, flag: String? = nil, images: [String?]? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "flag": flag, "images": images, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(ChargerStatus.self)),
              GraphQLField("power", type: .scalar(Double.self)),
              GraphQLField("type", type: .scalar(PowerType.self)),
              GraphQLField("plugs", type: .list(.object(Plug.selections))),
              GraphQLField("ocppVersion", type: .nonNull(.scalar(OCPPVersion.self))),
              GraphQLField("machineId", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil, ocppVersion: OCPPVersion, machineId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, "ocppVersion": ocppVersion, "machineId": machineId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: ChargerStatus? {
            get {
              return resultMap["status"] as? ChargerStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var power: Double? {
            get {
              return resultMap["power"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "power")
            }
          }

          public var type: PowerType? {
            get {
              return resultMap["type"] as? PowerType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var plugs: [Plug?]? {
            get {
              return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
            }
          }

          public var ocppVersion: OCPPVersion {
            get {
              return resultMap["ocppVersion"]! as! OCPPVersion
            }
            set {
              resultMap.updateValue(newValue, forKey: "ocppVersion")
            }
          }

          public var machineId: String? {
            get {
              return resultMap["machineId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "machineId")
            }
          }

          public struct Plug: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Plug"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(PlugStatus.self)),
                GraphQLField("supportedPort", type: .scalar(String.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: PlugStatus? {
              get {
                return resultMap["status"] as? PlugStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var supportedPort: String? {
              get {
                return resultMap["supportedPort"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "supportedPort")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var perUnitAcCharge: Double? {
              get {
                return resultMap["perUnitAcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
              }
            }

            public var perUnitDcCharge: Double? {
              get {
                return resultMap["perUnitDcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
              }
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(PlugStatus.self)),
              GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPorts: [String?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPorts": supportedPorts])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: PlugStatus? {
            get {
              return resultMap["status"] as? PlugStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var supportedPorts: [String?]? {
            get {
              return resultMap["supportedPorts"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPorts")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("role", type: .scalar(UserRole.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(UserStatus.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(_id: String? = nil, id: String? = nil, name: String? = nil, role: UserRole? = nil, email: String? = nil, phone: String? = nil, status: UserStatus? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "_id": _id, "id": id, "name": name, "role": role, "email": email, "phone": phone, "status": status])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var _id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var role: UserRole? {
            get {
              return resultMap["role"] as? UserRole
            }
            set {
              resultMap.updateValue(newValue, forKey: "role")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var status: UserStatus? {
            get {
              return resultMap["status"] as? UserStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }
        }
      }
    }
  }
}

public final class PayForReservationFromWalletMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation PayForReservationFromWallet($reservationId: String!) {
      payForReservationFromWallet(reservationId: $reservationId) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            flag
            images
            chargers {
              __typename
              id
              name
              status
              power
              type
              plugs {
                __typename
                id
                name
                status
                supportedPort
                power
                perUnitAcCharge
                perUnitDcCharge
              }
              ocppVersion
              machineId
            }
            cpo {
              __typename
              id
              name
              revenuePlan
              address {
                __typename
                line1
                line2
                city
                state
                zip
                phone
              }
              phone
              kind
              perUnitAcCharge
              perUnitDcCharge
              billing {
                __typename
                gst
                pan
                accountNumber
                ifsc
              }
            }
            location
          }
          charger {
            __typename
            id
            name
            status
            power
            type
            plugs {
              __typename
              id
              name
              status
              supportedPort
              power
              perUnitAcCharge
              perUnitDcCharge
            }
            ocppVersion
            machineId
          }
          plug {
            __typename
            id
            name
            status
            supportedPorts
          }
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          plugActivationCode
          unitsConsumed
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            _id
            id
            name
            role
            email
            phone
            status
          }
          holdExpiry
          reservedAmount
          reserveAmountSource
          acceptStartChargingAt
          cfPaymentStatus
          autoCancelReservationAt
        }
        error
      }
    }
    """

  public let operationName: String = "PayForReservationFromWallet"

  public var reservationId: String

  public init(reservationId: String) {
    self.reservationId = reservationId
  }

  public var variables: GraphQLMap? {
    return ["reservationId": reservationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("payForReservationFromWallet", arguments: ["reservationId": GraphQLVariable("reservationId")], type: .object(PayForReservationFromWallet.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(payForReservationFromWallet: PayForReservationFromWallet? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "payForReservationFromWallet": payForReservationFromWallet.flatMap { (value: PayForReservationFromWallet) -> ResultMap in value.resultMap }])
    }

    public var payForReservationFromWallet: PayForReservationFromWallet? {
      get {
        return (resultMap["payForReservationFromWallet"] as? ResultMap).flatMap { PayForReservationFromWallet(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "payForReservationFromWallet")
      }
    }

    public struct PayForReservationFromWallet: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("plugActivationCode", type: .scalar(String.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
            GraphQLField("holdExpiry", type: .scalar(String.self)),
            GraphQLField("reservedAmount", type: .scalar(Int.self)),
            GraphQLField("reserveAmountSource", type: .scalar(ReservationAmountSource.self)),
            GraphQLField("acceptStartChargingAt", type: .scalar(String.self)),
            GraphQLField("cfPaymentStatus", type: .scalar(String.self)),
            GraphQLField("autoCancelReservationAt", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, plugActivationCode: String? = nil, unitsConsumed: Double? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil, holdExpiry: String? = nil, reservedAmount: Int? = nil, reserveAmountSource: ReservationAmountSource? = nil, acceptStartChargingAt: String? = nil, cfPaymentStatus: String? = nil, autoCancelReservationAt: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "plugActivationCode": plugActivationCode, "unitsConsumed": unitsConsumed, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }, "holdExpiry": holdExpiry, "reservedAmount": reservedAmount, "reserveAmountSource": reserveAmountSource, "acceptStartChargingAt": acceptStartChargingAt, "cfPaymentStatus": cfPaymentStatus, "autoCancelReservationAt": autoCancelReservationAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var plugActivationCode: String? {
          get {
            return resultMap["plugActivationCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugActivationCode")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public var holdExpiry: String? {
          get {
            return resultMap["holdExpiry"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "holdExpiry")
          }
        }

        public var reservedAmount: Int? {
          get {
            return resultMap["reservedAmount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "reservedAmount")
          }
        }

        public var reserveAmountSource: ReservationAmountSource? {
          get {
            return resultMap["reserveAmountSource"] as? ReservationAmountSource
          }
          set {
            resultMap.updateValue(newValue, forKey: "reserveAmountSource")
          }
        }

        public var acceptStartChargingAt: String? {
          get {
            return resultMap["acceptStartChargingAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "acceptStartChargingAt")
          }
        }

        public var cfPaymentStatus: String? {
          get {
            return resultMap["cfPaymentStatus"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "cfPaymentStatus")
          }
        }

        public var autoCancelReservationAt: String? {
          get {
            return resultMap["autoCancelReservationAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "autoCancelReservationAt")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("chargers", type: .list(.object(Charger.selections))),
              GraphQLField("cpo", type: .object(Cpo.selections)),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, cpo: Cpo? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "cpo": cpo.flatMap { (value: Cpo) -> ResultMap in value.resultMap }, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var chargers: [Charger?]? {
            get {
              return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
            }
          }

          public var cpo: Cpo? {
            get {
              return (resultMap["cpo"] as? ResultMap).flatMap { Cpo(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "cpo")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }

          public struct Charger: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Charger"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(ChargerStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("type", type: .scalar(PowerType.self)),
                GraphQLField("plugs", type: .list(.object(Plug.selections))),
                GraphQLField("ocppVersion", type: .nonNull(.scalar(OCPPVersion.self))),
                GraphQLField("machineId", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil, ocppVersion: OCPPVersion, machineId: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, "ocppVersion": ocppVersion, "machineId": machineId])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: ChargerStatus? {
              get {
                return resultMap["status"] as? ChargerStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var type: PowerType? {
              get {
                return resultMap["type"] as? PowerType
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var plugs: [Plug?]? {
              get {
                return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
              }
              set {
                resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
              }
            }

            public var ocppVersion: OCPPVersion {
              get {
                return resultMap["ocppVersion"]! as! OCPPVersion
              }
              set {
                resultMap.updateValue(newValue, forKey: "ocppVersion")
              }
            }

            public var machineId: String? {
              get {
                return resultMap["machineId"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "machineId")
              }
            }

            public struct Plug: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Plug"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(String.self)),
                  GraphQLField("name", type: .scalar(String.self)),
                  GraphQLField("status", type: .scalar(PlugStatus.self)),
                  GraphQLField("supportedPort", type: .scalar(String.self)),
                  GraphQLField("power", type: .scalar(Double.self)),
                  GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                  GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
                self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: String? {
                get {
                  return resultMap["id"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var name: String? {
                get {
                  return resultMap["name"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "name")
                }
              }

              public var status: PlugStatus? {
                get {
                  return resultMap["status"] as? PlugStatus
                }
                set {
                  resultMap.updateValue(newValue, forKey: "status")
                }
              }

              public var supportedPort: String? {
                get {
                  return resultMap["supportedPort"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "supportedPort")
                }
              }

              public var power: Double? {
                get {
                  return resultMap["power"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "power")
                }
              }

              public var perUnitAcCharge: Double? {
                get {
                  return resultMap["perUnitAcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
                }
              }

              public var perUnitDcCharge: Double? {
                get {
                  return resultMap["perUnitDcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
                }
              }
            }
          }

          public struct Cpo: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["CPO"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("revenuePlan", type: .scalar(String.self)),
                GraphQLField("address", type: .object(Address.selections)),
                GraphQLField("phone", type: .scalar(String.self)),
                GraphQLField("kind", type: .scalar(CPOKind.self)),
                GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
                GraphQLField("billing", type: .object(Billing.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, revenuePlan: String? = nil, address: Address? = nil, phone: String? = nil, kind: CPOKind? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, billing: Billing? = nil) {
              self.init(unsafeResultMap: ["__typename": "CPO", "id": id, "name": name, "revenuePlan": revenuePlan, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "phone": phone, "kind": kind, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "billing": billing.flatMap { (value: Billing) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var revenuePlan: String? {
              get {
                return resultMap["revenuePlan"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "revenuePlan")
              }
            }

            public var address: Address? {
              get {
                return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "address")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }

            public var kind: CPOKind? {
              get {
                return resultMap["kind"] as? CPOKind
              }
              set {
                resultMap.updateValue(newValue, forKey: "kind")
              }
            }

            public var perUnitAcCharge: Double? {
              get {
                return resultMap["perUnitAcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
              }
            }

            public var perUnitDcCharge: Double? {
              get {
                return resultMap["perUnitDcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
              }
            }

            public var billing: Billing? {
              get {
                return (resultMap["billing"] as? ResultMap).flatMap { Billing(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "billing")
              }
            }

            public struct Address: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Address"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("line1", type: .scalar(String.self)),
                  GraphQLField("line2", type: .scalar(String.self)),
                  GraphQLField("city", type: .scalar(String.self)),
                  GraphQLField("state", type: .scalar(String.self)),
                  GraphQLField("zip", type: .scalar(String.self)),
                  GraphQLField("phone", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var line1: String? {
                get {
                  return resultMap["line1"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line1")
                }
              }

              public var line2: String? {
                get {
                  return resultMap["line2"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line2")
                }
              }

              public var city: String? {
                get {
                  return resultMap["city"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "city")
                }
              }

              public var state: String? {
                get {
                  return resultMap["state"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "state")
                }
              }

              public var zip: String? {
                get {
                  return resultMap["zip"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "zip")
                }
              }

              public var phone: String? {
                get {
                  return resultMap["phone"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "phone")
                }
              }
            }

            public struct Billing: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["CPOBillingInfo"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("gst", type: .scalar(String.self)),
                  GraphQLField("pan", type: .scalar(String.self)),
                  GraphQLField("accountNumber", type: .scalar(String.self)),
                  GraphQLField("ifsc", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(gst: String? = nil, pan: String? = nil, accountNumber: String? = nil, ifsc: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "CPOBillingInfo", "gst": gst, "pan": pan, "accountNumber": accountNumber, "ifsc": ifsc])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var gst: String? {
                get {
                  return resultMap["gst"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "gst")
                }
              }

              public var pan: String? {
                get {
                  return resultMap["pan"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "pan")
                }
              }

              public var accountNumber: String? {
                get {
                  return resultMap["accountNumber"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "accountNumber")
                }
              }

              public var ifsc: String? {
                get {
                  return resultMap["ifsc"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "ifsc")
                }
              }
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(ChargerStatus.self)),
              GraphQLField("power", type: .scalar(Double.self)),
              GraphQLField("type", type: .scalar(PowerType.self)),
              GraphQLField("plugs", type: .list(.object(Plug.selections))),
              GraphQLField("ocppVersion", type: .nonNull(.scalar(OCPPVersion.self))),
              GraphQLField("machineId", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil, ocppVersion: OCPPVersion, machineId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, "ocppVersion": ocppVersion, "machineId": machineId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: ChargerStatus? {
            get {
              return resultMap["status"] as? ChargerStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var power: Double? {
            get {
              return resultMap["power"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "power")
            }
          }

          public var type: PowerType? {
            get {
              return resultMap["type"] as? PowerType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var plugs: [Plug?]? {
            get {
              return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
            }
          }

          public var ocppVersion: OCPPVersion {
            get {
              return resultMap["ocppVersion"]! as! OCPPVersion
            }
            set {
              resultMap.updateValue(newValue, forKey: "ocppVersion")
            }
          }

          public var machineId: String? {
            get {
              return resultMap["machineId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "machineId")
            }
          }

          public struct Plug: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Plug"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(PlugStatus.self)),
                GraphQLField("supportedPort", type: .scalar(String.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: PlugStatus? {
              get {
                return resultMap["status"] as? PlugStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var supportedPort: String? {
              get {
                return resultMap["supportedPort"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "supportedPort")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var perUnitAcCharge: Double? {
              get {
                return resultMap["perUnitAcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
              }
            }

            public var perUnitDcCharge: Double? {
              get {
                return resultMap["perUnitDcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
              }
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(PlugStatus.self)),
              GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPorts: [String?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPorts": supportedPorts])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: PlugStatus? {
            get {
              return resultMap["status"] as? PlugStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var supportedPorts: [String?]? {
            get {
              return resultMap["supportedPorts"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPorts")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("role", type: .scalar(UserRole.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(UserStatus.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(_id: String? = nil, id: String? = nil, name: String? = nil, role: UserRole? = nil, email: String? = nil, phone: String? = nil, status: UserStatus? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "_id": _id, "id": id, "name": name, "role": role, "email": email, "phone": phone, "status": status])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var _id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var role: UserRole? {
            get {
              return resultMap["role"] as? UserRole
            }
            set {
              resultMap.updateValue(newValue, forKey: "role")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var status: UserStatus? {
            get {
              return resultMap["status"] as? UserStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }
        }
      }
    }
  }
}

public final class PortTypesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query PortTypes {
      PortTypes {
        __typename
        id
        name
        type
        supportedPowers
      }
    }
    """

  public let operationName: String = "PortTypes"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("PortTypes", type: .list(.object(PortType.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(portTypes: [PortType?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "PortTypes": portTypes.flatMap { (value: [PortType?]) -> [ResultMap?] in value.map { (value: PortType?) -> ResultMap? in value.flatMap { (value: PortType) -> ResultMap in value.resultMap } } }])
    }

    public var portTypes: [PortType?]? {
      get {
        return (resultMap["PortTypes"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [PortType?] in value.map { (value: ResultMap?) -> PortType? in value.flatMap { (value: ResultMap) -> PortType in PortType(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [PortType?]) -> [ResultMap?] in value.map { (value: PortType?) -> ResultMap? in value.flatMap { (value: PortType) -> ResultMap in value.resultMap } } }, forKey: "PortTypes")
      }
    }

    public struct PortType: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PortType"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("type", type: .scalar(PowerType.self)),
          GraphQLField("supportedPowers", type: .list(.scalar(Double.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, name: String? = nil, type: PowerType? = nil, supportedPowers: [Double?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PortType", "id": id, "name": name, "type": type, "supportedPowers": supportedPowers])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var type: PowerType? {
        get {
          return resultMap["type"] as? PowerType
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var supportedPowers: [Double?]? {
        get {
          return resultMap["supportedPowers"] as? [Double?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "supportedPowers")
        }
      }
    }
  }
}

public final class RefreshTokenMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RefreshToken {
      refreshToken {
        __typename
        success
        message
        data {
          __typename
          token
        }
        error
      }
    }
    """

  public let operationName: String = "RefreshToken"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("refreshToken", type: .object(RefreshToken.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(refreshToken: RefreshToken? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "refreshToken": refreshToken.flatMap { (value: RefreshToken) -> ResultMap in value.resultMap }])
    }

    public var refreshToken: RefreshToken? {
      get {
        return (resultMap["refreshToken"] as? ResultMap).flatMap { RefreshToken(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "refreshToken")
      }
    }

    public struct RefreshToken: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUserResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "LoginUserResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["LoginUserResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "LoginUserResponseData", "token": token])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }
      }
    }
  }
}

public final class RegisterUserMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation RegisterUser($input: IRegisterUser!) {
      registerUser(input: $input) {
        __typename
        success
        message
        data {
          __typename
          id
          name
        }
        error
      }
    }
    """

  public let operationName: String = "RegisterUser"

  public var input: IRegisterUser

  public init(input: IRegisterUser) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("registerUser", arguments: ["input": GraphQLVariable("input")], type: .object(RegisterUser.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(registerUser: RegisterUser? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "registerUser": registerUser.flatMap { (value: RegisterUser) -> ResultMap in value.resultMap }])
    }

    public var registerUser: RegisterUser? {
      get {
        return (resultMap["registerUser"] as? ResultMap).flatMap { RegisterUser(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "registerUser")
      }
    }

    public struct RegisterUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserRegistrationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserRegistrationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserRegistrationResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserRegistrationResponseData", "id": id, "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}

public final class ResendLoginOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ResendLoginOTP($id: String!) {
      resendLoginOTP(id: $id) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "ResendLoginOTP"

  public var id: String

  public init(id: String) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("resendLoginOTP", arguments: ["id": GraphQLVariable("id")], type: .object(ResendLoginOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resendLoginOtp: ResendLoginOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "resendLoginOTP": resendLoginOtp.flatMap { (value: ResendLoginOtp) -> ResultMap in value.resultMap }])
    }

    public var resendLoginOtp: ResendLoginOtp? {
      get {
        return (resultMap["resendLoginOTP"] as? ResultMap).flatMap { ResendLoginOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "resendLoginOTP")
      }
    }

    public struct ResendLoginOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ResendLoginOTPResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ResendLoginOTPResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class ResendRegistrationOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ResendRegistrationOTP($userId: String!) {
      resendRegistrationOTP(userId: $userId) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "ResendRegistrationOTP"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("resendRegistrationOTP", arguments: ["userId": GraphQLVariable("userId")], type: .object(ResendRegistrationOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resendRegistrationOtp: ResendRegistrationOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "resendRegistrationOTP": resendRegistrationOtp.flatMap { (value: ResendRegistrationOtp) -> ResultMap in value.resultMap }])
    }

    public var resendRegistrationOtp: ResendRegistrationOtp? {
      get {
        return (resultMap["resendRegistrationOTP"] as? ResultMap).flatMap { ResendRegistrationOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "resendRegistrationOTP")
      }
    }

    public struct ResendRegistrationOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class ReservationsListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ReservationsList($pagination: IPagination!, $filter: IReservationFilter!) {
      Reservations(pagination: $pagination, filter: $filter) {
        __typename
        docs {
          __typename
          id
          station {
            __typename
            id
            name
            description
            images
            status
            perUnitAcCharge
            perUnitDcCharge
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
          }
          charger {
            __typename
            id
            name
          }
          plug {
            __typename
            id
            name
          }
          reservedAmount
          reserveAmountSource
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          unitsConsumed
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            id
            name
          }
        }
        pagination {
          __typename
          totalDocs
          limit
          page
          totalPages
          hasPrevPage
          hasNextPage
          prevPage
          nextPage
        }
      }
    }
    """

  public let operationName: String = "ReservationsList"

  public var pagination: IPagination
  public var filter: IReservationFilter

  public init(pagination: IPagination, filter: IReservationFilter) {
    self.pagination = pagination
    self.filter = filter
  }

  public var variables: GraphQLMap? {
    return ["pagination": pagination, "filter": filter]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("Reservations", arguments: ["pagination": GraphQLVariable("pagination"), "filter": GraphQLVariable("filter")], type: .object(Reservation.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(reservations: Reservation? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Reservations": reservations.flatMap { (value: Reservation) -> ResultMap in value.resultMap }])
    }

    public var reservations: Reservation? {
      get {
        return (resultMap["Reservations"] as? ResultMap).flatMap { Reservation(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Reservations")
      }
    }

    public struct Reservation: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Reservations"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("docs", type: .list(.object(Doc.selections))),
          GraphQLField("pagination", type: .object(Pagination.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(docs: [Doc?]? = nil, pagination: Pagination? = nil) {
        self.init(unsafeResultMap: ["__typename": "Reservations", "docs": docs.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, "pagination": pagination.flatMap { (value: Pagination) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var docs: [Doc?]? {
        get {
          return (resultMap["docs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Doc?] in value.map { (value: ResultMap?) -> Doc? in value.flatMap { (value: ResultMap) -> Doc in Doc(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, forKey: "docs")
        }
      }

      public var pagination: Pagination? {
        get {
          return (resultMap["pagination"] as? ResultMap).flatMap { Pagination(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "pagination")
        }
      }

      public struct Doc: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("reservedAmount", type: .scalar(Int.self)),
            GraphQLField("reserveAmountSource", type: .scalar(ReservationAmountSource.self)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, reservedAmount: Int? = nil, reserveAmountSource: ReservationAmountSource? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, unitsConsumed: Double? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "reservedAmount": reservedAmount, "reserveAmountSource": reserveAmountSource, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "unitsConsumed": unitsConsumed, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var reservedAmount: Int? {
          get {
            return resultMap["reservedAmount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "reservedAmount")
          }
        }

        public var reserveAmountSource: ReservationAmountSource? {
          get {
            return resultMap["reserveAmountSource"] as? ReservationAmountSource
          }
          set {
            resultMap.updateValue(newValue, forKey: "reserveAmountSource")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, images: [String?]? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, address: Address? = nil, position: Position? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "images": images, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }

      public struct Pagination: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Pagination"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("totalDocs", type: .scalar(Int.self)),
            GraphQLField("limit", type: .scalar(Int.self)),
            GraphQLField("page", type: .scalar(Int.self)),
            GraphQLField("totalPages", type: .scalar(Int.self)),
            GraphQLField("hasPrevPage", type: .scalar(Int.self)),
            GraphQLField("hasNextPage", type: .scalar(Int.self)),
            GraphQLField("prevPage", type: .scalar(Int.self)),
            GraphQLField("nextPage", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDocs: Int? = nil, limit: Int? = nil, page: Int? = nil, totalPages: Int? = nil, hasPrevPage: Int? = nil, hasNextPage: Int? = nil, prevPage: Int? = nil, nextPage: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "Pagination", "totalDocs": totalDocs, "limit": limit, "page": page, "totalPages": totalPages, "hasPrevPage": hasPrevPage, "hasNextPage": hasNextPage, "prevPage": prevPage, "nextPage": nextPage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var totalDocs: Int? {
          get {
            return resultMap["totalDocs"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDocs")
          }
        }

        public var limit: Int? {
          get {
            return resultMap["limit"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "limit")
          }
        }

        public var page: Int? {
          get {
            return resultMap["page"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "page")
          }
        }

        public var totalPages: Int? {
          get {
            return resultMap["totalPages"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalPages")
          }
        }

        public var hasPrevPage: Int? {
          get {
            return resultMap["hasPrevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasPrevPage")
          }
        }

        public var hasNextPage: Int? {
          get {
            return resultMap["hasNextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasNextPage")
          }
        }

        public var prevPage: Int? {
          get {
            return resultMap["prevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "prevPage")
          }
        }

        public var nextPage: Int? {
          get {
            return resultMap["nextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "nextPage")
          }
        }
      }
    }
  }
}

public final class ReserveMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation Reserve($userId: String!, $input: IReservation!) {
      reserve(userId: $userId, input: $input) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            flag
            images
            chargers {
              __typename
              id
              name
              status
              power
              type
              plugs {
                __typename
                id
                name
                status
                supportedPort
                power
                perUnitAcCharge
                perUnitDcCharge
              }
            }
            location
          }
          charger {
            __typename
            id
            name
          }
          plug {
            __typename
            id
            name
          }
          reservedAmount
          reserveAmountSource
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          unitsConsumed
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            id
            name
          }
        }
        error
      }
    }
    """

  public let operationName: String = "Reserve"

  public var userId: String
  public var input: IReservation

  public init(userId: String, input: IReservation) {
    self.userId = userId
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("reserve", arguments: ["userId": GraphQLVariable("userId"), "input": GraphQLVariable("input")], type: .object(Reserve.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(reserve: Reserve? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "reserve": reserve.flatMap { (value: Reserve) -> ResultMap in value.resultMap }])
    }

    public var reserve: Reserve? {
      get {
        return (resultMap["reserve"] as? ResultMap).flatMap { Reserve(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "reserve")
      }
    }

    public struct Reserve: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("reservedAmount", type: .scalar(Int.self)),
            GraphQLField("reserveAmountSource", type: .scalar(ReservationAmountSource.self)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, reservedAmount: Int? = nil, reserveAmountSource: ReservationAmountSource? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, unitsConsumed: Double? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "reservedAmount": reservedAmount, "reserveAmountSource": reserveAmountSource, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "unitsConsumed": unitsConsumed, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var reservedAmount: Int? {
          get {
            return resultMap["reservedAmount"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "reservedAmount")
          }
        }

        public var reserveAmountSource: ReservationAmountSource? {
          get {
            return resultMap["reserveAmountSource"] as? ReservationAmountSource
          }
          set {
            resultMap.updateValue(newValue, forKey: "reserveAmountSource")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("chargers", type: .list(.object(Charger.selections))),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var chargers: [Charger?]? {
            get {
              return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }

          public struct Charger: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Charger"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(ChargerStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("type", type: .scalar(PowerType.self)),
                GraphQLField("plugs", type: .list(.object(Plug.selections))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: ChargerStatus? {
              get {
                return resultMap["status"] as? ChargerStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var type: PowerType? {
              get {
                return resultMap["type"] as? PowerType
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var plugs: [Plug?]? {
              get {
                return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
              }
              set {
                resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
              }
            }

            public struct Plug: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Plug"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(String.self)),
                  GraphQLField("name", type: .scalar(String.self)),
                  GraphQLField("status", type: .scalar(PlugStatus.self)),
                  GraphQLField("supportedPort", type: .scalar(String.self)),
                  GraphQLField("power", type: .scalar(Double.self)),
                  GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                  GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
                self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: String? {
                get {
                  return resultMap["id"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var name: String? {
                get {
                  return resultMap["name"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "name")
                }
              }

              public var status: PlugStatus? {
                get {
                  return resultMap["status"] as? PlugStatus
                }
                set {
                  resultMap.updateValue(newValue, forKey: "status")
                }
              }

              public var supportedPort: String? {
                get {
                  return resultMap["supportedPort"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "supportedPort")
                }
              }

              public var power: Double? {
                get {
                  return resultMap["power"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "power")
                }
              }

              public var perUnitAcCharge: Double? {
                get {
                  return resultMap["perUnitAcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
                }
              }

              public var perUnitDcCharge: Double? {
                get {
                  return resultMap["perUnitDcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
                }
              }
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class ResetUserPasswordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation ResetUserPassword($userId: String!, $password: String!) {
      resetUserPassword(userId: $userId, password: $password) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "ResetUserPassword"

  public var userId: String
  public var password: String

  public init(userId: String, password: String) {
    self.userId = userId
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("resetUserPassword", arguments: ["userId": GraphQLVariable("userId"), "password": GraphQLVariable("password")], type: .object(ResetUserPassword.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resetUserPassword: ResetUserPassword? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "resetUserPassword": resetUserPassword.flatMap { (value: ResetUserPassword) -> ResultMap in value.resultMap }])
    }

    public var resetUserPassword: ResetUserPassword? {
      get {
        return (resultMap["resetUserPassword"] as? ResultMap).flatMap { ResetUserPassword(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "resetUserPassword")
      }
    }

    public struct ResetUserPassword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class ResolveChargeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ResolveCharge($chargerId: String!, $plugId: String) {
      ResolveCharge(chargerId: $chargerId, plugId: $plugId) {
        __typename
        charger {
          __typename
          id
          templateId
          name
          status
          power
          type
          plugs {
            __typename
            id
            name
            status
            power
            supportedPort
            supportedPorts
            perUnitAcCharge
            perUnitDcCharge
          }
          ocppVersion
          machineId
          supportedPorts
        }
        station {
          __typename
          id
          name
          description
          address {
            __typename
            line1
            line2
            city
            state
            zip
            phone
          }
          position {
            __typename
            type
            coordinates
          }
          status
          flag
          images
          chargers {
            __typename
            id
            templateId
            name
            status
            power
            type
            plugs {
              __typename
              id
              name
              status
              power
              supportedPort
              supportedPorts
              perUnitAcCharge
              perUnitDcCharge
            }
            ocppVersion
            machineId
            supportedPorts
          }
          cpo {
            __typename
            id
            name
            revenuePlan
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            phone
            kind
            perUnitAcCharge
            perUnitDcCharge
          }
          location
          supportedPorts
          perUnitAcCharge
          perUnitDcCharge
        }
        plug {
          __typename
          id
          name
          status
          power
          supportedPort
          supportedPorts
          perUnitAcCharge
          perUnitDcCharge
        }
      }
    }
    """

  public let operationName: String = "ResolveCharge"

  public var chargerId: String
  public var plugId: String?

  public init(chargerId: String, plugId: String? = nil) {
    self.chargerId = chargerId
    self.plugId = plugId
  }

  public var variables: GraphQLMap? {
    return ["chargerId": chargerId, "plugId": plugId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("ResolveCharge", arguments: ["chargerId": GraphQLVariable("chargerId"), "plugId": GraphQLVariable("plugId")], type: .object(ResolveCharge.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(resolveCharge: ResolveCharge? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "ResolveCharge": resolveCharge.flatMap { (value: ResolveCharge) -> ResultMap in value.resultMap }])
    }

    public var resolveCharge: ResolveCharge? {
      get {
        return (resultMap["ResolveCharge"] as? ResultMap).flatMap { ResolveCharge(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "ResolveCharge")
      }
    }

    public struct ResolveCharge: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ResolveCharge"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("charger", type: .object(Charger.selections)),
          GraphQLField("station", type: .object(Station.selections)),
          GraphQLField("plug", type: .object(Plug.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(charger: Charger? = nil, station: Station? = nil, plug: Plug? = nil) {
        self.init(unsafeResultMap: ["__typename": "ResolveCharge", "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var charger: Charger? {
        get {
          return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "charger")
        }
      }

      public var station: Station? {
        get {
          return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "station")
        }
      }

      public var plug: Plug? {
        get {
          return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "plug")
        }
      }

      public struct Charger: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Charger"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("templateId", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ChargerStatus.self)),
            GraphQLField("power", type: .scalar(Double.self)),
            GraphQLField("type", type: .scalar(PowerType.self)),
            GraphQLField("plugs", type: .list(.object(Plug.selections))),
            GraphQLField("ocppVersion", type: .nonNull(.scalar(OCPPVersion.self))),
            GraphQLField("machineId", type: .scalar(String.self)),
            GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, templateId: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil, ocppVersion: OCPPVersion, machineId: String? = nil, supportedPorts: [String?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "templateId": templateId, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, "ocppVersion": ocppVersion, "machineId": machineId, "supportedPorts": supportedPorts])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var templateId: String? {
          get {
            return resultMap["templateId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "templateId")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var status: ChargerStatus? {
          get {
            return resultMap["status"] as? ChargerStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var power: Double? {
          get {
            return resultMap["power"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "power")
          }
        }

        public var type: PowerType? {
          get {
            return resultMap["type"] as? PowerType
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var plugs: [Plug?]? {
          get {
            return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
          }
        }

        public var ocppVersion: OCPPVersion {
          get {
            return resultMap["ocppVersion"]! as! OCPPVersion
          }
          set {
            resultMap.updateValue(newValue, forKey: "ocppVersion")
          }
        }

        public var machineId: String? {
          get {
            return resultMap["machineId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "machineId")
          }
        }

        public var supportedPorts: [String?]? {
          get {
            return resultMap["supportedPorts"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "supportedPorts")
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(PlugStatus.self)),
              GraphQLField("power", type: .scalar(Double.self)),
              GraphQLField("supportedPort", type: .scalar(String.self)),
              GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, power: Double? = nil, supportedPort: String? = nil, supportedPorts: [String?]? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "power": power, "supportedPort": supportedPort, "supportedPorts": supportedPorts, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: PlugStatus? {
            get {
              return resultMap["status"] as? PlugStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var power: Double? {
            get {
              return resultMap["power"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "power")
            }
          }

          public var supportedPort: String? {
            get {
              return resultMap["supportedPort"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPort")
            }
          }

          public var supportedPorts: [String?]? {
            get {
              return resultMap["supportedPorts"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPorts")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }
        }
      }

      public struct Station: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Station"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("address", type: .object(Address.selections)),
            GraphQLField("position", type: .object(Position.selections)),
            GraphQLField("status", type: .scalar(StationStatus.self)),
            GraphQLField("flag", type: .scalar(String.self)),
            GraphQLField("images", type: .list(.scalar(String.self))),
            GraphQLField("chargers", type: .list(.object(Charger.selections))),
            GraphQLField("cpo", type: .object(Cpo.selections)),
            GraphQLField("location", type: .scalar(String.self)),
            GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
            GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, status: StationStatus? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, cpo: Cpo? = nil, location: String? = nil, supportedPorts: [String?]? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "status": status, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "cpo": cpo.flatMap { (value: Cpo) -> ResultMap in value.resultMap }, "location": location, "supportedPorts": supportedPorts, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }

        public var address: Address? {
          get {
            return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "address")
          }
        }

        public var position: Position? {
          get {
            return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "position")
          }
        }

        public var status: StationStatus? {
          get {
            return resultMap["status"] as? StationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var flag: String? {
          get {
            return resultMap["flag"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "flag")
          }
        }

        public var images: [String?]? {
          get {
            return resultMap["images"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "images")
          }
        }

        public var chargers: [Charger?]? {
          get {
            return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
          }
        }

        public var cpo: Cpo? {
          get {
            return (resultMap["cpo"] as? ResultMap).flatMap { Cpo(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "cpo")
          }
        }

        public var location: String? {
          get {
            return resultMap["location"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "location")
          }
        }

        public var supportedPorts: [String?]? {
          get {
            return resultMap["supportedPorts"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "supportedPorts")
          }
        }

        public var perUnitAcCharge: Double? {
          get {
            return resultMap["perUnitAcCharge"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
          }
        }

        public var perUnitDcCharge: Double? {
          get {
            return resultMap["perUnitDcCharge"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
          }
        }

        public struct Address: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Address"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("line1", type: .scalar(String.self)),
              GraphQLField("line2", type: .scalar(String.self)),
              GraphQLField("city", type: .scalar(String.self)),
              GraphQLField("state", type: .scalar(String.self)),
              GraphQLField("zip", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var line1: String? {
            get {
              return resultMap["line1"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "line1")
            }
          }

          public var line2: String? {
            get {
              return resultMap["line2"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "line2")
            }
          }

          public var city: String? {
            get {
              return resultMap["city"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "city")
            }
          }

          public var state: String? {
            get {
              return resultMap["state"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "state")
            }
          }

          public var zip: String? {
            get {
              return resultMap["zip"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "zip")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }
        }

        public struct Position: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Point"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("type", type: .scalar(String.self)),
              GraphQLField("coordinates", type: .list(.scalar(Double.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(type: String? = nil, coordinates: [Double?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var type: String? {
            get {
              return resultMap["type"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var coordinates: [Double?]? {
            get {
              return resultMap["coordinates"] as? [Double?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "coordinates")
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("templateId", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(ChargerStatus.self)),
              GraphQLField("power", type: .scalar(Double.self)),
              GraphQLField("type", type: .scalar(PowerType.self)),
              GraphQLField("plugs", type: .list(.object(Plug.selections))),
              GraphQLField("ocppVersion", type: .nonNull(.scalar(OCPPVersion.self))),
              GraphQLField("machineId", type: .scalar(String.self)),
              GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, templateId: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil, ocppVersion: OCPPVersion, machineId: String? = nil, supportedPorts: [String?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "templateId": templateId, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, "ocppVersion": ocppVersion, "machineId": machineId, "supportedPorts": supportedPorts])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var templateId: String? {
            get {
              return resultMap["templateId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "templateId")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var status: ChargerStatus? {
            get {
              return resultMap["status"] as? ChargerStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var power: Double? {
            get {
              return resultMap["power"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "power")
            }
          }

          public var type: PowerType? {
            get {
              return resultMap["type"] as? PowerType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var plugs: [Plug?]? {
            get {
              return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
            }
          }

          public var ocppVersion: OCPPVersion {
            get {
              return resultMap["ocppVersion"]! as! OCPPVersion
            }
            set {
              resultMap.updateValue(newValue, forKey: "ocppVersion")
            }
          }

          public var machineId: String? {
            get {
              return resultMap["machineId"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "machineId")
            }
          }

          public var supportedPorts: [String?]? {
            get {
              return resultMap["supportedPorts"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPorts")
            }
          }

          public struct Plug: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Plug"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(PlugStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("supportedPort", type: .scalar(String.self)),
                GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
                GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, power: Double? = nil, supportedPort: String? = nil, supportedPorts: [String?]? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
              self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "power": power, "supportedPort": supportedPort, "supportedPorts": supportedPorts, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: PlugStatus? {
              get {
                return resultMap["status"] as? PlugStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var supportedPort: String? {
              get {
                return resultMap["supportedPort"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "supportedPort")
              }
            }

            public var supportedPorts: [String?]? {
              get {
                return resultMap["supportedPorts"] as? [String?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "supportedPorts")
              }
            }

            public var perUnitAcCharge: Double? {
              get {
                return resultMap["perUnitAcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
              }
            }

            public var perUnitDcCharge: Double? {
              get {
                return resultMap["perUnitDcCharge"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
              }
            }
          }
        }

        public struct Cpo: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["CPO"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("revenuePlan", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("kind", type: .scalar(CPOKind.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, revenuePlan: String? = nil, address: Address? = nil, phone: String? = nil, kind: CPOKind? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "CPO", "id": id, "name": name, "revenuePlan": revenuePlan, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "phone": phone, "kind": kind, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var revenuePlan: String? {
            get {
              return resultMap["revenuePlan"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "revenuePlan")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var kind: CPOKind? {
            get {
              return resultMap["kind"] as? CPOKind
            }
            set {
              resultMap.updateValue(newValue, forKey: "kind")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }
        }
      }

      public struct Plug: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Plug"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(PlugStatus.self)),
            GraphQLField("power", type: .scalar(Double.self)),
            GraphQLField("supportedPort", type: .scalar(String.self)),
            GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
            GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, power: Double? = nil, supportedPort: String? = nil, supportedPorts: [String?]? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "power": power, "supportedPort": supportedPort, "supportedPorts": supportedPorts, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var status: PlugStatus? {
          get {
            return resultMap["status"] as? PlugStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var power: Double? {
          get {
            return resultMap["power"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "power")
          }
        }

        public var supportedPort: String? {
          get {
            return resultMap["supportedPort"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "supportedPort")
          }
        }

        public var supportedPorts: [String?]? {
          get {
            return resultMap["supportedPorts"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "supportedPorts")
          }
        }

        public var perUnitAcCharge: Double? {
          get {
            return resultMap["perUnitAcCharge"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
          }
        }

        public var perUnitDcCharge: Double? {
          get {
            return resultMap["perUnitDcCharge"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
          }
        }
      }
    }
  }
}

public final class SearchQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Search($pagination: IPagination, $filter: ISearchFilter, $sort: SearchSortOption) {
      Search(pagination: $pagination, filter: $filter, sort: $sort) {
        __typename
        docs {
          __typename
          station {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            amenities {
              __typename
              name
              value
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            flag
            images
            chargers {
              __typename
              id
              name
              status
              power
              type
              plugs {
                __typename
                id
                name
                status
                supportedPort
                power
                perUnitAcCharge
                perUnitDcCharge
              }
            }
            cpo {
              __typename
              id
              name
              revenuePlan
              address {
                __typename
                line1
                line2
                city
                state
                zip
                phone
              }
            }
            location
          }
          calculatedDistance
        }
        pagination {
          __typename
          totalDocs
          limit
          hasPrevPage
          hasNextPage
          page
          totalPages
          prevPage
          nextPage
        }
      }
    }
    """

  public let operationName: String = "Search"

  public var pagination: IPagination?
  public var filter: ISearchFilter?
  public var sort: SearchSortOption?

  public init(pagination: IPagination? = nil, filter: ISearchFilter? = nil, sort: SearchSortOption? = nil) {
    self.pagination = pagination
    self.filter = filter
    self.sort = sort
  }

  public var variables: GraphQLMap? {
    return ["pagination": pagination, "filter": filter, "sort": sort]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("Search", arguments: ["pagination": GraphQLVariable("pagination"), "filter": GraphQLVariable("filter"), "sort": GraphQLVariable("sort")], type: .object(Search.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(search: Search? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Search": search.flatMap { (value: Search) -> ResultMap in value.resultMap }])
    }

    public var search: Search? {
      get {
        return (resultMap["Search"] as? ResultMap).flatMap { Search(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Search")
      }
    }

    public struct Search: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SearchResults"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("docs", type: .list(.object(Doc.selections))),
          GraphQLField("pagination", type: .object(Pagination.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(docs: [Doc?]? = nil, pagination: Pagination? = nil) {
        self.init(unsafeResultMap: ["__typename": "SearchResults", "docs": docs.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, "pagination": pagination.flatMap { (value: Pagination) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var docs: [Doc?]? {
        get {
          return (resultMap["docs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Doc?] in value.map { (value: ResultMap?) -> Doc? in value.flatMap { (value: ResultMap) -> Doc in Doc(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, forKey: "docs")
        }
      }

      public var pagination: Pagination? {
        get {
          return (resultMap["pagination"] as? ResultMap).flatMap { Pagination(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "pagination")
        }
      }

      public struct Doc: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SearchResult"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("calculatedDistance", type: .scalar(Double.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(station: Station? = nil, calculatedDistance: Double? = nil) {
          self.init(unsafeResultMap: ["__typename": "SearchResult", "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "calculatedDistance": calculatedDistance])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var calculatedDistance: Double? {
          get {
            return resultMap["calculatedDistance"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "calculatedDistance")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("amenities", type: .list(.object(Amenity.selections))),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("chargers", type: .list(.object(Charger.selections))),
              GraphQLField("cpo", type: .object(Cpo.selections)),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, amenities: [Amenity?]? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, cpo: Cpo? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "amenities": amenities.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "cpo": cpo.flatMap { (value: Cpo) -> ResultMap in value.resultMap }, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var amenities: [Amenity?]? {
            get {
              return (resultMap["amenities"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Amenity?] in value.map { (value: ResultMap?) -> Amenity? in value.flatMap { (value: ResultMap) -> Amenity in Amenity(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, forKey: "amenities")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var chargers: [Charger?]? {
            get {
              return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
            }
          }

          public var cpo: Cpo? {
            get {
              return (resultMap["cpo"] as? ResultMap).flatMap { Cpo(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "cpo")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }

          public struct Amenity: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Amenities"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("value", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(name: String? = nil, value: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Amenities", "name": name, "value": value])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var value: String? {
              get {
                return resultMap["value"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "value")
              }
            }
          }

          public struct Charger: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Charger"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(ChargerStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("type", type: .scalar(PowerType.self)),
                GraphQLField("plugs", type: .list(.object(Plug.selections))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: ChargerStatus? {
              get {
                return resultMap["status"] as? ChargerStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var type: PowerType? {
              get {
                return resultMap["type"] as? PowerType
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var plugs: [Plug?]? {
              get {
                return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
              }
              set {
                resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
              }
            }

            public struct Plug: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Plug"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(String.self)),
                  GraphQLField("name", type: .scalar(String.self)),
                  GraphQLField("status", type: .scalar(PlugStatus.self)),
                  GraphQLField("supportedPort", type: .scalar(String.self)),
                  GraphQLField("power", type: .scalar(Double.self)),
                  GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                  GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
                self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: String? {
                get {
                  return resultMap["id"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var name: String? {
                get {
                  return resultMap["name"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "name")
                }
              }

              public var status: PlugStatus? {
                get {
                  return resultMap["status"] as? PlugStatus
                }
                set {
                  resultMap.updateValue(newValue, forKey: "status")
                }
              }

              public var supportedPort: String? {
                get {
                  return resultMap["supportedPort"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "supportedPort")
                }
              }

              public var power: Double? {
                get {
                  return resultMap["power"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "power")
                }
              }

              public var perUnitAcCharge: Double? {
                get {
                  return resultMap["perUnitAcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
                }
              }

              public var perUnitDcCharge: Double? {
                get {
                  return resultMap["perUnitDcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
                }
              }
            }
          }

          public struct Cpo: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["CPO"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("revenuePlan", type: .scalar(String.self)),
                GraphQLField("address", type: .object(Address.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, revenuePlan: String? = nil, address: Address? = nil) {
              self.init(unsafeResultMap: ["__typename": "CPO", "id": id, "name": name, "revenuePlan": revenuePlan, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var revenuePlan: String? {
              get {
                return resultMap["revenuePlan"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "revenuePlan")
              }
            }

            public var address: Address? {
              get {
                return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "address")
              }
            }

            public struct Address: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Address"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("line1", type: .scalar(String.self)),
                  GraphQLField("line2", type: .scalar(String.self)),
                  GraphQLField("city", type: .scalar(String.self)),
                  GraphQLField("state", type: .scalar(String.self)),
                  GraphQLField("zip", type: .scalar(String.self)),
                  GraphQLField("phone", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var line1: String? {
                get {
                  return resultMap["line1"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line1")
                }
              }

              public var line2: String? {
                get {
                  return resultMap["line2"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line2")
                }
              }

              public var city: String? {
                get {
                  return resultMap["city"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "city")
                }
              }

              public var state: String? {
                get {
                  return resultMap["state"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "state")
                }
              }

              public var zip: String? {
                get {
                  return resultMap["zip"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "zip")
                }
              }

              public var phone: String? {
                get {
                  return resultMap["phone"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "phone")
                }
              }
            }
          }
        }
      }

      public struct Pagination: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Pagination"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("totalDocs", type: .scalar(Int.self)),
            GraphQLField("limit", type: .scalar(Int.self)),
            GraphQLField("hasPrevPage", type: .scalar(Int.self)),
            GraphQLField("hasNextPage", type: .scalar(Int.self)),
            GraphQLField("page", type: .scalar(Int.self)),
            GraphQLField("totalPages", type: .scalar(Int.self)),
            GraphQLField("prevPage", type: .scalar(Int.self)),
            GraphQLField("nextPage", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDocs: Int? = nil, limit: Int? = nil, hasPrevPage: Int? = nil, hasNextPage: Int? = nil, page: Int? = nil, totalPages: Int? = nil, prevPage: Int? = nil, nextPage: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "Pagination", "totalDocs": totalDocs, "limit": limit, "hasPrevPage": hasPrevPage, "hasNextPage": hasNextPage, "page": page, "totalPages": totalPages, "prevPage": prevPage, "nextPage": nextPage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var totalDocs: Int? {
          get {
            return resultMap["totalDocs"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDocs")
          }
        }

        public var limit: Int? {
          get {
            return resultMap["limit"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "limit")
          }
        }

        public var hasPrevPage: Int? {
          get {
            return resultMap["hasPrevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasPrevPage")
          }
        }

        public var hasNextPage: Int? {
          get {
            return resultMap["hasNextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasNextPage")
          }
        }

        public var page: Int? {
          get {
            return resultMap["page"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "page")
          }
        }

        public var totalPages: Int? {
          get {
            return resultMap["totalPages"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalPages")
          }
        }

        public var prevPage: Int? {
          get {
            return resultMap["prevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "prevPage")
          }
        }

        public var nextPage: Int? {
          get {
            return resultMap["nextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "nextPage")
          }
        }
      }
    }
  }
}

public final class SocialLoginUpdateUserDetailsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SocialLoginUpdateUserDetails($token: String!, $input: IRegisterUser!) {
      socialLoginUpdateUserDetails(token: $token, input: $input) {
        __typename
        success
        error
        message
        data {
          __typename
          name
          id
        }
      }
    }
    """

  public let operationName: String = "SocialLoginUpdateUserDetails"

  public var token: String
  public var input: IRegisterUser

  public init(token: String, input: IRegisterUser) {
    self.token = token
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["token": token, "input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("socialLoginUpdateUserDetails", arguments: ["token": GraphQLVariable("token"), "input": GraphQLVariable("input")], type: .object(SocialLoginUpdateUserDetail.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(socialLoginUpdateUserDetails: SocialLoginUpdateUserDetail? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "socialLoginUpdateUserDetails": socialLoginUpdateUserDetails.flatMap { (value: SocialLoginUpdateUserDetail) -> ResultMap in value.resultMap }])
    }

    public var socialLoginUpdateUserDetails: SocialLoginUpdateUserDetail? {
      get {
        return (resultMap["socialLoginUpdateUserDetails"] as? ResultMap).flatMap { SocialLoginUpdateUserDetail(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "socialLoginUpdateUserDetails")
      }
    }

    public struct SocialLoginUpdateUserDetail: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserRegistrationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("error", type: .scalar(String.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, error: String? = nil, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserRegistrationResponse", "success": success, "error": error, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserRegistrationResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("id", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String? = nil, id: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserRegistrationResponseData", "name": name, "id": id])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }
      }
    }
  }
}

public final class SocialLoginWithGoogleTokenMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation SocialLoginWithGoogleToken($token: String!) {
      socialLoginWithGoogleToken(token: $token) {
        __typename
        success
        error
        message
        data {
          __typename
          userExists
          token
          user {
            __typename
            _id
            id
            name
            role
            email
            phone
            status
            preference {
              __typename
              plug
              stationIds
            }
          }
          registration {
            __typename
            name
            id
          }
        }
      }
    }
    """

  public let operationName: String = "SocialLoginWithGoogleToken"

  public var token: String

  public init(token: String) {
    self.token = token
  }

  public var variables: GraphQLMap? {
    return ["token": token]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("socialLoginWithGoogleToken", arguments: ["token": GraphQLVariable("token")], type: .object(SocialLoginWithGoogleToken.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(socialLoginWithGoogleToken: SocialLoginWithGoogleToken? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "socialLoginWithGoogleToken": socialLoginWithGoogleToken.flatMap { (value: SocialLoginWithGoogleToken) -> ResultMap in value.resultMap }])
    }

    public var socialLoginWithGoogleToken: SocialLoginWithGoogleToken? {
      get {
        return (resultMap["socialLoginWithGoogleToken"] as? ResultMap).flatMap { SocialLoginWithGoogleToken(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "socialLoginWithGoogleToken")
      }
    }

    public struct SocialLoginWithGoogleToken: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SocialLoginGoogleResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("error", type: .scalar(String.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, error: String? = nil, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "SocialLoginGoogleResponse", "success": success, "error": error, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SocialLoginGoogleResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("userExists", type: .scalar(Bool.self)),
            GraphQLField("token", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
            GraphQLField("registration", type: .object(Registration.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userExists: Bool? = nil, token: String? = nil, user: User? = nil, registration: Registration? = nil) {
          self.init(unsafeResultMap: ["__typename": "SocialLoginGoogleResponseData", "userExists": userExists, "token": token, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }, "registration": registration.flatMap { (value: Registration) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userExists: Bool? {
          get {
            return resultMap["userExists"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "userExists")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public var registration: Registration? {
          get {
            return (resultMap["registration"] as? ResultMap).flatMap { Registration(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "registration")
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("role", type: .scalar(UserRole.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(UserStatus.self)),
              GraphQLField("preference", type: .object(Preference.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(_id: String? = nil, id: String? = nil, name: String? = nil, role: UserRole? = nil, email: String? = nil, phone: String? = nil, status: UserStatus? = nil, preference: Preference? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "_id": _id, "id": id, "name": name, "role": role, "email": email, "phone": phone, "status": status, "preference": preference.flatMap { (value: Preference) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var _id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var role: UserRole? {
            get {
              return resultMap["role"] as? UserRole
            }
            set {
              resultMap.updateValue(newValue, forKey: "role")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var status: UserStatus? {
            get {
              return resultMap["status"] as? UserStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var preference: Preference? {
            get {
              return (resultMap["preference"] as? ResultMap).flatMap { Preference(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "preference")
            }
          }

          public struct Preference: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["UserPreference"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("plug", type: .scalar(String.self)),
                GraphQLField("stationIds", type: .list(.scalar(String.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(plug: String? = nil, stationIds: [String?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "UserPreference", "plug": plug, "stationIds": stationIds])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var plug: String? {
              get {
                return resultMap["plug"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "plug")
              }
            }

            public var stationIds: [String?]? {
              get {
                return resultMap["stationIds"] as? [String?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "stationIds")
              }
            }
          }
        }

        public struct Registration: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["UserRegistrationResponseData"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("id", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(name: String? = nil, id: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "UserRegistrationResponseData", "name": name, "id": id])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }
        }
      }
    }
  }
}

public final class StartChargingMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation StartCharging($reservationId: String!) {
      startCharging(reservationId: $reservationId) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
          }
          charger {
            __typename
            id
            name
          }
          plug {
            __typename
            id
            name
          }
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          unitsConsumed
          plugActivationCode
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            id
            name
          }
        }
        error
      }
    }
    """

  public let operationName: String = "StartCharging"

  public var reservationId: String

  public init(reservationId: String) {
    self.reservationId = reservationId
  }

  public var variables: GraphQLMap? {
    return ["reservationId": reservationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("startCharging", arguments: ["reservationId": GraphQLVariable("reservationId")], type: .object(StartCharging.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(startCharging: StartCharging? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "startCharging": startCharging.flatMap { (value: StartCharging) -> ResultMap in value.resultMap }])
    }

    public var startCharging: StartCharging? {
      get {
        return (resultMap["startCharging"] as? ResultMap).flatMap { StartCharging(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "startCharging")
      }
    }

    public struct StartCharging: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("plugActivationCode", type: .scalar(String.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, unitsConsumed: Double? = nil, plugActivationCode: String? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "unitsConsumed": unitsConsumed, "plugActivationCode": plugActivationCode, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var plugActivationCode: String? {
          get {
            return resultMap["plugActivationCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugActivationCode")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class StopChargingMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation StopCharging($reservationId: String!) {
      stopCharging(reservationId: $reservationId) {
        __typename
        success
        message
        data {
          __typename
          id
          station {
            __typename
            id
            name
          }
          charger {
            __typename
            id
            name
          }
          plug {
            __typename
            id
            name
          }
          startTime
          endTime
          status
          plugType
          chargeType
          powerRating
          unitsConsumed
          plugActivationCode
          pricePerUnit
          amount
          createdAt
          user {
            __typename
            id
            name
          }
        }
        error
      }
    }
    """

  public let operationName: String = "StopCharging"

  public var reservationId: String

  public init(reservationId: String) {
    self.reservationId = reservationId
  }

  public var variables: GraphQLMap? {
    return ["reservationId": reservationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("stopCharging", arguments: ["reservationId": GraphQLVariable("reservationId")], type: .object(StopCharging.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(stopCharging: StopCharging? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "stopCharging": stopCharging.flatMap { (value: StopCharging) -> ResultMap in value.resultMap }])
    }

    public var stopCharging: StopCharging? {
      get {
        return (resultMap["stopCharging"] as? ResultMap).flatMap { StopCharging(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "stopCharging")
      }
    }

    public struct StopCharging: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ReservationResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ReservationResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Reservation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("station", type: .object(Station.selections)),
            GraphQLField("charger", type: .object(Charger.selections)),
            GraphQLField("plug", type: .object(Plug.selections)),
            GraphQLField("startTime", type: .scalar(String.self)),
            GraphQLField("endTime", type: .scalar(String.self)),
            GraphQLField("status", type: .scalar(ReservationStatus.self)),
            GraphQLField("plugType", type: .scalar(String.self)),
            GraphQLField("chargeType", type: .scalar(String.self)),
            GraphQLField("powerRating", type: .scalar(Double.self)),
            GraphQLField("unitsConsumed", type: .scalar(Double.self)),
            GraphQLField("plugActivationCode", type: .scalar(String.self)),
            GraphQLField("pricePerUnit", type: .scalar(Double.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, station: Station? = nil, charger: Charger? = nil, plug: Plug? = nil, startTime: String? = nil, endTime: String? = nil, status: ReservationStatus? = nil, plugType: String? = nil, chargeType: String? = nil, powerRating: Double? = nil, unitsConsumed: Double? = nil, plugActivationCode: String? = nil, pricePerUnit: Double? = nil, amount: Double? = nil, createdAt: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "Reservation", "id": id, "station": station.flatMap { (value: Station) -> ResultMap in value.resultMap }, "charger": charger.flatMap { (value: Charger) -> ResultMap in value.resultMap }, "plug": plug.flatMap { (value: Plug) -> ResultMap in value.resultMap }, "startTime": startTime, "endTime": endTime, "status": status, "plugType": plugType, "chargeType": chargeType, "powerRating": powerRating, "unitsConsumed": unitsConsumed, "plugActivationCode": plugActivationCode, "pricePerUnit": pricePerUnit, "amount": amount, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var station: Station? {
          get {
            return (resultMap["station"] as? ResultMap).flatMap { Station(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "station")
          }
        }

        public var charger: Charger? {
          get {
            return (resultMap["charger"] as? ResultMap).flatMap { Charger(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "charger")
          }
        }

        public var plug: Plug? {
          get {
            return (resultMap["plug"] as? ResultMap).flatMap { Plug(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "plug")
          }
        }

        public var startTime: String? {
          get {
            return resultMap["startTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "startTime")
          }
        }

        public var endTime: String? {
          get {
            return resultMap["endTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endTime")
          }
        }

        public var status: ReservationStatus? {
          get {
            return resultMap["status"] as? ReservationStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var plugType: String? {
          get {
            return resultMap["plugType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugType")
          }
        }

        public var chargeType: String? {
          get {
            return resultMap["chargeType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "chargeType")
          }
        }

        public var powerRating: Double? {
          get {
            return resultMap["powerRating"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerRating")
          }
        }

        public var unitsConsumed: Double? {
          get {
            return resultMap["unitsConsumed"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "unitsConsumed")
          }
        }

        public var plugActivationCode: String? {
          get {
            return resultMap["plugActivationCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plugActivationCode")
          }
        }

        public var pricePerUnit: Double? {
          get {
            return resultMap["pricePerUnit"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "pricePerUnit")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Charger: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Charger"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct Plug: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Plug"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class TopUpWalletMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation TopUpWallet($userId: String, $amount: Int) {
      topUpWallet(userId: $userId, amount: $amount) {
        __typename
        success
        message
        data {
          __typename
          token
          transactionId
        }
        error
      }
    }
    """

  public let operationName: String = "TopUpWallet"

  public var userId: String?
  public var amount: Int?

  public init(userId: String? = nil, amount: Int? = nil) {
    self.userId = userId
    self.amount = amount
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "amount": amount]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("topUpWallet", arguments: ["userId": GraphQLVariable("userId"), "amount": GraphQLVariable("amount")], type: .object(TopUpWallet.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(topUpWallet: TopUpWallet? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "topUpWallet": topUpWallet.flatMap { (value: TopUpWallet) -> ResultMap in value.resultMap }])
    }

    public var topUpWallet: TopUpWallet? {
      get {
        return (resultMap["topUpWallet"] as? ResultMap).flatMap { TopUpWallet(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "topUpWallet")
      }
    }

    public struct TopUpWallet: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["TopUpWalletResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "TopUpWalletResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["TopUpWalletData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .scalar(String.self)),
            GraphQLField("transactionId", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String? = nil, transactionId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "TopUpWalletData", "token": token, "transactionId": transactionId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }

        public var transactionId: String? {
          get {
            return resultMap["transactionId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "transactionId")
          }
        }
      }
    }
  }
}

public final class UpdateUserPreferenceMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation UpdateUserPreference($userId: String!, $input: IUserPreference!) {
      updateUserPreference(userId: $userId, input: $input) {
        __typename
        success
        message
        data
        error
      }
    }
    """

  public let operationName: String = "UpdateUserPreference"

  public var userId: String
  public var input: IUserPreference

  public init(userId: String, input: IUserPreference) {
    self.userId = userId
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("updateUserPreference", arguments: ["userId": GraphQLVariable("userId"), "input": GraphQLVariable("input")], type: .object(UpdateUserPreference.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateUserPreference: UpdateUserPreference? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "updateUserPreference": updateUserPreference.flatMap { (value: UpdateUserPreference) -> ResultMap in value.resultMap }])
    }

    public var updateUserPreference: UpdateUserPreference? {
      get {
        return (resultMap["updateUserPreference"] as? ResultMap).flatMap { UpdateUserPreference(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateUserPreference")
      }
    }

    public struct UpdateUserPreference: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .scalar(String.self)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: String? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainResponse", "success": success, "message": message, "data": data, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: String? {
        get {
          return resultMap["data"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }
    }
  }
}

public final class UserQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query User($userId: String!) {
      User(userId: $userId) {
        __typename
        _id
        id
        name
        role
        email
        phone
        status
        preference {
          __typename
          plug
          stationIds
          stations {
            __typename
            id
            name
            description
            address {
              __typename
              line1
              line2
              city
              state
              zip
              phone
            }
            position {
              __typename
              type
              coordinates
            }
            amenities {
              __typename
              name
              value
            }
            status
            perUnitAcCharge
            perUnitDcCharge
            flag
            images
            chargers {
              __typename
              id
              name
              status
              power
              type
              plugs {
                __typename
                id
                name
                status
                supportedPort
                power
                perUnitAcCharge
                perUnitDcCharge
              }
            }
            cpo {
              __typename
              id
              name
              revenuePlan
              address {
                __typename
                line1
                line2
                city
                state
                zip
                phone
              }
            }
            location
          }
        }
      }
    }
    """

  public let operationName: String = "User"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("User", arguments: ["userId": GraphQLVariable("userId")], type: .object(User.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(user: User? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "User": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
    }

    public var user: User? {
      get {
        return (resultMap["User"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "User")
      }
    }

    public struct User: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["User"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("_id", type: .scalar(String.self)),
          GraphQLField("id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("role", type: .scalar(UserRole.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("phone", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(UserStatus.self)),
          GraphQLField("preference", type: .object(Preference.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(_id: String? = nil, id: String? = nil, name: String? = nil, role: UserRole? = nil, email: String? = nil, phone: String? = nil, status: UserStatus? = nil, preference: Preference? = nil) {
        self.init(unsafeResultMap: ["__typename": "User", "_id": _id, "id": id, "name": name, "role": role, "email": email, "phone": phone, "status": status, "preference": preference.flatMap { (value: Preference) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var _id: String? {
        get {
          return resultMap["_id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "_id")
        }
      }

      public var id: String? {
        get {
          return resultMap["id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var role: UserRole? {
        get {
          return resultMap["role"] as? UserRole
        }
        set {
          resultMap.updateValue(newValue, forKey: "role")
        }
      }

      public var email: String? {
        get {
          return resultMap["email"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var phone: String? {
        get {
          return resultMap["phone"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "phone")
        }
      }

      public var status: UserStatus? {
        get {
          return resultMap["status"] as? UserStatus
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var preference: Preference? {
        get {
          return (resultMap["preference"] as? ResultMap).flatMap { Preference(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "preference")
        }
      }

      public struct Preference: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserPreference"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("plug", type: .scalar(String.self)),
            GraphQLField("stationIds", type: .list(.scalar(String.self))),
            GraphQLField("stations", type: .list(.object(Station.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(plug: String? = nil, stationIds: [String?]? = nil, stations: [Station?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserPreference", "plug": plug, "stationIds": stationIds, "stations": stations.flatMap { (value: [Station?]) -> [ResultMap?] in value.map { (value: Station?) -> ResultMap? in value.flatMap { (value: Station) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var plug: String? {
          get {
            return resultMap["plug"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "plug")
          }
        }

        public var stationIds: [String?]? {
          get {
            return resultMap["stationIds"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "stationIds")
          }
        }

        public var stations: [Station?]? {
          get {
            return (resultMap["stations"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Station?] in value.map { (value: ResultMap?) -> Station? in value.flatMap { (value: ResultMap) -> Station in Station(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Station?]) -> [ResultMap?] in value.map { (value: Station?) -> ResultMap? in value.flatMap { (value: Station) -> ResultMap in value.resultMap } } }, forKey: "stations")
          }
        }

        public struct Station: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Station"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("description", type: .scalar(String.self)),
              GraphQLField("address", type: .object(Address.selections)),
              GraphQLField("position", type: .object(Position.selections)),
              GraphQLField("amenities", type: .list(.object(Amenity.selections))),
              GraphQLField("status", type: .scalar(StationStatus.self)),
              GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
              GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
              GraphQLField("flag", type: .scalar(String.self)),
              GraphQLField("images", type: .list(.scalar(String.self))),
              GraphQLField("chargers", type: .list(.object(Charger.selections))),
              GraphQLField("cpo", type: .object(Cpo.selections)),
              GraphQLField("location", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, description: String? = nil, address: Address? = nil, position: Position? = nil, amenities: [Amenity?]? = nil, status: StationStatus? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil, flag: String? = nil, images: [String?]? = nil, chargers: [Charger?]? = nil, cpo: Cpo? = nil, location: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Station", "id": id, "name": name, "description": description, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }, "position": position.flatMap { (value: Position) -> ResultMap in value.resultMap }, "amenities": amenities.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, "status": status, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge, "flag": flag, "images": images, "chargers": chargers.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, "cpo": cpo.flatMap { (value: Cpo) -> ResultMap in value.resultMap }, "location": location])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var description: String? {
            get {
              return resultMap["description"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "description")
            }
          }

          public var address: Address? {
            get {
              return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "address")
            }
          }

          public var position: Position? {
            get {
              return (resultMap["position"] as? ResultMap).flatMap { Position(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "position")
            }
          }

          public var amenities: [Amenity?]? {
            get {
              return (resultMap["amenities"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Amenity?] in value.map { (value: ResultMap?) -> Amenity? in value.flatMap { (value: ResultMap) -> Amenity in Amenity(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Amenity?]) -> [ResultMap?] in value.map { (value: Amenity?) -> ResultMap? in value.flatMap { (value: Amenity) -> ResultMap in value.resultMap } } }, forKey: "amenities")
            }
          }

          public var status: StationStatus? {
            get {
              return resultMap["status"] as? StationStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var perUnitAcCharge: Double? {
            get {
              return resultMap["perUnitAcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
            }
          }

          public var perUnitDcCharge: Double? {
            get {
              return resultMap["perUnitDcCharge"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
            }
          }

          public var flag: String? {
            get {
              return resultMap["flag"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "flag")
            }
          }

          public var images: [String?]? {
            get {
              return resultMap["images"] as? [String?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "images")
            }
          }

          public var chargers: [Charger?]? {
            get {
              return (resultMap["chargers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Charger?] in value.map { (value: ResultMap?) -> Charger? in value.flatMap { (value: ResultMap) -> Charger in Charger(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Charger?]) -> [ResultMap?] in value.map { (value: Charger?) -> ResultMap? in value.flatMap { (value: Charger) -> ResultMap in value.resultMap } } }, forKey: "chargers")
            }
          }

          public var cpo: Cpo? {
            get {
              return (resultMap["cpo"] as? ResultMap).flatMap { Cpo(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "cpo")
            }
          }

          public var location: String? {
            get {
              return resultMap["location"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "location")
            }
          }

          public struct Address: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Address"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("line1", type: .scalar(String.self)),
                GraphQLField("line2", type: .scalar(String.self)),
                GraphQLField("city", type: .scalar(String.self)),
                GraphQLField("state", type: .scalar(String.self)),
                GraphQLField("zip", type: .scalar(String.self)),
                GraphQLField("phone", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var line1: String? {
              get {
                return resultMap["line1"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line1")
              }
            }

            public var line2: String? {
              get {
                return resultMap["line2"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "line2")
              }
            }

            public var city: String? {
              get {
                return resultMap["city"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "city")
              }
            }

            public var state: String? {
              get {
                return resultMap["state"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "state")
              }
            }

            public var zip: String? {
              get {
                return resultMap["zip"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "zip")
              }
            }

            public var phone: String? {
              get {
                return resultMap["phone"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "phone")
              }
            }
          }

          public struct Position: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Point"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("type", type: .scalar(String.self)),
                GraphQLField("coordinates", type: .list(.scalar(Double.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(type: String? = nil, coordinates: [Double?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Point", "type": type, "coordinates": coordinates])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var type: String? {
              get {
                return resultMap["type"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var coordinates: [Double?]? {
              get {
                return resultMap["coordinates"] as? [Double?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "coordinates")
              }
            }
          }

          public struct Amenity: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Amenities"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("value", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(name: String? = nil, value: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Amenities", "name": name, "value": value])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var value: String? {
              get {
                return resultMap["value"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "value")
              }
            }
          }

          public struct Charger: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Charger"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("status", type: .scalar(ChargerStatus.self)),
                GraphQLField("power", type: .scalar(Double.self)),
                GraphQLField("type", type: .scalar(PowerType.self)),
                GraphQLField("plugs", type: .list(.object(Plug.selections))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, status: ChargerStatus? = nil, power: Double? = nil, type: PowerType? = nil, plugs: [Plug?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "Charger", "id": id, "name": name, "status": status, "power": power, "type": type, "plugs": plugs.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var status: ChargerStatus? {
              get {
                return resultMap["status"] as? ChargerStatus
              }
              set {
                resultMap.updateValue(newValue, forKey: "status")
              }
            }

            public var power: Double? {
              get {
                return resultMap["power"] as? Double
              }
              set {
                resultMap.updateValue(newValue, forKey: "power")
              }
            }

            public var type: PowerType? {
              get {
                return resultMap["type"] as? PowerType
              }
              set {
                resultMap.updateValue(newValue, forKey: "type")
              }
            }

            public var plugs: [Plug?]? {
              get {
                return (resultMap["plugs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Plug?] in value.map { (value: ResultMap?) -> Plug? in value.flatMap { (value: ResultMap) -> Plug in Plug(unsafeResultMap: value) } } }
              }
              set {
                resultMap.updateValue(newValue.flatMap { (value: [Plug?]) -> [ResultMap?] in value.map { (value: Plug?) -> ResultMap? in value.flatMap { (value: Plug) -> ResultMap in value.resultMap } } }, forKey: "plugs")
              }
            }

            public struct Plug: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Plug"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(String.self)),
                  GraphQLField("name", type: .scalar(String.self)),
                  GraphQLField("status", type: .scalar(PlugStatus.self)),
                  GraphQLField("supportedPort", type: .scalar(String.self)),
                  GraphQLField("power", type: .scalar(Double.self)),
                  GraphQLField("perUnitAcCharge", type: .scalar(Double.self)),
                  GraphQLField("perUnitDcCharge", type: .scalar(Double.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: String? = nil, name: String? = nil, status: PlugStatus? = nil, supportedPort: String? = nil, power: Double? = nil, perUnitAcCharge: Double? = nil, perUnitDcCharge: Double? = nil) {
                self.init(unsafeResultMap: ["__typename": "Plug", "id": id, "name": name, "status": status, "supportedPort": supportedPort, "power": power, "perUnitAcCharge": perUnitAcCharge, "perUnitDcCharge": perUnitDcCharge])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: String? {
                get {
                  return resultMap["id"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var name: String? {
                get {
                  return resultMap["name"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "name")
                }
              }

              public var status: PlugStatus? {
                get {
                  return resultMap["status"] as? PlugStatus
                }
                set {
                  resultMap.updateValue(newValue, forKey: "status")
                }
              }

              public var supportedPort: String? {
                get {
                  return resultMap["supportedPort"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "supportedPort")
                }
              }

              public var power: Double? {
                get {
                  return resultMap["power"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "power")
                }
              }

              public var perUnitAcCharge: Double? {
                get {
                  return resultMap["perUnitAcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitAcCharge")
                }
              }

              public var perUnitDcCharge: Double? {
                get {
                  return resultMap["perUnitDcCharge"] as? Double
                }
                set {
                  resultMap.updateValue(newValue, forKey: "perUnitDcCharge")
                }
              }
            }
          }

          public struct Cpo: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["CPO"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("revenuePlan", type: .scalar(String.self)),
                GraphQLField("address", type: .object(Address.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: String? = nil, name: String? = nil, revenuePlan: String? = nil, address: Address? = nil) {
              self.init(unsafeResultMap: ["__typename": "CPO", "id": id, "name": name, "revenuePlan": revenuePlan, "address": address.flatMap { (value: Address) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: String? {
              get {
                return resultMap["id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var revenuePlan: String? {
              get {
                return resultMap["revenuePlan"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "revenuePlan")
              }
            }

            public var address: Address? {
              get {
                return (resultMap["address"] as? ResultMap).flatMap { Address(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "address")
              }
            }

            public struct Address: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Address"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("line1", type: .scalar(String.self)),
                  GraphQLField("line2", type: .scalar(String.self)),
                  GraphQLField("city", type: .scalar(String.self)),
                  GraphQLField("state", type: .scalar(String.self)),
                  GraphQLField("zip", type: .scalar(String.self)),
                  GraphQLField("phone", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(line1: String? = nil, line2: String? = nil, city: String? = nil, state: String? = nil, zip: String? = nil, phone: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Address", "line1": line1, "line2": line2, "city": city, "state": state, "zip": zip, "phone": phone])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var line1: String? {
                get {
                  return resultMap["line1"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line1")
                }
              }

              public var line2: String? {
                get {
                  return resultMap["line2"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "line2")
                }
              }

              public var city: String? {
                get {
                  return resultMap["city"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "city")
                }
              }

              public var state: String? {
                get {
                  return resultMap["state"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "state")
                }
              }

              public var zip: String? {
                get {
                  return resultMap["zip"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "zip")
                }
              }

              public var phone: String? {
                get {
                  return resultMap["phone"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "phone")
                }
              }
            }
          }
        }
      }
    }
  }
}

public final class VehicleManufacturerQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query VehicleManufacturer($filter: IVehicleManufacturerFilter) {
      VehicleManufacturer(filter: $filter) {
        __typename
        id
        name
        type
      }
    }
    """

  public let operationName: String = "VehicleManufacturer"

  public var filter: IVehicleManufacturerFilter?

  public init(filter: IVehicleManufacturerFilter? = nil) {
    self.filter = filter
  }

  public var variables: GraphQLMap? {
    return ["filter": filter]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("VehicleManufacturer", arguments: ["filter": GraphQLVariable("filter")], type: .list(.object(VehicleManufacturer.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(vehicleManufacturer: [VehicleManufacturer?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "VehicleManufacturer": vehicleManufacturer.flatMap { (value: [VehicleManufacturer?]) -> [ResultMap?] in value.map { (value: VehicleManufacturer?) -> ResultMap? in value.flatMap { (value: VehicleManufacturer) -> ResultMap in value.resultMap } } }])
    }

    public var vehicleManufacturer: [VehicleManufacturer?]? {
      get {
        return (resultMap["VehicleManufacturer"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [VehicleManufacturer?] in value.map { (value: ResultMap?) -> VehicleManufacturer? in value.flatMap { (value: ResultMap) -> VehicleManufacturer in VehicleManufacturer(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [VehicleManufacturer?]) -> [ResultMap?] in value.map { (value: VehicleManufacturer?) -> ResultMap? in value.flatMap { (value: VehicleManufacturer) -> ResultMap in value.resultMap } } }, forKey: "VehicleManufacturer")
      }
    }

    public struct VehicleManufacturer: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["VehicleManufacturer"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("type", type: .list(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, name: String? = nil, type: [String?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "VehicleManufacturer", "id": id, "name": name, "type": type])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var type: [String?]? {
        get {
          return resultMap["type"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }
    }
  }
}

public final class VehicleModelsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query VehicleModels($pagination: IPagination!, $filter: IVehicleModelFilter!) {
      VehicleModels(pagination: $pagination, filter: $filter) {
        __typename
        docs {
          __typename
          id
          type
          name
          manufacturerId
          maxPower
          powerType
          supportedPorts
          supportedPortTypes {
            __typename
            id
            name
            type
            supportedPowers
          }
        }
        pagination {
          __typename
          totalDocs
          limit
          hasPrevPage
          hasNextPage
          page
          totalPages
          prevPage
          nextPage
        }
      }
    }
    """

  public let operationName: String = "VehicleModels"

  public var pagination: IPagination
  public var filter: IVehicleModelFilter

  public init(pagination: IPagination, filter: IVehicleModelFilter) {
    self.pagination = pagination
    self.filter = filter
  }

  public var variables: GraphQLMap? {
    return ["pagination": pagination, "filter": filter]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("VehicleModels", arguments: ["pagination": GraphQLVariable("pagination"), "filter": GraphQLVariable("filter")], type: .object(VehicleModel.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(vehicleModels: VehicleModel? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "VehicleModels": vehicleModels.flatMap { (value: VehicleModel) -> ResultMap in value.resultMap }])
    }

    public var vehicleModels: VehicleModel? {
      get {
        return (resultMap["VehicleModels"] as? ResultMap).flatMap { VehicleModel(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "VehicleModels")
      }
    }

    public struct VehicleModel: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["VehicleModels"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("docs", type: .list(.object(Doc.selections))),
          GraphQLField("pagination", type: .object(Pagination.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(docs: [Doc?]? = nil, pagination: Pagination? = nil) {
        self.init(unsafeResultMap: ["__typename": "VehicleModels", "docs": docs.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, "pagination": pagination.flatMap { (value: Pagination) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var docs: [Doc?]? {
        get {
          return (resultMap["docs"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Doc?] in value.map { (value: ResultMap?) -> Doc? in value.flatMap { (value: ResultMap) -> Doc in Doc(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Doc?]) -> [ResultMap?] in value.map { (value: Doc?) -> ResultMap? in value.flatMap { (value: Doc) -> ResultMap in value.resultMap } } }, forKey: "docs")
        }
      }

      public var pagination: Pagination? {
        get {
          return (resultMap["pagination"] as? ResultMap).flatMap { Pagination(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "pagination")
        }
      }

      public struct Doc: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["VehicleModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("type", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("manufacturerId", type: .scalar(String.self)),
            GraphQLField("maxPower", type: .scalar(Double.self)),
            GraphQLField("powerType", type: .list(.scalar(PowerType.self))),
            GraphQLField("supportedPorts", type: .list(.scalar(String.self))),
            GraphQLField("supportedPortTypes", type: .list(.object(SupportedPortType.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, type: String? = nil, name: String? = nil, manufacturerId: String? = nil, maxPower: Double? = nil, powerType: [PowerType?]? = nil, supportedPorts: [String?]? = nil, supportedPortTypes: [SupportedPortType?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "VehicleModel", "id": id, "type": type, "name": name, "manufacturerId": manufacturerId, "maxPower": maxPower, "powerType": powerType, "supportedPorts": supportedPorts, "supportedPortTypes": supportedPortTypes.flatMap { (value: [SupportedPortType?]) -> [ResultMap?] in value.map { (value: SupportedPortType?) -> ResultMap? in value.flatMap { (value: SupportedPortType) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var type: String? {
          get {
            return resultMap["type"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var manufacturerId: String? {
          get {
            return resultMap["manufacturerId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "manufacturerId")
          }
        }

        public var maxPower: Double? {
          get {
            return resultMap["maxPower"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "maxPower")
          }
        }

        public var powerType: [PowerType?]? {
          get {
            return resultMap["powerType"] as? [PowerType?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "powerType")
          }
        }

        public var supportedPorts: [String?]? {
          get {
            return resultMap["supportedPorts"] as? [String?]
          }
          set {
            resultMap.updateValue(newValue, forKey: "supportedPorts")
          }
        }

        public var supportedPortTypes: [SupportedPortType?]? {
          get {
            return (resultMap["supportedPortTypes"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SupportedPortType?] in value.map { (value: ResultMap?) -> SupportedPortType? in value.flatMap { (value: ResultMap) -> SupportedPortType in SupportedPortType(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [SupportedPortType?]) -> [ResultMap?] in value.map { (value: SupportedPortType?) -> ResultMap? in value.flatMap { (value: SupportedPortType) -> ResultMap in value.resultMap } } }, forKey: "supportedPortTypes")
          }
        }

        public struct SupportedPortType: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PortType"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("type", type: .scalar(PowerType.self)),
              GraphQLField("supportedPowers", type: .list(.scalar(Double.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String? = nil, name: String? = nil, type: PowerType? = nil, supportedPowers: [Double?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "PortType", "id": id, "name": name, "type": type, "supportedPowers": supportedPowers])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var type: PowerType? {
            get {
              return resultMap["type"] as? PowerType
            }
            set {
              resultMap.updateValue(newValue, forKey: "type")
            }
          }

          public var supportedPowers: [Double?]? {
            get {
              return resultMap["supportedPowers"] as? [Double?]
            }
            set {
              resultMap.updateValue(newValue, forKey: "supportedPowers")
            }
          }
        }
      }

      public struct Pagination: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Pagination"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("totalDocs", type: .scalar(Int.self)),
            GraphQLField("limit", type: .scalar(Int.self)),
            GraphQLField("hasPrevPage", type: .scalar(Int.self)),
            GraphQLField("hasNextPage", type: .scalar(Int.self)),
            GraphQLField("page", type: .scalar(Int.self)),
            GraphQLField("totalPages", type: .scalar(Int.self)),
            GraphQLField("prevPage", type: .scalar(Int.self)),
            GraphQLField("nextPage", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDocs: Int? = nil, limit: Int? = nil, hasPrevPage: Int? = nil, hasNextPage: Int? = nil, page: Int? = nil, totalPages: Int? = nil, prevPage: Int? = nil, nextPage: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "Pagination", "totalDocs": totalDocs, "limit": limit, "hasPrevPage": hasPrevPage, "hasNextPage": hasNextPage, "page": page, "totalPages": totalPages, "prevPage": prevPage, "nextPage": nextPage])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var totalDocs: Int? {
          get {
            return resultMap["totalDocs"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDocs")
          }
        }

        public var limit: Int? {
          get {
            return resultMap["limit"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "limit")
          }
        }

        public var hasPrevPage: Int? {
          get {
            return resultMap["hasPrevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasPrevPage")
          }
        }

        public var hasNextPage: Int? {
          get {
            return resultMap["hasNextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasNextPage")
          }
        }

        public var page: Int? {
          get {
            return resultMap["page"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "page")
          }
        }

        public var totalPages: Int? {
          get {
            return resultMap["totalPages"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalPages")
          }
        }

        public var prevPage: Int? {
          get {
            return resultMap["prevPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "prevPage")
          }
        }

        public var nextPage: Int? {
          get {
            return resultMap["nextPage"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "nextPage")
          }
        }
      }
    }
  }
}

public final class VerifyLoginOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation VerifyLoginOTP($id: String!, $otp: String!) {
      verifyLoginOTP(id: $id, otp: $otp) {
        __typename
        success
        message
        data {
          __typename
          id
          resetToken
        }
        error
      }
    }
    """

  public let operationName: String = "VerifyLoginOTP"

  public var id: String
  public var otp: String

  public init(id: String, otp: String) {
    self.id = id
    self.otp = otp
  }

  public var variables: GraphQLMap? {
    return ["id": id, "otp": otp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("verifyLoginOTP", arguments: ["id": GraphQLVariable("id"), "otp": GraphQLVariable("otp")], type: .object(VerifyLoginOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(verifyLoginOtp: VerifyLoginOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "verifyLoginOTP": verifyLoginOtp.flatMap { (value: VerifyLoginOtp) -> ResultMap in value.resultMap }])
    }

    public var verifyLoginOtp: VerifyLoginOtp? {
      get {
        return (resultMap["verifyLoginOTP"] as? ResultMap).flatMap { VerifyLoginOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "verifyLoginOTP")
      }
    }

    public struct VerifyLoginOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["VerifyLoginOTPResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "VerifyLoginOTPResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["VerifyLoginOTPResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(String.self)),
            GraphQLField("resetToken", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String? = nil, resetToken: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "VerifyLoginOTPResponseData", "id": id, "resetToken": resetToken])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String? {
          get {
            return resultMap["id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var resetToken: String? {
          get {
            return resultMap["resetToken"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "resetToken")
          }
        }
      }
    }
  }
}

public final class VerifyRegistrationOtpMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation VerifyRegistrationOTP($userId: String!, $otp: String!) {
      verifyRegistrationOTP(userId: $userId, otp: $otp) {
        __typename
        success
        message
        data {
          __typename
          token
          user {
            __typename
            _id
            id
            name
            role
            email
            phone
            status
            preference {
              __typename
              plug
              stationIds
            }
          }
        }
        error
      }
    }
    """

  public let operationName: String = "VerifyRegistrationOTP"

  public var userId: String
  public var otp: String

  public init(userId: String, otp: String) {
    self.userId = userId
    self.otp = otp
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "otp": otp]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("verifyRegistrationOTP", arguments: ["userId": GraphQLVariable("userId"), "otp": GraphQLVariable("otp")], type: .object(VerifyRegistrationOtp.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(verifyRegistrationOtp: VerifyRegistrationOtp? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "verifyRegistrationOTP": verifyRegistrationOtp.flatMap { (value: VerifyRegistrationOtp) -> ResultMap in value.resultMap }])
    }

    public var verifyRegistrationOtp: VerifyRegistrationOtp? {
      get {
        return (resultMap["verifyRegistrationOTP"] as? ResultMap).flatMap { VerifyRegistrationOtp(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "verifyRegistrationOTP")
      }
    }

    public struct VerifyRegistrationOtp: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["LoginUserResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
          GraphQLField("error", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool? = nil, message: String? = nil, data: Datum? = nil, error: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "LoginUserResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }, "error": error])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public var error: String? {
        get {
          return resultMap["error"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "error")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["LoginUserResponseData"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .scalar(String.self)),
            GraphQLField("user", type: .object(User.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String? = nil, user: User? = nil) {
          self.init(unsafeResultMap: ["__typename": "LoginUserResponseData", "token": token, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String? {
          get {
            return resultMap["token"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }

        public var user: User? {
          get {
            return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "user")
          }
        }

        public struct User: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["User"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("id", type: .scalar(String.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("role", type: .scalar(UserRole.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("phone", type: .scalar(String.self)),
              GraphQLField("status", type: .scalar(UserStatus.self)),
              GraphQLField("preference", type: .object(Preference.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(_id: String? = nil, id: String? = nil, name: String? = nil, role: UserRole? = nil, email: String? = nil, phone: String? = nil, status: UserStatus? = nil, preference: Preference? = nil) {
            self.init(unsafeResultMap: ["__typename": "User", "_id": _id, "id": id, "name": name, "role": role, "email": email, "phone": phone, "status": status, "preference": preference.flatMap { (value: Preference) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var _id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var id: String? {
            get {
              return resultMap["id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var role: UserRole? {
            get {
              return resultMap["role"] as? UserRole
            }
            set {
              resultMap.updateValue(newValue, forKey: "role")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var phone: String? {
            get {
              return resultMap["phone"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "phone")
            }
          }

          public var status: UserStatus? {
            get {
              return resultMap["status"] as? UserStatus
            }
            set {
              resultMap.updateValue(newValue, forKey: "status")
            }
          }

          public var preference: Preference? {
            get {
              return (resultMap["preference"] as? ResultMap).flatMap { Preference(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "preference")
            }
          }

          public struct Preference: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["UserPreference"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("plug", type: .scalar(String.self)),
                GraphQLField("stationIds", type: .list(.scalar(String.self))),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(plug: String? = nil, stationIds: [String?]? = nil) {
              self.init(unsafeResultMap: ["__typename": "UserPreference", "plug": plug, "stationIds": stationIds])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var plug: String? {
              get {
                return resultMap["plug"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "plug")
              }
            }

            public var stationIds: [String?]? {
              get {
                return resultMap["stationIds"] as? [String?]
              }
              set {
                resultMap.updateValue(newValue, forKey: "stationIds")
              }
            }
          }
        }
      }
    }
  }
}

public final class WalletQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Wallet($userId: String!) {
      Wallet(userId: $userId) {
        __typename
        id
        credit
        reserved
        transactions {
          __typename
          _id
          type
          amount
          consumedAmount
          reserved
          status
          createdAt
          updatedAt
        }
      }
    }
    """

  public let operationName: String = "Wallet"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("Wallet", arguments: ["userId": GraphQLVariable("userId")], type: .object(Wallet.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(wallet: Wallet? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Wallet": wallet.flatMap { (value: Wallet) -> ResultMap in value.resultMap }])
    }

    public var wallet: Wallet? {
      get {
        return (resultMap["Wallet"] as? ResultMap).flatMap { Wallet(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Wallet")
      }
    }

    public struct Wallet: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Wallet"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(String.self)),
          GraphQLField("credit", type: .scalar(Double.self)),
          GraphQLField("reserved", type: .scalar(Double.self)),
          GraphQLField("transactions", type: .list(.object(Transaction.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String? = nil, credit: Double? = nil, reserved: Double? = nil, transactions: [Transaction?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Wallet", "id": id, "credit": credit, "reserved": reserved, "transactions": transactions.flatMap { (value: [Transaction?]) -> [ResultMap?] in value.map { (value: Transaction?) -> ResultMap? in value.flatMap { (value: Transaction) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String? {
        get {
          return resultMap["id"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var credit: Double? {
        get {
          return resultMap["credit"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "credit")
        }
      }

      public var reserved: Double? {
        get {
          return resultMap["reserved"] as? Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "reserved")
        }
      }

      public var transactions: [Transaction?]? {
        get {
          return (resultMap["transactions"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Transaction?] in value.map { (value: ResultMap?) -> Transaction? in value.flatMap { (value: ResultMap) -> Transaction in Transaction(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Transaction?]) -> [ResultMap?] in value.map { (value: Transaction?) -> ResultMap? in value.flatMap { (value: Transaction) -> ResultMap in value.resultMap } } }, forKey: "transactions")
        }
      }

      public struct Transaction: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["WalletTransaction"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("type", type: .scalar(WalletTransactionType.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("consumedAmount", type: .scalar(Double.self)),
            GraphQLField("reserved", type: .scalar(Bool.self)),
            GraphQLField("status", type: .scalar(WalletTransactionStatus.self)),
            GraphQLField("createdAt", type: .scalar(String.self)),
            GraphQLField("updatedAt", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(_id: String? = nil, type: WalletTransactionType? = nil, amount: Double? = nil, consumedAmount: Double? = nil, reserved: Bool? = nil, status: WalletTransactionStatus? = nil, createdAt: String? = nil, updatedAt: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "WalletTransaction", "_id": _id, "type": type, "amount": amount, "consumedAmount": consumedAmount, "reserved": reserved, "status": status, "createdAt": createdAt, "updatedAt": updatedAt])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var _id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var type: WalletTransactionType? {
          get {
            return resultMap["type"] as? WalletTransactionType
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var consumedAmount: Double? {
          get {
            return resultMap["consumedAmount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "consumedAmount")
          }
        }

        public var reserved: Bool? {
          get {
            return resultMap["reserved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "reserved")
          }
        }

        public var status: WalletTransactionStatus? {
          get {
            return resultMap["status"] as? WalletTransactionStatus
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["createdAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var updatedAt: String? {
          get {
            return resultMap["updatedAt"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "updatedAt")
          }
        }
      }
    }
  }
}
