//
//  FacebookLoginManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 24/06/22.
//

import Foundation
import UIKit
import FBSDKLoginKit

class FacebookSignInManager {
    
    static var shared = FacebookSignInManager()
    let loginManager = LoginManager()

    var signInResponseClosure: (()->())?
    var signInResponse: FacebookSignInResponse? {
        didSet {
            self.signInResponseClosure?()
        }
    }

    var showAlertClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoggedIn: Bool {
        return AccessToken.current != nil || AuthenticationToken.current != nil
    }
    
    var configuration: LoginConfiguration? {
        return LoginConfiguration(
            permissions: [.publicProfile, .email],
            tracking: .limited
        )
    }
    
    func restoreUserSignInData() {
        self.signInResponse = nil
        if isLoggedIn {
            // Show the app's signed-in state.
            self.signInResponse = FacebookSignInResponse(isLoggedIn:isLoggedIn)
        }
    }
    
    func performSignIn(presentingVC:UIViewController) {

        loginManager.logIn(
            viewController: presentingVC,
            configuration: configuration!
        ) { [unowned self] result in
            switch result {
            case .cancelled:
                self.alertMessage = Constants.Messages.SocialLoginCancelled
            case .failed(let error):
                let nsError = error as NSError

                guard let sdkMessage = nsError.userInfo["com.facebook.sdk:FBSDKErrorDeveloperMessageKey"] as? String
                    else {
                        preconditionFailure("Errors from the SDK should have a developer facing message")
                }
                self.alertMessage = sdkMessage
            case .success:
                self.signInResponse = FacebookSignInResponse(isLoggedIn:isLoggedIn)
            }
        }
    }
    
    func performSignOut(){
        
        if isLoggedIn {
            loginManager.logOut()
        }
    }
}

class FacebookSignInResponse:NSObject {
    
    var userId: String?
    var token: String?
    var accessToken:String?
    var fullName: String?
    var firstName: String?
    var middleName: String?
    var lastName: String?
    var email: String?
    var imageURL : URL?

    init(isLoggedIn:Bool) {
        if let profile = Profile.current {
          
            // Perform any operations on signed in user here.
            self.userId = profile.userID
            self.token = AuthenticationToken.current?.tokenString // Safe to send to the server
            self.accessToken = AccessToken.current?.tokenString
            
            self.fullName = profile.name
            self.firstName = profile.firstName
            self.middleName = profile.middleName
            self.lastName = profile.lastName

            self.email = profile.email
            
            print("\nUserId = \(String(describing: userId)) \nToken = \(String(describing: token)) \nFullName = \(String(describing: fullName)) \nEmail = \(String(describing: email))");
            if let url = profile.imageURL {
                print("Image URL = \(String(describing: url))")
                self.imageURL = url
            }
        }
    }
}
