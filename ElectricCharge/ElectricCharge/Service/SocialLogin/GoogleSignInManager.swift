//
//  GoogleLoginManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 24/06/22.
//

import Foundation
import UIKit
import GoogleSignIn

class GoogleSignInManager {
    
    static var shared = GoogleSignInManager()
    let googleSignIn = GIDSignIn.sharedInstance
    let signInConfig = GIDConfiguration(clientID: Constants.Keys.SocialLogin.Google.ClientId)
    var signInResponseClosure: (()->())?
    var signInResponse: GoogleSignInResponse? {
        didSet {
            self.signInResponseClosure?()
        }
    }
    var showAlertClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    func restoreUserSignInData() {
        signInResponse = nil
        googleSignIn.restorePreviousSignIn { user, error in
            if error != nil || user == nil {
                // Show the app's signed-out state.
                if let errorMessage = error?.localizedDescription {
                    print("Error = \(errorMessage)")
                    self.alertMessage = errorMessage
                }
            } else {
                // Show the app's signed-in state.
                if let user = user {
                    self.signInResponse = GoogleSignInResponse(user: user)
                }
            }
        }
    }
    
    func performSignIn(presentingVC:UIViewController) {
            
        googleSignIn.signIn(with: signInConfig,
                            presenting: presentingVC) { user, error in
                
            if error != nil || user == nil {
                if let errorMessage = error?.localizedDescription {
                    print("Error = \(errorMessage)")
                    self.alertMessage = errorMessage
                }
            } else {
                // Show the app's signed-in state.
                if let user = user {
                    self.signInResponse = GoogleSignInResponse(user: user)
                }
            }
        }
    }
    
    func performSignOut(){
        
        googleSignIn.signOut()
    }
}

class GoogleSignInResponse:NSObject {
    var userId: String?
    var token: String?
    var fullName: String?
    var givenName: String?
    var familyName: String?
    var email: String?
    var hasImage : Bool?
    var imageURL : URL?

    init(user:GIDGoogleUser) {
        if let profile = user.profile {
          
          // Perform any operations on signed in user here.
            self.userId = user.userID                  // For client-side use only!
            self.token = user.authentication.idToken // Safe to send to the server
                    
            self.fullName = profile.name
            self.givenName = profile.givenName
            self.familyName = profile.familyName
            self.email = profile.email
            self.hasImage = profile.hasImage
            
            print("\nUserId = \(String(describing: userId)) \nToken = \(String(describing: token)) \nFullName = \(String(describing: fullName)) \nGivenName = \(String(describing: givenName)) \nFamilyName = \(String(describing: familyName)) \nEmail = \(String(describing: email))");
            if profile.hasImage {
                if let url = profile.imageURL(withDimension: 250) {
                    print("Image URL = \(String(describing: url))")
                    self.imageURL = url
                }
            }
        }
    }
}

