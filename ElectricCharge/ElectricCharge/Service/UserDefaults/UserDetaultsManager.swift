//
//  UserDetaults.swift
//  ElectricCharge
//
//  Created by Raghavendra on 30/06/22.
//

import Foundation

extension UserDefaults {
    
    enum Keys {
        static let sessionToken = "sessionToken"
        static let userId = "userId"
        static let vehicleType = "vehicleType"
        static let vehicleMake = "vehicleMake"
        static let vehicleModel = "vehicleModel"
        static let plugType = "plugType"
        static let userName = "userName"
        static let userEmail = "userEmail"
        static let userPhoneNumber = "userPhoneNumber"
        static let createdNewReservation = "createdNewReservation"
        static let favouriteStations = "favouriteStations"
        static let favouriteStationsChanged = "favouriteStationsChanged"
        static let UPIIds = "upiIds"
    }
    
    func clearAppData(){
        
        self.removeObject(forKey: Keys.userId)
        self.removeObject(forKey: Keys.sessionToken)
        self.removeObject(forKey: Keys.userName)
        self.removeObject(forKey: Keys.userEmail)
        self.removeObject(forKey: Keys.userPhoneNumber)
        self.removeObject(forKey: Keys.plugType)
        self.removeObject(forKey: Keys.vehicleType)
        self.removeObject(forKey: Keys.vehicleMake)
        self.removeObject(forKey: Keys.vehicleModel)
        self.removeObject(forKey: Keys.createdNewReservation)
        self.removeObject(forKey: Keys.favouriteStations)
        self.removeObject(forKey: Keys.favouriteStationsChanged)
        self.removeObject(forKey: Keys.UPIIds)
        self.synchronize()
    }
    
    func saveUserDetails(user:UserDetailsModel){
        
        if let sessionTokenValue = user.sessionToken {
            self.set(sessionTokenValue, forKey: UserDefaults.Keys.sessionToken)
        }
        if let idValue = user.id {
            self.set(idValue, forKey: UserDefaults.Keys.userId)
        }
        if let nameValue = user.name {
            self.set(nameValue, forKey: UserDefaults.Keys.userName)
        }
        if let emailValue = user.email {
            self.set(emailValue, forKey: UserDefaults.Keys.userEmail)
        }
        if let phoneValue = user.phone {
            self.set(phoneValue, forKey: UserDefaults.Keys.userPhoneNumber)
        }
        if let stationIds = user.stationIds {
            self.set(stationIds, forKey: UserDefaults.Keys.favouriteStations)
        }
    }
}
