//
//  NetworkReachability.swift
//  ElectricCharge
//
//  Created by Raghavendra on 22/03/22.
//

import Foundation

public enum NetworkConnectionState : Int32 {
    case none = 0
    case wifi = 1
    case mobile = 2
    case lan = 3
    case unkonwn = 4
}

class NetworkReachability:NSObject {
    
    public static let shared = NetworkReachability()
    
    var reachability: Reachability?
    var connectionState:NetworkConnectionState = .unkonwn
    
    func startReachability() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        print("--- start Reachability")
        reachability = try? Reachability()
        try? reachability?.startNotifier()
        updateReachabilityStatus()
    }
    
    func stopReachability() {
        print("--- stop Reachability")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        updateReachabilityStatus()
    }
    
    func updateReachabilityStatus() {
        let status = reachability?.connection
        switch status {
            case .cellular:
                //Remove Network unavailable UI
                connectionState = .mobile
                break
            case .wifi:
                //Remove Network unavailable UI
                connectionState = .wifi
                break
            case .unavailable, .none:
                connectionState = .none
                //Show Network unavailable UI on top of all window
                break
        }
    }
    
    deinit {
        stopReachability()
    }
}
