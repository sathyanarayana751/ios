//
//  ChashfreePaymentGatewayManager.swift
//  ElectricCharge
//
//  Created by Raghavendra on 01/09/22.
//

import Foundation
import UIKit
import CashfreePG
import CashfreePGCoreSDK
import CashfreePGUISDK

class ChashfreePaymentGatewayManager {
    
    static let sharedInstance = ChashfreePaymentGatewayManager()
    let paymentGatewayService = CFPaymentGatewayService.getInstance()
    
    var paymentGatewayResponseClosure: (()->())?
    var paymentGatewayResponse: (Bool?,String?) {
        didSet {
            self.paymentGatewayResponseClosure?()
        }
    }
        
    private func getSession(orderId:String,
                            orderToken:String) -> CFSession? {
        do {
            print("CashfreePGUISDKVersionNumber = \(CashfreePGUISDKVersionNumber)")
            let session = try CFSession.CFSessionBuilder()
                .setEnvironment(.SANDBOX)
                .setOrderID(orderId)
                .setOrderToken(orderToken)
                .build()
            return session
        } catch let e {
            let error = e as! CashfreeError
            print(error.localizedDescription)
            // Handle errors here
        }
        return nil
    }
    
    func invokeNativeiOSSDK(orderId:String,
                            orderToken:String,
                            controller:UIViewController) {
        if let session = self.getSession(orderId:orderId,
                                         orderToken:orderToken) {
            do {
              
                // Set Components
                /*"order-details",
                 "card",
                 "paylater",
                 "wallet",
                 "emi",
                 "netbanking",
                 "upi"*/
                let paymentComponents = try CFPaymentComponent.CFPaymentComponentBuilder()
                    .enableComponents([
                        "order-details",
                        "card",
                        "emi",
                        "netbanking",
                        "upi"
                    ])
                    .build()
                
                // Set Theme
                let theme = try CFTheme.CFThemeBuilder()
                    .setPrimaryFont("Source Sans Pro")
                    .setSecondaryFont("Gill Sans")
                    .setButtonTextColor("#FFFFFF")
                    .setButtonBackgroundColor("#2ECE54")//2ECE54  FF0000
                    .setNavigationBarTextColor("#FFFFFF")
                    .setNavigationBarBackgroundColor("#2ECE54")//2ECE54  FF0000
                    .setPrimaryTextColor("#2ECE54")//2ECE54  FF0000
                    .setSecondaryTextColor("#2ECE54")//2ECE54  FF0000
                    .build()
                
                // Native payment
                let nativePayment = try CFDropCheckoutPayment.CFDropCheckoutPaymentBuilder()
                    .setSession(session)
                    .setTheme(theme)
                    .setComponent(paymentComponents)
                    .build()
                
                // Invoke SDK
                self.paymentGatewayService.setCallback(self)
                try self.paymentGatewayService.doPayment(nativePayment, viewController: controller)
                
            } catch let e {
                let error = e as! CashfreeError
                print(error.localizedDescription)
                // Handle errors here
            }
        }
    }
       
}

extension ChashfreePaymentGatewayManager: CFResponseDelegate {
    
    func onError(_ error: CFErrorResponse, order_id: String) {
        print(error.message ?? "")
        self.paymentGatewayResponse = (false,error.message)
    }
    
    func verifyPayment(order_id: String) {
        // Verify The Payment here
        self.paymentGatewayResponse = (true,nil)
    }
    
}
