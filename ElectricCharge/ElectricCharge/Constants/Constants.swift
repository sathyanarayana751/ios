//
//  Appconstants.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 24/10/21.
//

import Foundation
import UIKit

enum Constants {

    enum KDevice {
        static let width = UIScreen.main.bounds.size.width
        static let height = UIScreen.main.bounds.size.height
    }
    
    enum Keys {
        
        enum Maps {
            
            static let Google = "AIzaSyAGy6JMRkC5leAt13B-RAtMuqKwU4btUzY"
                //"AIzaSyCSGqQCu2Xm-Bn-c6C0DMXu58KuOkxPPZ8"//"AIzaSyAJO60crxhBDw0QlASa5WsQSbUdZwaqp6I"
            static let MapboxAccessToken = "pk.eyJ1IjoicmFnaGF2ZW5kcmF2ZW5rYXRlc2g2NDc2IiwiYSI6ImNsNGp1dHpnMzA4dnIzZm8xZzk4ejJkcnIifQ.QXP-oRQhGZYjYE2VM1Cffg"
        }
        enum Search {
            static let Google = "AIzaSyDqTw2jFqpVEFYSEcJ8SofT-RXiDkWx2GE"
        }
        enum SocialLogin {
            enum Google {
                static let ClientId = "122201168535-j6vhbckf8lfo6ur52tv3u7jhp2acrel0.apps.googleusercontent.com"
            }
            enum Facebook {
                static let AppId = "2268595263288157"
            }
        }
    }
    
    enum Alert {
        static let title = "Alert"
        static let logoutMessage = "Are you sure you want to logout?"
        static let locationEnable = "Please enable location to show nearby charging station"
        enum Action {
            static let ok = "ok"
        }
    }
    
    enum Messages {
        
        static let FavouriteStationsUnavailable = "There is no favorite \n charging stations near you."
        static let SocialLoginCancelled = "Login attempt was cancelled"
        static let SocialLoginUserNotExist = "This user is not registered please signup"
        static let FavouriteStationSuccess = "Station favourited successfully"
        static let PaymentAuthorize = "Authorize for estimated cost and pay later \n as u use"
        static let PaymentLocation = "Please confirm location when vehicle is with in 500 meter distance to station"
        static let StartCharging = "Please start charge when charger is pluged to vehicle"
        static let InsufficientWalletBalance = "InsufficientBalance"
        static let NilResponse = "Null Response"

    }
    
    enum InputFields {
            
        static let MissingEmail = "Please enter email"
        static let MissingPhone = "Please enter phone number"
        static let MissingEmailOrPhoneNumber = "Please enter email or phone number"

        static let MissingUserName = "Please enter username"
        static let MissingPassword = "Please enter password"
        static let MissingConfirmPassword = "Please enter confirm password"
        static let MismatchPasswordAndConfirmPassword = "Password and confirm password should be same"

        static let MissingOTP = "Please enter valid otp"
        static let MissingUserId = "Missing user-id please sign up again"

        static let InvalidEmail = "Invalid email"
        static let InvalidPassword = "Invalid password"
        static let InvalidPhoneNumber = "Invalid phone number"
        
        static let MissingPlugType = "Please select plug type"
        
        static let InvalidStartTime = "Please select valid start time"
        static let InvalidEndTime = "Please select valid end time"
        static let InvalidEndTime1 = "End time should be greater than start time"
        static let InvalidEndTime2 = "End time should be less than or equal to 6 hours from start time"

        static let InvalidSlotsSelection = "Please select valid slots"
        
        static let InvalidChargingDuration = "Charging duration should be within selected availabilty time range"

        static let MissingUPIId = "Please enter UPI ID"
        static let InvalidUPIId = "Please enter valid UPI ID"

        static let MissingAmount = "Please enter amount"
        static let MissingMinimumAmount = "Amount should be minimum 500"
    }
    
    enum Filters {
        
        static let AvailableFilters = [
            [
                "title":"AVAILABILITY",
                "data":["Available Only"]
            ],
            [
                "title":"SORT BY",
                "data":["Nearest","Cheapest"]
            ],
            [
                "title":"CHARGE TYPE",
                "data":["DC","AC"]
            ],
            [
                "title":"PLUG TYPE",
                "data":[
                    "DC":["CCS-2","CHADEMO","GB/T"],
                    "AC":["AC-001"]
                ]
            ],
            [
                "title":"RANGE",
                "data":["5","10","20","50","100"]
            ]
        ]
        
        static let PortTypes = [
            [
              "id": "GBT",
              "name": "GB/T",
              "type": "DC",
              "supportedPowers": [
                15
              ]
            ],
            [
              "id": "CCS",
              "name": "CCS-2",
              "type": "DC",
              "supportedPowers": [
                25,
                30
              ]
            ],
            [
              "id": "CHADEMO",
              "name": "CHADEMO",
              "type": "DC",
              "supportedPowers": [
                30
              ]
            ],
            [
              "id": "AC001",
              "name": "AC-001",
              "type": "AC",
              "supportedPowers": [
                3.3
              ]
            ]
          ]
        
        static let InitialSelectedFilters:[String:Any] = [
                                                "AVAILABILITY":["Available Only"],
                                                "SORT BY":["Nearest"],
                                                "CHARGE TYPE":["DC"],
                                                "PLUG TYPE":["DC":["CCS-2"], "AC":["AC-001"]],
                                                "RANGE":["10"]
                                            ]
    }
}
