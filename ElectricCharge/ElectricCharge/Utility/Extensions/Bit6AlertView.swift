//
//  ViewExt.swift
//  ElectricCharge
//
//  Created by iosdev on 2/15/22.
//

import UIKit

/*public class CommonAlertControllerPlaceHolderViewController : UIViewController {
    
    var statusBarStyle = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarStyle
    var isStatusBarHidden = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.isStatusBarHidden

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle!
    }

    override public var prefersStatusBarHidden: Bool {
        return isStatusBarHidden!
    }

}

public class Bit6AlertView: NSObject {

    private var window:UIWindow!
    public var alertController:UIAlertController!
    public var presentingViewController:CommonAlertControllerPlaceHolderViewController
    var title:String?
    var message:String?
    
    public init(title:String?, message:String?) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.presentingViewController = CommonAlertControllerPlaceHolderViewController()
        self.window.rootViewController = self.presentingViewController
        self.window.windowLevel = UIWindow.Level.alert + 1
        self.title = title
        self.message = message
    }

    public func show() {
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: Constants.Alert.Action.ok, style: .default) { action in
            self.dismiss()
        }
        self.alertController.addAction(okAction)
//        DispatchQueue.main.async { () in
//            self.window.makeKeyAndVisible()
//            self.window.rootViewController?.present(self.alertController, animated: true, completion: nil)
//        }
        
        
        var rootViewController:UIViewController?
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        } else if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }
        //...
        rootViewController?.present(alertController, animated: true, completion: nil)
    }

    public func dismiss() {
        DispatchQueue.main.async { [unowned self] () in
            self.window.rootViewController?.dismiss(animated: true, completion: nil)
            self.window.rootViewController = nil
            self.window.isHidden = true
            UIApplication.shared.windows.first!.makeKeyAndVisible()
            self.window = nil
            self.alertController = nil
        }
    }
}*/

class Bit6AlertView {
    private var window = UIWindow(frame: UIScreen.main.bounds)
    public var alertController:UIAlertController?
    public var presentingViewController: Bit6AlertControllerPlaceHolderViewController
    
    public init(title: String?, message: String?) {
        self.presentingViewController = Bit6AlertControllerPlaceHolderViewController()
        self.window.rootViewController = self.presentingViewController
        self.window.windowLevel = UIWindow.Level.alert + 1
        
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    public func show() {
        if let alertController = alertController {
            DispatchQueue.main.async {
                self.window.makeKeyAndVisible()
                self.window.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }

    public func addAction(_ action: UIAlertAction) {
        if let alertController = alertController {
            alertController.addAction(action)
        }
    }
    
    public func addCancelButton(string: String) {
        if let alertController = alertController {
            alertController.addAction(UIAlertAction(title: string, style: .cancel, handler: nil))
        }
    }
}

class Bit6AlertControllerPlaceHolderViewController : UIViewController {
//    var statusBarStyle = UIApplication.shared.statusBarStyle
//    var isStatusBarHidden = UIApplication.shared.isStatusBarHidden
        
    var statusBarStyle = (UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarStyle)!
    var isStatusBarHidden = (UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.isStatusBarHidden)!
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override public var prefersStatusBarHidden: Bool {
        return isStatusBarHidden
    }
}
