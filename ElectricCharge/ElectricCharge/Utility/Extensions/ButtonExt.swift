//
//  File.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 20/12/21.
//

import UIKit
import Foundation

class LoadingButton: UIButton {
    var originalButtonText: String?
    var originalButtonImage: UIImage?
    var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.originalButtonText = self.titleLabel?.text
            self.originalButtonImage = self.imageView?.image
            self.setTitle("", for: .normal)
            self.setImage(UIImage(), for: .normal)
            
            if self.activityIndicator == nil {
                self.activityIndicator = self.createActivityIndicator()
            }
            self.isUserInteractionEnabled = false
            self.showSpinning()
        }
    }
    
    func hideLoading() {
        if activityIndicator == nil {
            return
        }
        self.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        self.setTitle(originalButtonText, for: .normal)
        self.setImage(originalButtonImage, for: .normal)
    }
 
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        return activityIndicator
    }
    
    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
