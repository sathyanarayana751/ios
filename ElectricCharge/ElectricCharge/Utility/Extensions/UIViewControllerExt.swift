//
//  UIViewControllerExt.swift
//  ElectricCharge
//
//  Created by iosdev on 2/27/22.
//

import Foundation
import UIKit

extension UIViewController {
    
    static let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    
    func displayMessageAlert(withMessage message: String) {
        
        let alert = UIAlertController(title: Constants.Alert.title, message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: Constants.Alert.Action.ok, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
