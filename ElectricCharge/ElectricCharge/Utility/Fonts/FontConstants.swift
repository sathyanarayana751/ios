//
//  FontConstants.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import Foundation

struct AppFonts {
    static let bold  = "Montserrat-Bold"
    static let blackItalic  = "Montserrat-BlackItalic"
    static let black  = "Montserrat-Black"
    static let bolldItalic  = "Montserrat-BoldItalic"
    static let extraBold  = "Montserrat-ExtraBold"
    static let extralight  = "Montserrat-ExtraLight"
    static let extraLightItalic  = "Montserrat-ExtraLightItalic"
    static let italic = "Montserrat-Italic"
    static let light = "Montserrat-Light"
    static let lightItalic = "Montserrat-LightItalic"
    static let medium  = "Montserrat-Medium"
    static let mediumItalic = "Montserrat-MediumItalic"
    static let regular = "Montserrat-Regular"
    static let semiBold = "Montserrat-SemiBold"
    static let semiBoldItalic = "Montserrat-SemiBoldItalic"
    static let thin =  "Montserrat-Thin"
    static let thinItalic  = "Montserrat-ThinItalic"
}
