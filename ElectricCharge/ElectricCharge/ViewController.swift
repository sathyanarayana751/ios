//
//  ViewController.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        for fontFamilyName in UIFont.familyNames{
            for fontName in UIFont.fontNames(forFamilyName: fontFamilyName){
                print("Family: \(fontFamilyName)     Font: \(fontName)")
            }
        }
        // Do any additional setup after loading the view.
    }

}

