//
//  BookingHistoryVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class BookingHistoryVC: UIViewController {
    
    @IBOutlet weak var historyTblVw: UITableView!
    @IBOutlet weak var bookingsUnavailableView: UIView!
    @IBOutlet weak var bookingsUnavailableImageView: UIImageView!
    @IBOutlet weak var bookingsUnavailableMessageLabel: UILabel!
        
    lazy var viewModel: BookingHistoryViewModel = {
        return BookingHistoryViewModel()
    }()
    var selectedDateFormatter = DateFormatter()
    var slotsTimeFormatter = DateFormatter()
    var favouriteStations:[String] = []
    
    var limit = 10
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTblVw.rowHeight = UITableView.automaticDimension
        maintainCreateNewReservationFlag()
        // Do any additional setup after loading the view.
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let createdNewReservation = UserDefaults.standard.object(forKey: UserDefaults.Keys.createdNewReservation) as? Bool {
            if createdNewReservation {
                viewModel.isLoading = true
                self.historyTblVw.reloadData()
                callReservationsListAPI(needToFetchFromServer: true)
                maintainCreateNewReservationFlag()
            }
        }
    }
    
    func initViewModel() {
        
        // Native binding
        if let favStations = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStations) as? [String] {
            self.favouriteStations = favStations
        }
        
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.viewModel.isLoading = false
                    self?.displayMessageAlert(withMessage: message )
                }
            }
        }
                
        viewModel.reloadReservedSlotsListClosure = { [weak self] () in
            DispatchQueue.main.async {
                
                self?.callPastBookingsListAPI()
            }
        }
        
        viewModel.reloadPastBookingsListClosure = { [weak self] () in
            DispatchQueue.main.async {
                
                if (self?.viewModel.isLoading)! {
                    self?.viewModel.isLoading = false
                } else {
                    self?.historyTblVw.reloadData()
                }
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.historyTblVw.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.historyTblVw.reloadData()
            }
        }
        
        viewModel.isLoading = true
        callReservationsListAPI(needToFetchFromServer: false)
        maintainCreateNewReservationFlag()
    }
    
    func maintainCreateNewReservationFlag() {
        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.createdNewReservation)
        UserDefaults.standard.synchronize()
    }
    
    func callReservationsListAPI(needToFetchFromServer:Bool) {
        
        viewModel.reserveSlotsListApiCall(needToFetchFromServer:needToFetchFromServer,
                                          status: [.reserved, .running, .hold])
    }
    
    @objc func callPastBookingsListAPI() {
        
        viewModel.pastBookingsListApiCall(status: [.completed, .canceled])
    }
    
    @IBAction func showReservedBookingDetailsAction(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if let bookingDetails = self.viewModel.arrayOfReservedSlots[button.tag] {
                let bookingStatus = bookingDetails.status ?? .completed
                if bookingStatus == .running {
                    self.moveToStopChargingView(bookingDetails: bookingDetails)
                } else {
                    self.moveToMachineDetailsView(bookingDetails: bookingDetails)
                }
            }
        }
    }
    
    @IBAction func showCompletedBookingDetailsAction(_ sender: Any) {
        
        if self.viewModel.arrayOfPastBookings.count > 0 {
            if let button = sender as? UIButton {
                if let bookingDetails = self.viewModel.arrayOfPastBookings[button.tag] {
                    self.moveToMachineDetailsView(bookingDetails: bookingDetails)
                }
            }
        }
    }
    
    func moveToMachineDetailsView(bookingDetails:ReservationsListQuery.Data.Reservation.Doc) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "MachineDetailsVC") as? MachineDetailsVC
        vc?.modalPresentationStyle = .overFullScreen
        vc?.isFromBookingHistory = true
        vc?.reserveSlotDetailsModel = ReserveSlotDetailsModel(bookingData: bookingDetails)

        if let stationData = bookingDetails.station {
            vc?.stationDetails = StationDetailsModel(bookingHistoryStationData: stationData)
            if let stationIdValue = stationData.id {
                vc?.stationId = stationIdValue
            }
        }

        if let chargeType = bookingDetails.chargeType {
            vc?.selectedChargerType = chargeType
        }
        
        if let plugType = bookingDetails.plugType {
            vc?.selectedPlugType = plugType
        }
        
        if let powerRating = bookingDetails.powerRating {
            vc?.selectedPowerValue = powerRating
        }

        if let selctedCharger = bookingDetails.charger, let selctedChargerId = selctedCharger.id {
            vc?.selectedChargerId = selctedChargerId
        }
        
        slotsTimeFormatter.locale = Locale.current
        slotsTimeFormatter.dateFormat = "h:mm a"

        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "MMM dd"
        
        if let startDateAndTime = bookingDetails.startTime, let endDateAndTime = bookingDetails.endTime {
            
            let reservationStartDate = Date(timeIntervalSince1970:TimeInterval(startDateAndTime)!/1000)
            let reservationEndDate = Date(timeIntervalSince1970:TimeInterval(endDateAndTime)!/1000)
                
            vc?.selectedDate = selectedDateFormatter.string(from: reservationStartDate)
            vc?.selectedStartAndEndTime = "\(slotsTimeFormatter.string(from: reservationStartDate)) - \(slotsTimeFormatter.string(from: reservationEndDate))"
            
            let count = Int(TimeInterval(endDateAndTime)! - TimeInterval(startDateAndTime)!)/1000
            
            let selectedDurationHour = count/3600
            let selectedDurationMinute = (count % 3600)/60
            
            var selectedDurationHourText = ""
            
            if selectedDurationHour > 0 {
                if selectedDurationHour > 1 {
                    selectedDurationHourText = "\(selectedDurationHour)hrs"
                } else {
                    selectedDurationHourText = "\(selectedDurationHour)hr"
                }
            }
            
            if selectedDurationMinute > 1 {
                selectedDurationHourText = selectedDurationHourText + " \(selectedDurationMinute)mins"
            } else {
                selectedDurationHourText = selectedDurationHourText + " \(selectedDurationMinute)min"
            }
            vc?.selectedDuration = selectedDurationHourText

        }
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
    
    func moveToStopChargingView(bookingDetails:ReservationsListQuery.Data.Reservation.Doc) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "StopChargingVC") as? StopChargingVC
        vc?.modalPresentationStyle = .overFullScreen
        
        if let reservationId = bookingDetails.id {
            vc?.reservationId = reservationId
        }
                
        if let reservationStartTime = bookingDetails.startTime {
            vc?.reservationStartTime = reservationStartTime
        }
        if let reservationEndTime = bookingDetails.endTime {
            vc?.reservationEndTime = reservationEndTime
        }
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}

extension BookingHistoryVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if viewModel.isLoading {
            return 2
        } else {
            if viewModel.arrayOfReservedSlots.count > 0 || viewModel.arrayOfPastBookings.count > 0 {
                bookingsUnavailableView.isHidden = true
                return 2
            } else {
                bookingsUnavailableView.isHidden = false
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }  else {
            if viewModel.isLoading {
                return 5
            } else {
                //TODO: Need to remove this
                if viewModel.arrayOfPastBookings.count > 0 {
                    if viewModel.hasNextPage {
                        return viewModel.arrayOfPastBookings.count + 2//HeaderView + LoaderView
                    } else {
                        return viewModel.arrayOfPastBookings.count + 1//HeaderView
                    }
                } else {
                    return 2
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: UpcomingBookingTblVwCell.identifier, for: indexPath) as? UpcomingBookingTblVwCell
            cell?.isLoading = viewModel.isLoading
            cell?.arrayOfReservations = viewModel.arrayOfReservedSlots
            cell?.favouriteStations = self.favouriteStations
            cell?.upcomingBookingsCollectionView.reloadData()
            return cell!
        } else {
            if indexPath.row == 0 {
                let cell  = tableView.dequeueReusableCell(withIdentifier: CompletedBookingTblVwCell.identifier, for: indexPath) as? CompletedBookingTblVwCell
                return cell!
            } else {
                                                        
                if !viewModel.isLoading {
                        
                    if viewModel.arrayOfPastBookings.count == 0 && indexPath.row == 1 {
                        //Show No Past booking view
                        let cell  = tableView.dequeueReusableCell(withIdentifier: BookingHistoryFilterTblCell.identifier, for: indexPath) as? BookingHistoryFilterTblCell
                        cell?.bgView.isHidden = true
                        cell?.bookingUnavailableView.isHidden = false
                        cell?.eachBookingIdentifier.isUserInteractionEnabled = false
                        return cell!
                        
                    } else if viewModel.hasNextPage && indexPath.row == viewModel.arrayOfPastBookings.count + 1 {
                        //Show Past Bookings With Bottom Loader
                        let loadingCell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath) as? LoadingCell
                        loadingCell?.activityIndicatorView.startAnimating()
                        return loadingCell!
                            
                    } else {
                        //Show Past Bookings Without Bottom Loader
                        let cell  = tableView.dequeueReusableCell(withIdentifier: BookingHistoryFilterTblCell.identifier, for: indexPath) as? BookingHistoryFilterTblCell
                                                
                        let indexValue = indexPath.row - 1
//                        if viewModel.hasNextPage {
//                            indexValue = indexPath.row - 2
//                        }
                        cell?.eachBookingIdentifier.tag = indexValue
                                                
                        //Show past bookings
                        cell?.bgView.isHidden = false
                        cell?.bookingUnavailableView.isHidden = true
                        cell?.eachBookingIdentifier.isUserInteractionEnabled = true

                        if viewModel.arrayOfPastBookings.count > 0 {
                            if let particularReservationData = viewModel.arrayOfPastBookings[indexValue] {
                                cell?.showReservationDetails(reservationData: particularReservationData)
                            }
                        }
                        return cell!
                    }
                } else {
                    
                    let cell  = tableView.dequeueReusableCell(withIdentifier: BookingHistoryFilterTblCell.identifier, for: indexPath) as? BookingHistoryFilterTblCell
                    return cell!
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView,
              willDisplay cell: UITableViewCell,
            forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
            if !viewModel.isLoading {
                if viewModel.arrayOfPastBookings.count > 0 && viewModel.hasNextPage && indexPath.row == viewModel.arrayOfPastBookings.count + 1 {
                    self.perform(#selector(callPastBookingsListAPI), with: nil, afterDelay: 2.0)
                }
            }
            if indexPath.row > 0 {
                cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
            }
        }
   }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 320
        }  else {
            if indexPath.row == 0 {
                return 95//Past Booking Header View
            } else if viewModel.arrayOfPastBookings.count == 0 && indexPath.row == 1 {
                return 165//No Past Booking View
            } else if viewModel.hasNextPage && indexPath.row == viewModel.arrayOfPastBookings.count + 1 {
                return 50//Loader View
            } else {
                return 165//Past Booking View
            }
        }
    }
    
}
