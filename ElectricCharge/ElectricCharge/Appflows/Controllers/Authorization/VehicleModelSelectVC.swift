//
//  VehicleModelSelectVC.swift
//  ElectricCharge
//
//  Created by iosdev on 2/12/22.
//

import UIKit

class VehicleModelSelectVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var typeTextField: UITextField!
    
    @IBOutlet weak var makeTextField: UITextField!
    
    @IBOutlet weak var modelTextField: UITextField!
    
    @IBOutlet weak var pickerTopView: UIView!
    
    @IBOutlet weak var pickerTitleLabel: UILabel!
    
    @IBOutlet weak var pickerCancelButton: UIButton!
    
    @IBOutlet weak var pickerSelectButton: UIButton!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var pickerTopViewBottomConstraint: NSLayoutConstraint!
        
//    var vehicleMakesAndModels = ["Hyundai":["Kona"], "Tata Motors":["Tigor","Nexon"], "MG":["ZS EV"]]
    
    var vehicleModels:[String] = []
    var vehicleMakes:[String] = []
    var isForType = false
    var isForMake = false
    var selectedType = ""
    var selectedMake = ""
    var selectedModel = ""
    var selectedTypeRowIndex = 0
    var selectedMakeRowIndex = 0
    var selectedModelRowIndex = 0
    var isFromProfile = false
    var isTypeOrMakeValueChanged = false
    
    lazy var viewModel: VehicleModelSelectViewModel = {
        return VehicleModelSelectViewModel()
    }()
    var loaderView:LoaderView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backView.cornerTheTop(radius: 30)
//        vehicleMakes = Array(vehicleMakesAndModels.keys)
        initViewModel()
        if isFromProfile {
            skipButton.isHidden = true
            backButton.isHidden = false
        } else {
            skipButton.isHidden = false
            backButton.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    func initViewModel() {
        
        //Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage: message)
                }
            }
        }
        
        viewModel.vehicleManufacturerResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process vehicle manufacturer response
                self?.loaderView?.hideLoader()
                self?.showPreviousSelectedVehicleDetails()
                //Save vehicle manufacturer result
            }
        }
        
        viewModel.vehicleModelsResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process vehicle manufacturer response
                self?.loaderView?.hideLoader()
                self?.showVehicleModelDetails()
                //Save vehicle manufacturer result
            }
        }
        
        viewModel.isLoadingChangedClosure = { () in
            
        }
        
        loaderView = LoaderView.shared
        loaderView?.showLoader()
        viewModel.getVehicleManufacturerAPICall()
    }
    
    func showPreviousSelectedVehicleDetails(){
        if isFromProfile {
            if let vehicleType = UserDefaults.standard.object(forKey: UserDefaults.Keys.vehicleType) as? String {
                if viewModel.arrayOfVehicleTypes.contains(vehicleType) {
                    selectedType = vehicleType
                    typeTextField.text = vehicleType
                    selectedTypeRowIndex = viewModel.arrayOfVehicleTypes.firstIndex(of: vehicleType) ?? 0
                } else {
                    selectedType = vehicleType
                    typeTextField.text = vehicleType
                }
                loaderView = LoaderView.shared
                loaderView?.showLoader()
                viewModel.getVehicleModelsAPICall(arrayOfManufactureIds: [], arrayOfTypes: [selectedType])
            }
            
            if let vehicleMake = UserDefaults.standard.object(forKey: UserDefaults.Keys.vehicleMake) as? String {
                
                vehicleMakes = viewModel.listOfVehicleTypes[selectedType] ?? []
                if vehicleMakes.contains(vehicleMake) {
                    selectedMake = vehicleMake
                    makeTextField.text = vehicleMake
                    selectedMakeRowIndex = vehicleMakes.firstIndex(of: vehicleMake) ?? 0
                } else {
                    selectedMake = vehicleMake
                    makeTextField.text = vehicleMake
                }
            }
        }
    }
    
    func showVehicleModelDetails(){
        if isFromProfile && !isTypeOrMakeValueChanged {
            if let vehicleModel = UserDefaults.standard.object(forKey: UserDefaults.Keys.vehicleModel) as? String {
                vehicleModels = viewModel.listOfVehicleModels[selectedType] ?? []
                if vehicleModels.contains(vehicleModel) {
                    selectedModel = vehicleModel
                    modelTextField.text = vehicleModel
                    selectedModelRowIndex = vehicleModels.firstIndex(of: vehicleModel) ?? 0
                } else {
                    selectedModel = vehicleModel
                    modelTextField.text = vehicleModel
                }
            }
        }
    }

    func showTypePickerView() {
        isForType = true
        pickerTitleLabel.text = "Select Type"
        pickerView.reloadAllComponents()
        pickerView.selectRow(selectedTypeRowIndex, inComponent: 0, animated: true)
        showPickerView()
    }
    
    func showMakePickerView() {
        if selectedType.count > 0 {
            isForType = false
            isForMake = true
            pickerTitleLabel.text = "Select Make"
            vehicleMakes = viewModel.listOfVehicleTypes[selectedType] ?? []
            pickerView.reloadAllComponents()
            pickerView.selectRow(selectedMakeRowIndex, inComponent: 0, animated: true)
            showPickerView()
        } else {
            showAlertViewController(title: "Alert!", message: "Please select type")
        }
    }
    
    func showModelPickerView() {
        
        if selectedMake.count > 0 {
            isForType = false
            isForMake = false
            pickerTitleLabel.text = "Select Model"
            vehicleModels = viewModel.listOfVehicleModels[selectedType] ?? []
            pickerView.reloadAllComponents()
            pickerView.selectRow(selectedModelRowIndex, inComponent: 0, animated: true)
            showPickerView()
        } else {
            showAlertViewController(title: "Alert!", message: "Please select make")
        }
    }
    
    
    func showPickerView() {
        self.pickerTopViewBottomConstraint.constant = 0

        UIView.animate(withDuration:0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func hidePickerView() {
        self.pickerTopViewBottomConstraint.constant = -250

        UIView.animate(withDuration:0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func typeButtonAction(_ sender: Any) {
        
        showTypePickerView()
    }
    
    @IBAction func makeButtonAction(_ sender: Any) {
        
        showMakePickerView()
    }
    
    @IBAction func modelButtonAction(_ sender: Any) {
        
        showModelPickerView()
    }
    
    
    @IBAction func pickerCancelButtonAction(_ sender: Any) {
        
        hidePickerView()
    }
    
    @IBAction func pickerSelectButtonAction(_ sender: Any) {
        
        if isForType {
            if pickerView.selectedRow(inComponent: 0) != selectedTypeRowIndex {
                selectedMakeRowIndex = 0
                selectedModelRowIndex = 0
                makeTextField.text = ""
                selectedMake = ""
                modelTextField.text = ""
                selectedModel = ""
                isTypeOrMakeValueChanged = true
            }
            selectedTypeRowIndex = pickerView.selectedRow(inComponent: 0)
            selectedType = viewModel.arrayOfVehicleTypes[pickerView.selectedRow(inComponent: 0)]
            typeTextField.text = selectedType
            
        } else if isForMake {
            
            if pickerView.selectedRow(inComponent: 0) != selectedMakeRowIndex {
                selectedModelRowIndex = 0
                modelTextField.text = ""
                selectedModel = ""
                isTypeOrMakeValueChanged = true
            }
            selectedMakeRowIndex = pickerView.selectedRow(inComponent: 0)
            selectedMake = vehicleMakes[pickerView.selectedRow(inComponent: 0)]
            makeTextField.text = selectedMake
            if (modelTextField.text?.isEmpty)! {
                loaderView = LoaderView.shared
                loaderView?.showLoader()
                viewModel.getVehicleModelsAPICall(arrayOfManufactureIds: [], arrayOfTypes: [selectedType])
            }
            
        } else {//For Vehicle Model
            selectedModelRowIndex = pickerView.selectedRow(inComponent: 0)
            selectedModel = vehicleModels[pickerView.selectedRow(inComponent: 0)]
            modelTextField.text = selectedModel
        }
        
        hidePickerView()
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
        self.moveToPlugTypeSelectVC();
    }
    
    
    @IBAction func selectButtonAction(_ sender: Any) {
        //Check and save make and model data
        if selectedMake.count > 0 && selectedModel.count > 0 {
            
            UserDefaults.standard.set("CCS-2", forKey: UserDefaults.Keys.plugType)
            UserDefaults.standard.set(selectedType, forKey: UserDefaults.Keys.vehicleType)
            UserDefaults.standard.set(selectedMake, forKey: UserDefaults.Keys.vehicleMake)
            UserDefaults.standard.set(selectedModel, forKey: UserDefaults.Keys.vehicleModel)
            if isFromProfile {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.moveToHomeScreen()
            }
        } else {
            showAlertViewController(title: "Alert!", message: "Please select make and model")
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToHomeScreen() {
        
        let tabbarVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "TabbarIdentifier") as?  UITabBarController
        tabbarVC?.modalPresentationStyle = .fullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(tabbarVC!, animated: true)
        } else {
            self.present(tabbarVC!, animated: false, completion: nil)
        }
    }
    
    func moveToPlugTypeSelectVC() {
        let plugTypeSelectVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "PlugTypeSelectVC") as? PlugTypeSelectVC
        plugTypeSelectVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(plugTypeSelectVC!, animated: true)
        } else {
            self.present(plugTypeSelectVC!, animated: false, completion: nil)
        }
    }
    
    func showAlertViewController(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                print("ok Action" )
            }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension VehicleModelSelectVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if isForType {
            return viewModel.arrayOfVehicleTypes.count
        } else if isForMake {
            return vehicleMakes.count
        } else {
            return vehicleModels.count
        }
    }
        
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if isForType {
            return viewModel.arrayOfVehicleTypes[row]
        } else if isForMake {
            return vehicleMakes[row]
        } else {
            return vehicleModels[row]
        }
    }
        
}
