//
//  ForgotPasswordVC.swift
//  ElectricCharge
//
//  Created by Raghavendra on 27/06/22.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift

class ForgotPasswordVC:UIViewController {
    
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var getOTPButton: LoadingButton!
    
    @IBOutlet weak var emailOrPhoneNumberTF: UITextField!
    
    @IBOutlet weak var emailOrPhoneNumberTFCheckMark: UIButton!

    lazy var viewModel: ForgotPasswordViewModel = {
        return ForgotPasswordViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.backView.cornerTheTop(radius: 30)
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.getOTPButton.hideLoading()
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage: message)
                }
            }
        }
        
        viewModel.forgetPasswordResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process forget password response
                self?.getOTPButton.hideLoading()
                if let userId = self?.viewModel.forgetPasswordResponse.data?.id {
                    self?.moveToOTPScreen(userId: userId)
                } else if let error = self?.viewModel.forgetPasswordResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                }
            }
        }
    }
    
    func updateTheButtonColor(text:String) {
       
        if text.isNumeric && text.isValidMobileNumber {
            emailOrPhoneNumberTFCheckMark.tintColor = UIColor(named: "2ECE54")
        } else if  text.isValidEmail() {
            emailOrPhoneNumberTFCheckMark.tintColor = UIColor(named: "2ECE54")
        } else {
            emailOrPhoneNumberTFCheckMark.tintColor = UIColor(named: "DDDDDD")
        }
    }
    
    func moveToOTPScreen(userId:String) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as? OTPVC
        vc?.isFromForgotPasswordVC = true
        vc?.forgotPasswordUserId = userId
        vc?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getOTPButtonAction(_ sender: Any) {
        
        if !checkEmailOrPhoneNumber() {
            
        } else {
            checkTheValidation()
        }
    }
    
    func checkEmailOrPhoneNumber() -> Bool {
        
        var isFieldValid = true
        
        if emailOrPhoneNumberTF.text?.isEmpty ?? true {//For Email or Phone Number
            isFieldValid = false
            self.displayMessageAlert(withMessage:Constants.InputFields.MissingEmailOrPhoneNumber)
        } else {
            if (emailOrPhoneNumberTF.text ?? "").isNumeric {//Phone Number
                if !(emailOrPhoneNumberTF.text ?? "").isValidMobileNumber {
                    isFieldValid = false
                    self.displayMessageAlert(withMessage:Constants.InputFields.InvalidPhoneNumber)
                }
            } else if !(emailOrPhoneNumberTF.text ?? "").isValidEmail() {//Email
                isFieldValid = false
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEmail)
            }
        }
        return isFieldValid
    }
    
    func checkTheValidation() {
        getOTPButton.showLoading()
        viewModel.forgetPasswordApiCall(userName: emailOrPhoneNumberTF.text ?? "")
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPasswordVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            updateTheButtonColor(text: updatedText)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 0 {
            self.emailOrPhoneNumberTF.becomeFirstResponder()
        }
        return true
    }
}
