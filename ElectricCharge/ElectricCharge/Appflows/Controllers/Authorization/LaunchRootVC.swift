//
//  LaunchRootVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit
import Lottie

class LaunchRootVC: UIViewController {
    
    //@IBOutlet weak var animateView: AnimationView!
    //@IBOutlet weak var animatedView: UIView!
//    let animateView = AnimationView(name: "quikplugs_splash")
    var locationManager = LocationManager.shared
    @IBOutlet weak var animateImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
//        self.addAnimationView()
        animateImageView.image = UIImage.gifImageWithName("animation_splash")
        // Do any additional setup after loading the view.
    }
    
//    func addAnimationView() {
//        //animateView.backgroundColor = .clear
//        //animateView.loopMode = .loop
//        //animateView.scalesLargeContentImage = true
//        //animateView.contentMode = .scaleAspectFit
//        //animateView.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
//        //animateView.frame = animatedView.bounds//CGRect(x: 0, y: 0, width: animatedView.frame.size.width, height: animatedView.frame.size.height)
//        //animatedView.addSubview(animateView)
//        //animateView.play()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.moveToNextView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    func moveToNextView() {
                
        if let sessionToken = UserDefaults.standard.object(forKey: UserDefaults.Keys.sessionToken) as? String, sessionToken.count > 0 {
            
            if let plugType = UserDefaults.standard.string(forKey: UserDefaults.Keys.plugType), plugType.count > 0 {
            
                let tabbarVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "TabbarIdentifier") as?  UITabBarController
                tabbarVC?.modalPresentationStyle = .fullScreen
                if self.navigationController != nil {
                    self.navigationController?.pushViewController(tabbarVC!, animated: true)
                } else {
                    self.present(tabbarVC!, animated: false, completion: nil)
                }
            } else {
                
                let vehicleModelSelectVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "VehicleModelSelectVC") as? VehicleModelSelectVC
                vehicleModelSelectVC?.modalPresentationStyle = .overFullScreen
                if self.navigationController != nil {
                    self.navigationController?.pushViewController(vehicleModelSelectVC!, animated: true)
                } else {
                    self.present(vehicleModelSelectVC!, animated: false, completion: nil)
                }
            }
            
        } else {
                        
            let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "AuthorizationVC") as? AuthorizationVC
            vc?.modalPresentationStyle = .overFullScreen
            if self.navigationController != nil {
                self.navigationController?.pushViewController(vc!, animated: true)
            } else {
                self.present(vc!, animated: false, completion: nil)
            }
        }
    }
}

extension LaunchRootVC:LocationManagerProtocol {
    func locationManagerDidChangeAuthorization(status: LocationAuthorizationStatus) {
   
        switch status {
            case .authorizedWhenInUse, .authorizedAlways:
                locationManager.startUpdatingLocation()
                break
            case .restricted, .denied:
                showAlertViewController(title: Constants.Alert.title, message: Constants.Alert.locationEnable)
                break
            default:
                break
        }
    }
    
    func showAlertViewController(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                print("ok Action" )
            }
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        
        alert.addAction(okAction)
        alert.addAction(settingsAction)
        
        self.present(alert, animated: true)
    }
}




