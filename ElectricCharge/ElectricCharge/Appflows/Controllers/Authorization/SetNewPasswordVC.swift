//
//  SetNewPasswordVC.swift
//  ElectricCharge
//
//  Created by Raghavendra on 27/06/22.
//

import Foundation
import UIKit

class SetNewPasswordVC:UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var resetPasswordButton: LoadingButton!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet weak var passwordTFCheckMark: UIButton!
    
    @IBOutlet weak var confirmPasswordTFCheckMark: UIButton!
    
    var forgotPasswordUserId = ""
    
    lazy var viewModel: SetNewPasswordViewModel = {
        return SetNewPasswordViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.backView.cornerTheTop(radius: 30)
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.resetPasswordButton.hideLoading()
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert(message, false)
                }
            }
        }
        
        viewModel.setNewPasswordResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process forget password response
                self?.resetPasswordButton.hideLoading()
                if let success = self?.viewModel.setNewPasswordResponse.success, success, let message = self?.viewModel.setNewPasswordResponse.message {
                    self?.showAlert(message, true)
                } else if let error = self?.viewModel.setNewPasswordResponse.error {
                    self?.showAlert(error, false)
                }
            }
        }
    }
    
    func showAlert( _ message: String, _ customAction:Bool ) {
        let alert = UIAlertController(title: Constants.Alert.title, message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: { action in
            if customAction {
                self.popBack(4)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.popBack(3)
    }
    
    @IBAction func resetPasswordButtonAction(_ sender: Any) {
        if passwordTF.text?.isEmpty ?? false {
            showAlert(Constants.InputFields.MissingPassword, false)
        } else if confirmPasswordTF.text?.isEmpty ?? false {
            showAlert(Constants.InputFields.MissingConfirmPassword, false)
        } else if confirmPasswordTF.text != passwordTF.text {
            showAlert(Constants.InputFields.MismatchPasswordAndConfirmPassword, false)
        } else {
            checkTheValidation()
        }
    }
        
    @IBAction func textFieldStatusButtonAction(_ sender: Any) {
        
        if let button = sender as? UIButton {
            
            if button.tag == 0 {//Password Field
                if passwordTF.isSecureTextEntry {
                    passwordTF.isSecureTextEntry = false
                } else {
                    passwordTF.isSecureTextEntry = true
                }
            } else {//Confirm Password Field
                
                if confirmPasswordTF.isSecureTextEntry {
                    confirmPasswordTF.isSecureTextEntry = false
                } else {
                    confirmPasswordTF.isSecureTextEntry = true
                }
            }
        }
    }
    
    func checkTheValidation() {
        resetPasswordButton.showLoading()
        viewModel.setNewPasswordApiCall(userId: forgotPasswordUserId, password: passwordTF.text ?? "")
    }
}

extension SetNewPasswordVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                
        if textField.tag == 0 {
            self.confirmPasswordTF.becomeFirstResponder()
        } else if textField.tag == 1 {
            textField.resignFirstResponder()
        }
        return true
    }
}
