//
//  AuthNavigator.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 24/10/21.
//

import Foundation
import UIKit

class AuthNavigator {
    class func moveToSigninOTPVC(presentVC: UIViewController) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "OTPVC") as? OTPVC
        vc?.modalPresentationStyle = .fullScreen
        if  presentVC.navigationController != nil {
            presentVC.navigationController?.pushViewController(vc!, animated: true)
        }else{
            presentVC.present(vc!, animated: true, completion: nil)
        }
    }
}
