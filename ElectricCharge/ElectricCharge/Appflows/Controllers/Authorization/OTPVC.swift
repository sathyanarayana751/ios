//
//  OTPVc.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 24/10/21.
//

import UIKit

class OTPVC: UIViewController {
    @IBOutlet weak var backButtonVC: UIButton!
    @IBOutlet weak var vwOtpFields: VPMOTPView!
    @IBOutlet weak var resend: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var verifyBtn: LoadingButton!
    
    var count = 60
    var timer: Timer?
    var hasEnteredOTP = false
    var presentAuthType:AuthType = .login
    var userName = ""
    var userEmail = ""
    var userPhoneNumber = ""
    var isFromForgotPasswordVC = false
    var forgotPasswordUserId = ""

    @IBOutlet weak var backView: UIView!
    
    var verificationCode = ""
    lazy var viewModel: OTPViewModel = {
        return OTPViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTheOTPView()
        self.backView.cornerTheTop(radius: 30)
        initViewModel()
        if !isFromForgotPasswordVC {
            sendOTP()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func setUpTheOTPView() {
        vwOtpFields.otpFieldsCount = 6
        vwOtpFields.otpFieldInputType = .numeric
        vwOtpFields.otpFieldDisplayType = .square
        vwOtpFields.otpFieldBorderWidth = 1
        vwOtpFields.shouldAllowIntermediateEditing = false
        vwOtpFields.otpFieldEntrySecureType = false
        vwOtpFields.delegate = self
        vwOtpFields.initializeUI()
        showTheTimer()
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message, false )
                }
            }
        }
        
        viewModel.sendRegisterOTPResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process send OTP response
                self?.verifyBtn.hideLoading()
                if let error = self?.viewModel.sendRegisterOTPResponse.error {
                    self?.showAlert(error, false)
                }
            }
        }
        
        viewModel.sendLoginOTPResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process send OTP response
                self?.verifyBtn.hideLoading()
                if let error = self?.viewModel.sendLoginOTPResponse.error {
                    self?.showAlert(error, true)
                }
            }
        }
        
        viewModel.verifyRegisterOTPResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process verify OTP response
                self?.verifyBtn.hideLoading()
                                            
                if let token = self?.viewModel.verifyRegisterOTPResponse.data?.token, let user = self?.viewModel.verifyRegisterOTPResponse.data?.user {
                    
                    UserDefaults.standard.saveUserDetails(user: UserDetailsModel(registerUserData: user, sessionToken: token))
                    self?.moveToVehicleModelSelectVC()
                    
                } else if let error = self?.viewModel.verifyRegisterOTPResponse.error {
                    self?.showAlert(error, false)
                }
            }
        }
        
        viewModel.verifyLoginOTPResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process verify OTP response
                self?.verifyBtn.hideLoading()
                if (self?.viewModel.verifyLoginOTPResponse.data?.resetToken) != nil {
                    self?.moveToSetNewPasswordVC()
                } else if let error = self?.viewModel.verifyLoginOTPResponse.error {
                    self?.showAlert(error, true)
                }
            }
        }
    }
    
    func showAlert( _ message: String, _ customAction:Bool ) {
        let alert = UIAlertController(title: Constants.Alert.title, message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: { action in
            if customAction {
                self.navigationController?.popViewController(animated: true)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func resendOtp(_ sender: Any) {
        self.timer = nil
        showTheTimer()
        sendOTP()
    }
    
    func showTheTimer() {
        if timer == nil {
            self.count = 60
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }
    
    @objc func update() {
        if count > 0 {
            count -= 1
            let minutes = String(format: "%02d", (count / 60))
            let seconds = String(format: "%02d", (count % 60))
            timerLabel.text = minutes + ":" + seconds
            timerLabel.isHidden = false
            resend.isHidden = true
        } else {
            timerLabel.text = "00:00"
            timerLabel.isHidden = true
            resend.isHidden = false
            if timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func verifyButtonAction(_ sender: Any) {
        
        if hasEnteredOTP && verificationCode.count > 0 {
                      
            if isFromForgotPasswordVC {
                if forgotPasswordUserId.count > 0 {
                    viewModel.verifyLoginOTPCall(userId: forgotPasswordUserId, otp: verificationCode)
                } else {
                    showAlert(Constants.InputFields.MissingUserId, false)
                }
            } else {
                if let userId = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
                    viewModel.verifyRegisterOTPCall(userId: userId, otp: verificationCode)
                } else {
                    showAlert(Constants.InputFields.MissingUserId, false)
                }
            }
        } else {
            showAlert(Constants.InputFields.MissingOTP, false)
        }
    }
    
    func sendOTP() {
        
        if isFromForgotPasswordVC {
            viewModel.resendLoginOTPCall(userId: forgotPasswordUserId)
        } else if let userId = UserDefaults.standard.object(forKey: UserDefaults.Keys.userId) as? String {
            viewModel.sendRegisterOTPCall(userId: userId)
        }
    }
    
    func moveToVehicleModelSelectVC() {
        let vehicleModelSelectVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "VehicleModelSelectVC") as? VehicleModelSelectVC
        vehicleModelSelectVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vehicleModelSelectVC!, animated: true)
        } else {
            self.present(vehicleModelSelectVC!, animated: false, completion: nil)
        }
    }
    
    func moveToSetNewPasswordVC() {
        let setNewPasswordVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "SetNewPasswordVC") as? SetNewPasswordVC
        setNewPasswordVC?.forgotPasswordUserId = forgotPasswordUserId
        setNewPasswordVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(setNewPasswordVC!, animated: true)
        } else {
            self.present(setNewPasswordVC!, animated: false, completion: nil)
        }
    }
}


extension OTPVC: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered {
            hasEnteredOTP = true
        } else {
            hasEnteredOTP = false
        }
        return hasEntered
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        verificationCode = otpString
        print("OTPString: \(otpString)")
    }
}
