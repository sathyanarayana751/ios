//
//  PlugTypeSelectVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 16/11/21.
//

import UIKit

class PlugTypeSelectVC: UIViewController {
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var colView: UICollectionView!
    
    @IBOutlet weak var selectButton: LoadingButton!
    
    /*var plugType:[PortTypesModel] = [][["type":"CCS2","typeImage":"type2",  "bgColor":"C3C7FF"],
                    ["type":"Chademo","typeImage":"type1",  "bgColor":"FFD6D6"],
                    ["type":"Type1","typeImage":"type3",  "bgColor":"C3FFCF"],
                    ["type":"Type2","typeImage":"type4",  "bgColor":"FFDCC3"],
                    ["type":"Tesla","typeImage":"type2",  "bgColor":"C3C7FF"],
                    ["type":"GBT","typeImage":"type1",  "bgColor":"FFD6D6"]]*/
    var selectedItem = -1
    lazy var viewModel: PlugTypeSelectViewModel = {
        return PlugTypeSelectViewModel()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        self.backView.cornerTheTop(radius: 30)
        // Do any additional setup after loading the view.
    }
    
    func initViewModel() {
        
        //Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage: message)
                }
            }
        }
        
        viewModel.portTypesResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process port types response
                if (self?.viewModel.isLoading)! {
                    self?.viewModel.isLoading = false
                } else {
                    self?.colView.reloadData()
                }
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.colView.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.colView.reloadData()
            }
        }
        viewModel.isLoading = true
        viewModel.getPortTypesAPICall()
    }
    
    func enableOrDisableSelectButton(_ enable:Bool) {
        
        if enable {
            selectButton.isEnabled = true
            selectButton.setTitleColor(UIColor.white, for: .normal)
            selectButton.backgroundColor = UIColor(hexString: "#2ECE54")
        } else {
            selectButton.isEnabled = false
            selectButton.setTitleColor(UIColor.lightText, for: .normal)
            selectButton.backgroundColor = UIColor.systemGray4
        }
    }
    
    
    @IBAction func plugSelection(_ sender: Any) {
        
        if selectedItem >= 0 {
            
            UserDefaults.standard.set(viewModel.portTypesResponse[selectedItem].name, forKey: UserDefaults.Keys.plugType)
            self.moveToHomeScreen()
            
        } else {
            
            showAlertViewController(title: Constants.Alert.title, message: Constants.InputFields.MissingPlugType)
        }
    }
    
    func showAlertViewController(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                print("ok Action" )
            }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveToHomeScreen() {
                
        let tabbarVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "TabbarIdentifier") as?  UITabBarController
        tabbarVC?.modalPresentationStyle = .fullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(tabbarVC!, animated: true)
        } else {
            self.present(tabbarVC!, animated: false, completion: nil)
        }
    }
    
}

extension PlugTypeSelectVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.isLoading {
            self.enableOrDisableSelectButton(false)
            return 5
        } else {
            self.enableOrDisableSelectButton(true)
            return viewModel.portTypesResponse.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlugTypeColViewCell", for: indexPath) as? PlugTypeColViewCell
        
        if !viewModel.isLoading {
            cell?.plugTypeImg.image = UIImage(named: viewModel.portTypesResponse[indexPath.row].typeImage!)
            cell?.lblType.text = viewModel.portTypesResponse[indexPath.row].name
            var color = ""
            
            /*[["type":"CCS2","typeImage":"type2",  "bgColor":"C3C7FF"],
             ["type":"Chademo","typeImage":"type1",  "bgColor":"FFD6D6"],
             ["type":"Type1","typeImage":"type3",  "bgColor":"C3FFCF"],
             ["type":"Type2","typeImage":"type4",  "bgColor":"FFDCC3"],
             ["type":"Tesla","typeImage":"type2",  "bgColor":"C3C7FF"],
             ["type":"GBT","typeImage":"type1",  "bgColor":"FFD6D6"]]*/
            
            switch (indexPath.row % 4) {
            case 0:
                color = "C3C7FF"
            case 1:
                color = "FFD6D6"
            case 2:
                color = "C3FFCF"
            case 3:
                color = "FFDCC3"
            default:
                color = "C3C7FF"
                
            }
            
            cell?.bgView.backgroundColor = UIColor(named: color)
            
            if selectedItem == indexPath.row {
                cell?.radioButton.isSelected = true
            } else {
                cell?.radioButton.isSelected = false
            }
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !viewModel.isLoading {
            selectedItem  = indexPath.row
            colView.reloadData()
        }
    }
}
