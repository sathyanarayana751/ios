//
//  AuthorizationVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 25/10/21.
//

import UIKit
import IQKeyboardManagerSwift
enum AuthType {
    case login
    case signup
    case corporateLogin
}

class AuthorizationVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var lblBottomTxt1: UILabel!
    @IBOutlet weak var lblBottomTxt2: UILabel!
    
    
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var bottomStackView: UIStackView!
    
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var lblViewtitle: UILabel!
    
    
    @IBOutlet weak var firstTF: UITextField!
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var fourthTF: UITextField!
    
    
    
    @IBOutlet weak var corporateBtn: UIButton!
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var lblCorporate: UILabel!
    @IBOutlet weak var third: UIView!
    @IBOutlet weak var heightOfCorporateVC: NSLayoutConstraint! //55, 109
    @IBOutlet weak var corporateView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var fourthView: UIView!
    
    @IBOutlet weak var signupBtn: LoadingButton!
    
    @IBOutlet weak var dontHaveAccountView: UIView!
    @IBOutlet weak var socialLoginTopView: UIView!
    @IBOutlet weak var socialLoginView: UIView!
    @IBOutlet weak var loginBtnView: UIView!
    var presentAuthType: AuthType = .login
    var previousAuthType: AuthType = .login
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var heightOfScrollHalfView: NSLayoutConstraint!
    
    @IBOutlet var checkBtns: [UIButton]!
    
    @IBOutlet weak var dontHaveAccountSignupButton: UIButton!
    
    
    var isCorporate = false
    lazy var viewModel: AuthorizationViewModel = {
        return AuthorizationViewModel()
    }()
    
    lazy var googleSignInManager: GoogleSignInManager = {
        return GoogleSignInManager()
    }()
    
    lazy var facebookSignInManager: FacebookSignInManager = {
        return FacebookSignInManager()
    }()
    
    var isForSocialSignUpFlow = false
    var socialLoginToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTheCurrentTestFieldPlaholders()
        backBtn.isHidden = true
        initViewModel()
        initSocialManagers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.vwBack.cornerTheTop(radius: 30)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.animate(withDuration: 0.5) {
                self.heightOfScrollHalfView.constant = Constants.KDevice.height - self.vwBack.frame.size.height < 250 ? 250 : Constants.KDevice.height - self.vwBack.frame.size.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func initSocialManagers() {
        
        // Naive binding
        googleSignInManager.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.signupBtn.hideLoading()
                if let message = self?.googleSignInManager.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        facebookSignInManager.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.signupBtn.hideLoading()
                if let message = self?.facebookSignInManager.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        googleSignInManager.signInResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process social login response
                if let token = self?.googleSignInManager.signInResponse?.token {
                    //TODO: Call Social Login API using this token
                    self?.socialLoginToken = token
                    self?.viewModel.socialLoginApiCall(token: token)
                } else {
                    self?.signupBtn.hideLoading()
                }
            }
        }
        
        facebookSignInManager.signInResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process social login response
                if let token = self?.facebookSignInManager.signInResponse?.token {
                    //TODO: Call Social Login API using this token
                    self?.socialLoginToken = token
                    self?.viewModel.socialLoginApiCall(token: token)
                } else {
                    self?.signupBtn.hideLoading()
                }
            }
        }
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.signupBtn.hideLoading()
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.loginResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process login response
                self?.signupBtn.hideLoading()
                if let token = self?.viewModel.loginResponse.data?.token, let user = self?.viewModel.loginResponse.data?.user {
                    UserDefaults.standard.saveUserDetails(user: UserDetailsModel(loginUserData: user, sessionToken: token))
                    self?.moveToVehicleModelSelectVC()
                    
                } else if let error = self?.viewModel.loginResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                }
            }
        }
        
        viewModel.registerResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process register response
                self?.signupBtn.hideLoading()
                                            
                if let userId = self?.viewModel.registerResponse.data?.id {
                    
                    UserDefaults.standard.set(userId, forKey: UserDefaults.Keys.userId)
                    self?.moveToOtpScreen()
                    
                } else if let error = self?.viewModel.registerResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                }
            }
        }
        
        viewModel.socialLoginResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.signupBtn.hideLoading()
                if let userExists = self?.viewModel.socialLoginResponse.data?.userExists {
                    if userExists {//Social login user already exists
                        if let token = self?.viewModel.socialLoginResponse.data?.token, let user = self?.viewModel.socialLoginResponse.data?.user {
                                                        
                            UserDefaults.standard.saveUserDetails(user: UserDetailsModel(socialLoginUserExistData: user, sessionToken: token))
                            
                            self?.moveToVehicleModelSelectVC()
                            
                        } else if let error = self?.viewModel.socialLoginResponse.error {
                            self?.displayMessageAlert(withMessage:error)
                        }
                    } else {
                        //TODO: Peform signup flow with google login response
                        self?.displayMessageAlert(withMessage:Constants.Messages.SocialLoginUserNotExist)
                        if let googleSignInResponse = self?.googleSignInManager.signInResponse {
                            self?.showSocialSignupFlow(userDetails: UserDetailsModel(googleSignInResponse: googleSignInResponse))
                        }
                    }
                } else if let error = self?.viewModel.socialLoginResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                }
            }
        }
        
        viewModel.socialLoginUpdateDetailsResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process Social Login Update Details response
                self?.signupBtn.hideLoading()
                                            
                if let userId = self?.viewModel.socialLoginUpdateDetailsResponse.data?.id {
                    
                    UserDefaults.standard.set(userId, forKey: UserDefaults.Keys.userId)
                    self?.moveToOtpScreen()
                    
                } else if let error = self?.viewModel.registerResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                }
            }
        }
    }
    
    func showSocialSignupFlow(userDetails:UserDetailsModel){
        
        self.isForSocialSignUpFlow = true
        self.signupButton(self.dontHaveAccountSignupButton!)
        DispatchQueue.main.async {
            if let username = userDetails.name {
                self.firstTF.text = username
                self.updateTheButtonColor(textFieldTag: self.firstTF.tag, text: username)
            }
            
            if let email = userDetails.email {
                self.secondTF.text = email
                self.updateTheButtonColor(textFieldTag: self.secondTF.tag, text: email)
            }
            
            if let phone = userDetails.phone {
                self.thirdTF.text = phone
                self.updateTheButtonColor(textFieldTag: self.thirdTF.tag, text: phone)
            }
        }
        
    }
    
    @IBAction func corporateTap(_ sender: Any) {
        if presentAuthType != .corporateLogin {
            previousAuthType = presentAuthType
            presentAuthType = .corporateLogin
            updateTheView()
        }
    }
    
    func updateTheView() {
        
        DispatchQueue.main.async {
            
            self.updateTheCurrentTestFieldPlaholders()
            
            switch self.presentAuthType {
            case .signup:
                self.lblViewtitle.fadeTransition(0.5)
                self.lblViewtitle.text = "Sign Up"
                self.signupBtn.setTitle("Sign Up", for: .normal)
                self.lblPageTitle.fadeTransition(0.5)
                self.lblPageTitle.text = "Sign up to find Charging Stations."
                self.backBtn.isHidden = false
                self.lblBottomTxt1.fadeTransition(0.5)
                self.lblBottomTxt1.text =  "Already have an account?"
                self.lblBottomTxt2.fadeTransition(0.5)
                self.lblBottomTxt2.text =  "Login"
                
                self.lblCorporate.font  = UIFont(name: AppFonts.bold, size: 24)
                self.corporateBtn.setTitleColor(UIColor(named: "2ECE54"), for: .normal)
                
                UIView.animate(withDuration: 0.8) {
                    
                    self.stackVw.removeArrangedSubview(self.loginView)
                    self.stackVw.removeArrangedSubview(self.third)
                    self.stackVw.removeArrangedSubview(self.fourthView)
                    self.stackVw.removeArrangedSubview(self.corporateView)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()
                    
                    self.bottomStackView.removeArrangedSubview(self.socialLoginTopView)
                    self.bottomStackView.removeArrangedSubview(self.socialLoginView)
                    self.bottomStackView.removeArrangedSubview(self.dontHaveAccountView)
                    self.bottomStackView.setNeedsLayout()
                    self.bottomStackView.layoutIfNeeded()
                    
                    self.lblCorporate.font  = UIFont(name: AppFonts.semiBold, size: 12)
                    self.lblCorporate.layoutIfNeeded()
                    
                    self.stackVw.insertArrangedSubview(self.loginView, at: 0)
                    self.stackVw.insertArrangedSubview(self.third, at: 2)
                    self.stackVw.insertArrangedSubview(self.fourthView, at: 3)
                    self.stackVw.insertArrangedSubview(self.corporateView, at: 4)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()
                    
                    self.bottomStackView.insertArrangedSubview(self.dontHaveAccountView, at: 0)
                    self.bottomStackView.setNeedsLayout()
                    self.bottomStackView.layoutIfNeeded()

                    self.third.isHidden  = false
                    self.fourthView.isHidden  = false

                    self.socialLoginTopView.isHidden  = true
                    self.socialLoginView.isHidden  = true
                    
                    self.bottomStackView.isHidden = false
                    self.lblForgotPassword.isHidden = true
                    self.view.layoutIfNeeded()
                    
                } completion: { (_) in
                    
                    UIView.animate(withDuration: 0.5) {
                        self.heightOfScrollHalfView.constant = Constants.KDevice.height - self.vwBack.frame.size.height < 250 ? 250 : Constants.KDevice.height - self.vwBack.frame.size.height
                        self.view.layoutIfNeeded()
                    }
                }
                break
            case .login:
                self.lblViewtitle.fadeTransition(0.5)
                self.lblViewtitle.text = "Login"
                self.signupBtn.setTitle("Login", for: .normal)
                self.lblPageTitle.fadeTransition(0.5)
                self.lblPageTitle.text = "Login to find Charging Stations."
                self.lblBottomTxt1.fadeTransition(0.5)
                self.lblBottomTxt1.text =  "Don’t have an account?"
                self.lblBottomTxt2.fadeTransition(0.5)
                self.lblBottomTxt2.text =  "Sign up"
                
                self.backBtn.isHidden = true
                
                self.lblCorporate.font = UIFont(name: AppFonts.bold, size: 24)
                self.corporateBtn.setTitleColor(UIColor(named: "2ECE54"), for: .normal)
            
                UIView.animate(withDuration: 0.8) {
                    
                    self.stackVw.removeArrangedSubview(self.loginView)
                    self.stackVw.removeArrangedSubview(self.corporateView)
                    self.stackVw.removeArrangedSubview(self.third)
                    self.stackVw.removeArrangedSubview(self.fourthView)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()
                    
                    self.bottomStackView.removeArrangedSubview(self.socialLoginTopView)
                    self.bottomStackView.removeArrangedSubview(self.socialLoginView)
                    self.bottomStackView.removeArrangedSubview(self.dontHaveAccountView)
                    self.bottomStackView.setNeedsLayout()
                    self.bottomStackView.layoutIfNeeded()
                    
                    self.lblCorporate.font  = UIFont(name: AppFonts.semiBold, size: 12)
                    self.lblCorporate.layoutIfNeeded()
                    
                    self.stackVw.insertArrangedSubview(self.loginView, at: 0)
                    self.stackVw.insertArrangedSubview(self.corporateView, at: 2)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()
                    
                    self.bottomStackView.insertArrangedSubview(self.socialLoginTopView, at: 0)
                    self.bottomStackView.insertArrangedSubview(self.socialLoginView, at: 1)
                    self.bottomStackView.insertArrangedSubview(self.dontHaveAccountView, at: 2)
                    self.bottomStackView.setNeedsLayout()
                    self.bottomStackView.layoutIfNeeded()

                    self.third.isHidden  = true
                    self.fourthView.isHidden  = true
                    
                    self.socialLoginTopView.isHidden  = false
                    self.socialLoginView.isHidden  = false
                    
                    self.bottomStackView.isHidden = false
                    self.lblForgotPassword.isHidden = false
                    self.view.layoutIfNeeded()
                }completion: { (_) in
                    UIView.animate(withDuration: 0.5) {
                        self.heightOfScrollHalfView.constant = Constants.KDevice.height - self.vwBack.frame.size.height < 250 ? 250 : Constants.KDevice.height - self.vwBack.frame.size.height
                        self.view.layoutIfNeeded()
                    }
                }
                break
            case .corporateLogin:
                self.lblPageTitle.fadeTransition(0.5)
                self.signupBtn.setTitle("Login", for: .normal)
                self.lblPageTitle.text = "Login to find Charging Stations."
                self.backBtn.isHidden = false
                self.lblForgotPassword.isHidden  = true
                
                self.lblCorporate.font  = UIFont(name: AppFonts.semiBold, size: 12)
                self.corporateBtn.setTitleColor(UIColor(named: "2A382C"), for: .normal)
                
                UIView.animate(withDuration: 0.8) {
                    
                    self.stackVw.removeArrangedSubview(self.loginView)
                    self.stackVw.removeArrangedSubview(self.corporateView)
                    self.stackVw.removeArrangedSubview(self.third)
                    self.stackVw.removeArrangedSubview(self.fourthView)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()
                    
                    self.lblCorporate.font  = UIFont(name: AppFonts.bold, size: 24)
                    self.lblCorporate.layoutIfNeeded()
                    self.stackVw.insertArrangedSubview(self.corporateView, at: 0)
                    self.stackVw.insertArrangedSubview(self.third, at: 2)
                    self.stackVw.setNeedsLayout()
                    self.stackVw.layoutIfNeeded()

                    self.third.isHidden  = false
                    self.fourthView.isHidden  = true
                    self.bottomStackView.isHidden = true
                    self.view.layoutIfNeeded()
                    
                } completion: { (_) in
                    UIView.animate(withDuration: 0.5) {
                        self.heightOfScrollHalfView.constant = Constants.KDevice.height - self.vwBack.frame.size.height < 250 ? 250 : Constants.KDevice.height - self.vwBack.frame.size.height
                        self.view.layoutIfNeeded()
                    }
                }
                break
            }
            
        }
    }
    
    
    @IBAction func textFieldStatusButtonAction(_ sender: Any) {
        if let button = sender as? UIButton {
            switch presentAuthType {
                case .login:
                    if button.tag == 2 {//Password Field
                        if secondTF.isSecureTextEntry {
                            secondTF.isSecureTextEntry = false
                        } else {
                            secondTF.isSecureTextEntry = true
                        }
                    }
                    break
                case .corporateLogin:
                    if button.tag == 3 {//Password Field
                        if thirdTF.isSecureTextEntry {
                            thirdTF.isSecureTextEntry = false
                        } else {
                            thirdTF.isSecureTextEntry = true
                        }
                    }
                    break
                case .signup:
                    if button.tag == 4 {//Password Field
                        if fourthTF.isSecureTextEntry {
                            fourthTF.isSecureTextEntry = false
                        } else {
                            fourthTF.isSecureTextEntry = true
                        }
                    }
                    break
            }
        }
    }
    
    
    @IBAction func socialMediaLoginButtonAction(_ sender: Any) {
        
        signupBtn.showLoading()
        if let button = sender as? UIButton {
            if button.tag == 1 {//Google
                googleSignInManager.performSignIn(presentingVC: self)
            } else {//Facebook
                facebookSignInManager.performSignIn(presentingVC: self)
            }
        }
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        let forgotPasswordVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        forgotPasswordVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(forgotPasswordVC!, animated: true)
        } else {
            self.present(forgotPasswordVC!, animated: false, completion: nil)
        }
    }
    
    @IBAction func signupButton(_ sender: Any) {
        if presentAuthType == .signup {
            presentAuthType = .login
            previousAuthType = .login
        } else {
            previousAuthType = presentAuthType
            presentAuthType = .signup
        }
        updateTheView()
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if presentAuthType == .corporateLogin {
         
        } else if presentAuthType == .login {
            
            if !checkEmailOrPhoneNumber() {
                
            } else if secondTF.text?.isEmpty  ?? false {
                self.displayMessageAlert(withMessage:Constants.InputFields.MissingPassword)
            } else {
                checkTheValidation()
            }
            
        } else if presentAuthType == .signup {
            if firstTF.text?.isEmpty ?? false {
                self.displayMessageAlert(withMessage:Constants.InputFields.MissingUserName)
            } else if secondTF.text?.isEmpty ?? false {
                self.displayMessageAlert(withMessage:Constants.InputFields.MissingEmail)
            } else if !(secondTF.text ?? "").isValidEmail() {
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEmail)
            } else if thirdTF.text?.isEmpty ?? false {
                self.displayMessageAlert(withMessage:Constants.InputFields.MissingPhone)
            } else if !(thirdTF.text ?? "").isValidMobileNumber {
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidPhoneNumber)
            } else if fourthTF.text?.isEmpty  ?? false {
                self.displayMessageAlert(withMessage:Constants.InputFields.MissingPassword)
            } else {
                checkTheValidation()
            }
        }
    }
    
    func checkEmailOrPhoneNumber() -> Bool {
        
        var isFieldValid = true
        
        if firstTF.text?.isEmpty ?? true {//For Email or Phone Number
            isFieldValid = false
            self.displayMessageAlert(withMessage:Constants.InputFields.MissingEmailOrPhoneNumber)
        } else {
            if (firstTF.text ?? "").isNumeric {//Phone Number
                if !(firstTF.text ?? "").isValidMobileNumber {
                    isFieldValid = false
                    self.displayMessageAlert(withMessage:Constants.InputFields.InvalidPhoneNumber)
                }
            } else if !(firstTF.text ?? "").isValidEmail() {//Email
                isFieldValid = false
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEmail)
            }
        }
        return isFieldValid
    }
    
    func checkTheValidation() {
        signupBtn.showLoading()
       if presentAuthType == .login {
           
           viewModel.loginApiCall(userEmail: firstTF.text ?? "", password: secondTF.text ?? "")
            
        } else if presentAuthType == .signup {
            
            if self.isForSocialSignUpFlow {
                viewModel.socialLoginUpdateDetailsApiCall(token: socialLoginToken, name:  firstTF.text ?? "", phone: thirdTF.text ?? "", email: secondTF.text ?? "", password: fourthTF.text ?? "")
            } else {
                viewModel.registerApiCall(name:  firstTF.text ?? "", phone: thirdTF.text ?? "", email: secondTF.text ?? "", password: fourthTF.text ?? "")
            }
        }
    }
    
    func moveToVehicleModelSelectVC() {
        let vehicleModelSelectVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "VehicleModelSelectVC") as? VehicleModelSelectVC
        vehicleModelSelectVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vehicleModelSelectVC!, animated: true)
        } else {
            self.present(vehicleModelSelectVC!, animated: false, completion: nil)
        }
    }
    
    
    func moveToOtpScreen() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as? OTPVC
        vc?.presentAuthType = presentAuthType
        
        if let userName = firstTF.text, let userEmail = secondTF.text, let userPhoneNumber = thirdTF.text {
            
            vc?.userName = userName
            vc?.userEmail = userEmail
            vc?.userPhoneNumber = userPhoneNumber
        }
        
        vc?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
    
    func moveToForgotPasswordVC() {
        let forgotPasswordVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        forgotPasswordVC?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(forgotPasswordVC!, animated: true)
        } else {
            self.present(forgotPasswordVC!, animated: false, completion: nil)
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        if presentAuthType == .signup {
            if self.isForSocialSignUpFlow {
                self.isForSocialSignUpFlow = false
            }
        }
        
        switch presentAuthType {
        case .corporateLogin:
            presentAuthType =  previousAuthType
            break
        case .login:
            presentAuthType =  .login
            previousAuthType = .login
            break
        case .signup:
            presentAuthType =  .login
            break
        }
        updateTheView()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !IQKeyboardManager.shared.keyboardShowing {
            if scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.height {
                scrollView.contentOffset.y = scrollView.contentSize.height - scrollView.bounds.height
            }
        }
    }
    
    func updateTheCurrentTestFieldPlaholders() {
        checkBtns.forEach({$0.setImage(UIImage(named: "check"), for: .normal)})
        checkBtns.forEach({$0.tintColor = UIColor(named: "DDDDDD")})
        firstTF.text =  ""
        secondTF.text =  ""
        thirdTF.text =  ""
        fourthTF.text =  ""
        secondTF.isSecureTextEntry = false
        fourthTF.isSecureTextEntry = false
        thirdTF.isSecureTextEntry = false
        switch presentAuthType {
        case .corporateLogin:
            firstTF.placeholder = "Company ID"
            secondTF.placeholder = "Employee ID"
            thirdTF.placeholder = "Password"
            firstTF.keyboardType = .default
            secondTF.keyboardType = .default
            thirdTF.isSecureTextEntry = true
            checkBtns[2].setImage(nil, for: .normal)
            checkBtns[2].setImage(UIImage(named:"eye"), for: .normal)
            break
        case .login:
            firstTF.placeholder = "Email / Phone"
            secondTF.placeholder = "Password"
            firstTF.keyboardType = .emailAddress
            secondTF.keyboardType = .default
            secondTF.isSecureTextEntry = true
            checkBtns[1].setImage(nil, for: .normal)
            checkBtns[1].setImage(UIImage(named:"eye"), for: .normal)
            break
        case .signup:
            firstTF.placeholder = "Name"
            secondTF.placeholder = "Email"
            thirdTF.placeholder = "Phone"
            fourthTF.placeholder = "Password"
            firstTF.keyboardType = .default
            secondTF.keyboardType = .default
            thirdTF.keyboardType = .phonePad
            secondTF.keyboardType = .emailAddress
            fourthTF.isSecureTextEntry = true
            checkBtns[3].setImage(nil, for: .normal)
            checkBtns[3].setImage(UIImage(named:"eye"), for: .normal)
            break
        }
        firstTF.resignFirstResponder()
        secondTF.resignFirstResponder()
        thirdTF.resignFirstResponder()
        fourthTF.resignFirstResponder()
    }
    
    
    
    func updateTheButtonColor(textFieldTag: Int, text:String) {
        switch presentAuthType {
        case .corporateLogin:
            break
            
        case .login:
            if textFieldTag == 0 {
                if text.isNumeric && text.isValidMobileNumber {
                    checkBtns[0].tintColor = UIColor(named: "2ECE54")
                } else if  text.isValidEmail() {
                    checkBtns[0].tintColor = UIColor(named: "2ECE54")
                } else {
                    checkBtns[0].tintColor = UIColor(named: "DDDDDD")
                }
                
            } else if textFieldTag == 1 {
                if text.isEmpty {
                    checkBtns[1].tintColor = UIColor(named: "DDDDDD")
                } else {
                    checkBtns[1].tintColor = UIColor(named: "2ECE54")
                }
            }
            break
        case .signup:
            if textFieldTag == 0 {
                if text.isEmpty {
                    checkBtns[0].tintColor = UIColor(named: "DDDDDD")
                } else {
                    checkBtns[0].tintColor = UIColor(named: "2ECE54")
                }
                
            } else if textFieldTag == 1 {
                if  text.isValidEmail() {
                    checkBtns[1].tintColor = UIColor(named: "2ECE54")
                } else {
                    checkBtns[1].tintColor = UIColor(named: "DDDDDD")
                }
            } else if textFieldTag == 2 {
                if text.isNumeric && text.isValidMobileNumber {
                    checkBtns[2].tintColor = UIColor(named: "2ECE54")
                } else {
                    checkBtns[2].tintColor = UIColor(named: "DDDDDD")
                }
            } else if textFieldTag == 3 {
                if text.isEmpty {
                    checkBtns[3].tintColor = UIColor(named: "DDDDDD")
                } else {
                    checkBtns[3].tintColor = UIColor(named: "2ECE54")
                }
            }
            break
        }
    }
}
extension AuthorizationVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            updateTheButtonColor(textFieldTag: textField.tag, text: updatedText)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch presentAuthType {
            case .corporateLogin:
                break
            
            case .login:
                if textField.tag == 0 {
                    self.secondTF.becomeFirstResponder()
                } else if textField.tag == 1 {
                    textField.resignFirstResponder()
                }
                break
            case .signup:
                if textField.tag == 0 {
                    self.secondTF.becomeFirstResponder()
                } else if textField.tag == 1 {
                    self.thirdTF.becomeFirstResponder()
                } else if textField.tag == 2 {
                    self.fourthTF.becomeFirstResponder()
                } else if textField.tag == 3 {
                    textField.resignFirstResponder()
                }
                break
        }
        return true
    }
}
