//
//  FavouriteBookingVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit
import GoogleMaps


class FavouriteBookingVC: UIViewController {

    @IBOutlet weak var listBaseView: UIView!
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var favouriteStationsUnavailableView: UIView!
    @IBOutlet weak var favouriteStationsUnavailableImageView: UIImageView!
    @IBOutlet weak var favouriteStationsUnavailableMessageLabel: UILabel!

    lazy var viewModel: FavouriteViewModel = {
        return FavouriteViewModel()
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        maintainFavouriteStationsChangedFlag()
        initViewModel()
        favouriteStationsUnavailableMessageLabel.text = Constants.Messages.FavouriteStationsUnavailable
        // Do any additional setup after loading the view.
    }
 
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = listTableView.tableHeaderView else {return}
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            listTableView.tableHeaderView = headerView
            listTableView.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let favouriteStationsChanged = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStationsChanged) as? Bool {
            if favouriteStationsChanged {
                viewModel.isLoading = true
                self.listTableView.reloadData()
                callGetUserDetailsAPI(needToFetchFromServer: true)
                maintainFavouriteStationsChangedFlag()
            }
        }
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.viewModel.isLoading = false
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
                
        viewModel.getUserDetailsResultClosure = { [weak self] () in
            DispatchQueue.main.async {
                if (self?.viewModel.isLoading)! {
                    self?.viewModel.isLoading = false
                } else {
                    self?.listTableView.reloadData()
                }
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.listTableView.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.listTableView.reloadData()
            }
        }
        
        viewModel.isLoading = true
        callGetUserDetailsAPI(needToFetchFromServer: false)
    }
    
    func maintainFavouriteStationsChangedFlag() {
        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.favouriteStationsChanged)
        UserDefaults.standard.synchronize()
    }
    
    func callGetUserDetailsAPI(needToFetchFromServer:Bool) {
                
        viewModel.getUserDetailsAPICall(needToFetchFromServer:needToFetchFromServer)
    }
    
    func moveToScheduleChargingView(stationDetails:StationDetailsModel) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "ScheduleChargingVC") as? ScheduleChargingVC
        vc?.stationDetails = stationDetails
        vc?.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension FavouriteBookingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isLoading {
            return 5
        } else {
            if viewModel.arrayOfStations.count > 0 {
                favouriteStationsUnavailableView.isHidden = true
            } else {
                favouriteStationsUnavailableView.isHidden = false
            }
            return viewModel.arrayOfStations.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as? ListTableViewCell

        if !viewModel.isLoading {
            let stationData = viewModel.arrayOfStations[indexPath.row]
            cell?.showStationDetails(stationData: stationData)
            return cell!
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView,
              willDisplay cell: UITableViewCell,
            forRowAt indexPath: IndexPath) {
       
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !viewModel.isLoading {
            let stationData = viewModel.arrayOfStations[indexPath.row]
            moveToScheduleChargingView(stationDetails: stationData)
        }
    }
}
