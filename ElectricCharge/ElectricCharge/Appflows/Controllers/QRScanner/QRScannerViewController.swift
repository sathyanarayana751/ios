//
//  QRScannerViewController.swift
//  Jio Commerce
//
//  Created by Vengababu Maparthi on 02/04/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerViewController: UIViewController {

    @IBOutlet weak var flashButton: UIButton!
    
    @IBOutlet weak var viewForBlur: UIView!
    
    @IBOutlet weak var toolBar: UIView!
    @IBOutlet weak var searchingText: UILabel!
    
    @IBOutlet weak var flashView: UIView!
    
    @IBOutlet weak var chargingDurationTopView: UIView!
    @IBOutlet weak var chargingDurationContentView: UIView!

    @IBOutlet weak var durationHourButton: UIButton!
    @IBOutlet weak var durationMinuteButton: UIButton!
    
    @IBOutlet weak var pickerTopView: UIView!
    @IBOutlet weak var pickerContentView: UIView!
    @IBOutlet weak var pickerContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerTitleLabel: UILabel!
    @IBOutlet weak var pickerCancelButton: UIButton!
    @IBOutlet weak var pickerSelectButton: UIButton!
    @IBOutlet weak var timePickerView: UIPickerView!
    
    public enum TimeSelectionType : Int32 {
        case durationHour = 1
        case durationMinute = 2
        static var count: Int { return Int(TimeSelectionType.durationMinute.rawValue + 1)}
    }
    
    var loaderView:LoaderView?
    lazy var viewModel: QRScannerViewModel = {
        return QRScannerViewModel()
    }()
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var device : AVCaptureDevice!
//    var productDetailsViewModel:QRScannerViewModel!
    var productSkuID:Int64 = 0
    
    let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                              AVMetadataObject.ObjectType.code39,
                              AVMetadataObject.ObjectType.code39Mod43,
                              AVMetadataObject.ObjectType.code93,
                              AVMetadataObject.ObjectType.code128,
                              AVMetadataObject.ObjectType.ean8,
                              AVMetadataObject.ObjectType.ean13,
                              AVMetadataObject.ObjectType.aztec,
                              AVMetadataObject.ObjectType.pdf417,
                              AVMetadataObject.ObjectType.itf14,
                              AVMetadataObject.ObjectType.dataMatrix,
                              AVMetadataObject.ObjectType.interleaved2of5,
                              AVMetadataObject.ObjectType.qr]
    
    var timeSelectionType = TimeSelectionType(rawValue: 1)

    let availableDurationHours = ["00","01", "02", "03", "04", "05"]
    let availableDurationMinutes = ["00", "15", "30", "45"]
    var selectedDurationHourIndex = 1
    var selectedDurationMinuteIndex = 0
    
    var selectedTimeFormatter = DateFormatter()
    var selectedDateFormatter = DateFormatter()
    var selectedTimeWithDateFormatter = DateFormatter()

    var selectedDate = Date()
    var selectedStartTime:Date!
    var selectedEndTime:Date!
    var stationDetails:StationDetailsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiateTheQrFunctionality()
        initViewModel()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkCameraAccess()
        if !captureSession.isRunning {
            initiateTheQrFunctionality()
        }
    }
    
    func initialSetup() {
        
        selectedTimeFormatter.locale = Locale.current
        selectedTimeFormatter.dateFormat = "h:mm a"
        
        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "dd MM yyyy"
        
        selectedTimeWithDateFormatter.locale = Locale.current
        selectedTimeWithDateFormatter.dateFormat = "dd MM yyyy hh:mm a"
        
        pickerTopView.isHidden = true
        chargingDurationTopView.isHidden = true

        selectedStartTime = Date().dateWithInterval(interval: (15.0 * 60.0), minimumInterval: (5.0 * 60.0))
        selectedEndTime = selectedStartTime.addingTimeInterval((60.0 * 60.0))
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                } else {
                    self.dismiss(animated: true, completion: nil)
                    print("Permission denied")
                }
            }
        default:
            break
        }
    }
    @IBAction func flashOfOnAction(_ sender: UIButton) {
        if sender.isSelected{
            flashOff()
            sender.isSelected = false
        }else{
            flashOn()
            sender.isSelected = true
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func flashOn()
    {
        let device = AVCaptureDevice.default(for: .video)
        let photosettings = AVCapturePhotoSettings()
        do{
            if (device!.hasTorch)
            {
                try device!.lockForConfiguration()
                device!.torchMode = .on
                photosettings.flashMode = .on
                device!.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    private func flashOff()
    {
        let device = AVCaptureDevice.default(for: .video)
        let photosettings = AVCapturePhotoSettings()
        do{
            if (device!.hasTorch){
                try device!.lockForConfiguration()
                device!.torchMode = .off
                photosettings.flashMode = .off
                device!.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    //MARK:- Binding with ViewModel
    func bindWithViewModel()  {
        
    }
    
    func initViewModel() {
        
        // Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.getResolveChargeResultClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.loaderView?.hideLoader()
                if let stationDetail = self?.viewModel.stationDetailsModel {
                    self?.stationDetails = stationDetail
                    self?.showChargingDuartionView()
                }
            }
        }
    }
    
    func showChargingDuartionView() {
        selectedDurationHourIndex = 1
        selectedDurationMinuteIndex = 0
        self.updateSelectedDurationDetails()
        self.view.bringSubviewToFront(self.chargingDurationTopView)
        self.chargingDurationTopView.isHidden = false
        self.chargingDurationContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.35) {
            self.chargingDurationContentView.transform = CGAffineTransform.identity
        } completion: { completion in}
    }
    
    func hideChargingDuartionView() {
        UIView.animate(withDuration: 0.35) {
            self.chargingDurationContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        } completion: { completion in
            self.chargingDurationContentView.transform = CGAffineTransform.identity
            self.chargingDurationTopView.isHidden = true
        }
    }
    
    func showTimeSelectionPickerView() {
        switch timeSelectionType {
            case .durationHour:
                pickerTitleLabel.text = "Select Hour"
                timePickerView.isHidden = false
                timePickerView.reloadAllComponents()
                timePickerView.selectRow(selectedDurationHourIndex, inComponent: 0, animated: false)
                pickerContentViewHeightConstraint.constant = 250
                break
            case .durationMinute:
                pickerTitleLabel.text = "Select Minute"
                timePickerView.isHidden = false
                timePickerView.reloadAllComponents()
                timePickerView.selectRow(selectedDurationMinuteIndex, inComponent: 0, animated: false)
                pickerContentViewHeightConstraint.constant = 250
                break
            default:
                break
        }
        showPickerView()
    }
    
    func showPickerView() {
        self.view.bringSubviewToFront(self.pickerTopView)
        self.pickerTopView.isHidden = false
        self.pickerContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.35) {
            self.pickerContentView.transform = CGAffineTransform.identity
        } completion: { completion in}
    }
    
    func hidePickerView() {
        UIView.animate(withDuration: 0.35) {
            self.pickerContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        } completion: { completion in
            self.pickerContentView.transform = CGAffineTransform.identity
            self.pickerTopView.isHidden = true
        }
    }
    
    func moveToSlotSelection() {
        
        flashView?.isHidden = false
        toolBar?.isHidden = false
        
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "SlotSelectionVC") as? SlotSelectionVC
        vc?.modalPresentationStyle = .overFullScreen
        vc?.selectedDate = selectedDate
        vc?.selectedStartTime = getTimeWithDate(selectedStartTime)
        vc?.selectedEndTime = getTimeWithDate(selectedEndTime)
        vc?.stationDetails = stationDetails
        vc?.isQRCodeBooking = true
        
        if let selectedDurationHourValue = Int(availableDurationHours[selectedDurationHourIndex]), let selectedDurationMinuteValue = Int(availableDurationMinutes[selectedDurationMinuteIndex]) {
            vc?.selectedDurationHour = selectedDurationHourValue
            vc?.selectedDurationMinute = selectedDurationMinuteValue
        }
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
    
    func updateSelectedDurationDetails() {
        self.durationHourButton.setTitle(availableDurationHours[selectedDurationHourIndex], for: .normal)
        self.durationMinuteButton.setTitle(availableDurationMinutes[selectedDurationMinuteIndex], for: .normal)
    }
    
    @IBAction func hourAndMinuteTimeSelectionButton(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if TimeSelectionType(rawValue: Int32(button.tag)) == .durationHour {
                timeSelectionType = .durationHour
            } else {
                timeSelectionType = .durationMinute
            }
            showTimeSelectionPickerView()
        }
    }
    
    @IBAction func checkAvailabilityButtonAction(_ sender: Any) {
        
        if !startTimeIsValid(selectedStartTime) {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidStartTime)
        } else if !endTimeIsValid(selectedEndTime) {
            //showAlert(Constants.InputFields.InvalidEndTime)
        } else if !checkSelectedChargingDurationIsValid() {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidChargingDuration)
        } else {
            hideChargingDuartionView()
            moveToSlotSelection()
        }
    }
        
    @IBAction func pickerCancelButtonAction(_ sender: Any) {
        hidePickerView()
    }
    
    @IBAction func pickerSelectButtonAction(_ sender: Any) {
        
        switch timeSelectionType {
            case .durationHour:
                selectedDurationHourIndex = timePickerView.selectedRow(inComponent: 0)
                self.updateSelectedDurationDetails()
                break
            case .durationMinute:
                selectedDurationMinuteIndex = timePickerView.selectedRow(inComponent: 0)
                self.updateSelectedDurationDetails()
                break
            default:
                break
        }
        hidePickerView()
    }
}

//MARK: Time Slot Selection Methods
extension QRScannerViewController {
    
    func startTimeIsValid(_ selectedTime:Date) -> Bool {
        var isValid = false
        if let selectedStartTimeWithDate = getTimeWithDate(selectedTime) {
            if selectedStartTimeWithDate.timeIntervalSince1970 > Date().timeIntervalSince1970 {
                isValid = true
            }
        }
        return isValid
    }
    
    func getTimeWithDate(_ selectedTime:Date) -> Date? {
        
        let selectedDateString = selectedDateFormatter.string(from: selectedDate)
        let selectedTimeString = selectedTimeFormatter.string(from: selectedTime)
        let selectedTimeStringWithDate = "\(selectedDateString) \(selectedTimeString)"
        if let selectedTimeWithDate = selectedTimeWithDateFormatter.date(from: selectedTimeStringWithDate) {
            return selectedTimeWithDate
        }
        return nil
    }
    
    func getTimeWithNextDate(_ selectedTime:Date) -> Date? {
        
        let selectedDateString = selectedDateFormatter.string(from: selectedDate.addingTimeInterval(60*60*24))
        let selectedTimeString = selectedTimeFormatter.string(from: selectedTime)
        let selectedTimeStringWithDate = "\(selectedDateString) \(selectedTimeString)"
        if let selectedTimeWithDate = selectedTimeWithDateFormatter.date(from: selectedTimeStringWithDate) {
            return selectedTimeWithDate
        }
        return nil
    }
    
    func endTimeIsValid(_ selectedTime:Date) -> Bool {
        var isValid = false
        let currentDateTimeInterval = Date().timeIntervalSince1970
        
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), var selectedEndTimeWithDate = getTimeWithDate(selectedTime) {
            
            if checkEndTimeIsNextDate(selectedTime) {
                if let selectedEndTimeWithNextDate = getTimeWithNextDate(selectedTime) {
                    selectedEndTimeWithDate = selectedEndTimeWithNextDate
                }
            }
                        
            if selectedEndTimeWithDate.timeIntervalSince1970 > currentDateTimeInterval {
                if selectedEndTimeWithDate.timeIntervalSince1970 > selectedStartTimeWithDate.timeIntervalSince1970 {
                    if (selectedEndTimeWithDate.timeIntervalSince1970 - selectedStartTimeWithDate.timeIntervalSince1970) <= (6*60*60) {
                        isValid = true
                    } else {
                        self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime2)
                    }
                } else {
                    self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime1)
                }
            } else {
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime)
            }
        } else {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime)
        }
        return isValid
    }
    
    func checkEndTimeIsNextDate(_ endTime:Date) -> Bool {
        var isNextDate = false
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), let selectedEndTimeWithDate = getTimeWithDate(endTime) {
            if selectedEndTimeWithDate.timeIntervalSince1970 < selectedStartTimeWithDate.timeIntervalSince1970 {
               //End time with in selected date
                isNextDate = true
            }
        }
        return isNextDate
    }
    
    func checkSelectedChargingDurationIsValid() -> Bool {
        
        var isValidChargingDuration = false
        
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), var selectedEndTimeWithDate = getTimeWithDate(selectedEndTime), let selectedDurationHourValue = Double(availableDurationHours[selectedDurationHourIndex]), let selectedDurationMinuteValue = Double(availableDurationMinutes[selectedDurationMinuteIndex]) {
            
            let selectedDurationInseconds = ((selectedDurationHourValue * 60.0 * 60.0) + (selectedDurationMinuteValue * 60.0))
            if selectedDurationInseconds > 0 {
                if checkEndTimeIsNextDate(selectedEndTime) {
                    if let selectedEndTimeWithNextDate = getTimeWithNextDate(selectedEndTime) {
                        selectedEndTimeWithDate = selectedEndTimeWithNextDate
                    }
                }
                            
                if selectedDurationInseconds <= (selectedEndTimeWithDate.timeIntervalSince1970 - selectedStartTimeWithDate.timeIntervalSince1970) {
                    isValidChargingDuration = true
                }
            }
        }
        
        return isValidChargingDuration
    }
}

extension QRScannerViewController:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        switch timeSelectionType {
            case .durationHour:
                return 1
            case .durationMinute:
                return 1
            default:
                return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch timeSelectionType {
            case .durationHour:
                return availableDurationHours.count
            case .durationMinute:
                return availableDurationMinutes.count
            default:
                return 0
        }
    }
        
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch timeSelectionType {
            case .durationHour:
                return availableDurationHours[row]
            case .durationMinute:
                return availableDurationMinutes[row]
            default:
                return ""
        }
    }
        
}


extension QRScannerViewController
{
    func initiateTheQrFunctionality()
    {
        self.captureSession = AVCaptureSession()
        
        guard let captureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            self.captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            self.captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = self.supportedCodeTypes
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        DispatchQueue.global().async {
            // Start video capture.
            self.captureSession.startRunning()
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.videoPreviewLayer?.frame = self.view.layer.bounds
        self.view.layer.addSublayer(self.videoPreviewLayer!)
        
        
        // Initialize QR Code Frame to highlight the QR code
        self.qrCodeFrameView = UIView()
        if let qrCodeFrameView = self.qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor(named: "2ECE54")!.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            self.view.addSubview(qrCodeFrameView)
            self.view.bringSubviewToFront(qrCodeFrameView)
            //            self.view.bringSubviewToFront(barCodePlacerImg)
            self.view.bringSubviewToFront(self.viewForBlur)
        }
        
        // Move the message label and top bar to the front
        self.view.bringSubviewToFront(self.searchingText)
        self.view.bringSubviewToFront(self.toolBar)
        
        
    }
    
    
    func launchApp(decodedURL: String) {
        
        if presentedViewController != nil {
            return
        }
//        if self.productSkuID != 0{
            stopScannerAndCallResolveChargeAPI(qrcode:decodedURL)
            return
//        }
        
//        if let url = URL(string: decodedURL) {
//            if UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                self.productSkuID = Int64(decodedURL)!
//                self.bindWithViewModel()
//            }
//        }
    }
    
    func stopScannerAndCallResolveChargeAPI(qrcode:String) {
        let result = qrcode.components(separatedBy: "_")
        if result.count == 2 {
            captureSession.stopRunning()
//            videoPreviewLayer?.removeFromSuperlayer()
//            viewForBlur?.isHidden = true
            flashView?.isHidden = true
            toolBar?.isHidden = true
            qrCodeFrameView?.removeFromSuperview()
            loaderView = LoaderView.shared
            loaderView?.showLoader()
            viewModel.getResolveChargeAPICall(chargerId:result[0], plugId: result[1])
        }
    }
}


extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//        if !AppConstants.Reachable {
//            ToastView.shared.showToastView(message: ("No internet connection", "", true), bgcolor: Colors.errorBackGroudColor, type: .presentBottom)
//           return
//        }
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            searchingText.isHidden = false
            qrCodeFrameView?.frame = CGRect.zero
//            searchingText.text = StringConstants.searching()
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            print(metadataObj.stringValue ?? "")
            if let metaDataValue = metadataObj.stringValue, metaDataValue.isValidScannerData() {
                searchingText.isHidden = true
                launchApp(decodedURL:metaDataValue)
                searchingText.text = metaDataValue
            }
        }
    }
}
