//
//  TabbarViewController.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class TabbarViewController: UITabBarController {
    var indexSelected = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func openTheQRScanner()  {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "QRScannerViewController") as? QRScannerViewController
        vc?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}

extension TabbarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController)!
        if selectedIndex == 2 {
            self.openTheQRScanner()
            return false
        }
        return true
    }
}
