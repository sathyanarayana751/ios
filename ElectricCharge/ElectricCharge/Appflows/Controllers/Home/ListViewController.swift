//
//  ListViewController.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 27/11/21.
//

import UIKit
import GoogleMaps
import UIView_Shimmer

enum HomeType {
    case mapType
    case listType
}

class ListViewController: UIViewController {
    @IBOutlet weak var mapBaseView: UIView!
    @IBOutlet weak var listBaseView: UIView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mapColView: UICollectionView!
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var stationsUnavailableView: UIView!
    @IBOutlet weak var stationsUnavailableImageView: UIImageView!
    @IBOutlet weak var stationsUnavailableMessageLabel: UILabel!
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    var locationManager = LocationManager.shared

    var jobMarkers  = [GMSMarker]()
    var bounds = GMSCoordinateBounds()
    let loaderView = LoaderView.shared

    var previousIndex = 0
    var currentLat =  0.0
    var currentLong =  0.0
    
    var searchedLat =  0.0
    var searchedLong =  0.0
    
    var isSearchedAddress = false
    
    var favouriteStations:[String] = []
    
    var homeType: HomeType = .listType
    var filters:[[String:Any]] = []
    /*[
        [
            "title":"AVAILABILITY",
            "data":["Available Only"]
        ],
        [
            "title":"SORT BY",
            "data":["Nearest","Cheapest"]
        ],
        [
            "title":"CHARGE TYPE",
            "data":["DC","AC"]
        ],
        [
            "title":"PLUG TYPE",
            "data":[
                "DC":["CCS2","CHADEMO","TESLA","GBT"],
                "AC":["TYPE1","TYPE2"]
            ]
        ],
        [
            "title":"RANGE",
            "data":["5","10","20","50","100"]
        ]
    ]*/
    var selectedFilters:[String:Any] = [:]/*[
                                            "AVAILABILITY":["Available Only"],
                                            "SORT BY":["Nearest"],
                                            "CHARGE TYPE":["DC"],
                                            "PLUG TYPE":["DC":["CCS2"], "AC":["TYPE1"]],
                                            "RANGE":["10"]
                                        ]*/
    var selectedFiltersChargeType = ""
    var selectedFiltersPlugType = ""
    var selectedFiltersSortBy = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationPermission()
        setCollectionViewInitialProperties()
        initiateTheMap()
        initViewModel()
        // Do any additional setup after loading the view.
    }
 
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = listTableView.tableHeaderView else {return}
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            listTableView.tableHeaderView = headerView
            listTableView.layoutIfNeeded()
        }
    }
    
    func checkLocationPermission() {
        locationManager.delegate = self
        
        if locationManager.locationAuthorizationStatus() == .authorizedWhenInUse || locationManager.locationAuthorizationStatus() == .authorizedAlways {
            
            locationManager.startUpdatingLocation()
            
        } else {
            
            showAlertViewController(title: Constants.Alert.title, message: Constants.Alert.locationEnable)
        }
    }
    
    func setCollectionViewInitialProperties() {
        
        let cellWidth = (0.85) * UIScreen.main.bounds.width
        let sectionSpacing = (1/13) * UIScreen.main.bounds.width
        let cellSpacing = (1/34) * UIScreen.main.bounds.width
        let cellHeight = 140.0
        
        let layout = PagingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: sectionSpacing, bottom: 0, right: sectionSpacing)
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.minimumLineSpacing = cellSpacing
        mapColView.setCollectionViewLayout(layout, animated: false)
        mapColView.decelerationRate = .fast
    }
    
    func initViewModel() {
        
        if let favStations = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStations) as? [String] {
            self.favouriteStations = favStations
        }
        
        // Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.viewModel.isLoading = false
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
                
        viewModel.reloadStationsListClosure = { [weak self] () in
            DispatchQueue.main.async {
                
                if (self?.viewModel.isLoading)! {
                    self?.viewModel.isLoading = false
                } else {
                    self?.mapColView.reloadData()
                    self?.listTableView.reloadData()
                }
                self?.placeTheMarkersOnMap()
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.listTableView.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.listTableView.reloadData()
                self?.mapColView.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.mapColView.reloadData()
            }
        }
        
        viewModel.favouriteStationResultClosure = {  [weak self] () in
            DispatchQueue.main.async {
                //self?.showAlert(Constants.Messages.FavouriteStationSuccess)
                self?.favouriteStations.append((self?.viewModel.favStationId)!)
                UserDefaults.standard.set(self?.favouriteStations, forKey: UserDefaults.Keys.favouriteStations)
                UserDefaults.standard.set(true, forKey: UserDefaults.Keys.favouriteStationsChanged)
                UserDefaults.standard.synchronize()
                self?.listTableView.reloadData()
                self?.mapColView.reloadData()
            }
        }
        
        filters = Constants.Filters.AvailableFilters
        selectedFilters = Constants.Filters.InitialSelectedFilters
        
        if let plugType = UserDefaults.standard.string(forKey: UserDefaults.Keys.plugType) {
            
            if var selectedPlugType = selectedFilters["PLUG TYPE"] as? [String:[String]] {
                
                let plugTypeValue = [plugType.uppercased()]
                
                if plugType.uppercased() == "AC-001" {
                    selectedPlugType["AC"] = plugTypeValue
                    selectedFilters["CHARGE TYPE"] = ["AC"]
                } else {
                    selectedPlugType["DC"] = plugTypeValue
                    selectedFilters["CHARGE TYPE"] = ["DC"]
                }
                selectedFilters["PLUG TYPE"] = selectedPlugType
            }
        }
        
        viewModel.isLoading = true
        callSearchAPI()
    }

    @IBAction func presentMapView(_ sender: UIButton) {
        if homeType  == .listType {
            mapBaseView.isHidden = false
            listBaseView.isHidden = true
            stationsUnavailableView.isHidden = true
            homeType = .mapType
            sender.setTitle("List", for: .normal)
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor(named: "2A382C"), for: .normal)
            sender.setImage(UIImage(named: "listIcon"), for: .normal)
        } else {
            mapBaseView.isHidden = true
            listBaseView.isHidden = false
            homeType = .listType
            sender.setTitle("Map", for: .normal)
            sender.backgroundColor = UIColor(named: "2A382C")
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.setImage(UIImage(named: "map-legend"), for: .normal)
            listTableView.reloadData()
        }
    }
    
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        let addressSearchView = AddressSearchView.shared
        
        addressSearchView.setup()
        
        addressSearchView.selectedAddressClosure = { [weak self] () in
            DispatchQueue.main.async {
                
                //TODO: Call search api by using updated address details
                addressSearchView.removeTheView()
                self?.isSearchedAddress = true
                self?.searchedLat = (addressSearchView.selectedAddress?.latitude)!
                self?.searchedLong = (addressSearchView.selectedAddress?.longitude)!
                self?.callSearchAPI()
            }
        }
    }
    
    @IBAction func showFilterView(_ sender: Any) {
        let filterView = FilterView.shared
        filterView.setup(filters: filters, currentlySelectedFilters: self.selectedFilters)
        
        filterView.selectedFiltersClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.selectedFilters = filterView.selectedFilters
                self?.callSearchAPI()
            }
        }
    }
    
    func callSearchAPI() {
                
        var surroundingParam:[String:Any] = [:]
        
        var rangeValue = 10000.0
        if let arrayOfSelectedRange = self.selectedFilters["RANGE"] as? [String], let selectedRangeValue = arrayOfSelectedRange.first?.toDouble() {
            rangeValue = selectedRangeValue * 1000
        }
        
        var chargeType:[String] = []
        var plugType:[String] = []
        
        if let arrayOfSelectedSortBy = self.selectedFilters["SORT BY"] as? [String], let selectedSortByValue = arrayOfSelectedSortBy.first {
            selectedFiltersSortBy = selectedSortByValue
        }

        if let arrayOfSelectedChargeType = self.selectedFilters["CHARGE TYPE"] as? [String], let selectedChargeType = arrayOfSelectedChargeType.first {
            
            chargeType = arrayOfSelectedChargeType
            selectedFiltersChargeType = selectedChargeType
            if let selectedPlugTypeDict = self.selectedFilters["PLUG TYPE"] as? [String:Any], let arrayOfSelectedPlugType = selectedPlugTypeDict[selectedChargeType] as? [String], let selectedPlugType = arrayOfSelectedPlugType.first{
                plugType = arrayOfSelectedPlugType
                selectedFiltersPlugType = selectedPlugType
            }
        }
        
        surroundingParam = [
            "radius":rangeValue,
            "latitude":locationManager.currentLat,
            "longitude":locationManager.currentLong,
        ]
        
        if(isSearchedAddress && searchedLat > 0.0 && searchedLong > 0.0){
            
            surroundingParam["latitude"] = searchedLat
            surroundingParam["longitude"] = searchedLong
        }
        
        viewModel.searchApiCall(query: "",
                                chargeType: chargeType,
                                plugType: plugType,
                                withIn: [:],
                                surrounding: surroundingParam,
                                isForWithIn: false,
                                sortBy: selectedFiltersSortBy)
    }
    
}

extension ListViewController {
    func initiateTheMap()  {
        
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.animate(toZoom: 15)

        let position = CLLocationCoordinate2D(latitude: locationManager.currentLat, longitude: locationManager.currentLong)
        mapView.animate(toLocation: position)
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "darkMode", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    //Please the marker for each job
    func placeTheMarkersOnMap() {
        jobMarkers = [GMSMarker]()
        mapView.clear()
        previousIndex = 0
        bounds = GMSCoordinateBounds()

        for index in 0..<viewModel.arrayOfStations.count {
            
            let eachStation = viewModel.arrayOfStations[index]
                
            if let coordinates = eachStation.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1] {
                
                let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                var marker = GMSMarker()
                marker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
                marker.appearAnimation = GMSMarkerAnimation.pop
                marker = GMSMarker(position: position)
                marker.userData = index
                if index == 0 {
                    marker.icon = #imageLiteral(resourceName: "selectedMarker")
                    mapView.animate(toLocation: position)
                }else{
                    marker.icon = #imageLiteral(resourceName: "unselectedMarker")
                }
                marker.map = mapView
                bounds = bounds.includingCoordinate(marker.position)
                jobMarkers.append(marker)
            }
        }
        fitAllMarkers()
        updateMapCameraPosition(isInitial: true)
    }
    
    func fitAllMarkers() {
        if(bounds.isValid) {
            mapView.setMinZoom(8, maxZoom: 15) // allow the user zoom in more than level 15 again
            let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
            mapView.animate(with: update)
            mapView.setMinZoom(8, maxZoom: 18) // allow the user zoom in more than level 15 again
        }
    }
    
    func updateMapCameraPosition(isInitial:Bool){
        
        if viewModel.arrayOfStations.count == 0 {
            var position = CLLocationCoordinate2D(latitude: locationManager.currentLat, longitude: locationManager.currentLong)
            if isSearchedAddress {
                position = CLLocationCoordinate2D(latitude: self.searchedLat, longitude: self.searchedLong)
            }
            mapView.animate(toLocation: position)
        } else {
            if isInitial {
                self.mapColView.scrollToItem(at: IndexPath(item: 0, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: true)
            } else {
                self.selectParticularMarker(index: 0)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if homeType == .mapType {
        
            if jobMarkers.count > 0 {
                DispatchQueue.main.async {
                    var visibleRect = CGRect()
                    self.jobMarkers[ self.previousIndex].icon = nil
                    self.jobMarkers[ self.previousIndex].icon =  #imageLiteral(resourceName: "unselectedMarker")
                    visibleRect.origin =  self.mapColView.contentOffset
                    visibleRect.size =  self.mapColView.bounds.size
                    let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                    guard let indexPath =  self.mapColView.indexPathForItem(at: visiblePoint) else { return }
                               
                    let eachStation = self.viewModel.arrayOfStations[indexPath.row]
                        
                    if let coordinates = eachStation.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1] {
                        
                        CATransaction.begin()
                        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                        let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                        let camera = GMSCameraPosition.camera(withLatitude:  position.latitude, longitude: position.longitude, zoom: 15.0)
                        self.mapView.animate(to: camera)
                        CATransaction.commit()
                        
                        self.jobMarkers[indexPath.row].icon = nil
                        self.jobMarkers[indexPath.row].icon =  #imageLiteral(resourceName: "selectedMarker")
                        self.jobMarkers[indexPath.row].appearAnimation = GMSMarkerAnimation.pop
                        self.previousIndex = indexPath.row
                        print("previousIndex = \(self.previousIndex)")
                        print(indexPath)
                    }
                }
            }
        }
    }
    
    func moveToScheduleChargingView(stationDetails:StationDetailsModel) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "ScheduleChargingVC") as? ScheduleChargingVC
        vc?.stationDetails = stationDetails
        vc?.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension ListViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let index = marker.userData as? Int {
            
            selectParticularMarker(index: index)
        }
        return true
    }
    
    func selectParticularMarker(index:Int){
        
        let indexPath = IndexPath(item: index, section: 0)
        self.mapColView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
        let eachStation = self.viewModel.arrayOfStations[indexPath.row]
            
        if let coordinates = eachStation.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1] {
            
            self.jobMarkers[ self.previousIndex].icon = nil
            self.jobMarkers[ self.previousIndex].icon =  #imageLiteral(resourceName: "unselectedMarker")
            
            CATransaction.begin()
            CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
            let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let camera = GMSCameraPosition.camera(withLatitude:  position.latitude, longitude: position.longitude, zoom: 15.0)
            self.mapView.animate(to: camera)
            CATransaction.commit()
            
            self.jobMarkers[indexPath.row].icon = nil
            self.jobMarkers[indexPath.row].icon =  #imageLiteral(resourceName: "selectedMarker")
            self.jobMarkers[indexPath.row].appearAnimation = GMSMarkerAnimation.pop
            self.previousIndex = indexPath.row
            print("previousIndex = \(self.previousIndex)")
            print(indexPath)
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        if isSearchedAddress {
            isSearchedAddress = false
            searchedLat = 0.0
            searchedLong = 0.0
            self.callSearchAPI()
        } else {
            updateMapCameraPosition(isInitial: false)
        }
        return true
    }
    
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if viewModel.isLoading {
            return 5
        } else {
            if homeType  == .listType {
                if viewModel.arrayOfStations.count > 0 {
                    stationsUnavailableView.isHidden = true
                } else {
                    stationsUnavailableView.isHidden = false
                }
            }
            return viewModel.arrayOfStations.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as? ListTableViewCell

        if !viewModel.isLoading {
            
            let stationData = viewModel.arrayOfStations[indexPath.row]
            stationData.selectedChargeType = selectedFiltersChargeType
            stationData.selectedPlugType = selectedFiltersPlugType
            cell?.showStationDetails(stationData: stationData)
            if self.favouriteStations.contains(stationData.id!) {
                cell?.favouriteButton.setImage(UIImage(named: "heart-outline-red"), for: .normal)
                cell?.favouriteButton.isUserInteractionEnabled = false
            } else {
                cell?.favouriteButton.setImage(UIImage(named: "heart-outline"), for: .normal)
                cell?.favouriteButton.isUserInteractionEnabled = true
            }
            cell?.favSelectClosure = { [weak self] () in
                DispatchQueue.main.async {
                    //TODO: Call Fav API
                    self?.viewModel.favoriteApiCall(stationDetailsModel: stationData)
                }
            }
            return cell!
        }
        return cell!
    }
        
     func tableView(_ tableView: UITableView,
               willDisplay cell: UITableViewCell,
             forRowAt indexPath: IndexPath) {
        
         cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !viewModel.isLoading {
            let stationData = viewModel.arrayOfStations[indexPath.row]
            moveToScheduleChargingView(stationDetails: stationData)
        }
    }
}

extension ListViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if viewModel.isLoading {
            return 5
        }
        return viewModel.arrayOfStations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MapCollectionViewCell", for: indexPath) as? MapCollectionViewCell
        
        if !viewModel.isLoading {
            let stationData = viewModel.arrayOfStations[indexPath.row]
            cell?.showStationDetails(stationData: stationData)
            
            if self.favouriteStations.contains(stationData.id!) {
                cell?.favouriteButton.setImage(UIImage(named: "heart-outline-red"), for: .normal)
                cell?.favouriteButton.isUserInteractionEnabled = false
            } else {
                cell?.favouriteButton.setImage(UIImage(named: "heart-outline"), for: .normal)
                cell?.favouriteButton.isUserInteractionEnabled = true
            }
            
            cell?.favSelectClosure = { [weak self] () in
                DispatchQueue.main.async {
                    //TODO: Call Fav API
                    self?.viewModel.favoriteApiCall(stationDetailsModel: stationData)
                }
            }
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let stationData = viewModel.arrayOfStations[indexPath.row]
        moveToScheduleChargingView(stationDetails: stationData)
    }
}

extension ListViewController:LocationManagerProtocol {
    
    func locationManagerDidChangeAuthorization(status: LocationAuthorizationStatus) {
   
        switch status {
            case .authorizedWhenInUse, .authorizedAlways:
                locationManager.startUpdatingLocation()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.callSearchAPI()
                }
                break
            case .restricted, .denied:
            showAlertViewController(title: Constants.Alert.title, message: Constants.Alert.locationEnable)
                break
            default:
                break
        }
    }
    
    func showAlertViewController(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                print("ok Action" )
            }
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        
        alert.addAction(okAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true)
    }
}
