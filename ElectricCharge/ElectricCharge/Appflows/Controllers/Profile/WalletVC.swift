//
//  WalletVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class WalletVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var colVwHeaders: UICollectionView!
    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var topWalletDetailsView: UIView!
    @IBOutlet weak var walletDetailsCollectionView: UICollectionView!
    
    var addMoneyTblVwCell:AddMoneyTblVwCell?
    var walletDetailsCollectionViewCell:WalletDetailsCollectionViewCell?

    lazy var paymentGateWayManager:ChashfreePaymentGatewayManager = {
        return ChashfreePaymentGatewayManager()
    }()
    
    lazy var viewModel: WalletViewModel = {
        return WalletViewModel()
    }()
    
    var selectedTabIndex = 0
    var arrayOfWalletHistory:[WalletTransactionDetailsModel] = []
    /*[["id":"D120768","date":"10 DEC","amount":"₹500"],
                                ["id":"D120780","date":"12 DEC","amount":"₹1000"],
                                ["id":"D120808","date":"18 DEC","amount":"₹500"],
                                ["id":"D120850","date":"20 DEC","amount":"₹1500"],
                                ["id":"D120920","date":"23 DEC","amount":"₹1000"],
                                ["id":"D120980","date":"26 DEC","amount":"₹500"]]*/
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    func initViewModel() {
        
        // Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.addMoneyTblVwCell?.btnAddAmount.hideLoading()
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.walletDetailsResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let walletId = self?.viewModel.walletDetailsResponse.id {
                    print("Wallet Id = \(walletId)")
                    self?.showWalletDetails()
                    if (self?.viewModel.isLoading)! {
                        self?.viewModel.isLoading = false
                    } else {
                        self?.tblVw.reloadData()
                    }
                }
            }
        }
                
        viewModel.topUpWalletResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.addMoneyTblVwCell?.btnAddAmount.hideLoading()
                if let error = self?.viewModel.topupWalletResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else {
                    if let orderId = self?.viewModel.topupWalletResponse.data?.transactionId, let orderToken = self?.viewModel.topupWalletResponse.data?.token {
                        self?.callPaymentGateWayAPI(orderId:orderId , orderToken:orderToken)
                    }
                }
            }
        }
        
        self.paymentGateWayManager.paymentGatewayResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let errorMessage = self?.paymentGateWayManager.paymentGatewayResponse.1 {
                    self?.displayMessageAlert(withMessage:errorMessage)
                } else {
                    self?.viewModel.isLoading = true
                    self?.callWalletDetailsAPI(needToFetchFromServer:true)
                }
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tblVw.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.tblVw.reloadData()
                self?.walletDetailsCollectionView.reloadData()
                self?.colVwHeaders.reloadData()
            }
        }
        self.viewModel.isLoading = true
        self.callWalletDetailsAPI(needToFetchFromServer:true)
    }
    
    func callPaymentGateWayAPI(orderId: String, orderToken: String) {
        self.paymentGateWayManager.invokeNativeiOSSDK(orderId: orderId, orderToken: orderToken, controller: self)
    }
    
    func callWalletDetailsAPI(needToFetchFromServer:Bool) {
        viewModel.walletDetailsAPICall(needToFetchFromServer:needToFetchFromServer)
    }
    
    func callTopupWalletAPI(_ amount:Int) {
        self.addMoneyTblVwCell?.btnAddAmount.showLoading()
        viewModel.topupWalletAPICall(amount: amount)
    }
    
    func showWalletDetails(){
        if let walletId = viewModel.walletDetailsResponse.id, let walletCredit = viewModel.walletDetailsResponse.credit, let walletReserved = viewModel.walletDetailsResponse.reserved {
            print("Wallet Id = \(walletId), Credit = \(walletCredit), Reserved = \(walletReserved)")
            walletDetailsCollectionViewCell?.lblWalletAmount.text = "₹\(walletCredit)"
            walletDetailsCollectionViewCell?.reservedAmountValueLabel.text = "₹\(walletReserved)"
            walletDetailsCollectionViewCell?.availableAmountValueLabel.text = "₹\(walletCredit - walletReserved)"
            arrayOfWalletHistory = []
            if let walletTransactions = viewModel.walletDetailsResponse.transactions {
                for eachTransaction in walletTransactions {
                    if let eachTransactionDetails = eachTransaction {
                        arrayOfWalletHistory.append(WalletTransactionDetailsModel(transactionDetails: eachTransactionDetails))
                    }
                }
            }
        }
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension WalletVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == walletDetailsCollectionView {
            return 1
        } else {
            return 2
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == walletDetailsCollectionView {
            if walletDetailsCollectionViewCell == nil {
                walletDetailsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalletDetailsCollectionViewCell", for: indexPath) as? WalletDetailsCollectionViewCell
            }
            return walletDetailsCollectionViewCell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SegmentHeaderColVwCell", for: indexPath) as? SegmentHeaderColVwCell
            if indexPath.row == selectedTabIndex {
                cell?.lblHeader.textColor = UIColor(hexString: "#2A382C")
                cell?.vwBottom.isHidden = false
            } else {
                cell?.lblHeader.textColor = UIColor(hexString: "#A4ACA5")
                cell?.vwBottom.isHidden = true
            }
            switch indexPath.row {
                case 0:
                    cell?.lblHeader.text = "Add Money"
                default:
                    cell?.lblHeader.text = "Wallet History"
            }
            return cell!
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == walletDetailsCollectionView {
            return collectionView.frame.size
        } else {
            return CGSize(width: collectionView.frame.size.width/2, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != walletDetailsCollectionView {
            selectedTabIndex = indexPath.row
            colVwHeaders.reloadData()
            tblVw.reloadData()
        }
    }
}

extension WalletVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTabIndex == 0 {
            return 1
        } else {
            if viewModel.isLoading {
                return 10
            } else {
                return arrayOfWalletHistory.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedTabIndex == 0 {
            if addMoneyTblVwCell == nil {
                addMoneyTblVwCell = tableView.dequeueReusableCell(withIdentifier: "AddMoneyTblVwCell", for: indexPath) as? AddMoneyTblVwCell
                addMoneyTblVwCell?.selectionStyle = .none
                addMoneyTblVwCell?.sourceVC = self
                addMoneyTblVwCell?.updateTextFieldValue()
            }
            return addMoneyTblVwCell!

        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "WalletHistoryTblVwCell", for: indexPath) as? WalletHistoryTblVwCell
            cell?.selectionStyle = .none
            
            if !viewModel.isLoading {
                
                switch arrayOfWalletHistory[indexPath.row].status {
                    case .completed:
                        cell?.lblAmount.textColor = UIColor(hexString: "#2ECE54")
                    case .pending:
                        cell?.lblAmount.textColor = UIColor(hexString: "#FFAE42")
                    default:
                        cell?.lblAmount.textColor = UIColor.lightGray
                }
                
                if (indexPath.row % 2) == 0 {
                    cell?.vwBackground.backgroundColor = UIColor(hexString: "#F8F8F8")
                } else {
                    cell?.vwBackground.backgroundColor = .clear
                }
                                
                cell?.lblAmountID.text = arrayOfWalletHistory[indexPath.row].id
                cell?.lblDate.text = arrayOfWalletHistory[indexPath.row].date
                if arrayOfWalletHistory[indexPath.row].type == .credit {
                    cell?.lblAmount.text = "+₹\(arrayOfWalletHistory[indexPath.row].amount ?? 0)"
                } else {
                    cell?.lblAmount.text = "-₹\(arrayOfWalletHistory[indexPath.row].amount ?? 0)"
                }
            }
            return cell!
        }
    }
    func tableView(_ tableView: UITableView,
              willDisplay cell: UITableViewCell,
            forRowAt indexPath: IndexPath) {
       
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
}


