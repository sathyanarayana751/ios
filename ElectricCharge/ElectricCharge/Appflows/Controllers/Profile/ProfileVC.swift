//
//  ProfileVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmailHeader: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobileHeader: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var tblVwSettings: UITableView!
    
    var isFromVehicleModelVC = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        showUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromVehicleModelVC {
            isFromVehicleModelVC = false
            tblVwSettings.reloadData()
        }
    }
    
    func showUserDetails(){
        
        if let userName = UserDefaults.standard.object(forKey: UserDefaults.Keys.userName) as? String, let userEmail = UserDefaults.standard.object(forKey: UserDefaults.Keys.userEmail) as? String, let userPhoneNumber = UserDefaults.standard.object(forKey: UserDefaults.Keys.userPhoneNumber) as? String {
            
            lblName.text = userName
            lblEmail.text = userEmail
            lblMobile.text = userPhoneNumber
        }
    }
    
    func moveToWalletDetailsView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "WalletVC") as? WalletVC
        vc?.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func moveToVehicleModelSelectVC() {
    
        let vehicleModelSelectVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "VehicleModelSelectVC") as? VehicleModelSelectVC
        vehicleModelSelectVC?.isFromProfile = true
        vehicleModelSelectVC?.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vehicleModelSelectVC!, animated: true)
    }
    
    func showLogoutAlert() {
        
        let alertController = UIAlertController(title: Constants.Alert.title,
                                                message: Constants.Alert.logoutMessage,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.moveToRootVC()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func moveToRootVC(){
        
        if let window = UIApplication.shared.windows.first {
            
            UserDefaults.standard.clearAppData()
            if let authorizationVC = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "AuthorizationVC") as? AuthorizationVC {
                authorizationVC.modalPresentationStyle = .overFullScreen
                let rootVC = UINavigationController(rootViewController: authorizationVC)
                rootVC.navigationBar.isHidden = true
                window.rootViewController = rootVC
            }
        }
    }
}
extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingDescTblVwCell", for: indexPath) as? ChargingDescTblVwCell
            cell?.selectionStyle = .none
        cell?.nextArrowImg.isHidden = true
            switch indexPath.row {
            case 0:
                cell?.lblDescription.text = "Selected Plug type"
                if let plugType = UserDefaults.standard.string(forKey: UserDefaults.Keys.plugType) {
                    cell?.lblValue.text = plugType
                } else {
                    cell?.lblValue.text = "CCS-2"
                }
            case 1:
                cell?.lblDescription.text = "Vehicle Model"
                if let vehicleModel = UserDefaults.standard.object(forKey: UserDefaults.Keys.vehicleModel) as? String {
                    cell?.lblValue.text = vehicleModel
                } else {
                    cell?.lblValue.text = "NA"
                }
            case 2:
                cell?.lblDescription.text = "Wallet"
//                cell?.lblValue.text = "₹432"
                cell?.lblValue.text = ""
                cell?.nextArrowImg.isHidden = false
            case 3:
                cell?.lblDescription.text = "Terms and Conditions"
                cell?.lblValue.text = ""
                cell?.nextArrowImg.isHidden = false
            default:
                cell?.lblDescription.text = "Logout"
                cell?.lblValue.text = ""
                cell?.nextArrowImg.isHidden = false
            }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            isFromVehicleModelVC = true
            moveToVehicleModelSelectVC()
        } else if indexPath.row == 2 {
            moveToWalletDetailsView()
        } else if indexPath.row == 4 {
            showLogoutAlert()
        }
    }
}
