//
//  StartChargingVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class StartChargingVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var lblOneTimePassword: UILabel!
    @IBOutlet weak var lblInstruction: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var btnStartCharginng: LoadingButton!
    
    var count = 30
    var timer: Timer?
    var reservationId = ""
    var hasEnteredOTP = false
    var verificationCode = ""
    
    lazy var viewModel: StartChargingViewModel = {
        return StartChargingViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        hidesBottomBarWhenPushed = true
        lblOneTimePassword.text = "One Time \nPassword"
        self.setUpTheOTPView()
        initViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func setUpTheOTPView() {
        otpView.otpFieldTextColor = .white
        otpView.otpFieldEnteredBackgroundColor = .clear
        otpView.otpFieldsCount = 6
        otpView.otpFieldInputType = .numeric
        otpView.otpFieldDisplayType = .square
        otpView.otpFieldBorderWidth = 1
        otpView.shouldAllowIntermediateEditing = false
        otpView.otpFieldEntrySecureType = false
        otpView.delegate = self
        otpView.initializeUI()
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.sendOTPResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                //Process send OTP response
                if let error = self?.viewModel.sendOTPResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else {
                    self?.showResendOTP()
                }
            }
        }
        disableResendOTPButton()
        sendOTP()
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOTPTap(_ sender: Any) {
        disableResendOTPButton()
        sendOTP()
    }
    
    @IBAction func startChargingTap(_ sender: Any) {
        if hasEnteredOTP && verificationCode.count > 0 {
            moveToStopChargingView()
        } else {
            self.displayMessageAlert(withMessage:Constants.InputFields.MissingOTP)
        }
    }
    
    func showResendOTP() {
        
        self.timer = nil
        showTheTimer()
    }

    func showTheTimer() {
        if timer == nil {
            self.count = 30
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }

    @objc func update() {
        if count > 0 {
            count -= 1
            let minutes = String(format: "%02d", (count / 60))
            let seconds = String(format: "%02d", (count % 60))
            lblTimer.text = minutes + ":" + seconds
            lblTimer.isHidden = false
        } else {
            lblTimer.text = "00:00"
            lblTimer.isHidden = true
            if timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
            enableResendOTPButton()
        }
    }
    
    func sendOTP() {
        viewModel.startChargingOTPAPICall(reservationId: reservationId)
    }
    
    func enableResendOTPButton() {
        
        btnResendOTP.isEnabled = true
        btnResendOTP.setTitleColor(UIColor(hexString: "#2ECE54"), for: .normal)
    }
    
    func disableResendOTPButton() {
        
        btnResendOTP.isEnabled = false
        btnResendOTP.setTitleColor(UIColor(hexString: "#6A746C"), for: .normal)
    }
    
    func moveToStopChargingView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "StopChargingVC") as? StopChargingVC
        vc?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}

extension StartChargingVC: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered {
            hasEnteredOTP = true
        } else {
            hasEnteredOTP = false
        }
        return hasEntered
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        verificationCode = otpString
        print("OTPString: \(otpString)")
    }
}
