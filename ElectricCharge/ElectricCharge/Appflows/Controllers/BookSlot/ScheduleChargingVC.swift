//
//  ScheduleChargingVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import Lottie
import MapKit
import SDWebImage

class ScheduleChargingVC: UIViewController {
    @IBOutlet weak var animateView: UIView!
    @IBOutlet weak var btnnShare: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAvailabilityTag: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPlugType: UILabel!
    @IBOutlet weak var colVwPlugType: UICollectionView!
    @IBOutlet weak var lblStationnImages: UILabel!
    @IBOutlet weak var colVwStationImages: UICollectionView!
    @IBOutlet weak var perUnitLabel: UILabel!
    @IBOutlet weak var perUnitValueLabel: UILabel!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var scheduleOverView: UIView!
    @IBOutlet weak var favButton: UIButton!
    
    let lottieAnimation = AnimationView(name: "details")
    var stationDetails:StationDetailsModel!
    let locationManager = LocationManager.shared
    var arrayOfImages:[String?] = []
    var stationLatitude = 0.0
    var stationLongitude = 0.0
    /*var supportedPortsProperties = [PlugType.ccs2:["typeImage":"type2","bgColor":"C3C7FF"],
                                    PlugType.chademo:["typeImage":"type1","bgColor":"FFD6D6"],
                                    PlugType.gbt:["typeImage":"type3","bgColor":"C3FFCF"],
                                    PlugType.type1:["typeImage":"type2","bgColor":"C3C7FF"],
                                    PlugType.type2:["typeImage":"type1","bgColor":"FFD6D6"],
                                    PlugType.tesla:["typeImage":"type3","bgColor":"C3FFCF"]] as [AnyHashable : [String : String]]*/
    
    var supportedPortsProperties = ["CCS":["typeImage":"type1","bgColor":"C3C7FF"],
                                    "CHADEMO":["typeImage":"type1","bgColor":"FFD6D6"],
                                    "GBT":["typeImage":"type1","bgColor":"C3FFCF"],
                                    "AC001":["typeImage":"type1","bgColor":"FFDCC3"]]
    
    var selectedPlugTypeIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.vwBottom.cornerTheTop(radius: 30)
        //self.vwBottom.dropShadow(opacity: 0.2, offSet: CGSize(width: 2.0, height: 2.0), radius: 10)
        self.loadTheAnimateView()
        self.showSationDetails()
    }
    
    func loadTheAnimateView() {
        lottieAnimation.backgroundColor = .clear
        lottieAnimation.loopMode = .loop
        lottieAnimation.contentMode = .scaleAspectFit
        lottieAnimation.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        lottieAnimation.frame = CGRect(x: 0, y: 0, width: animateView.frame.size.width, height: animateView.frame.size.height)
        animateView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    func showSationDetails() {
        
        lblDescription.text = stationDetails.name
        var stationAddress = stationDetails.description
        
        if let addressDict = stationDetails.address, let addressLine1 = addressDict.line1, let addressLine2 = addressDict.line2, let city = addressDict.city, let state = addressDict.state, let zipCode = addressDict.zip {
            stationAddress = addressLine1 + " " + addressLine2 + " " + city + " " + state + " " + zipCode
        }
        
        lblAddress.text = stationAddress
        
        var stationAvailable = false
        if let status = stationDetails.status {
            if status == .active {
                stationAvailable = true
            }
        }
        
        if stationAvailable {
            lblAvailabilityTag.backgroundColor = UIColor(named: "2ECE54")
            lblAvailabilityTag.borderWidth = 0
            lblAvailabilityTag.borderColor = UIColor.clear
            lblAvailabilityTag.textColor = UIColor.white
            lblAvailabilityTag.text = "Available"
        } else {
            lblAvailabilityTag.backgroundColor = UIColor.clear
            lblAvailabilityTag.borderWidth = 1
            lblAvailabilityTag.borderColor = UIColor(named: "2A382C")
            lblAvailabilityTag.textColor = UIColor(named: "2A382C")
            lblAvailabilityTag.text = "Not Available"
        }
        
        if let coordinates = stationDetails.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1], let searchLatitude = stationDetails.searchLatitude, let searchLongitude = stationDetails.searchLongitude {
            
            stationLatitude = latitude
            stationLongitude = longitude

            let distance = locationManager.calculateDistance(loc1Latitude: searchLatitude,
                                                             loc1Longitude: searchLongitude,
                                                             loc2Latitude: latitude,
                                                             loc2Longitude: longitude)
            
            lblDistance.text = String(format: "%.01f kms away", distance)
        }
        
        if let images = stationDetails.images {
            arrayOfImages = images
        }
        
        var chargeType = "DC"
        if let selectedChargeType = stationDetails.selectedChargeType {
            chargeType = selectedChargeType
        }
        
        if chargeType == "AC" {
            if let perUnitACCharge = stationDetails.perUnitACCharge {
                self.perUnitValueLabel.text = "₹\(Int(perUnitACCharge))"
            } else {
                self.perUnitValueLabel.text = "₹10"
            }
        } else {
            if let perUnitDCCharge = stationDetails.perUnitDCCharge {
                self.perUnitValueLabel.text = "₹\(Int(perUnitDCCharge))"
            } else {
                self.perUnitValueLabel.text = "₹10"
            }
        }
        
        if let favStations = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStations) as? [String] {
            
            if favStations.contains(stationDetails.id!) {
                self.favButton.setImage(UIImage(named: "heart-outline-red"), for: .normal)
            } else {
                self.favButton.setImage(UIImage(named: "heart-outline"), for: .normal)
            }
        }
        self.favButton.isUserInteractionEnabled = false
    }
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareBtnTap(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Open Location", message: "Choose an app to open direction", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { _ in
            // Pass the coordinate inside this URL
            let url = URL(string:                 "comgooglemaps://?saddr=&daddr=\(self.stationLatitude),\(self.stationLongitude)&directionsmode=driving")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { _ in
            // Pass the coordinate that you want here
            let coordinate = CLLocationCoordinate2DMake(self.stationLatitude,self.stationLongitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
            mapItem.name = "Destination"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func scheduleBtnTap(_ sender: Any) {
        moveToSlotSelectionView()
    }
    
    func moveToSlotSelectionView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "CheckAvailabilitySlotVC") as? CheckAvailabilitySlotVC
        vc?.modalPresentationStyle = .overFullScreen
        vc?.stationDetails = stationDetails
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}

extension ScheduleChargingVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.colVwPlugType {
            return stationDetails.supportedPorts.count
        } else {
            return arrayOfImages.count
        }
    }
    
    func parsePortNameValue(_ portValue:String) -> String {
        return portValue.components(separatedBy: ",").first ?? ""
    }
    
    func parsePowerValue(_ portValue:String) -> String {
        return portValue.components(separatedBy: ",").last ?? ""
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.colVwPlugType {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlugTypeColVwCell", for: indexPath) as? PlugTypeColVwCell
            cell?.lblCount.text = "\(stationDetails.supportedPortsCounts[stationDetails.supportedPorts[indexPath.row]] ?? 1)"
            
            let portName = self.parsePortNameValue(stationDetails.supportedPorts[indexPath.row])
            let portPowerValue = self.parsePowerValue(stationDetails.supportedPorts[indexPath.row])

            cell?.imgVwPlugType.image = UIImage(named: supportedPortsProperties[portName]?["typeImage"] ?? "type2")
            
            if let plugTypeDetails = Constants.Filters.PortTypes.first(where: { $0["id"] as? String == portName }), let plugName = plugTypeDetails["name"] as? String {
                cell?.lblType.text = plugName
            }
            
            cell?.bgView.backgroundColor = UIColor(named: supportedPortsProperties[portName]?["bgColor"] ?? "C3C7FF")
            cell?.lblPower.text = "\(portPowerValue) kw"
            if selectedPlugTypeIndex == indexPath.row {
                cell?.radioBtnn.isSelected = true
                self.perUnitValueLabel.text = "₹\(stationDetails.supportedPortsPerUnitCharge[stationDetails.supportedPorts[indexPath.row]] ?? 10)"
            } else {
                cell?.radioBtnn.isSelected = false
            }
//            cell?.radioBtnn.isHidden = true
            return cell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StationImageColVwCell", for: indexPath) as? StationImageColVwCell
            
            if let imageURL = arrayOfImages[indexPath.row] {
                
                cell?.imgVwStationImage.sd_setImage(with: URL(string: imageURL), placeholderImage: nil)
//                cell?.imgVwStationImage.imageFromServerURL(imageURL, placeHolder: UIImage(named: "dummy5"))
            }
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.colVwPlugType {
            return CGSize(width: 138, height: collectionView.frame.size.height)
        } else {
            return CGSize(width: 133, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.colVwPlugType {
            selectedPlugTypeIndex = indexPath.row
            collectionView.reloadData()
        }
    }
}

