//
//  SlotSelectionnVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import UIView_Shimmer

enum SlotState {
    case selected
    case unselected
    case reserved
}

class SlotSelectionVC: UIViewController {

    @IBOutlet weak var lblSlotSelection: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSlotRange: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblTimeDuration: UILabel!
    @IBOutlet weak var collectionVwTimeSlots: UICollectionView!
    @IBOutlet weak var bookSlotsButton: LoadingButton!
    
    
//    let timeSlots = ["03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30"]
//    var timeSlotsState: [slotState] = [.unselected, .selected, .selected, .selected, .selected, .selected, .unselected, .unavailable, .unavailable, .unselected]
    
    var timeSlotsState:[SlotState] = []
    
    var selectedDate:Date!
    var selectedStartTime:Date!
    var selectedEndTime:Date!
    
    var selectedDurationHour = 0
    var selectedDurationMinute = 0
    
    var selectedTimeFormatter = DateFormatter()
    var selectedDateFormatter = DateFormatter()
    var selectedDateAndTimeUTCFormatter = DateFormatter()
    var slotsTimeFormatter = DateFormatter()
    
    lazy var viewModel: SlotSelectionViewModel = {
        return SlotSelectionViewModel()
    }()
    
    var stationDetails:StationDetailsModel!
    var stationId = ""
    var selectedChargerType = ""
    var selectedPlugType = ""
    var selectedChargerId = ""
    var selectedPlugId = ""
    var selectedPowerValue = 0.0
    var selectedStartTimeValue = ""
    var selectedEndTimeValue = ""
    var selectedDurationValue = 0
    var minimumNumberOfSlots = 0

    var isSlotsSelected = false
    
    var selectedSlotFirstIndex = 0
    var selectedSlotLastIndex = 0
    
    var isQRCodeBooking = false


    override func viewDidLoad() {
        super.viewDidLoad()
        hidesBottomBarWhenPushed = true
        showInitialData()
        initViewModel()
        callAvailableSlotsAPI()
    }
    
    @IBAction func bookSlotsBtnTap(_ sender: Any) {
        callReserveSlotAPI()
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showInitialData() {
                
        slotsTimeFormatter.locale = Locale.current
        slotsTimeFormatter.dateFormat = "h:mm a"

        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "MMM dd"
        
        selectedTimeFormatter.locale = Locale.current
        selectedTimeFormatter.dateFormat = "h:mm a"
        
        lblDate.text = selectedDateFormatter.string(from: selectedDate)
        lblSlotRange.text = "\(selectedTimeFormatter.string(from: selectedStartTime)) - \(selectedTimeFormatter.string(from: selectedEndTime))"
        
        var selectedDurationHourText = ""
        if selectedDurationHour > 0 {
            if selectedDurationHour > 1 {
                selectedDurationHourText = "\(selectedDurationHour)hrs"
            } else {
                selectedDurationHourText = "\(selectedDurationHour)hr"
            }
        }
        
        if selectedDurationMinute > 1 {
            selectedDurationHourText = selectedDurationHourText + " \(selectedDurationMinute)mins"
        } else {
            selectedDurationHourText = selectedDurationHourText + " \(selectedDurationMinute)min"
        }
        lblDuration.text = selectedDurationHourText
        
        
        if let stationIdValue = stationDetails.id {
            stationId = stationIdValue
        }
        
        let arrayOfChargers = stationDetails.chargers
        if arrayOfChargers.count > 0, let chargerDetails = arrayOfChargers.first {
            
            if let powerValue = chargerDetails.power, let chargeTypeValue = chargerDetails.type, let chargerId = chargerDetails.id {
                
                let arrayOfPlugs = chargerDetails.plugs
                selectedPowerValue = powerValue
                selectedChargerType = chargeTypeValue.rawValue
                selectedChargerId = chargerId

                if arrayOfPlugs.count > 0, let plugDetails = arrayOfPlugs.first, let firstPlugTypeValue = plugDetails.supportedPort, let plugId = plugDetails.id {
                    
                    selectedPlugType = firstPlugTypeValue
                    selectedPlugId = plugId
                }
            }
        }
        
        selectedDateAndTimeUTCFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        selectedDateAndTimeUTCFormatter.timeZone = TimeZone(identifier: "UTC")
        selectedStartTimeValue = selectedDateAndTimeUTCFormatter.string(from: selectedStartTime)
        selectedEndTimeValue = selectedDateAndTimeUTCFormatter.string(from: selectedEndTime)
        selectedDurationValue = (selectedDurationHour * 60) + selectedDurationMinute
        minimumNumberOfSlots = selectedDurationValue/15
    }
    
    func initViewModel() {
        
        // Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.viewModel.isLoading = false
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
                
        viewModel.reloadSlotsListClosure = { [weak self] () in
            DispatchQueue.main.async {
                
                if (self?.viewModel.isLoading)! {
                    self?.viewModel.isLoading = false
                } else {
                    self?.updateInitialTimeSlotsState()
                }
            }
        }
        
        viewModel.isLoadingChangedClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.collectionVwTimeSlots.isUserInteractionEnabled = !(self?.viewModel.isLoading)!
                self?.updateInitialTimeSlotsState()
            }
        }
        
        viewModel.reloadReserveSlotClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.bookSlotsButton.hideLoading()
                if let error = self?.viewModel.reserveSlotResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else {
                    UserDefaults.standard.set(true, forKey: UserDefaults.Keys.createdNewReservation)
                    UserDefaults.standard.synchronize()
                    if let reserveData = self?.viewModel.reserveSlotResponse.data {
                        self?.moveToMachineDetailsView(reserveSlotDetailsModel: ReserveSlotDetailsModel(reserveData: reserveData))
                    }
                }
            }
        }
        
        viewModel.isLoading = true
    }
    
    func callAvailableSlotsAPI() {
    
        viewModel.availableSlotsApiCall(stationId: stationId,
                                        chargeType: selectedChargerType,
                                        powerValue: selectedPowerValue,
                                        plugType: selectedPlugType,
                                        startTime: selectedStartTimeValue,
                                        endTime: selectedEndTimeValue,
                                        durationInMin: selectedDurationValue)
    }
    
    func callReserveSlotAPI() {
        
        var selectedSlotStartTime = ""
        var selectedSlotEndTime = ""
        
        if let selectedStartSlotData = viewModel.arrayOfAvailableSlots[selectedSlotFirstIndex], let selectedEndSlotData = viewModel.arrayOfAvailableSlots[selectedSlotLastIndex], let slotStartTime = selectedStartSlotData.startTime, let slotStartTimeStamp = Double(slotStartTime), let slotEndTime = selectedEndSlotData.endTime, let slotEndTimeStamp = Double(slotEndTime) {
                    
            selectedSlotStartTime = selectedDateAndTimeUTCFormatter.string(from: Date(timeIntervalSince1970: slotStartTimeStamp/1000))
            selectedSlotEndTime = selectedDateAndTimeUTCFormatter.string(from: Date(timeIntervalSince1970: slotEndTimeStamp/1000))
        }
    
        self.bookSlotsButton.showLoading()
        //TODO: Call reserve api then call
        viewModel.reserveSlotApiCall(stationId: stationId,
                                     chargeType: selectedChargerType,
                                     powerValue: selectedPowerValue,
                                     plugType: selectedPlugType,
                                     startTime: selectedSlotStartTime,
                                     endTime: selectedSlotEndTime,
                                     chargeId:selectedChargerId,
                                     plugId:selectedPlugId)
    }
    
    func bookSlotButtonEnableOrDisable(_ enable:Bool) {
        
        if enable {
            bookSlotsButton.isEnabled = true
            bookSlotsButton.setTitleColor(UIColor.white, for: .normal)
            bookSlotsButton.backgroundColor = UIColor(hexString: "#2ECE54")
        } else {
            bookSlotsButton.isEnabled = false
            bookSlotsButton.setTitleColor(UIColor.lightText, for: .normal)
            bookSlotsButton.backgroundColor = UIColor.systemGray4
        }
    }
        
    func moveToMachineDetailsView(reserveSlotDetailsModel:ReserveSlotDetailsModel) {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "MachineDetailsVC") as? MachineDetailsVC
        vc?.modalPresentationStyle = .overFullScreen
        vc?.stationDetails = stationDetails
        vc?.reserveSlotDetailsModel = reserveSlotDetailsModel
        vc?.stationId = stationId
        vc?.selectedPlugType = selectedPlugType
        vc?.selectedChargerType = selectedChargerType
        vc?.selectedPowerValue = selectedPowerValue
        vc?.selectedChargerId = selectedChargerId
        vc?.selectedPlugId = selectedPlugId
        vc?.isQRCodeBooking = isQRCodeBooking
        
        if let selectedDateString = lblDate.text {
            vc?.selectedDate = selectedDateString
        }
        
        if let selectedDurationString = lblDuration.text {
            vc?.selectedDuration = selectedDurationString
        }
                
        if let selectedStartSlotData = viewModel.arrayOfAvailableSlots[selectedSlotFirstIndex], let selectedEndSlotData = viewModel.arrayOfAvailableSlots[selectedSlotLastIndex], let slotStartTime = selectedStartSlotData.startTime, let slotStartTimeStamp = Double(slotStartTime), let slotEndTime = selectedEndSlotData.endTime, let slotEndTimeStamp = Double(slotEndTime) {
                    
            vc?.selectedSlotStartTime = selectedDateAndTimeUTCFormatter.string(from: Date(timeIntervalSince1970: slotStartTimeStamp/1000))
            vc?.selectedSlotEndTime = selectedDateAndTimeUTCFormatter.string(from: Date(timeIntervalSince1970: slotEndTimeStamp/1000))
            
            vc?.selectedStartAndEndTime = "\(slotsTimeFormatter.string(from: Date(timeIntervalSince1970: slotStartTimeStamp/1000))) - \(slotsTimeFormatter.string(from: Date(timeIntervalSince1970: slotEndTimeStamp/1000)))"
        }
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}
 
//MARK: - Time Slots Selection Methods -
extension SlotSelectionVC {
    
    func updateInitialTimeSlotsState() {
        selectedSlotFirstIndex = 0
        selectedSlotLastIndex = 0
        isSlotsSelected = false
        timeSlotsState = []
        for i in 0..<viewModel.arrayOfAvailableSlots.count {
            if let eachSlotData = viewModel.arrayOfAvailableSlots[i] {
                var slotStatus = SlotState.unselected
                if let slotReserved = eachSlotData.reserved {
                    if slotReserved {
                        slotStatus = .reserved
                    }
                }
                timeSlotsState.append(slotStatus)
            }
        }
        self.selectInitilaTimeSlots()
        
        DispatchQueue.main.async {
            self.collectionVwTimeSlots.reloadData()
        }
    }
    
    func selectInitilaTimeSlots() {
        
        for i in 0..<viewModel.arrayOfAvailableSlots.count {
            
            if(checkSelectedTimeSlotIsValid(i)) {
                selectSlotsFromIndex(i)
                break
            }
        }
    }
    
    func selectSlotsFromIndex(_ index: Int) {
        isSlotsSelected = true
        for j in index..<(index + minimumNumberOfSlots) {
            timeSlotsState[j] = .selected
        }
        selectedSlotFirstIndex = index
        selectedSlotLastIndex = (index + minimumNumberOfSlots) - 1
    }
    
    func checkSelectedTimeSlotIsValid(_ selectedIndex:Int) -> Bool {
        
        var isValidIndex = false
        if (selectedIndex + minimumNumberOfSlots) <= timeSlotsState.count {
            
            for i in selectedIndex..<(selectedIndex + minimumNumberOfSlots) {
                
                if timeSlotsState[i] == .reserved {
                    isValidIndex = false
                    break
                } else {
                    isValidIndex = true
                }
            }
        }
        return isValidIndex
    }
    
    func unselectAllAvailableTimeSlots(){
        
        for i in 0..<viewModel.arrayOfAvailableSlots.count {
            
            if(timeSlotsState[i] == .selected) {
                timeSlotsState[i] = .unselected
            }
        }
        selectedSlotFirstIndex = 0
        selectedSlotLastIndex = 0
        isSlotsSelected = false
    }
}

extension SlotSelectionVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.isLoading {
            self.bookSlotButtonEnableOrDisable(false)
            return 10
        } else {
            if viewModel.arrayOfAvailableSlots.count > 0 {
                self.bookSlotButtonEnableOrDisable(true)
            } else {
                self.bookSlotButtonEnableOrDisable(false)
            }
            return viewModel.arrayOfAvailableSlots.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSlotColVwCell", for: indexPath) as? TimeSlotColVwCell
        
        if !viewModel.isLoading {
            
            if let particularSlotData = viewModel.arrayOfAvailableSlots[indexPath.row], let slotStartTime = particularSlotData.startTime, let slotStartTimeStamp = Double(slotStartTime) {
            
                cell?.lblTime.text = slotsTimeFormatter.string(from: Date(timeIntervalSince1970: slotStartTimeStamp/1000))
                
                if timeSlotsState.count > 0 {
                    switch timeSlotsState[indexPath.row] {
                        case .unselected:
                            cell?.lblTime.textColor = UIColor(hexString: "#6A746C")
                            cell?.vwTime.backgroundColor = UIColor(hexString: "#F4F4F4")
                        case .selected:
                            cell?.lblTime.textColor = UIColor.white
                            cell?.vwTime.backgroundColor = UIColor(hexString: "#2ECE54")
                        case .reserved:
                            cell?.lblTime.textColor = UIColor(hexString: "#DDDDDD")
                            cell?.vwTime.backgroundColor = .white
                            cell?.vwTime.borderWidth = 1
                            cell?.vwTime.borderColor = UIColor(hexString: "#DDDDDD")
                    }
                }
            }
        }        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 93, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if checkSelectedTimeSlotIsValid(indexPath.row){
            unselectAllAvailableTimeSlots()
            selectSlotsFromIndex(indexPath.row)
            collectionVwTimeSlots.reloadData()
        } else {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidSlotsSelection)
        }
    }
}
