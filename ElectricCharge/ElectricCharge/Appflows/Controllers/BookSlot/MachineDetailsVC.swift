//
//  MachineDetailsVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import Lottie
import MapKit
import SDWebImage

class MachineDetailsVC: UIViewController {

    @IBOutlet weak var animateView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnnShare: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var getOTPButton: LoadingButton!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var walletButton: LoadingButton!
    @IBOutlet weak var upiButton: LoadingButton!
    @IBOutlet weak var paymentMessageLabel: UILabel!
    @IBOutlet weak var machineDetailsTableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var upiPaymentTopView: UIView!
    @IBOutlet weak var upiPaymentView: UIView!
    @IBOutlet weak var upiTextField: UITextField!
    @IBOutlet weak var upiCheckButton: UIButton!
    @IBOutlet weak var upiConfirmButton: LoadingButton!
    
    
    @IBOutlet weak var upiPaymentWaitingTopView: UIView!
    @IBOutlet weak var upiPaymentWaitingView: UIView!
    @IBOutlet weak var upiPaymentTimerLabel: UILabel!
    @IBOutlet weak var cancelUPIPaymentButton: LoadingButton!
    @IBOutlet weak var upiPaymentWaitingActivityIndicator: UIActivityIndicatorView!
    
    
    let lottieAnimation = AnimationView(name: "details")
    var stationDetails:StationDetailsModel!
    let locationManager = LocationManager.shared
    
    var arrayOfStaionImages:[String?] = []
    var stationLatitude = 0.0
    var stationLongitude = 0.0
    
    var stationId = ""
    var selectedPlugType = ""
    var selectedChargerType = ""
    var selectedPowerValue = 0.0
    var selectedDate = ""
    var selectedStartAndEndTime = ""
    var selectedSlotStartTime = ""
    var selectedSlotEndTime = ""
    var selectedDuration = ""
    var selectedChargerId = ""
    var selectedPlugId = ""
    
    var reserveSlotDetailsModel:ReserveSlotDetailsModel!
    var estimatedAmount = 0.0

    var isFromBookingHistory = false
    var isQRCodeBooking = false

    var paymentIsSuccess = false
    var locationIsConfirmed = false
    
    lazy var viewModel: MachineDetailsViewModel = {
        return MachineDetailsViewModel()
    }()

    var timer: Timer?
    var count = 300 //300 seconds = 5 minutes
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hidesBottomBarWhenPushed = true
        loadTheAnimateView()
        showSationDetails()
        initViewModel()
        hideUPIPaymentView()
        hideUPIPaymentWaitingView()
        checkBookingHistoryFlowOrNot()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func loadTheAnimateView() {
        lottieAnimation.backgroundColor = .clear
        lottieAnimation.loopMode = .loop
        lottieAnimation.contentMode = .scaleAspectFit
        lottieAnimation.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        lottieAnimation.frame = CGRect(x: 0, y: 0, width: animateView.frame.size.width, height: animateView.frame.size.height)
        animateView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    func checkBookingHistoryFlowOrNot(){
        
        let bookingStatus = self.reserveSlotDetailsModel.status ?? .completed
        
        if bookingStatus == .reserved {//If Booking Payment is Success
            paymentIsSuccess = true
        }
        
        if isFromBookingHistory && (bookingStatus == .completed || bookingStatus == .canceled) {
            //If Booking Status is "COMPLETED" & "CANCELED"
            self.machineDetailsTableViewBottomConstraint.constant = 15
            self.bottomView.isHidden = true
        } else {
            //If New booking is created or If Booking Status is "HOLD" and "RESERVED"
            self.machineDetailsTableViewBottomConstraint.constant = bottomViewHeightConstraint.constant + 15
            self.bottomView.isHidden = false
        }
        maintainConfirmButtonOptions()
    }
    
    func maintainConfirmButtonOptions(){
        if paymentIsSuccess {
            self.paymentView.isHidden = true
            self.getOTPButton.isHidden = false
            if locationIsConfirmed {
                paymentMessageLabel.text = Constants.Messages.StartCharging
                self.getOTPButton.setTitle("Start Charging", for: .normal)
            } else {
                paymentMessageLabel.text = Constants.Messages.PaymentLocation
                self.getOTPButton.setTitle("Confirm Location", for: .normal)
            }
        } else {
            paymentMessageLabel.text = Constants.Messages.PaymentAuthorize
            self.paymentView.isHidden = false
            self.getOTPButton.isHidden = true
        }
    }
    
    func showAlertMessageView(isForSuccess:Bool, message:String) {
        let alertMessageView = AlertMessageView.shared
        alertMessageView.showMessage(isForSuccess: isForSuccess, message: message)
        alertMessageView.actionButtonClosure = { [weak self] (isForSuccess) in
                DispatchQueue.main.async {
                if isForSuccess {
                    if self?.isFromBookingHistory ?? false {
                        self?.popBack(2)//Pop to BookingHistory VC
                    } else {
                        if self?.isQRCodeBooking ?? false {
                            self?.popBack(4)//Pop to ListVC
                        } else {
                            self?.popBack(5)//Pop to ListVC
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                            if let tabBarViewController = (UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController as? UINavigationController)?.viewControllers.last as? TabbarViewController {
                                tabBarViewController.selectedIndex = 3//Showing BookingHistory VC
                            }
                        }
                    }
                } else {
                    self?.homeButtonAction((self?.homeButton)!)
                }
            }
        }
    }
        
    func showSationDetails() {
        
        lblTitle.text = stationDetails.name
        var stationAddress = stationDetails.description
        
        if let addressDict = stationDetails.address, let addressLine1 = addressDict.line1, let addressLine2 = addressDict.line2, let city = addressDict.city, let state = addressDict.state, let zipCode = addressDict.zip {
            stationAddress = addressLine1 + " " + addressLine2 + " " + city + " " + state + " " + zipCode
        }
        
        lblAddress.text = stationAddress
                
        if let coordinates = stationDetails.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1], let searchLatitude = stationDetails.searchLatitude, let searchLongitude = stationDetails.searchLongitude {

            stationLatitude = latitude
            stationLongitude = longitude
            
            let distance = locationManager.calculateDistance(loc1Latitude: searchLatitude,
                                                             loc1Longitude: searchLongitude,
                                                             loc2Latitude: latitude,
                                                             loc2Longitude: longitude)
            
            lblDistance.text = String(format: "%.01f kms away", distance)
        }
        
        if let images = stationDetails.images {
            arrayOfStaionImages = images
        }
        
        if let favStations = UserDefaults.standard.object(forKey: UserDefaults.Keys.favouriteStations) as? [String] {
            
            if favStations.contains(stationDetails.id!) {
                self.btnLike.setImage(UIImage(named: "heart-outline-red"), for: .normal)
            } else {
                self.btnLike.setImage(UIImage(named: "heart-outline"), for: .normal)
            }
        }
        self.btnLike.isUserInteractionEnabled = false
    }
    
    func initViewModel() {
        
        // Native binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.startChargingResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.getOTPButton.hideLoading()
                if let error = self?.viewModel.startChargingResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else {
                    //TODO: Currently skipping start charging VC
                    self?.moveToStopChargingView()
                }
            }
        }
        
        viewModel.upiPaymentResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.upiConfirmButton.hideLoading()
                self?.hideUPIPaymentView()
                if let error = self?.viewModel.upiPaymentResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else if let success = self?.viewModel.upiPaymentResponse.success, success == false, let message = self?.viewModel.upiPaymentResponse.message {
                    self?.displayMessageAlert(withMessage:message)
                } else {
                    //TODO: Save UPI Id when UPI Payment is success
                    var upiIds:[String] = []
                    if let UPIIds = UserDefaults.standard.object(forKey: UserDefaults.Keys.UPIIds) as? [String] {
                        upiIds = UPIIds
                        if let upiId = self?.upiTextField.text {
                            upiIds.append(upiId)
                        }
                    }
                    UserDefaults.standard.set(upiIds, forKey: UserDefaults.Keys.UPIIds)
                    UserDefaults.standard.synchronize()
                    self?.showUPIPaymentWaitingView()
                }
            }
        }
        
        viewModel.walletPaymentResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.walletButton.hideLoading()
                if let error = self?.viewModel.walletPaymentResponse.error, let errorMessage = self?.viewModel.walletPaymentResponse.message {
                    print("Pay Using Wallet Error = \(error)")
                    if errorMessage == Constants.Messages.InsufficientWalletBalance {
                        self?.showCustomAlert(error)
                    } else {
                        self?.displayMessageAlert(withMessage:errorMessage)
                    }
                    
                } else if let error = self?.viewModel.walletPaymentResponse.error {
//                    self?.displayMessageAlert(withMessage:error)
                    self?.showAlertMessageView(isForSuccess: false, message: error)
                } else {
                    self?.showViewAfterPaymentSuccess()
                    self?.showAlertMessageView(isForSuccess: true, message: "Booking created successfully!")
                }
            }
        }
    }
    
    func showCustomAlert(_ message: String) {
        let alert = UIAlertController(title: Constants.Alert.title, message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Recharge Wallet", style: .default, handler: { action in
            print("Recharge Wallet Action")
            self.moveToWalletDetailsView()
        }))
        
        alert.addAction( UIAlertAction(title: "Ok", style: .destructive, handler: { action in
            print("ok Action" )
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUPIPaymentView() {
        upiPaymentTopView.isHidden = false
    }
    
    func hideUPIPaymentView() {
        self.upiTextField.text = ""
        upiCheckButton.tintColor = UIColor(named: "DDDDDD")
        upiPaymentTopView.isHidden = true
    }
    
    func showUPIPaymentWaitingView() {
        //TODO: Show UPI Waiting UI and call UPI payment status API with some interval
        upiPaymentWaitingTopView.isHidden = false
        self.timer = nil
        showTheTimer()
        self.upiPaymentWaitingActivityIndicator.startAnimating()
    }
    
    func hideUPIPaymentWaitingView() {
        self.upiPaymentWaitingActivityIndicator.stopAnimating()
        upiPaymentWaitingTopView.isHidden = true
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    //MARK: UPI Waiting Timer Methods
    func showTheTimer() {
        if timer == nil {
            self.count = 300
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }

    @objc func update() {
        if count > 0 {
            count -= 1
            let minutes = String(format: "%02d", (count / 60))
            let seconds = String(format: "%02d", (count % 60))
            upiPaymentTimerLabel.text = minutes + " : " + seconds
        } else {
            upiPaymentTimerLabel.text = "00 : 00"
            if timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
            hideUPIPaymentWaitingView()
        }
    }
    
    func showViewAfterPaymentSuccess(){
        UserDefaults.standard.set(true, forKey: UserDefaults.Keys.createdNewReservation)
        UserDefaults.standard.synchronize()
        paymentIsSuccess = true
        maintainConfirmButtonOptions()
    }
    
    func startChargingAPICall() {
        if let reservationId = self.reserveSlotDetailsModel.id {
            viewModel.startChargingAPICall(reservationId: reservationId)
        }
    }
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func directionButtonAction(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Open Location", message: "Choose an app to open direction", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { _ in
            // Pass the coordinate inside this URL
            let url = URL(string:                 "comgooglemaps://?saddr=&daddr=\(self.stationLatitude),\(self.stationLongitude)&directionsmode=driving")!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { _ in
            // Pass the coordinate that you want here
            let coordinate = CLLocationCoordinate2DMake(self.stationLatitude,self.stationLongitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
            mapItem.name = "Destination"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func likeBtnTap(_ sender: Any) {
        
    }
    
    @IBAction func shareBtnTap(_ sender: Any) {
        
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        if isFromBookingHistory {
            popBack(2)//Pop to BookingHistoryVC
        } else {
            if self.isQRCodeBooking {
                self.popBack(4)//Pop to ListVC
            } else {
                self.popBack(5)//Pop to ListVC
            }
        }
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    @IBAction func getOTPBtnTap(_ sender: Any) {
        
        if paymentIsSuccess {
            if locationIsConfirmed {
                getOTPButton.showLoading()
                startChargingAPICall()
                //TODO: Call start charging api
            } else {
                
                if let coordinates = stationDetails.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1] {

                    stationLatitude = latitude
                    stationLongitude = longitude
                    
                    let distance = locationManager.calculateDistance(loc1Latitude: locationManager.currentLat,
                                                                     loc1Longitude: locationManager.currentLong,
                                                                     loc2Latitude: stationLatitude,
                                                                     loc2Longitude: stationLongitude) * 1000
                    
                    if distance < 500 {//Current location with in 500 meter to charging station
                        locationIsConfirmed = true
                        maintainConfirmButtonOptions()
                    } else {
                        self.displayMessageAlert(withMessage: Constants.Messages.PaymentLocation)
                    }
                }
            }
        }
    }
    
    
    @IBAction func upiButtonAction(_ sender: Any) {
        //TODO: Show Input UPI UI
        showUPIPaymentView()
    }
    
    @IBAction func walletButtonAction(_ sender: Any) {
        walletButton.showLoading()
        if let reservationId = self.reserveSlotDetailsModel.id {
            viewModel.walletPaymentAPICall(reservationId: reservationId)
        }
    }
    
    @IBAction func upiConfirmButtonAction(_ sender: Any) {
        
        if upiTextField.text?.isEmpty ?? false {
            self.displayMessageAlert(withMessage:Constants.InputFields.MissingUPIId)
        } else if !(upiTextField.text?.isValidUPIId())! {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidUPIId)
        } else {
            if let reservationId = self.reserveSlotDetailsModel.id, let upiId = upiTextField.text {
                self.upiConfirmButton.showLoading()
                viewModel.upiPaymentAPICall(reservationId: reservationId, UPIId: upiId)
            }
        }
    }
    
    @IBAction func upiPaymentViewCloseButtonAction(_ sender: Any) {
        hideUPIPaymentView()
    }
    
    @IBAction func upiPaymentWaitingViewCloseButtonAction(_ sender: Any) {
        hideUPIPaymentWaitingView()
    }
    
    @IBAction func cancelUPIPaymentButtonAction(_ sender: Any) {
        //TODO: Call Cancel UPI Payment API
        hideUPIPaymentWaitingView()
    }
    
    /*func moveToStartChargingView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "StartChargingVC") as? StartChargingVC
        vc?.modalPresentationStyle = .overFullScreen
        if let reservationId = reserveSlotDetailsModel.id {
            vc?.reservationId = reservationId
        }
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }*/
    
    func moveToStopChargingView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "StopChargingVC") as? StopChargingVC
        vc?.modalPresentationStyle = .overFullScreen
        
        if let reservationId = reserveSlotDetailsModel.id {
            vc?.reservationId = reservationId
        }
                
        if let reservationStartTime = reserveSlotDetailsModel.startTime {
            vc?.reservationStartTime = reservationStartTime
        }
        if let reservationEndTime = reserveSlotDetailsModel.endTime {
            vc?.reservationEndTime = reservationEndTime
        }
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
    
    func moveToWalletDetailsView() {
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "WalletVC") as? WalletVC
        vc?.modalPresentationStyle = .overFullScreen
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }
}

extension MachineDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MachineDetailsTblVWCell", for: indexPath) as? MachineDetailsTblVWCell
            cell?.selectionStyle = .none
            
            if arrayOfStaionImages.count > 0, let firstImageURL = arrayOfStaionImages.first as? String {
                cell?.imgVwMachine.sd_setImage(with: URL(string: firstImageURL), placeholderImage: nil)
            }
            
            if let plugDetails = reserveSlotDetailsModel.plug, let plugName = plugDetails.name {
                cell?.lblPlugName.text = plugName
            }
            
            if let chargerDetails = reserveSlotDetailsModel.charger, let chargerName = chargerDetails.name {
                cell?.lblMachineNameValue.text = chargerName
            }
            
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingDescTblVwCell", for: indexPath) as? ChargingDescTblVwCell
            cell?.selectionStyle = .none
            switch indexPath.row {
            case 1:
                if self.reserveSlotDetailsModel.status == .canceled || self.reserveSlotDetailsModel.status == .completed {
                    cell?.lblDescription.text = "Amount"
                } else {
                    cell?.lblDescription.text = "Estimated Amount"
                }
                if let estimatedAmount = self.reserveSlotDetailsModel.reservedAmount {
                    cell?.lblValue.text = String(format: "₹%.2f", Double(estimatedAmount))
                } else {
                    cell?.lblValue.text = String(format: "₹%.2f", 10.0)
                }
            case 2:
                cell?.lblDescription.text = "Booked Date"
                cell?.lblValue.text = self.selectedDate
            case 3:
                cell?.lblDescription.text = "Booking Status"
                cell?.lblValue.text = self.reserveSlotDetailsModel.status?.rawValue
                switch self.reserveSlotDetailsModel.status {
                    case .hold:
                    cell?.lblValue.textColor = UIColor.lightGray
                    cell?.lblValue.text = ReservationStatus.hold.rawValue
                        break
                    case .reserved:
                    cell?.lblValue.textColor = UIColor(named: "FFAE42")
                    cell?.lblValue.text = ReservationStatus.reserved.rawValue
                        break
                    case .running:
                    cell?.lblValue.textColor = UIColor(named: "2ECE54")
                    cell?.lblValue.text = ReservationStatus.running.rawValue
                        break
                    case .completed:
                    cell?.lblValue.textColor = UIColor(named: "2ECE54")
                    cell?.lblValue.text = ReservationStatus.completed.rawValue
                        break
                    case .canceled:
                    cell?.lblValue.textColor = UIColor(named: "FF3B30")
                    cell?.lblValue.text = ReservationStatus.canceled.rawValue
                        break
                    case .none,.some(.__unknown(_)):
                        break
                }
            case 4:
                cell?.lblDescription.text = "Duration"
                cell?.lblValue.text = self.selectedDuration
            case 5:
                cell?.lblDescription.text = "Selected Slots"
                cell?.lblValue.text = self.selectedStartAndEndTime
            default:
                cell?.lblDescription.text = "Selected Plug type"
                cell?.lblValue.text = self.selectedPlugType
            }
            return cell!
        }
    }
}

extension MachineDetailsVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            if textField == upiTextField {
                if updatedText.isValidUPIId() {
                    upiCheckButton.tintColor = UIColor(named: "2ECE54")
                } else {
                    upiCheckButton.tintColor = UIColor(named: "DDDDDD")
                }
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
