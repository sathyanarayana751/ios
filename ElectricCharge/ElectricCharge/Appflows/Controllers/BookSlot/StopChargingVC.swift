//
//  StopChargingVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import Lottie

class StopChargingVC: UIViewController {
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var lblChargeInProgress: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnStopCharging: LoadingButton!
    
    @IBOutlet weak var animateView: UIView!
    
    var count = 0
    var reservationId = ""
    var reservationStartTime = ""
    var reservationEndTime = ""

    var timer: Timer?

    lazy var viewModel: StopChargingViewModel = {
        return StopChargingViewModel()
    }()
    
    let lottieAnimation = AnimationView(name: "chargeAnimater")
    override func viewDidLoad() {
        super.viewDidLoad()
        hidesBottomBarWhenPushed = true
        lblChargeInProgress.text = "Charging on \nProgress"
        loadTheAnimateView()
        initViewModel()
        //showTheTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func loadTheAnimateView() {
        lottieAnimation.backgroundColor = .clear
        lottieAnimation.loopMode = .loop
        lottieAnimation.contentMode = .scaleAspectFit
        lottieAnimation.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        lottieAnimation.frame = CGRect(x: 0, y: 0, width: animateView.frame.size.width, height: animateView.frame.size.height)
        animateView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    func initViewModel() {
        
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.displayMessageAlert(withMessage:message)
                }
            }
        }
        
        viewModel.stopChargingResponseClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.btnStopCharging.hideLoading()
                if let error = self?.viewModel.stopChargingResponse.error {
                    self?.displayMessageAlert(withMessage:error)
                } else {
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        
        count = Int(TimeInterval(reservationEndTime)! - TimeInterval(reservationStartTime)!)/1000
        showTheTimer()
    }
    
    func showTheTimer() {
        if timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }

    @objc func update() {
        if count > 0 {
            count -= 1
            let hour = String(format: "%02d", (count / 3600))
            let minutes = String(format: "%02d", ((count % 3600) / 60 ))
            let seconds = String(format: "%02d", (count % 3600) % 60)
            lblDuration.text = hour + " : " + minutes + " : " + seconds
            lblDuration.isHidden = false
        } else {
            lblDuration.text = "00 : 00 : 00"
            lblDuration.isHidden = true
            if timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
        }
    }
    
    
    @IBAction func stopChargingTap(_ sender: Any) {
        btnStopCharging.showLoading()
        viewModel.stopChargingAPICall(reservationId: reservationId)
    }
    
    @IBAction func homeBtnnTap(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
