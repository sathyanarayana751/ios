//
//  CheckAvailabilitySlotVC.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class CheckAvailabilitySlotVC: UIViewController {
    @IBOutlet weak var checkAvailabilityTblView: UITableView!
    @IBOutlet weak var pickerTopView: UIView!
    @IBOutlet weak var pickerContentView: UIView!
    @IBOutlet weak var pickerContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerTitleLabel: UILabel!
    @IBOutlet weak var pickerCancelButton: UIButton!
    @IBOutlet weak var pickerSelectButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePickerView: UIPickerView!
    
    public enum TimeSelectionType : Int32 {

        case startTime = 1
        case endTime = 2
        case durationHour = 3
        case durationMinute = 4
        static var count: Int { return Int(TimeSelectionType.durationMinute.rawValue + 1)}
    }
    
    var timeSelectionType = TimeSelectionType(rawValue: 1)
    
    var stationDetails:StationDetailsModel!
    
    let availableDurationHours = ["00","01", "02", "03", "04", "05"]
    let availableDurationMinutes = ["00", "15", "30", "45"]
    
    var selectedDurationHourIndex = 1
    var selectedDurationMinuteIndex = 0
    
    var selectedTimeFormatter = DateFormatter()
    var selectedDateFormatter = DateFormatter()
    var selectedTimeWithDateFormatter = DateFormatter()

    var selectedDate:Date!
    var selectedStartTime:Date!
    var selectedEndTime:Date!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    func initialSetup() {
        
        selectedTimeFormatter.locale = Locale.current
        selectedTimeFormatter.dateFormat = "h:mm a"
        
        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "dd MM yyyy"
        
        selectedTimeWithDateFormatter.locale = Locale.current
        selectedTimeWithDateFormatter.dateFormat = "dd MM yyyy hh:mm a"
        
        checkAvailabilityTblView.rowHeight = UITableView.automaticDimension
        pickerTopView.isHidden = true
        
        selectedStartTime = Date().dateWithInterval(interval: (15.0 * 60.0), minimumInterval: (5.0 * 60.0))
        selectedEndTime = selectedStartTime.addingTimeInterval((60.0 * 60.0))
    }
    
    func showTimeSelectionPickerView() {
        switch timeSelectionType {
            case .startTime:
                pickerTitleLabel.text = "Select Start Time"
                datePicker.isHidden = false
                timePickerView.isHidden = true
                datePicker.setDate(self.getTimeWithDate(selectedStartTime)!, animated: true)
                pickerContentViewHeightConstraint.constant = 280
                break
            case .endTime:
                pickerTitleLabel.text = "Select End Time"
                datePicker.isHidden = false
                timePickerView.isHidden = true
                datePicker.setDate(self.getTimeWithDate(selectedEndTime)!, animated: true)
                pickerContentViewHeightConstraint.constant = 280
                break
            case .durationHour:
                pickerTitleLabel.text = "Select Hour"
                datePicker.isHidden = true
                timePickerView.isHidden = false
                timePickerView.reloadAllComponents()
                timePickerView.selectRow(selectedDurationHourIndex, inComponent: 0, animated: false)
                pickerContentViewHeightConstraint.constant = 250
                break
            case .durationMinute:
                pickerTitleLabel.text = "Select Minute"
                datePicker.isHidden = true
                timePickerView.isHidden = false
                timePickerView.reloadAllComponents()
                timePickerView.selectRow(selectedDurationMinuteIndex, inComponent: 0, animated: false)
                pickerContentViewHeightConstraint.constant = 250
                break
            default:
                break
        }
        showPickerView()
    }
    
    func showPickerView() {
        self.pickerTopView.isHidden = false
        self.pickerContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.35) {
            self.pickerContentView.transform = CGAffineTransform.identity
        } completion: { completion in}
    }
    
    func hidePickerView() {
        UIView.animate(withDuration: 0.35) {
            self.pickerContentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        } completion: { completion in
            self.pickerContentView.transform = CGAffineTransform.identity
            self.pickerTopView.isHidden = true
        }
    }
    
    @IBAction func moveToBackVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fromAndToTimeSelectionButtonAction(_ sender: Any) {
        if let button = sender as? UIButton {
            if TimeSelectionType(rawValue: Int32(button.tag)) == .startTime {
                timeSelectionType = .startTime
            } else {
                timeSelectionType = .endTime
            }
            showTimeSelectionPickerView()
        }
    }
    
    @IBAction func hourAndMinuteTimeSelectionButton(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if TimeSelectionType(rawValue: Int32(button.tag)) == .durationHour {
                timeSelectionType = .durationHour
            } else {
                timeSelectionType = .durationMinute
            }
            showTimeSelectionPickerView()
        }
    }
    
    @IBAction func checkAvailabilityButtonAction(_ sender: Any) {
        
        if !startTimeIsValid(selectedStartTime) {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidStartTime)
        } else if !endTimeIsValid(selectedEndTime) {
            //showAlert(Constants.InputFields.InvalidEndTime)
        } else if !checkSelectedChargingDurationIsValid() {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidChargingDuration)
        } else {
            moveToSlotSelection()
        }
    }
        
    @IBAction func pickerCancelButtonAction(_ sender: Any) {
        hidePickerView()
    }
    
    @IBAction func pickerSelectButtonAction(_ sender: Any) {
        
        switch timeSelectionType {
            case .startTime:
                if startTimeIsValid(datePicker.date) {
                    selectedStartTime = datePicker.date
                    self.checkAvailabilityTblView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                } else {
                    self.displayMessageAlert(withMessage:Constants.InputFields.InvalidStartTime)
                    return
                }
                break
            case .endTime:
                if endTimeIsValid(datePicker.date) {
                    selectedEndTime = datePicker.date
                    self.checkAvailabilityTblView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                } else {
                    //showAlert(Constants.InputFields.InvalidEndTime)
                    return
                }
                break
            case .durationHour:
                selectedDurationHourIndex = timePickerView.selectedRow(inComponent: 0)
                self.checkAvailabilityTblView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
                break
            case .durationMinute:
                selectedDurationMinuteIndex = timePickerView.selectedRow(inComponent: 0)
                self.checkAvailabilityTblView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
                break
            default:
                break
        }
        hidePickerView()
    }
        
    @objc func moveToSlotSelection() {
        
        let vc = ViewController.mainStoryBoard.instantiateViewController(withIdentifier: "SlotSelectionVC") as? SlotSelectionVC
        vc?.modalPresentationStyle = .overFullScreen
        vc?.selectedDate = selectedDate
        vc?.selectedStartTime = getTimeWithDate(selectedStartTime)
        
        if checkEndTimeIsNextDate(selectedEndTime) {
            if let selectedEndTimeWithNextDate = getTimeWithNextDate(selectedEndTime) {
                vc?.selectedEndTime = selectedEndTimeWithNextDate
            }
        } else {
            vc?.selectedEndTime = getTimeWithDate(selectedEndTime)
        }
       
        vc?.stationDetails = stationDetails
        
        if let selectedDurationHourValue = Int(availableDurationHours[selectedDurationHourIndex]), let selectedDurationMinuteValue = Int(availableDurationMinutes[selectedDurationMinuteIndex]) {
            vc?.selectedDurationHour = selectedDurationHourValue
            vc?.selectedDurationMinute = selectedDurationMinuteValue
        }        
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            self.present(vc!, animated: false, completion: nil)
        }
    }

}


//MARK: Time Slot Selection Methods
extension CheckAvailabilitySlotVC {
    
    func startTimeIsValid(_ selectedTime:Date) -> Bool {
        var isValid = false
        if let selectedStartTimeWithDate = getTimeWithDate(selectedTime) {
            if selectedStartTimeWithDate.timeIntervalSince1970 > Date().timeIntervalSince1970 {
                isValid = true
            }
        }
        return isValid
    }
    
    func getTimeWithDate(_ selectedTime:Date) -> Date? {
        
        let selectedDateString = selectedDateFormatter.string(from: selectedDate)
        let selectedTimeString = selectedTimeFormatter.string(from: selectedTime)
        let selectedTimeStringWithDate = "\(selectedDateString) \(selectedTimeString)"
        if let selectedTimeWithDate = selectedTimeWithDateFormatter.date(from: selectedTimeStringWithDate) {
            return selectedTimeWithDate
        }
        return nil
    }
    
    func getTimeWithNextDate(_ selectedTime:Date) -> Date? {
        
        let selectedDateString = selectedDateFormatter.string(from: selectedDate.addingTimeInterval(60*60*24))
        let selectedTimeString = selectedTimeFormatter.string(from: selectedTime)
        let selectedTimeStringWithDate = "\(selectedDateString) \(selectedTimeString)"
        if let selectedTimeWithDate = selectedTimeWithDateFormatter.date(from: selectedTimeStringWithDate) {
            return selectedTimeWithDate
        }
        return nil
    }
    
    func endTimeIsValid(_ selectedTime:Date) -> Bool {
        var isValid = false
        let currentDateTimeInterval = Date().timeIntervalSince1970
        
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), var selectedEndTimeWithDate = getTimeWithDate(selectedTime) {
            
            if checkEndTimeIsNextDate(selectedTime) {
                if let selectedEndTimeWithNextDate = getTimeWithNextDate(selectedTime) {
                    selectedEndTimeWithDate = selectedEndTimeWithNextDate
                }
            }
                        
            if selectedEndTimeWithDate.timeIntervalSince1970 > currentDateTimeInterval {
                if selectedEndTimeWithDate.timeIntervalSince1970 > selectedStartTimeWithDate.timeIntervalSince1970 {
                    if (selectedEndTimeWithDate.timeIntervalSince1970 - selectedStartTimeWithDate.timeIntervalSince1970) <= (6*60*60) {
                        isValid = true
                    } else {
                        self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime2)
                    }
                } else {
                    self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime1)
                }
            } else {
                self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime)
            }
        } else {
            self.displayMessageAlert(withMessage:Constants.InputFields.InvalidEndTime)
        }
        return isValid
    }
    
    func checkEndTimeIsNextDate(_ endTime:Date) -> Bool {
        var isNextDate = false
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), let selectedEndTimeWithDate = getTimeWithDate(endTime) {
            if selectedEndTimeWithDate.timeIntervalSince1970 < selectedStartTimeWithDate.timeIntervalSince1970 {
               //End time with in selected date
                isNextDate = true
            }
        }
        return isNextDate
    }
    
    func checkSelectedChargingDurationIsValid() -> Bool {
        
        var isValidChargingDuration = false
        
        if let selectedStartTimeWithDate = getTimeWithDate(selectedStartTime), var selectedEndTimeWithDate = getTimeWithDate(selectedEndTime), let selectedDurationHourValue = Double(availableDurationHours[selectedDurationHourIndex]), let selectedDurationMinuteValue = Double(availableDurationMinutes[selectedDurationMinuteIndex]) {
            
            let selectedDurationInseconds = ((selectedDurationHourValue * 60.0 * 60.0) + (selectedDurationMinuteValue * 60.0))
            if selectedDurationInseconds > 0 {
                if checkEndTimeIsNextDate(selectedEndTime) {
                    if let selectedEndTimeWithNextDate = getTimeWithNextDate(selectedEndTime) {
                        selectedEndTimeWithDate = selectedEndTimeWithNextDate
                    }
                }
                            
                if selectedDurationInseconds <= (selectedEndTimeWithDate.timeIntervalSince1970 - selectedStartTimeWithDate.timeIntervalSince1970) {
                    isValidChargingDuration = true
                }
            }
        }
        
        return isValidChargingDuration
    }
}

extension CheckAvailabilitySlotVC: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDateTblVwCell", for: indexPath) as? SelectDateTblVwCell
            
            cell?.dateChangeClosure = { [weak self] () in
                DispatchQueue.main.async {
                    if let selectedDateValue = cell?.selectedDate {
                        self?.selectedDate = selectedDateValue
                    }
                }
            }
            return cell!
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckAvailabilityTblVwCell", for: indexPath) as? CheckAvailabilityTblVwCell
            cell?.startTimeButton.setTitle(selectedTimeFormatter.string(from: selectedStartTime), for: .normal)
            cell?.endTimeButton.setTitle(selectedTimeFormatter.string(from: selectedEndTime), for: .normal)
            
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChargeDurationTblVwCell", for: indexPath) as? ChargeDurationTblVwCell
            cell?.durationHourButton.setTitle(availableDurationHours[selectedDurationHourIndex], for: .normal)
            cell?.durationMinuteButton.setTitle(availableDurationMinutes[selectedDurationMinuteIndex], for: .normal)
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckAvailabilityBtnTblVwCell", for: indexPath) as? CheckAvailabilityBtnTblVwCell
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 215
        case 1:
            return 190
        case 2:
            return 270
        default:
            return 70
        }
    }
}

extension CheckAvailabilitySlotVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        switch timeSelectionType {
            case .durationHour:
                return 1
            case .durationMinute:
                return 1
            default:
                return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch timeSelectionType {
            case .durationHour:
                return availableDurationHours.count
            case .durationMinute:
                return availableDurationMinutes.count
            default:
                return 0
        }
    }
        
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch timeSelectionType {
            case .durationHour:
                return availableDurationHours[row]
            case .durationMinute:
                return availableDurationMinutes[row]
            default:
                return ""
        }
    }
        
}
