//
//  HomeViewModel.swift
//  ElectricCharge
//
//  Created by iosdev on 2/16/22.
//

import Foundation

class HomeViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var reloadStationsListClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoadingChangedClosure: (()->())?
    
    var isLoading = true {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    var arrayOfStations:[StationDetailsModel] = []
//    var paginations:SearchQuery.Data.Search.Pagination = SearchQuery.Data.Search.Pagination()

    var searchResult: SearchQuery.Data.Search = SearchQuery.Data.Search() {
        didSet {
            self.reloadStationsListClosure?()
        }
    }
    var favouriteStationResultClosure: (()->())?
    var favStationId:String = "" {
        didSet {
            self.favouriteStationResultClosure?()
        }
    }
        
    func searchApiCall(query: String,
                       chargeType: [String],
                       plugType: [String],
                       withIn:[String:Any],
                       surrounding:[String:Any],
                       isForWithIn:Bool,
                       sortBy:String){
        
        let searchRequest = SearchRequestModel(query: query,
                                               chargeType: chargeType,
                                               plugType: plugType,
                                               withIn: withIn,
                                               surrounding: surrounding,
                                               isForWithIn: isForWithIn,
                                               sortBy: sortBy)
        
        networkManager.searchAPICall(searchRequest: searchRequest) { status, searchResult, error in
            if status {
                if let searchResponse = searchResult {
                    print("Search Response = \(searchResponse)")
                    self.arrayOfStations = []
                    if let stations = searchResponse.docs {
                        for eachStationDetails in stations {
                            if let stationsData = eachStationDetails?.station, let searchLat = surrounding["latitude"] as? Double, let searchLong = surrounding["longitude"] as? Double {
                                self.arrayOfStations.append(StationDetailsModel(searchStationData: stationsData, searchLatitude: searchLat, searchLongitude: searchLong))
                            }
                        }
                    }
                    self.searchResult = searchResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
        
    func favoriteApiCall(stationDetailsModel:StationDetailsModel){
            
        networkManager.favouriteApiCall(stationDetailsModel: stationDetailsModel) { status, favouriteStationResult, error in
            if status {
                if let favouriteStationResponse = favouriteStationResult {
                    print("Favourite Station Response = \(favouriteStationResponse)")
                    self.favStationId = stationDetailsModel.id!
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}


