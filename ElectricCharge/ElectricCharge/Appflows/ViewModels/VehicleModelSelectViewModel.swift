//
//  VehicleModelSelectViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 01/05/23.
//

import Foundation

class VehicleModelSelectViewModel {
    
    private let networkManager = NetworkManager.shared
    
    var showAlertClosure: (()->())?
    var vehicleManufacturerResponseClosure: (()->())?
    var vehicleModelsResponseClosure: (()->())?
    var arrayOfVehicleTypes:[String] = []
    var listOfVehicleTypes:[String:[String]] = [:]
    var listOfVehicleModels:[String:[String]] = [:]
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var vehicleManufacturerResponse:[VehicleManufacturerModel] = [] {
        didSet {
            self.vehicleManufacturerResponseClosure?()
        }
    }
    
    var vehicleModelsResponse:VehicleModelsQuery.Data.VehicleModel = VehicleModelsQuery.Data.VehicleModel() {
        didSet {
            self.vehicleModelsResponseClosure?()
        }
    }
      
    var isLoadingChangedClosure: (()->())?

    var isLoading = false {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    func getVehicleManufacturerAPICall() {
        
        networkManager.getVehicleManufacturerApiCall() { status, getVehicleManufacturerResponse, error in
            
            if status {
                var arrayOfVehicleManufacturer:[VehicleManufacturerModel] = []
                if let getVehicleManufacturerResult = getVehicleManufacturerResponse {
                    print("Get Vehicle Manufacturer Response = \(getVehicleManufacturerResult)")
                    for eachVehicleManufacturerDetails in getVehicleManufacturerResult {
                        if let vehicleManufacturerDetails = eachVehicleManufacturerDetails {
                            arrayOfVehicleManufacturer.append(VehicleManufacturerModel(vehicleManufacturerResponse: vehicleManufacturerDetails))
                        }
                    }
                }
                
                self.listOfVehicleTypes = [:]
                for eachVehicleManufacturer in arrayOfVehicleManufacturer {
                    for eachType in (eachVehicleManufacturer.type ?? []) {
                        if let eachTypeValue = eachType, eachTypeValue.count > 0, let makeValue = eachVehicleManufacturer.name {
                            if var arrayOfMake = self.listOfVehicleTypes[eachTypeValue] {
                                if !arrayOfMake.contains(makeValue) {
                                    arrayOfMake.append(makeValue)
                                }
                                self.listOfVehicleTypes[eachTypeValue] = arrayOfMake
                            } else {
                                self.listOfVehicleTypes[eachTypeValue] = [makeValue]
                            }
                        }
                    }
                }
                self.arrayOfVehicleTypes = self.listOfVehicleTypes.keys.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                self.vehicleManufacturerResponse = arrayOfVehicleManufacturer
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func getVehicleModelsAPICall(arrayOfManufactureIds:[String],
                                 arrayOfTypes:[String]) {
        
        networkManager.getVehicleModelsApiCall(arrayOfManufactureIds: arrayOfManufactureIds, arrayOfTypes: arrayOfTypes) { status, getVehicleModelsResponse, error in
            
            if status {
                self.listOfVehicleModels = [:]
                var arrayOfVehicleModels:[VehicleModel] = []
                if let vehicleModelsResult = getVehicleModelsResponse {
                    print("Get Vehicle Models Response = \(vehicleModelsResult)")
                    if let vehicleModels = vehicleModelsResult.docs {
                        for eachVehicleModel in vehicleModels {
                            if let eachVehicleModelDetails = eachVehicleModel {
                                arrayOfVehicleModels.append(VehicleModel(vehicleModelResponse: eachVehicleModelDetails))
                            }
                        }
                    }
                }
                for eachVehicleModel in arrayOfVehicleModels {
                    if let eachTypeValue = eachVehicleModel.type, let modelNameValue = eachVehicleModel.name {
                        if var arrayOfModels = self.listOfVehicleModels[eachTypeValue] {
                            if !arrayOfModels.contains(modelNameValue) {
                                arrayOfModels.append(modelNameValue)
                            }
                            self.listOfVehicleModels[eachTypeValue] = arrayOfModels
                        } else {
                            self.listOfVehicleModels[eachTypeValue] = [modelNameValue]
                        }
                    }
                }
                if let vehicleModelsResult = getVehicleModelsResponse {
                    self.vehicleModelsResponse = vehicleModelsResult
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
