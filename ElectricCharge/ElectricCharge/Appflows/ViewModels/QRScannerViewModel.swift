//
//  QRScannerViewModel.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 27/11/21.
//

import Foundation

class QRScannerViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoadingChangedClosure: (()->())?

    var isLoading = false {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    var stationDetailsModel:StationDetailsModel?
    var getResolveChargeResultClosure: (()->())?
    private var getResolveChargeResult: ResolveChargeQuery.Data.ResolveCharge = ResolveChargeQuery.Data.ResolveCharge() {
        didSet {
            self.getResolveChargeResultClosure?()
        }
    }

    
    func getResolveChargeAPICall(chargerId:String,
                                 plugId:String){
        
        networkManager.getResolveChargeApiCall(chargerId:chargerId,
                                               plugId:plugId) { status, resolveChargeResult, error in
            if status {
                if let resolveChargeResponse = resolveChargeResult {
                    print("Resolve Charge Response = \(resolveChargeResponse)")
                    if let stationDetails = resolveChargeResponse.station {
                        self.stationDetailsModel = StationDetailsModel(resolveChargeStationData: stationDetails)
                    }
                    self.getResolveChargeResult = resolveChargeResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
