//
//  WalletViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 01/09/22.
//

import Foundation

class WalletViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var walletDetailsResponseClosure: (()->())?
    var topUpWalletResponseClosure: (()->())?
    var isLoadingChangedClosure: (()->())?

    var isLoading = false {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var walletDetailsResponse: WalletQuery.Data.Wallet = WalletQuery.Data.Wallet()  {
        didSet {
            self.walletDetailsResponseClosure?()
        }
    }
    
    var topupWalletResponse: TopUpWalletMutation.Data.TopUpWallet = TopUpWalletMutation.Data.TopUpWallet()  {
        didSet {
            self.topUpWalletResponseClosure?()
        }
    }
    
    
    func walletDetailsAPICall(needToFetchFromServer:Bool){
        
        networkManager.walletDetailsApiCall(needToFetchFromServer:needToFetchFromServer) { status, walletDetailsResponse, error in
            if status {
                if let wallet = walletDetailsResponse {
                    print("Wallet Details Response = \(wallet)")
                    self.walletDetailsResponse = wallet
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func topupWalletAPICall(amount: Int){
        
        networkManager.topupWalletApiCall(amount: amount) { status, topupWalletResponse, error in
            if status {
                if let topupWallet = topupWalletResponse {
                    print("Topup Wallet Response = \(topupWallet)")
                    self.topupWalletResponse = topupWallet
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
