//
//  StartChargingViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 22/05/22.
//

import Foundation

class StartChargingViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var sendOTPResponseClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var sendOTPResponse: GenerateOtpMutation.Data.GenerateOtp = GenerateOtpMutation.Data.GenerateOtp() {
        didSet {
            self.sendOTPResponseClosure?()
        }
    }
    
    func startChargingOTPAPICall(reservationId: String){
        
        networkManager.startChargingOTPApiCall(reservationId: reservationId) { status, generateOTPResponse, error in
            if status {
                if let sendOTPResponse = generateOTPResponse {
                    print("Start Charging Send OTP Response = \(sendOTPResponse)")
                    self.sendOTPResponse = sendOTPResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
