//
//  OTPViewModel.swift
//  ElectricCharge
//
//  Created by iosdev on 2/24/22.
//

import Foundation

class OTPViewModel {
    
    private let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    
    var sendRegisterOTPResponseClosure: (()->())?
    var verifyRegisterOTPResponseClosure: (()->())?
    var sendLoginOTPResponseClosure: (()->())?
    var verifyLoginOTPResponseClosure: (()->())?
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var sendRegisterOTPResponse:ResendRegistrationOtpMutation.Data.ResendRegistrationOtp = ResendRegistrationOtpMutation.Data.ResendRegistrationOtp() {
        didSet {
            self.sendRegisterOTPResponseClosure?()
        }
    }
    
    var verifyRegisterOTPResponse:VerifyRegistrationOtpMutation.Data.VerifyRegistrationOtp = VerifyRegistrationOtpMutation.Data.VerifyRegistrationOtp() {
        didSet {
            self.verifyRegisterOTPResponseClosure?()
        }
    }
    
    var sendLoginOTPResponse:ResendLoginOtpMutation.Data.ResendLoginOtp = ResendLoginOtpMutation.Data.ResendLoginOtp() {
        didSet {
            self.sendLoginOTPResponseClosure?()
        }
    }
    
    var verifyLoginOTPResponse:VerifyLoginOtpMutation.Data.VerifyLoginOtp = VerifyLoginOtpMutation.Data.VerifyLoginOtp() {
        didSet {
            self.verifyLoginOTPResponseClosure?()
        }
    }
    
    func sendRegisterOTPCall(userId: String)  {
        
        networkManager.sendRegisterOTPCall(userId: userId){ status, sendOTPResponse, error in
            
            if status {
                if let resendRegistrationOtp = sendOTPResponse {
                    print("Send Register OTP Response = \(resendRegistrationOtp)")
                    self.sendRegisterOTPResponse = resendRegistrationOtp
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func resendLoginOTPCall(userId: String)  {
        
        networkManager.resendLoginOTPCall(userId: userId){ status, sendOTPResponse, error in
            
            if status {
                if let resendRegistrationOtp = sendOTPResponse {
                    print("Resend Login OTP Response = \(resendRegistrationOtp)")
                    self.sendLoginOTPResponse = resendRegistrationOtp
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func verifyRegisterOTPCall(userId: String, otp: String)  {
        
        networkManager.verifyRegisterOTPCall(userId: userId,
                                             otp: otp) { status, verifyOTPResponse, error in
            
            if status {
                if let verifyRegistrationOtp = verifyOTPResponse {
                    print("Verify Register OTP Response = \(verifyRegistrationOtp)")
                    self.verifyRegisterOTPResponse = verifyRegistrationOtp
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func verifyLoginOTPCall(userId: String, otp: String)  {
        
        networkManager.verifyLoginOTPCall(userId: userId,
                                             otp: otp) { status, verifyOTPResponse, error in
            
            if status {
                if let verifyLoginOtp = verifyOTPResponse {
                    print("Verify Login OTP Response = \(verifyLoginOtp)")
                    self.verifyLoginOTPResponse = verifyLoginOtp
                } else if verifyOTPResponse == nil {
                    self.alertMessage = Constants.Messages.NilResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
