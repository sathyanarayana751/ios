//
//  PlugTypeSelectViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 08/01/23.
//

import Foundation

class PlugTypeSelectViewModel {
    
    private let networkManager = NetworkManager.shared
    
    var showAlertClosure: (()->())?
    var portTypesResponseClosure: (()->())?

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var portTypesResponse:[PortTypesModel] = [] {
        didSet {
            self.portTypesResponseClosure?()
        }
    }
      
    var isLoadingChangedClosure: (()->())?

    var isLoading = false {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    func getPortTypesAPICall() {
        
        networkManager.getPortTypesApiCall() { status, getPortTypesResponse, error in
            
            if status {
                var arrayOfPortTypes:[PortTypesModel] = []
                if let getPortTypesResult = getPortTypesResponse {
                    print("Get Port Types Response = \(getPortTypesResult)")
                    for eachPortTypeDetails in getPortTypesResult {
                        if let portTypeDetails = eachPortTypeDetails {
                            arrayOfPortTypes.append(PortTypesModel(portTypesResponse: portTypeDetails))
                        }
                    }
                }
                self.portTypesResponse = arrayOfPortTypes
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}

