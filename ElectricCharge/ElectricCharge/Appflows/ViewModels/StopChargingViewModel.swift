//
//  StopChargingViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 23/05/22.
//

import Foundation

class StopChargingViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var stopChargingResponseClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var stopChargingResponse: StopChargingMutation.Data.StopCharging = StopChargingMutation.Data.StopCharging() {
        didSet {
            self.stopChargingResponseClosure?()
        }
    }
    
    func stopChargingAPICall(reservationId: String){
        
        networkManager.stopChargingApiCall(reservationId: reservationId) { status, stopChargingResponse, error in
            if status {
                if let stopCharging = stopChargingResponse {
                    print("Stop Charging Response = \(stopCharging)")
                    self.stopChargingResponse = stopCharging
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
