//
//  MachineDetailsViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 22/05/22.
//

import Foundation

class MachineDetailsViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var startChargingResponseClosure: (()->())?
    var upiPaymentResponseClosure: (()->())?
    var walletPaymentResponseClosure: (()->())?

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var startChargingResponse: StartChargingMutation.Data.StartCharging = StartChargingMutation.Data.StartCharging() {
        didSet {
            self.startChargingResponseClosure?()
        }
    }
    
    var upiPaymentResponse: PayForReservationFromUpiMutation.Data.PayForReservationFromUpi = PayForReservationFromUpiMutation.Data.PayForReservationFromUpi() {
        didSet {
            self.upiPaymentResponseClosure?()
        }
    }
    
    var walletPaymentResponse: PayForReservationFromWalletMutation.Data.PayForReservationFromWallet = PayForReservationFromWalletMutation.Data.PayForReservationFromWallet() {
        didSet {
            self.walletPaymentResponseClosure?()
        }
    }
    
    func startChargingAPICall(reservationId: String){
        
        networkManager.startChargingApiCall(reservationId: reservationId) { status, startChargingResponse, error in
            if status {
                if let startCharging = startChargingResponse {
                    print("Start Charging Response = \(startCharging)")
                    self.startChargingResponse = startCharging
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func upiPaymentAPICall(reservationId: String,
                           UPIId:String){
        
        networkManager.upiPaymentApiCall(reservationId: reservationId,
                                         UPIId: UPIId) { status, upiPaymentResponse, error in
            if status {
                if let upiPayment = upiPaymentResponse {
                    print("UPI Payment Response = \(upiPayment)")
                    self.upiPaymentResponse = upiPayment
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func walletPaymentAPICall(reservationId: String){
        
        networkManager.walletPaymentApiCall(reservationId: reservationId) { status, walletPaymentResponse, error in
            if status {
                if let walletPayment = walletPaymentResponse {
                    print("Wallet Payment Response = \(walletPayment)")
                    self.walletPaymentResponse = walletPayment
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
