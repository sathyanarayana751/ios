//
//  AuthViewModel.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 19/12/21.
//

import Foundation

class AuthorizationViewModel {
    
    private let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    
    var loginResponseClosure: (()->())?
    var registerResponseClosure: (()->())?
    var socialLoginResponseClosure: (()->())?
    var socialLoginUpdateDetailsResponseClosure: (()->())?
    var forgetPasswordResponseClosure: (()->())?

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    var loginResponse:LoginMutation.Data.LoginUser = LoginMutation.Data.LoginUser() {
        didSet {
            self.loginResponseClosure?()
        }
    }
    
    var registerResponse:RegisterUserMutation.Data.RegisterUser = RegisterUserMutation.Data.RegisterUser() {
        didSet {
            self.registerResponseClosure?()
        }
    }
    
    var socialLoginResponse:SocialLoginWithGoogleTokenMutation.Data.SocialLoginWithGoogleToken = SocialLoginWithGoogleTokenMutation.Data.SocialLoginWithGoogleToken() {
        didSet {
            self.socialLoginResponseClosure?()
        }
    }
    
    var socialLoginUpdateDetailsResponse:SocialLoginUpdateUserDetailsMutation.Data.SocialLoginUpdateUserDetail = SocialLoginUpdateUserDetailsMutation.Data.SocialLoginUpdateUserDetail() {
        didSet {
            self.socialLoginUpdateDetailsResponseClosure?()
        }
    }
    
    var forgetPasswordResponse:ForgetPasswordMutation.Data.ForgetPassword = ForgetPasswordMutation.Data.ForgetPassword() {
        didSet {
            self.forgetPasswordResponseClosure?()
        }
    }
    
    func loginApiCall(userEmail: String,
                      password: String) {
        
        networkManager.loginApiCall(userEmail: userEmail,
                                    password: password) { status, loginResponse, error in
            
            if status {
                if let loginUser = loginResponse {
                    print("Login Response = \(loginUser)")
                    self.loginResponse = loginUser
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func registerApiCall(name: String,
                         phone: String,
                         email: String,
                         password: String)  {
        
        networkManager.registerApiCall(name: name,
                                       phone: phone,
                                       email: email,
                                       password: password) { status, registerResponse, error in
            
            if status {
                if let registerUser = registerResponse {
                    print("Register Response = \(registerUser)")
                    self.registerResponse = registerUser
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func socialLoginApiCall(token: String) {
        
        networkManager.socialLoginApiCall(token: token) { status, socialLoginResponse, error in
            
            if status {
                print("Social Login Response = \(String(describing: socialLoginResponse))")
                if let socialLoginUser = socialLoginResponse {
                    self.socialLoginResponse = socialLoginUser
                } else {
                    self.socialLoginResponse = SocialLoginWithGoogleTokenMutation.Data.SocialLoginWithGoogleToken()
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func socialLoginUpdateDetailsApiCall(token:String,
                                         name: String,
                                         phone: String,
                                         email: String,
                                         password: String)  {
        
        networkManager.socialLoginUpdateUserDetailsApiCall(token: token,
                                                           name: name,
                                                           phone: phone,
                                                           email: email,
                                                           password: password) { status, socialLoginUpdateDetailsResponse, error in
            
            if status {
                print("Social Login Update Details Response = \(String(describing: socialLoginUpdateDetailsResponse))")
                if let socialLoginUpdateDetailsResult = socialLoginUpdateDetailsResponse {
                    self.socialLoginUpdateDetailsResponse = socialLoginUpdateDetailsResult
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func forgetPasswordApiCall(userName: String) {
        
        networkManager.forgetPasswordApiCall(userName: userName) { status, forgetPasswordResponse, error in
            
            if status {
                if let forgetPasswordResult = forgetPasswordResponse {
                    print("Forget Password Response = \(forgetPasswordResult)")
                    self.forgetPasswordResponse = forgetPasswordResult
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
