//
//  SlotSelectionViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 17/03/22.
//

import Foundation

class SlotSelectionViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var reloadSlotsListClosure: (()->())?
    var reloadReserveSlotClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoadingChangedClosure: (()->())?
    
    var isLoading = true {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    var arrayOfAvailableSlots:[AvailabilitySlotQuery.Data.AvailabilitySlot.Datum.FormattedSlot?] = []
    var paginations:SearchQuery.Data.Search.Pagination = SearchQuery.Data.Search.Pagination()

    var availableSlotsResult: AvailabilitySlotQuery.Data.AvailabilitySlot = AvailabilitySlotQuery.Data.AvailabilitySlot() {
        didSet {
            self.reloadSlotsListClosure?()
        }
    }
    
    var reserveSlotResponse: ReserveMutation.Data.Reserve = ReserveMutation.Data.Reserve() {
        didSet {
            self.reloadReserveSlotClosure?()
        }
    }
    
    
    func availableSlotsApiCall(stationId: String,
                               chargeType: String,
                               powerValue: Double,
                               plugType:String,
                               startTime:String,
                               endTime:String,
                               durationInMin:Int){
        
        let availableSlotRequest = AvailableSlotRequestModel(stationId: stationId,
                                                        powerValue: powerValue,
                                                        chargeType: chargeType,
                                                        plugType: plugType,
                                                        startTime: startTime,
                                                        endTime: endTime,
                                                        durationInMin: durationInMin)
        
        networkManager.availableSlotAPICall(availableSlotRequest: availableSlotRequest) { status, availableSlotResult, error in
            if status {
                if let availableSlotResponse = availableSlotResult {
                    print("Available Slot Response = \(availableSlotResponse)")
                    self.arrayOfAvailableSlots = []
                    if let success = availableSlotResponse.success {
                        if success {
                            if let availableSlots = availableSlotResponse.data?.formattedSlots {
                                self.arrayOfAvailableSlots = availableSlots
                            } else {
                                if let message = availableSlotResponse.message {
                                    self.alertMessage = message
                                }
                            }
                        } else {
                            
                            if let errorMessage = availableSlotResponse.error {
                                self.alertMessage = errorMessage
                            }
                        }
                    }
                    self.availableSlotsResult = availableSlotResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    
    func reserveSlotApiCall(stationId: String,
                            chargeType: String,
                            powerValue: Double,
                            plugType:String,
                            startTime:String,
                            endTime:String,
                            chargeId:String,
                            plugId:String){
        
        let reserveSlotRequest = ReserveSlotRequestModel(stationId: stationId,
                                                         powerValue: powerValue,
                                                         chargeType: chargeType,
                                                         plugType: plugType,
                                                         startTime: startTime,
                                                         endTime: endTime,
                                                         chargerId: chargeId,
                                                         plugId: plugId)
        
        networkManager.reserveSlotApiCall(reserveSlotRequestModel: reserveSlotRequest) { status, reserveSlotResult, error in
            if status {
                if let reserveSlotResponse = reserveSlotResult {
                    print("Reserve slot response = \(reserveSlotResponse)")
                    if let success = reserveSlotResponse.success {
                        self.reserveSlotResponse = reserveSlotResponse
                        if !success {
                            if let errorMessage = reserveSlotResponse.error {
                                self.alertMessage = errorMessage
                            }
                        }
                    } else {
                        self.reserveSlotResponse = reserveSlotResponse
                    }
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
