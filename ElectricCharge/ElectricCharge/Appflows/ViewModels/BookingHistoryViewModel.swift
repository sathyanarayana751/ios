//
//  BookingHistoryViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 23/05/22.
//

import Foundation

class BookingHistoryViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var reloadReservedSlotsListClosure: (()->())?
    var reloadPastBookingsListClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoadingChangedClosure: (()->())?
    
    var isLoading = true {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    
    var arrayOfReservedSlots:[ReservationsListQuery.Data.Reservation.Doc?] = []
    var arrayOfPastBookings:[ReservationsListQuery.Data.Reservation.Doc?] = []

//    var paginations:ReservationsListQuery.Data.Reservation.Pagination = ReservationsListQuery.Data.Reservation.Pagination()
    
    var hasNextPage = true
    var currentPage = 0
    var eachPageLimit = 10

    var reservedSlotsResult: ReservationsListQuery.Data.Reservation = ReservationsListQuery.Data.Reservation() {
        didSet {
            self.reloadReservedSlotsListClosure?()
        }
    }
    
    var pastBookingsListResult: ReservationsListQuery.Data.Reservation = ReservationsListQuery.Data.Reservation() {
        didSet {
            self.reloadPastBookingsListClosure?()
        }
    }
    
    //.reserved, .running
    func reserveSlotsListApiCall(needToFetchFromServer:Bool,status:[ReservationStatus]) {
        
        networkManager.reservationsListApiCall(page: 1, limit: 10, status: status, needToFetchFromServer:needToFetchFromServer) { status, reserveSlotsListResult, error in
         
            if status {
                if let reservedSlotsResponse = reserveSlotsListResult {
                    print("Reserved Slots List Response = \(reservedSlotsResponse)")
                    self.arrayOfReservedSlots = []
                    if let reservedSlots = reservedSlotsResponse.docs {
                        print("Reserved Slots Count = \(reservedSlots.count)")
                        for eachSlot in reservedSlots {
                            if eachSlot?.status == .reserved || eachSlot?.status == .running || eachSlot?.status == .hold {
                                if self.arrayOfReservedSlots.count < 3 {
                                    self.arrayOfReservedSlots.append(eachSlot)
                                }
                            }
                        }
                    }
                    self.reservedSlotsResult = reservedSlotsResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
    //.canceled, .completed
    func pastBookingsListApiCall(status:[ReservationStatus]) {
        
        networkManager.reservationsListApiCall(page: currentPage + 1,
                                               limit: eachPageLimit,
                                               status: status,
                                               needToFetchFromServer:false) { status, pastBookingsListResult, error in
         
            if status {
                if let pastBookingsListResponse = pastBookingsListResult {
                    print("Past Bookings List Response = \(pastBookingsListResponse)")
                    if let pastBookings = pastBookingsListResponse.docs {
                        for eachBooking in pastBookings {
                            if eachBooking?.status == .canceled || eachBooking?.status == .completed {
                                self.arrayOfPastBookings.append(eachBooking)
                            }
                        }
                        if let pagination = pastBookingsListResponse.pagination, let hasNextPage = pagination.hasNextPage, let currentPage = pagination.page  {
                            self.hasNextPage = (hasNextPage == 1)
                            self.currentPage = currentPage
                        }
                    }
                    self.pastBookingsListResult = pastBookingsListResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
