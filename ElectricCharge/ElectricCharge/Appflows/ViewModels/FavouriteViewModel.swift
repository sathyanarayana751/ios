//
//  FavouriteViewModel.swift
//  ElectricCharge
//
//  Created by iosdev on 2/24/22.
//

import Foundation

class FavouriteViewModel {
    
    let networkManager = NetworkManager.shared
    var showAlertClosure: (()->())?
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoadingChangedClosure: (()->())?

    var isLoading = false {
        didSet {
            isLoadingChangedClosure?()
        }
    }
    var arrayOfStations:[StationDetailsModel] = []
    var getUserDetailsResultClosure: (()->())?
    private var getUserDetailsResult: UserQuery.Data.User = UserQuery.Data.User() {
        didSet {
            self.getUserDetailsResultClosure?()
        }
    }

    
    func getUserDetailsAPICall(needToFetchFromServer:Bool){
        
        networkManager.getUserDetailsAPICall(needToFetchFromServer:needToFetchFromServer) { status, userDetailsResult, error in
            if status {
                if let userDetailsResponse = userDetailsResult {
                    print("User Details Response = \(userDetailsResponse)")
                    self.arrayOfStations = []
                    if let stations = userDetailsResponse.preference?.stations {
                        for eachStationDetails in stations {
                            if let stationsData = eachStationDetails {
                                self.arrayOfStations.append(StationDetailsModel(favStationData: stationsData))
                            }
                        }
                    }
                    self.getUserDetailsResult = userDetailsResponse
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
