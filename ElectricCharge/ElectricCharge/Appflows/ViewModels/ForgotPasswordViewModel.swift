//
//  ForgotPasswordViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 27/06/22.
//

import Foundation

class ForgotPasswordViewModel {
    
    private let networkManager = NetworkManager.shared
    
    var showAlertClosure: (()->())?
    var forgetPasswordResponseClosure: (()->())?

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var forgetPasswordResponse:ForgetPasswordMutation.Data.ForgetPassword = ForgetPasswordMutation.Data.ForgetPassword() {
        didSet {
            self.forgetPasswordResponseClosure?()
        }
    }
        
    func forgetPasswordApiCall(userName: String) {
        
        networkManager.forgetPasswordApiCall(userName: userName) { status, forgetPasswordResponse, error in
            
            if status {
                if let forgetPasswordResult = forgetPasswordResponse {
                    print("Forget Password Response = \(forgetPasswordResult)")
                    self.forgetPasswordResponse = forgetPasswordResult
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
