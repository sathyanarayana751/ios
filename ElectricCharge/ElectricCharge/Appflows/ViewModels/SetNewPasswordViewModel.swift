//
//  SetNewPasswordViewModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 27/06/22.
//

import Foundation

class SetNewPasswordViewModel {
    
    private let networkManager = NetworkManager.shared
    
    var showAlertClosure: (()->())?
    var setNewPasswordResponseClosure: (()->())?

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var setNewPasswordResponse:ResetUserPasswordMutation.Data.ResetUserPassword = ResetUserPasswordMutation.Data.ResetUserPassword() {
        didSet {
            self.setNewPasswordResponseClosure?()
        }
    }
        
    func setNewPasswordApiCall(userId:String,
                               password: String) {
        
        networkManager.setNewPasswordApiCall(userId:userId,
                                             password: password) { status, setNewPasswordResponse, error in
            
            if status {
                if let setNewPasswordResult = setNewPasswordResponse {
                    print("Set New Password Response = \(setNewPasswordResult)")
                    self.setNewPasswordResponse = setNewPasswordResult
                }
            } else {
                //show error message
                if let errorMessage = error?.localizedDescription {
                    self.alertMessage = errorMessage
                }
            }
        }
    }
}
