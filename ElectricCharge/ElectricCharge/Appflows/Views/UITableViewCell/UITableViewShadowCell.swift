//
//  BookingHistoryBaseFilterTableVwCell.swift
//  ElectricCharge
//
//  Created by Raghavendra on 19/03/22.
//

import UIKit

class UITableViewShadowCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 3.0
    }
    
    override func layoutSubviews() {
//        self.bgView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
