//
//  AlertMessageView.swift
//  ElectricCharge
//
//  Created by Raghavendra on 26/03/23.
//

import Foundation
import UIKit
import Lottie

class AlertMessageView: UIView {
    
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var animationView: UIView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var actionButton: LoadingButton!
    
    var lottieAnimation = AnimationView(name: "success_loader")
    private static var obj: AlertMessageView? = nil
    var actionButtonClosure: ((Bool)->())?
    var isForSuccess = false

    static var shared: AlertMessageView {
        if obj == nil {
            obj = UINib(nibName: "AlertMessageView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? AlertMessageView
        }
        return obj!
    }
    
    func loadTheAnimateView() {
        lottieAnimation.backgroundColor = .clear
        lottieAnimation.loopMode = .loop
        lottieAnimation.contentMode = .scaleAspectFit
        lottieAnimation.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        lottieAnimation.frame = CGRect(x: 0, y: 0, width: animationView.frame.size.width, height: animationView.frame.size.height)
        animationView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    func showMessage(isForSuccess:Bool, message:String) {
        self.isForSuccess = isForSuccess
        if isForSuccess {
            lottieAnimation = AnimationView(name: "success_loader")
            title.text = "Success"
            self.message.text = message
        } else {
            lottieAnimation = AnimationView(name: "failure_loader")
            title.text = "Failure"
            self.message.text = message
        }
        self.loadTheAnimateView()
        DispatchQueue.main.async {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
           
            self.frame = window.frame
            window.addSubview(self)
            self.layoutIfNeeded()
            self.alpha = 1
            
            self.alertView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.35) {
                self.alertView.transform = CGAffineTransform.identity
            } completion: { completion in}
        }
    }
    
    func removeTheView() {
        
        UIView.animate(withDuration: 0.35) {
            self.alertView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        } completion: { [weak self] completion in
            self?.alertView.transform = CGAffineTransform.identity
            self?.alpha = 0
            self?.layoutIfNeeded()
            self?.removeFromSuperview()
            AlertMessageView.obj = nil
        }
    }
    
    @IBAction func actionButtonAction(_ sender: Any) {
        self.actionButtonClosure?(self.isForSuccess)
        self.removeTheView()
    }
}
