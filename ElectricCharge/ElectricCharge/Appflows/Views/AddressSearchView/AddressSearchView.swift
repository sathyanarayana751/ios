//
//  AddressSearchView.swift
//  ElectricCharge
//
//  Created by Raghavendra on 17/06/22.
//

import Foundation
import UIKit

class AddressSearchView: UIView {
    
    @IBOutlet weak var topSearchView: UIView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var searchAddressTableView: AddressSearchTableView!
    
    @IBOutlet weak var searchAddressTableTopView: UIView!
    
    @IBOutlet weak var searchAddressTableViewHeightConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var topSearchViewWidthConstraint: NSLayoutConstraint!
    
    
    private static var obj: AddressSearchView? = nil
    let geocodeManager = GooglePlacesManager.shared
    
    private var places = [GooglePlace]() {
        didSet { searchAddressTableView.reloadData() }
    }
        
    var selectedAddressClosure: (()->())?

    var selectedAddress: GooglePlaceDetails? {
        didSet {
            self.selectedAddressClosure?()
        }
    }
    
    static var shared: AddressSearchView {
        if obj == nil {
            obj = UINib(nibName: "AddressSearchView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? AddressSearchView
        } else {
            obj?.searchAddressTableView.reloadData()
            obj?.searchAddressTableView.setContentOffset(CGPoint.zero, animated:true)
        }

        return obj!
    }
    
    func setup() {

        DispatchQueue.main.async {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
           
            self.frame = window.frame
            window.addSubview(self)
            self.layoutIfNeeded()
            self.alpha = 1
            self.searchAddressTableTopView.dropShadow(radius: 15)
            self.cancelButton.isHidden = true
            
            self.topSearchViewWidthConstraint.constant = self.frame.width - (80 * 2)

            UIView.animate(withDuration:0.3, animations: {
                self.layoutIfNeeded()
                self.alpha = 1
            }){ completed in
                self.cancelButton.isHidden = false
                self.searchTextField.becomeFirstResponder()
            }
        }
    }
    
    func removeTheView() {
        searchTextField.resignFirstResponder()
        places = []
        self.searchAddressTableView.reloadData()
        
        self.alpha = 1
        self.topSearchViewWidthConstraint.constant = 50
        self.cancelButton.isHidden = true

        UIView.animate(withDuration: 0.35) {
            self.layoutIfNeeded()
        } completion: { [weak self] (_) in
            self?.alpha = 0
            self?.layoutIfNeeded()
            self?.removeFromSuperview()
            AddressSearchView.obj = nil
        }
    }
    
    func showTableView(){
        self.layoutIfNeeded()
        self.searchAddressTableViewHeightConstarint.constant = 250
        
        UIView.animate(withDuration:0.3, animations: {
            self.layoutIfNeeded()
        })
    }
    
    func hideTableView(){
        
        self.layoutIfNeeded()
        self.searchAddressTableViewHeightConstarint.constant = 0
        UIView.animate(withDuration:0.3, animations: {
            self.layoutIfNeeded()
        })
    }
    
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        removeTheView()
    }
    
}

extension AddressSearchView:  UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if places.count > 0 {
            self.showTableView()
        } else {
            self.hideTableView()
        }
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let addressCell = tableView.dequeueReusableCell(withIdentifier: "AddressDetailsTableViewCell") as? AddressDetailsTableViewCell
        addressCell?.addressLabel.attributedText = places[indexPath.row].fullAddress
        return addressCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let placeId = places[indexPath.row].id {
            geocodeManager.getPlaceDetails(id: placeId) { [unowned self] in
                guard let value = $0 else { return }
                self.selectedAddress = value
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

class AddressSearchTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        register(AddressDetailsTableViewCell.nib, forCellReuseIdentifier: AddressDetailsTableViewCell.identifier!)
    }
}


extension AddressSearchView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else { return false }
//        let parameters = geocodeManager.getParameters(for: text)
        geocodeManager.getPlaces(with: text) {
            self.places = $0
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            
            if updatedText.isEmpty {
                places = []
            } else {
//                let parameters = geocodeManager.getParameters(for: updatedText)
                geocodeManager.getPlaces(with: updatedText) {
                    self.places = $0
                }
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        places = []
        return true
    }
}
