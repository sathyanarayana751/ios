//
//  LoaderView.swift
//  ElectricCharge
//
//  Created by Raghavendra on 12/03/22.
//

import UIKit
import Lottie

class LoaderView: UIView {
    
    @IBOutlet weak var animateView: UIView!

    private static var obj: LoaderView? = nil
    let lottieAnimation = AnimationView(name: "chargeAnimater")
    
    static var shared: LoaderView {
        if obj == nil {
            obj = UINib(nibName: "LoaderView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? LoaderView
        }
        return obj!
    }
    
    func loadTheAnimateView() {
        lottieAnimation.backgroundColor = .clear
        lottieAnimation.loopMode = .loop
        lottieAnimation.contentMode = .scaleAspectFit
        lottieAnimation.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        lottieAnimation.frame = CGRect(x: 0, y: 0, width: animateView.frame.size.width, height: animateView.frame.size.height)
        animateView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    func showLoader() {
        
        self.loadTheAnimateView()

        DispatchQueue.main.async {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
           
            self.frame = window.frame
            window.addSubview(self)
            self.layoutIfNeeded()
            self.alpha = 0
            UIView.animate(withDuration:0.3, animations: {
                self.alpha = 1
                self.layoutIfNeeded()
            })
        }
    }
    
    func hideLoader() {
        if LoaderView.obj != nil {
            UIView.animate(withDuration: 0.5) {
                self.alpha = 0
            } completion: { [weak self] (_) in
                self?.lottieAnimation.stop()
                self?.layoutIfNeeded()
                self?.removeFromSuperview()
                LoaderView.obj = nil
            }
        }
    }
    
}
