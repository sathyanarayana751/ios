//
//  FilterView.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

public enum FilterViewSections : Int32 {

    case availability = 0
    case sortBy = 1
    case chargeType = 2
    case plugType = 3
    case range = 4
    static var count: Int { return Int(FilterViewSections.range.rawValue + 1)}
}

class FilterView: UIView {
    
    private static var obj: FilterView? = nil
    @IBOutlet weak var filterTableView: FilterTableView!
    @IBOutlet weak var backView: UIView!
    
    var availabilityCell:FilterCollectionTblViewCell?
    var sortByCell:FilterCollectionTblViewCell?
    var chargeTypeCell:FilterCollectionTblViewCell?
    var plugTypeCell:FilterCollectionTblViewCell?
    var rangeCell:FilterCollectionTblViewCell?
    
    var arrayOfFilters:[[String:Any]] = []
    var selectedFiltersClosure: (()->())?

    var selectedFilters: [String:Any] = [:] {
        didSet {
            self.selectedFiltersClosure?()
        }
    }
    
    var internalSelectedFilters: [String:Any] = [:]
    
    override func layoutSubviews() {
        
//        self.backView.layer.masksToBounds = false
//        self.backView.clipsToBounds = true
//        self.backView.layer.shadowColor = UIColor.black.cgColor
//        self.backView.layer.shadowPath = UIBezierPath(roundedRect: self.backView.bounds, cornerRadius: 10).cgPath
//        self.backView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
//        self.backView.layer.shadowOpacity = 0.7
//        self.backView.layer.shadowRadius = 6.0
        
//        self.backView.dropShadow()
    }

    static var shared: FilterView {
        if obj == nil {
            obj = UINib(nibName: "FillterView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? FilterView
        } else {
            obj?.filterTableView.setContentOffset(CGPoint.zero, animated:true)
        }

        return obj!
    }
    
    func setup(filters:[[String:Any]], currentlySelectedFilters:[String:Any]) {
        
        arrayOfFilters = filters
        selectedFilters = currentlySelectedFilters
        internalSelectedFilters = selectedFilters

        DispatchQueue.main.async {
            guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else {
                return
            }
           
            self.frame = window.frame
            window.addSubview(self)
            self.layoutIfNeeded()
            self.alpha = 0
            self.backView.dropShadow(radius: 10)
            UIView.animate(withDuration:0.3, animations: {
                self.alpha = 1
                self.layoutIfNeeded()
            })
        }
    }
    
    func removeTheView() {
        self.alpha = 1
        UIView.animate(withDuration: 0.5) {
            self.alpha = 0
        } completion: { [weak self] (_) in
            self?.layoutIfNeeded()
            self?.removeFromSuperview()
            FilterView.obj = nil
        }
    }
    
    
    @IBAction func closeFilterView(_ sender: Any) {
        removeTheView()
    }
    
    @IBAction func applyFilterView(_ sender: Any) {
        
        /*var currentlySelectedFilters:[String:Any] = [:]
        var selectedChargeType = ""
        for i in 0..<FilterViewSections.count {
        
            let indexPath = IndexPath(row: 0, section: i)
            if let filterCell = filterTableView.cellForRow(at: indexPath) as? FilterCollectionTblViewCell {
                
                let eachFilterInfo = arrayOfFilters[i]
                var selectedFilterInfo:[String] = []
                                
                if let title = eachFilterInfo["title"] as? String {
                    currentlySelectedFilters[title] = selectedFilterInfo
                    
                    if title == "CHARGE TYPE" {
                        selectedChargeType = filterCell.selectedFilterValue
                    }
                    
                    if title == "PLUG TYPE" {
                        
                        selectedFilterInfo.append(filterCell.selectedFilterValue)
                        var selectedPlugTypeDict:[String:Any] = [:]
                        
                        if selectedChargeType == "DC" {
                            selectedPlugTypeDict = ["DC":selectedFilterInfo,
                                                    "AC":["Type 1"]]

                        } else {
                            selectedPlugTypeDict = ["AC":selectedFilterInfo,
                                                    "DC":["CCS - 2"]]
                        }
                        currentlySelectedFilters[title] = selectedPlugTypeDict
                        
                    } else {
                        
                        selectedFilterInfo.append(filterCell.selectedFilterValue)
                        currentlySelectedFilters[title] = selectedFilterInfo
                        
                        /*if let eachFilterDataValue = eachFilterInfo["data"] as? [String] {
                           
                            /*for j in 0..<filterCell.selectedFliterData.count {
                                
                                if filterCell.selectedFliterData[j] {
                                    
                                    selectedFilterInfo.append(eachFilterDataValue[j])
                                }
                            }*/
                            selectedFilterInfo.append(filterCell.selectedFilterValue)
                            currentlySelectedFilters[title] = selectedFilterInfo
                        }*/
                    }
                }
            }
        }
        //TODO: Test Passed selected filters to home screen
        selectedFilters = currentlySelectedFilters*/
        selectedFilters = internalSelectedFilters
        removeTheView()
    }
    
    func getSelectedFilters(_ filter:String) -> [String] {
        
        if filter == "PLUG TYPE" {
            
            if let currentlySelectedFilters = internalSelectedFilters[filter] as? [String:Any], let selectedPlugTypeFilters = currentlySelectedFilters[self.getSelectedChargeType()] as? [String] {
                
                return selectedPlugTypeFilters
            }
            
        } else {
            
            if let currentlySelectedFilters = internalSelectedFilters[filter] as? [String] {
                
                return currentlySelectedFilters
            }
        }
        
        return []
    }
    
    func getSelectedChargeType() -> String {
        
        if let currentlySelectedFilters = internalSelectedFilters["CHARGE TYPE"] as? [String], currentlySelectedFilters.count > 0 {
            
            return currentlySelectedFilters[0]
        }
        return ""
    }
    
    func updateInternalSelectedFilters(_ filterType:FilterViewSections, filterCell:FilterCollectionTblViewCell){
                                
        let eachFilterInfo = arrayOfFilters[Int(filterType.rawValue)]
        var selectedFilterInfo:[String] = []
                            
        if let title = eachFilterInfo["title"] as? String {
                
            internalSelectedFilters[title] = selectedFilterInfo
                
            if title == "PLUG TYPE" {
                
                selectedFilterInfo.append(filterCell.selectedFilterValue)
                var selectedPlugTypeDict:[String:Any] = [:]
                
                if getSelectedChargeType() == "DC" {
                    selectedPlugTypeDict = ["DC":selectedFilterInfo,
                                            "AC":["AC-001"]]

                } else {
                    selectedPlugTypeDict = ["AC":selectedFilterInfo,
                                            "DC":["CCS-2"]]
                }
                internalSelectedFilters[title] = selectedPlugTypeDict
                
            } else {
                    
                selectedFilterInfo.append(filterCell.selectedFilterValue)
                internalSelectedFilters[title] = selectedFilterInfo
                var selectedPlugTypeDict:[String:Any] = [:]

                if title == "CHARGE TYPE" {
                    
                    selectedPlugTypeDict = ["DC":["CCS-2"],
                                                "AC":["AC-001"]]
                    
                    internalSelectedFilters["PLUG TYPE"] = selectedPlugTypeDict
                }
            }
        }
    }
}

extension FilterView:  UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return FilterViewSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderTblCell") as? FilterHeaderTblCell {
            
            if let title = arrayOfFilters[section]["title"] as? String {
                cell.headerTitle.text = title
            }
            return cell.contentView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if(section != FilterViewSections.range.rawValue) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "FilterFooterTblCell") as? FilterFooterTblCell {
                return cell.contentView
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if(section != FilterViewSections.range.rawValue) {
            return 10.0
        }
        return 0.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = FilterViewSections(rawValue: Int32(indexPath.section))!
        
        switch section {
        case .availability:
            if availabilityCell == nil {
                availabilityCell = tableView.dequeueReusableCell(withIdentifier: "FilterCollectionTblViewCell") as? FilterCollectionTblViewCell
                
                if let filterTitle = arrayOfFilters[indexPath.section]["title"] as? String {
                    availabilityCell?.updateTheContent(type: section, filterData: arrayOfFilters[indexPath.section], currentlySelectedFilters: self.getSelectedFilters(filterTitle), selectedChargeType: self.getSelectedChargeType())
                }
                
                availabilityCell?.filterChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.updateInternalSelectedFilters(.availability, filterCell: (self?.availabilityCell!)!)
                    }
                }
                
                return availabilityCell!
            }
            return availabilityCell!
        case .sortBy:
            if sortByCell == nil {
                sortByCell = tableView.dequeueReusableCell(withIdentifier: "FilterCollectionTblViewCell") as? FilterCollectionTblViewCell
                if let filterTitle = arrayOfFilters[indexPath.section]["title"] as? String {
                    sortByCell?.updateTheContent(type: section, filterData: arrayOfFilters[indexPath.section], currentlySelectedFilters: self.getSelectedFilters(filterTitle), selectedChargeType: self.getSelectedChargeType())
                }
                sortByCell?.filterChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.updateInternalSelectedFilters(.sortBy, filterCell: (self?.sortByCell!)!)
                    }
                }
                return sortByCell!
            }
            return sortByCell!
        case .chargeType:
            if chargeTypeCell == nil {
                chargeTypeCell = tableView.dequeueReusableCell(withIdentifier: "FilterCollectionTblViewCell") as? FilterCollectionTblViewCell
                if let filterTitle = arrayOfFilters[indexPath.section]["title"] as? String {
                    chargeTypeCell?.updateTheContent(type: section, filterData: arrayOfFilters[indexPath.section], currentlySelectedFilters: self.getSelectedFilters(filterTitle), selectedChargeType: self.getSelectedChargeType())
                }
                
                chargeTypeCell?.filterChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.updateInternalSelectedFilters(.chargeType, filterCell: (self?.chargeTypeCell!)!)
                    }
                }
                
                chargeTypeCell?.chargeTypeChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.filterTableView.reloadData()
                    }
                }
                return chargeTypeCell!
            }
            return chargeTypeCell!
        case .plugType:
//            if plugTypeCell == nil {
                plugTypeCell = tableView.dequeueReusableCell(withIdentifier: "FilterCollectionTblViewCell") as? FilterCollectionTblViewCell
                if let filterTitle = arrayOfFilters[indexPath.section]["title"] as? String {
                    plugTypeCell?.updateTheContent(type: section, filterData: arrayOfFilters[indexPath.section], currentlySelectedFilters: self.getSelectedFilters(filterTitle), selectedChargeType: self.getSelectedChargeType())
                }
                
                plugTypeCell?.filterChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.updateInternalSelectedFilters(.plugType, filterCell: (self?.plugTypeCell!)!)
                    }
                }
                return plugTypeCell!
//            }
//            return plugTypeCell!
        case .range:
            if rangeCell == nil {
                rangeCell = tableView.dequeueReusableCell(withIdentifier: "FilterCollectionTblViewCell") as? FilterCollectionTblViewCell
                if let filterTitle = arrayOfFilters[indexPath.section]["title"] as? String {
                    rangeCell?.updateTheContent(type: section, filterData: arrayOfFilters[indexPath.section], currentlySelectedFilters: self.getSelectedFilters(filterTitle), selectedChargeType: self.getSelectedChargeType())
                }
                
                rangeCell?.filterChangeClosure = { [weak self] () in
                    DispatchQueue.main.async {
                        self?.updateInternalSelectedFilters(.range, filterCell: (self?.rangeCell!)!)
                    }
                }
                return rangeCell!
            }
            return rangeCell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = FilterViewSections(rawValue: Int32(indexPath.section))!
        
        switch section {
            case .availability:
                return (availabilityCell?.colView.collectionViewLayout.collectionViewContentSize.height)! + 30
            case .sortBy:
                return (sortByCell?.colView.collectionViewLayout.collectionViewContentSize.height)! + 30
            case .chargeType:
                return (chargeTypeCell?.colView.collectionViewLayout.collectionViewContentSize.height)! + 30
            case .plugType:
                return (plugTypeCell?.colView.collectionViewLayout.collectionViewContentSize.height)! + 30
            case .range:
                return (rangeCell?.colView.collectionViewLayout.collectionViewContentSize.height)! + 30
        }
    }
}

class FilterTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        register(FilterHeaderTblCell.nib, forCellReuseIdentifier: FilterHeaderTblCell.identifier!)
        register(FilterFooterTblCell.nib, forCellReuseIdentifier: FilterFooterTblCell.identifier!)
        register(FilterCollectionTblViewCell.nib, forCellReuseIdentifier: FilterCollectionTblViewCell.identifier!)
    }
}
