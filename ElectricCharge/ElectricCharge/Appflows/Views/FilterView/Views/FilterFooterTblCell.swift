//
//  File.swift
//  ElectricCharge
//
//  Created by iosdev on 2/17/22.
//

import UIKit

class FilterFooterTblCell: UITableViewCell {
    
    
    @IBOutlet weak var dividerView: UIView!
    
    @IBOutlet weak var topView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
