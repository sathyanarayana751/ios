//
//  FilterCollectionTblViewCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class FilterCollectionTblViewCell: UITableViewCell {
    
    @IBOutlet weak var colView: FilterColView!
    
    var currentFilter: FilterViewSections = .availability
    var filterData:[String] = []
    //var selectedFliterData:[Bool] = []
    var selectedIndex = -1
    var selectedFilterValue = ""
    
    var chargeTypeChangeClosure: (()->())?
    var filterChangeClosure: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func updateTheContent(type: FilterViewSections, filterData:[String:Any], currentlySelectedFilters:[String], selectedChargeType:String) {
        
        self.currentFilter = type
        
        if self.currentFilter == .plugType {
                
            if let fileterdDataValue = filterData["data"] as? [String:Any], let plugTypeDataValue = fileterdDataValue[selectedChargeType] as? [String] {
                            
                self.filterData = plugTypeDataValue
            }
        } else {
            
            if let fileterdDataValue = filterData["data"] as? [String] {
                            
                self.filterData = fileterdDataValue
            }
        }
        
        selectedIndex = -1
        for i in 0..<self.filterData.count {
            
            if currentlySelectedFilters.contains(self.filterData[i]) {
                selectedIndex = i
            }
        }
        if selectedIndex > -1 {
            selectedFilterValue = self.filterData[selectedIndex]
        }

        self.colView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.updateSelectedFilters(index: -1)
        }
    }
    
    func updateSelectedFilters(index:Int){
        
        if(index >= 0){
            
            /*if selectedFliterData[index] {
                
                selectedFliterData[index] = false
                
            } else {
                
                selectedFliterData[index] = true
            }*/
            var filterChanged = false
            if selectedIndex != index {
                
                filterChanged = true
            }
            selectedIndex = index
            selectedFilterValue = filterData[selectedIndex]
            
            if filterChanged {
                self.filterChangeClosure?()
                if self.currentFilter == .chargeType {
                    //Reload PlugType section in FilterView
                    self.chargeTypeChangeClosure?()
                }
            }
        }
        colView.reloadData()
    }
}

extension FilterCollectionTblViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        filterData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch currentFilter {
            
            case .availability:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CheckBoxColVwCell", for: indexPath) as? CheckBoxColVwCell
                cell?.titleLabel.text = filterData[indexPath.row]
                if selectedIndex == indexPath.row {//if selectedFliterData[indexPath.row] {
                    cell?.checkBoxButton.isSelected = true
                } else {
                    cell?.checkBoxButton.isSelected = false
                }

                return cell!
            case .chargeType:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CheckBoxColVwCell", for: indexPath) as? CheckBoxColVwCell
                cell?.titleLabel.text = filterData[indexPath.row]
                if selectedIndex == indexPath.row {//if selectedFliterData[indexPath.row] {
                    cell?.checkBoxButton.isSelected = true
                } else {
                    cell?.checkBoxButton.isSelected = false
                }

                return cell!
            case .sortBy:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RadioBtnColVwCell", for: indexPath) as? RadioBtnColVwCell
                cell?.titleLabel.text = filterData[indexPath.row]
                if selectedIndex == indexPath.row {//if selectedFliterData[indexPath.row] {
                    cell?.radioButton.isSelected = true
                } else {
                    cell?.radioButton.isSelected = false
                }

                return cell!
            case .plugType:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RadioBtnColVwCell", for: indexPath) as? RadioBtnColVwCell
                cell?.titleLabel.text = filterData[indexPath.row]
                if selectedIndex == indexPath.row {//if selectedFliterData[indexPath.row] {
                    cell?.radioButton.isSelected = true
                } else {
                    cell?.radioButton.isSelected = false
                }

                return cell!
            case .range:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PowerColVwCell", for: indexPath) as? PowerColVwCell
                cell?.lblVolts.text = filterData[indexPath.row] + "KM"
                cell?.bgView.borderWidth = 1
                
                if selectedIndex == indexPath.row {//if selectedFliterData[indexPath.row] {
                    cell?.lblVolts.textColor = UIColor.white
                    cell?.bgView.borderColor = UIColor.clear
                    cell?.bgView.backgroundColor = UIColor(named: "2ECE54")
                } else {
                    cell?.lblVolts.textColor = UIColor(named: "2A382C")
                    cell?.bgView.borderColor = UIColor(named: "2ECE54")
                    cell?.bgView.backgroundColor = UIColor.white
                }

                return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let collectionViewWidth = collectionView.bounds.width
        var numberofItem: CGFloat = 1
        var itemHeight:CGFloat = 0.0
        switch currentFilter {
            case .availability:
                numberofItem = 1
                itemHeight = 35.0
//                return CGSize(width: collectionView.bounds.width, height: 35)
            case .sortBy:
                numberofItem = 1
                itemHeight = 35.0
//                return CGSize(width: collectionView.bounds.width, height: 35)
            case .chargeType:
                numberofItem = 2
                itemHeight = 35.0
//                return CGSize(width: (collectionView.bounds.width)/2, height: 35)
            case .plugType:
                numberofItem = 2
                itemHeight = 35.0
//                return CGSize(width: (collectionView.bounds.width)/2, height: 35)
            case .range:
                numberofItem = 3
                itemHeight = 50.0
//                return CGSize(width: (collectionView.bounds.width)/3, height: 50)
        }
        
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

        let itemWidth = CGFloat((collectionViewWidth - extraSpace - inset) / numberofItem)

        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateSelectedFilters(index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class FilterColView: UICollectionView {
        
    override func awakeFromNib() {
        super.awakeFromNib()
        register(PowerColVwCell.nib, forCellWithReuseIdentifier: PowerColVwCell.identifier!)
        register(CheckBoxColVwCell.nib, forCellWithReuseIdentifier: CheckBoxColVwCell.identifier!)
        register(RadioBtnColVwCell.nib, forCellWithReuseIdentifier: RadioBtnColVwCell.identifier!)
    }
}

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)

        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }

            layoutAttribute.frame.origin.x = leftMargin

            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }

        return attributes
    }
}
