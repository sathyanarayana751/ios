//
//  PowerColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class PowerColVwCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblVolts: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

}
