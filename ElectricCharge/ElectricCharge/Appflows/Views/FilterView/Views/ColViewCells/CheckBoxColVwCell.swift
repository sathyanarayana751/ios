//
//  CheckBoxColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class CheckBoxColVwCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var checkBoxButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
