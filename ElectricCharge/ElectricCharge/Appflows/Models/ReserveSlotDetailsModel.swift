//
//  ReserveSlotDetailsModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 29/08/22.
//

import Foundation

class ReserveSlotDetailsModel {
    var id : String?
    var station : StationDetailsModel?
    var startTime : String?
    var endTime: String?
    var charger : ChargerModel?
    var plug : PlugModel?
    var status : ReservationStatus?
    var plugType : String?
    var chargeType : String?
    var powerRating : Double?
    var unitsConsumed : Double?
    var pricePerUnit : Double?
    var reservedAmount : Int?
    var reserveAmountSource:ReservationAmountSource?
    var createdAt : String?
    var user : UserDetailsModel?

    init(reserveData:ReserveMutation.Data.Reserve.Datum) {
        if let idValue = reserveData.id {
            self.id = idValue
        }
        if let startTimeValue = reserveData.startTime {
            self.startTime = startTimeValue
        }
        if let endTimeValue = reserveData.endTime {
            self.endTime = endTimeValue
        }
        if let plugTypeValue = reserveData.plugType {
            self.plugType = plugTypeValue
        }
        if let chargeTypeValue = reserveData.chargeType {
            self.chargeType = chargeTypeValue
        }
        
        if let powerRatingValue = reserveData.powerRating {
            self.powerRating = powerRatingValue
        }
        
        if let unitsConsumedValue = reserveData.unitsConsumed {
            self.unitsConsumed = unitsConsumedValue
        }
        
        if let pricePerUnitValue = reserveData.pricePerUnit {
            self.pricePerUnit = pricePerUnitValue
        }
        if let reservedAmountValue = reserveData.reservedAmount {
            self.reservedAmount = reservedAmountValue
        }
        if let reserveAmountSourceValue = reserveData.reserveAmountSource {
            self.reserveAmountSource = reserveAmountSourceValue
        }
        if let createdAtValue = reserveData.createdAt {
            self.createdAt = createdAtValue
        }
        
        if let statusValue = reserveData.status {
            self.status = statusValue
        }
        
        if let stationData = reserveData.station {
            self.station = StationDetailsModel(reserveStationData:stationData)
        }
        
        if let chargerData = reserveData.charger {
            self.charger = ChargerModel(reserveChargerData:chargerData)
        }
        
        if let plugData = reserveData.plug {
            self.plug = PlugModel(reservePlugData:plugData)
        }
    }
    
    init(bookingData:ReservationsListQuery.Data.Reservation.Doc) {
        if let idValue = bookingData.id {
            self.id = idValue
        }
        if let startTimeValue = bookingData.startTime {
            self.startTime = startTimeValue
        }
        if let endTimeValue = bookingData.endTime {
            self.endTime = endTimeValue
        }
        if let plugTypeValue = bookingData.plugType {
            self.plugType = plugTypeValue
        }
        if let chargeTypeValue = bookingData.chargeType {
            self.chargeType = chargeTypeValue
        }
        
        if let powerRatingValue = bookingData.powerRating {
            self.powerRating = powerRatingValue
        }
        
        if let unitsConsumedValue = bookingData.unitsConsumed {
            self.unitsConsumed = unitsConsumedValue
        }
        
        if let pricePerUnitValue = bookingData.pricePerUnit {
            self.pricePerUnit = pricePerUnitValue
        }
        if let reservedAmountValue = bookingData.reservedAmount {
            self.reservedAmount = reservedAmountValue
        }
        if let reserveAmountSourceValue = bookingData.reserveAmountSource {
            self.reserveAmountSource = reserveAmountSourceValue
        }
        if let createdAtValue = bookingData.createdAt {
            self.createdAt = createdAtValue
        }
        
        if let statusValue = bookingData.status {
            self.status = statusValue
        }
        
        if let stationData = bookingData.station {
            self.station = StationDetailsModel(bookingHistoryStationData:stationData)
        }
        
        if let chargerData = bookingData.charger {
            self.charger = ChargerModel(bookingChargerData:chargerData)
        }
        
        if let plugData = bookingData.plug {
            self.plug = PlugModel(bookingPlugData:plugData)
        }
    }
}
