//
//  PortTypesModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 08/01/23.
//

import Foundation

class PortTypesModel {
    var id : String?
    var name : String?
    var type : PowerType?
    var supportedPowers: [Double]?
    var typeImage : String?
//    var bgColor : String?

    init(portTypesResponse:PortTypesQuery.Data.PortType) {
        if let idValue = portTypesResponse.id {
            self.id = idValue
        }
        if let nameValue = portTypesResponse.name {
            self.name = nameValue
        }
        if let typeValue = portTypesResponse.type {
            self.type = typeValue
        }
        if let supportedPowersValue = portTypesResponse.supportedPowers {
            for eachSupportedPower in supportedPowersValue {
                self.supportedPowers?.append(eachSupportedPower ?? 0)
            }
        }
        self.typeImage = "type1"
    }
    
    init(vehicleModelPortTypesResponse:VehicleModelsQuery.Data.VehicleModel.Doc.SupportedPortType) {
        if let idValue = vehicleModelPortTypesResponse.id {
            self.id = idValue
        }
        if let nameValue = vehicleModelPortTypesResponse.name {
            self.name = nameValue
        }
        if let typeValue = vehicleModelPortTypesResponse.type {
            self.type = typeValue
        }
        if let supportedPowersValue = vehicleModelPortTypesResponse.supportedPowers {
            for eachSupportedPower in supportedPowersValue {
                self.supportedPowers?.append(eachSupportedPower ?? 0)
            }
        }
        self.typeImage = "type1"
    }
}
