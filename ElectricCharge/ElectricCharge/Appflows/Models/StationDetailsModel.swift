//
//  StationDetailsModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 24/05/22.
//

import Foundation

class StationDetailsModel {
    var id : String?
    var name : String?
    var description : String?
    var status: StationStatus?
    var address : AddressModel?
    var locationCoordinates : [Double?]?
    var images : [String?]?
    var chargers : [ChargerModel] = []
    var perUnitDCCharge : Double?
    var perUnitACCharge : Double?
    var isForSearchAddress : Bool?
    var searchLatitude : Double?
    var searchLongitude : Double?
    var selectedChargeType:String?
    var selectedPlugType:String?
    var supportedPorts: [String] = []
    var supportedPortsCounts: [String: Int] = [:]
    var supportedPortsPerUnitCharge: [String: Double] = [:]

    init(searchStationData:SearchQuery.Data.Search.Doc.Station, searchLatitude:Double, searchLongitude:Double) {
        if let idValue = searchStationData.id {
            self.id = idValue
        }
        if let nameValue = searchStationData.name {
            self.name = nameValue
        }
        if let descriptionValue = searchStationData.description {
            self.description = descriptionValue
        }
        if let statusValue = searchStationData.status {
            self.status = statusValue
        }
        if let addressValue = searchStationData.address {
            self.address = AddressModel(searchAddressData: addressValue)
        }
        if let coordinatesValue = searchStationData.position?.coordinates {
            self.locationCoordinates = coordinatesValue
        }
        if let imagesValue = searchStationData.images {
            self.images = imagesValue
        }
        if let arrayOfChargers = searchStationData.chargers {
            for charger in arrayOfChargers {
                if let chargerData = charger {
                    self.chargers.append(ChargerModel(searchChargerData: chargerData))
                }
            }
        }
        if let perUnitDCChargeValue = searchStationData.perUnitDcCharge {
            self.perUnitDCCharge = perUnitDCChargeValue
        }
        
        if let perUnitACChargeValue = searchStationData.perUnitAcCharge {
            self.perUnitACCharge = perUnitACChargeValue
        }
        
        self.searchLatitude = searchLatitude
        self.searchLongitude = searchLongitude
        
        for eachCharger in self.chargers {
            for eachPlugs in eachCharger.plugs {
                /*for eachSupportedPort in eachPlugs.supportedPorts {
                    if let eachSupportedPortValue = eachSupportedPort {
                        supportedPortsCounts[eachSupportedPortValue] = (supportedPortsCounts[eachSupportedPortValue] ?? 0) + 1
                        supportedPortsPowerValue[eachSupportedPortValue] = eachCharger.power
                        if !supportedPowers.contains(eachSupportedPortValue) {
                            supportedPorts.append(eachSupportedPortValue)
                        }
                    }
                }*/
                if let eachSupportedPortValue = eachPlugs.supportedPort, let eachSupportedPortPower = eachPlugs.power {
                    let plugkeyValue = eachSupportedPortValue.appending(String(",\(eachSupportedPortPower)"))
                    supportedPortsCounts[plugkeyValue] = (supportedPortsCounts[plugkeyValue] ?? 0) + 1
                    
                    if !supportedPortsPerUnitCharge.keys.contains(plugkeyValue) {
                        var perUnitChargeValue = 0.0
                        if let perUnitDcCharge = eachPlugs.perUnitDcCharge, let perUnitAcCharge = eachPlugs.perUnitAcCharge {
                            perUnitChargeValue = perUnitDcCharge
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitAcCharge
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        } else {
                            perUnitChargeValue = perUnitDCCharge ?? 0.0
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitACCharge ?? 0.0
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        }
                    }

                    if !supportedPorts.contains(plugkeyValue) {
                        supportedPorts.append(plugkeyValue)
                    }
                }
            }
        }
        supportedPorts = supportedPorts.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    }
    
    init(favStationData:UserQuery.Data.User.Preference.Station) {
        if let idValue = favStationData.id {
            self.id = idValue
        }
        if let nameValue = favStationData.name {
            self.name = nameValue
        }
        if let descriptionValue = favStationData.description {
            self.description = descriptionValue
        }
        if let statusValue = favStationData.status {
            self.status = statusValue
        }
        if let addressValue = favStationData.address {
            self.address = AddressModel(favAddressData: addressValue)
        }
        if let coordinatesValue = favStationData.position?.coordinates {
            self.locationCoordinates = coordinatesValue
        }
        if let imagesValue = favStationData.images {
            self.images = imagesValue
        }
        if let arrayOfChargers = favStationData.chargers {
            for charger in arrayOfChargers {
                if let chargerData = charger {
                    self.chargers.append(ChargerModel(favChargerData: chargerData))
                }
            }
        }
        if let perUnitDCChargeValue = favStationData.perUnitDcCharge {
            self.perUnitDCCharge = perUnitDCChargeValue
        }
        
        if let perUnitACChargeValue = favStationData.perUnitAcCharge {
            self.perUnitACCharge = perUnitACChargeValue
        }
        
        
        for eachCharger in self.chargers {
            for eachPlugs in eachCharger.plugs {
                /*for eachSupportedPort in eachPlugs.supportedPorts {
                    if let eachSupportedPortValue = eachSupportedPort {
                        supportedPortsCounts[eachSupportedPortValue] = (supportedPortsCounts[eachSupportedPortValue] ?? 0) + 1
                        supportedPortsPowerValue[eachSupportedPortValue] = eachCharger.power
                        if !supportedPorts.contains(eachSupportedPortValue) {
                            supportedPorts.append(eachSupportedPortValue)
                        }
                    }
                }*/
                if let eachSupportedPortValue = eachPlugs.supportedPort, let eachSupportedPortPower = eachPlugs.power {
                    let plugkeyValue = eachSupportedPortValue.appending(String(",\(eachSupportedPortPower)"))
                    supportedPortsCounts[plugkeyValue] = (supportedPortsCounts[plugkeyValue] ?? 0) + 1
                    if !supportedPortsPerUnitCharge.keys.contains(plugkeyValue) {
                        var perUnitChargeValue = 0.0
                        if let perUnitDcCharge = eachPlugs.perUnitDcCharge, let perUnitAcCharge = eachPlugs.perUnitAcCharge {
                            perUnitChargeValue = perUnitDcCharge
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitAcCharge
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        } else {
                            perUnitChargeValue = perUnitDCCharge ?? 0.0
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitACCharge ?? 0.0
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        }
                    }
                    if !supportedPorts.contains(plugkeyValue) {
                        supportedPorts.append(plugkeyValue)
                    }
                }
            }
        }
        supportedPorts = supportedPorts.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    }
    
    init(resolveChargeStationData:ResolveChargeQuery.Data.ResolveCharge.Station) {
        if let idValue = resolveChargeStationData.id {
            self.id = idValue
        }
        if let nameValue = resolveChargeStationData.name {
            self.name = nameValue
        }
        if let descriptionValue = resolveChargeStationData.description {
            self.description = descriptionValue
        }
        if let statusValue = resolveChargeStationData.status {
            self.status = statusValue
        }
        if let addressValue = resolveChargeStationData.address {
            self.address = AddressModel(resolveChargeData: addressValue)
        }
        if let coordinatesValue = resolveChargeStationData.position?.coordinates {
            self.locationCoordinates = coordinatesValue
        }
        if let imagesValue = resolveChargeStationData.images {
            self.images = imagesValue
        }
        if let arrayOfChargers = resolveChargeStationData.chargers {
            for charger in arrayOfChargers {
                if let chargerData = charger {
                    self.chargers.append(ChargerModel(resolveChargerData: chargerData))
                }
            }
        }
        if let perUnitDCChargeValue = resolveChargeStationData.perUnitDcCharge {
            self.perUnitDCCharge = perUnitDCChargeValue
        }
        
        if let perUnitACChargeValue = resolveChargeStationData.perUnitAcCharge {
            self.perUnitACCharge = perUnitACChargeValue
        }
        
        
        for eachCharger in self.chargers {
            for eachPlugs in eachCharger.plugs {
                /*for eachSupportedPort in eachPlugs.supportedPorts {
                    if let eachSupportedPortValue = eachSupportedPort {
                        supportedPortsCounts[eachSupportedPortValue] = (supportedPortsCounts[eachSupportedPortValue] ?? 0) + 1
                        supportedPortsPowerValue[eachSupportedPortValue] = eachCharger.power
                        if !supportedPorts.contains(eachSupportedPortValue) {
                            supportedPorts.append(eachSupportedPortValue)
                        }
                    }
                }*/
                if let eachSupportedPortValue = eachPlugs.supportedPort, let eachSupportedPortPower = eachPlugs.power {
                    let plugkeyValue = eachSupportedPortValue.appending(String(",\(eachSupportedPortPower)"))
                    supportedPortsCounts[plugkeyValue] = (supportedPortsCounts[plugkeyValue] ?? 0) + 1
                    if !supportedPortsPerUnitCharge.keys.contains(plugkeyValue) {
                        var perUnitChargeValue = 0.0
                        if let perUnitDcCharge = eachPlugs.perUnitDcCharge, let perUnitAcCharge = eachPlugs.perUnitAcCharge {
                            perUnitChargeValue = perUnitDcCharge
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitAcCharge
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        } else {
                            perUnitChargeValue = perUnitDCCharge ?? 0.0
                            if eachCharger.type == PowerType.ac {
                                perUnitChargeValue = perUnitACCharge ?? 0.0
                            }
                            supportedPortsPerUnitCharge[plugkeyValue] = perUnitChargeValue
                        }
                    }
                    if !supportedPorts.contains(plugkeyValue) {
                        supportedPorts.append(plugkeyValue)
                    }
                }
            }
        }
        supportedPorts = supportedPorts.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    }
    
    init(reserveStationData:ReserveMutation.Data.Reserve.Datum.Station) {
        if let idValue = reserveStationData.id {
            self.id = idValue
        }
        if let nameValue = reserveStationData.name {
            self.name = nameValue
        }
        if let descriptionValue = reserveStationData.description {
            self.description = descriptionValue
        }
        if let statusValue = reserveStationData.status {
            self.status = statusValue
        }
        if let addressValue = reserveStationData.address {
            self.address = AddressModel(reserveData: addressValue)
        }
        if let coordinatesValue = reserveStationData.position?.coordinates {
            self.locationCoordinates = coordinatesValue
        }
        if let imagesValue = reserveStationData.images {
            self.images = imagesValue
        }
        if let perUnitDCChargeValue = reserveStationData.perUnitDcCharge {
            self.perUnitDCCharge = perUnitDCChargeValue
        }
        
        if let perUnitACChargeValue = reserveStationData.perUnitAcCharge {
            self.perUnitACCharge = perUnitACChargeValue
        }
    }
    
    init(bookingHistoryStationData:ReservationsListQuery.Data.Reservation.Doc.Station) {
        if let idValue = bookingHistoryStationData.id {
            self.id = idValue
        }
        if let nameValue = bookingHistoryStationData.name {
            self.name = nameValue
        }
        if let descriptionValue = bookingHistoryStationData.description {
            self.description = descriptionValue
        }
        if let statusValue = bookingHistoryStationData.status {
            self.status = statusValue
        }
        if let addressValue = bookingHistoryStationData.address {
            self.address = AddressModel(bookingHistoryData: addressValue)
        }
        if let coordinatesValue = bookingHistoryStationData.position?.coordinates {
            self.locationCoordinates = coordinatesValue
        }
        if let imagesValue = bookingHistoryStationData.images {
            self.images = imagesValue
        }
        if let perUnitDCChargeValue = bookingHistoryStationData.perUnitDcCharge {
            self.perUnitDCCharge = perUnitDCChargeValue
        }
        if let perUnitACChargeValue = bookingHistoryStationData.perUnitAcCharge {
            self.perUnitACCharge = perUnitACChargeValue
        }
    }
}

class AddressModel {
    
    var line1 : String?
    var line2 : String?
    var city : String?
    var state : String?
    var zip : String?
    var phone : String?
    
    init(searchAddressData:SearchQuery.Data.Search.Doc.Station.Address) {
        
        if let line1Value = searchAddressData.line1 {
            self.line1 = line1Value
        }
        if let line2Value = searchAddressData.line2 {
            self.line2 = line2Value
        }
        if let cityValue = searchAddressData.city {
            self.city = cityValue
        }
        if let stateValue = searchAddressData.state {
            self.state = stateValue
        }
        if let zipValue = searchAddressData.zip {
            self.zip = zipValue
        }
        if let phoneValue = searchAddressData.phone {
            self.phone = phoneValue
        }
    }
    
    init(favAddressData:UserQuery.Data.User.Preference.Station.Address) {
        
        if let line1Value = favAddressData.line1 {
            self.line1 = line1Value
        }
        if let line2Value = favAddressData.line2 {
            self.line2 = line2Value
        }
        if let cityValue = favAddressData.city {
            self.city = cityValue
        }
        if let stateValue = favAddressData.state {
            self.state = stateValue
        }
        if let zipValue = favAddressData.zip {
            self.zip = zipValue
        }
        if let phoneValue = favAddressData.phone {
            self.phone = phoneValue
        }
    }
    
    init(resolveChargeData:ResolveChargeQuery.Data.ResolveCharge.Station.Address) {
        
        if let line1Value = resolveChargeData.line1 {
            self.line1 = line1Value
        }
        if let line2Value = resolveChargeData.line2 {
            self.line2 = line2Value
        }
        if let cityValue = resolveChargeData.city {
            self.city = cityValue
        }
        if let stateValue = resolveChargeData.state {
            self.state = stateValue
        }
        if let zipValue = resolveChargeData.zip {
            self.zip = zipValue
        }
        if let phoneValue = resolveChargeData.phone {
            self.phone = phoneValue
        }
    }
    
    init(bookingHistoryData:ReservationsListQuery.Data.Reservation.Doc.Station.Address) {
        
        if let line1Value = bookingHistoryData.line1 {
            self.line1 = line1Value
        }
        if let line2Value = bookingHistoryData.line2 {
            self.line2 = line2Value
        }
        if let cityValue = bookingHistoryData.city {
            self.city = cityValue
        }
        if let stateValue = bookingHistoryData.state {
            self.state = stateValue
        }
        if let zipValue = bookingHistoryData.zip {
            self.zip = zipValue
        }
        if let phoneValue = bookingHistoryData.phone {
            self.phone = phoneValue
        }
    }
    
    init(reserveData:ReserveMutation.Data.Reserve.Datum.Station.Address) {
        
        if let line1Value = reserveData.line1 {
            self.line1 = line1Value
        }
        if let line2Value = reserveData.line2 {
            self.line2 = line2Value
        }
        if let cityValue = reserveData.city {
            self.city = cityValue
        }
        if let stateValue = reserveData.state {
            self.state = stateValue
        }
        if let zipValue = reserveData.zip {
            self.zip = zipValue
        }
        if let phoneValue = reserveData.phone {
            self.phone = phoneValue
        }
    }
}

class ChargerModel {
    
    var id : String?
    var name : String?
    var status : ChargerStatus?
    var power : Double?
    var type : PowerType?
    var plugs : [PlugModel] = []

    init() {
        
    }
    init(searchChargerData:SearchQuery.Data.Search.Doc.Station.Charger) {
        
        if let idValue = searchChargerData.id {
            self.id = idValue
        }
        if let nameValue = searchChargerData.name {
            self.name = nameValue
        }
        if let statusValue = searchChargerData.status {
            self.status = statusValue
        }
        if let powerValue = searchChargerData.power {
            self.power = powerValue
        }
        if let typeValue = searchChargerData.type {
            self.type = typeValue
        }
        if let arrayOfPlugs = searchChargerData.plugs {
            for plug in arrayOfPlugs {
                if let plugData = plug {
                    self.plugs.append(PlugModel(searchPlugData: plugData))
                }
            }
        }
    }
    
    init(favChargerData:UserQuery.Data.User.Preference.Station.Charger) {
        
        if let idValue = favChargerData.id {
            self.id = idValue
        }
        if let nameValue = favChargerData.name {
            self.name = nameValue
        }
        if let statusValue = favChargerData.status {
            self.status = statusValue
        }
        if let powerValue = favChargerData.power {
            self.power = powerValue
        }
        if let typeValue = favChargerData.type {
            self.type = typeValue
        }
        if let arrayOfPlugs = favChargerData.plugs {
            for plug in arrayOfPlugs {
                if let plugData = plug {
                    self.plugs.append(PlugModel(favPlugData: plugData))
                }
            }
        }
    }
    
    init(resolveChargerData:ResolveChargeQuery.Data.ResolveCharge.Station.Charger) {
        
        if let idValue = resolveChargerData.id {
            self.id = idValue
        }
        if let nameValue = resolveChargerData.name {
            self.name = nameValue
        }
        if let statusValue = resolveChargerData.status {
            self.status = statusValue
        }
        if let powerValue = resolveChargerData.power {
            self.power = powerValue
        }
        if let typeValue = resolveChargerData.type {
            self.type = typeValue
        }
        if let arrayOfPlugs = resolveChargerData.plugs {
            for plug in arrayOfPlugs {
                if let plugData = plug {
                    self.plugs.append(PlugModel(resolveChargeData: plugData))
                }
            }
        }
    }
    
    init(reserveChargerData:ReserveMutation.Data.Reserve.Datum.Charger) {
        
        if let idValue = reserveChargerData.id {
            self.id = idValue
        }
        if let nameValue = reserveChargerData.name {
            self.name = nameValue
        }
    }
    
    init(bookingChargerData:ReservationsListQuery.Data.Reservation.Doc.Charger) {
        
        if let idValue = bookingChargerData.id {
            self.id = idValue
        }
        if let nameValue = bookingChargerData.name {
            self.name = nameValue
        }
    }
}

class PlugModel {
    
    var id : String?
    var name : String?
    var status : PlugStatus?
    var supportedPort : String?
    var power:Double?
    var perUnitAcCharge:Double?
    var perUnitDcCharge:Double?
    
    init() {
        
    }
    
    init(searchPlugData:SearchQuery.Data.Search.Doc.Station.Charger.Plug) {
        
        if let idValue = searchPlugData.id {
            self.id = idValue
        }
        if let nameValue = searchPlugData.name {
            self.name = nameValue
        }
        if let statusValue = searchPlugData.status {
            self.status = statusValue
        }
        if let supportedPortValue = searchPlugData.supportedPort {
//            for supportedPort in arrayOfsupportedPorts {
//                if let supportedPortData = supportedPort {
//                    self.supportedPorts.append(supportedPortData)
//                }
//            }
            self.supportedPort = supportedPortValue
        }
        if let powerValue = searchPlugData.power {
            self.power = powerValue
        }
        if let perUnitAcChargeValue = searchPlugData.perUnitAcCharge {
            self.perUnitAcCharge = perUnitAcChargeValue
        }
        if let perUnitDcChargeValue = searchPlugData.perUnitDcCharge {
            self.perUnitDcCharge = perUnitDcChargeValue
        }
    }
    
    init(favPlugData:UserQuery.Data.User.Preference.Station.Charger.Plug) {
        
        if let idValue = favPlugData.id {
            self.id = idValue
        }
        if let nameValue = favPlugData.name {
            self.name = nameValue
        }
        if let statusValue = favPlugData.status {
            self.status = statusValue
        }
        if let supportedPortValue = favPlugData.supportedPort {
//            for supportedPort in arrayOfsupportedPorts {
//                if let supportedPortData = supportedPort {
//                    self.supportedPorts.append(supportedPortData)
//                }
//            }
            self.supportedPort = supportedPortValue
        }
        if let powerValue = favPlugData.power {
            self.power = powerValue
        }
        if let perUnitAcChargeValue = favPlugData.perUnitAcCharge {
            self.perUnitAcCharge = perUnitAcChargeValue
        }
        if let perUnitDcChargeValue = favPlugData.perUnitDcCharge {
            self.perUnitDcCharge = perUnitDcChargeValue
        }
    }
    
    init(resolveChargeData:ResolveChargeQuery.Data.ResolveCharge.Station.Charger.Plug) {
        
        if let idValue = resolveChargeData.id {
            self.id = idValue
        }
        if let nameValue = resolveChargeData.name {
            self.name = nameValue
        }
        if let statusValue = resolveChargeData.status {
            self.status = statusValue
        }
        if let supportedPortValue = resolveChargeData.supportedPort {
//            for supportedPort in arrayOfsupportedPorts {
//                if let supportedPortData = supportedPort {
//                    self.supportedPorts.append(supportedPortData)
//                }
//            }
            self.supportedPort = supportedPortValue
        }
        if let powerValue = resolveChargeData.power {
            self.power = powerValue
        }
        if let perUnitAcChargeValue = resolveChargeData.perUnitAcCharge {
            self.perUnitAcCharge = perUnitAcChargeValue
        }
        if let perUnitDcChargeValue = resolveChargeData.perUnitDcCharge {
            self.perUnitDcCharge = perUnitDcChargeValue
        }
    }
    
    init(reservePlugData:ReserveMutation.Data.Reserve.Datum.Plug) {
        if let idValue = reservePlugData.id {
            self.id = idValue
        }
        if let nameValue = reservePlugData.name {
            self.name = nameValue
        }
    }
    
    init(bookingPlugData:ReservationsListQuery.Data.Reservation.Doc.Plug) {
        if let idValue = bookingPlugData.id {
            self.id = idValue
        }
        if let nameValue = bookingPlugData.name {
            self.name = nameValue
        }
    }
}
