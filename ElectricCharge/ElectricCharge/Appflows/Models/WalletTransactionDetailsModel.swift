//
//  WalletTransactionDetailsModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 12/10/22.
//

import Foundation

class WalletTransactionDetailsModel {
    var id : String?
    var date : String?
    var amount: Double?
    var type : WalletTransactionType?
    var status : WalletTransactionStatus?
    var reserved : Bool?
    var dateFormatter = DateFormatter()
    
    init(transactionDetails:WalletQuery.Data.Wallet.Transaction) {
        if let idValue = transactionDetails._id {
            self.id = idValue
        }
        if let amountValue = transactionDetails.amount {
            self.amount = amountValue
        }
        if let typeValue = transactionDetails.type {
            self.type = typeValue
        }
        if let statusValue = transactionDetails.status {
            self.status = statusValue
        }
        if let reservedValue = transactionDetails.reserved {
            self.reserved = reservedValue
        }
        
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "dd MMM yyyy"
        if let createdAtValue = transactionDetails.createdAt, let timeStampValue = Double(createdAtValue) {
            self.date = dateFormatter.string(from: Date(timeIntervalSince1970: timeStampValue/1000))
        } else {
            self.date = dateFormatter.string(from: Date())
        }
    }
}
