//
//  SearchRequest.swift
//  ElectricCharge
//
//  Created by iosdev on 2/23/22.
//

import Foundation

class SearchRequestModel {
    var query : String = ""
    var chargeType : [String] = []
    var plugType : [String] = []
    var withIn : [String:Any] = [:]
    var surrounding : [String:Any] = [:]
    var isForWithIn : Bool = false
    var sortBy : String = ""

    init(query:String, chargeType:[String], plugType:[String], withIn:[String:Any], surrounding:[String:Any], isForWithIn:Bool, sortBy:String) {
        self.query = query
        self.chargeType = chargeType
        self.plugType = plugType
        self.withIn = withIn
        self.surrounding = surrounding
        self.isForWithIn = isForWithIn
        self.sortBy = sortBy
    }
    
}
