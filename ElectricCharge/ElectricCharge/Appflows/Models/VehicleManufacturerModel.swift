//
//  VehicleManufacturerModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 01/05/23.
//

import Foundation

class VehicleManufacturerModel {
    var id : String?
    var name : String?
    var type : [String?]?

    init(vehicleManufacturerResponse:VehicleManufacturerQuery.Data.VehicleManufacturer) {
        if let idValue = vehicleManufacturerResponse.id {
            self.id = idValue
        }
        if let nameValue = vehicleManufacturerResponse.name {
            self.name = nameValue
        }
        if let typeValue = vehicleManufacturerResponse.type {
            self.type = typeValue
        }
    }
}

