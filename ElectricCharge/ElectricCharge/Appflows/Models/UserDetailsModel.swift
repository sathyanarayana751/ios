//
//  UserModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 28/06/22.
//

import Foundation

class UserDetailsModel {
    var sessionToken : String?
    var id : String?
    var name : String?
    var email : String?
    var phone : String?
    var stationIds: [String]?
    
    init(googleSignInResponse:GoogleSignInResponse) {
        if let idValue = googleSignInResponse.userId {
            self.id = idValue
        }
        if let nameValue = googleSignInResponse.fullName {
            self.name = nameValue
        }
        if let emailValue = googleSignInResponse.email {
            self.email = emailValue
        }
    }
    
    init(socialLoginUserExistData:SocialLoginWithGoogleTokenMutation.Data.SocialLoginWithGoogleToken.Datum.User, sessionToken:String) {
        
        self.sessionToken = sessionToken
        
        if let idValue = socialLoginUserExistData.id {
            self.id = idValue
        }
        if let nameValue = socialLoginUserExistData.name {
            self.name = nameValue
        }
        if let emailValue = socialLoginUserExistData.email {
            self.email = emailValue
        }
        if let phoneValue = socialLoginUserExistData.phone {
            self.phone = phoneValue
        }
        if let preferences = socialLoginUserExistData.preference, let stationIds = preferences.stationIds {
            
            self.stationIds = []
            for stationId in stationIds {
                if let eachStationId = stationId {
                    self.stationIds?.append(eachStationId)
                }
            }
        }
    }
    
    init(loginUserData:LoginMutation.Data.LoginUser.Datum.User, sessionToken:String) {
        
        self.sessionToken = sessionToken
        
        if let idValue = loginUserData.id {
            self.id = idValue
        }
        if let nameValue = loginUserData.name {
            self.name = nameValue
        }
        if let emailValue = loginUserData.email {
            self.email = emailValue
        }
        if let phoneValue = loginUserData.phone {
            self.phone = phoneValue
        }
        if let preferences = loginUserData.preference, let stationIds = preferences.stationIds {
            
            self.stationIds = []
            for stationId in stationIds {
                if let eachStationId = stationId {
                    self.stationIds?.append(eachStationId)
                }
            }
        }
    }
    
    init(registerUserData:VerifyRegistrationOtpMutation.Data.VerifyRegistrationOtp.Datum.User, sessionToken:String) {
        
        self.sessionToken = sessionToken
        
        if let idValue = registerUserData.id {
            self.id = idValue
        }
        if let nameValue = registerUserData.name {
            self.name = nameValue
        }
        if let emailValue = registerUserData.email {
            self.email = emailValue
        }
        if let phoneValue = registerUserData.phone {
            self.phone = phoneValue
        }
        if let preferences = registerUserData.preference, let stationIds = preferences.stationIds {
            
            self.stationIds = []
            for stationId in stationIds {
                if let eachStationId = stationId {
                    self.stationIds?.append(eachStationId)
                }
            }
        }
    }
    
    init(token:String, id:String, name:String, email:String, phoneNumber:String, stationIds:[String]) {
        
        self.sessionToken = token
        self.id = id
        self.name = name
        self.email = email
        self.phone = phoneNumber
        self.stationIds = stationIds
    }
}

