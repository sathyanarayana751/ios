//
//  ReserveRequestModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 18/03/22.
//

import Foundation

class ReserveSlotRequestModel {

    var stationId = ""
    var powerValue = 0.0
    var chargeType = ""
    var plugType = ""
    var startTime = ""
    var endTime = ""
    var chargerId = ""
    var plugId = ""

    init(stationId:String,
         powerValue:Double,
         chargeType:String,
         plugType:String,
         startTime:String,
         endTime:String,
         chargerId:String,
         plugId:String) {
        
        self.stationId = stationId
        self.powerValue = powerValue
        self.chargeType = chargeType
        self.plugType = plugType
        self.startTime = startTime
        self.endTime = endTime
        self.chargerId = chargerId
        self.plugId = plugId
    }
    
}
