//
//  AvailableSlotRequest.swift
//  ElectricCharge
//
//  Created by Raghavendra on 17/03/22.
//

import Foundation

class AvailableSlotRequestModel {

    var stationId = ""
    var powerValue = 0.0
    var chargeType = ""
    var plugType = ""
    var startTime = ""
    var endTime = ""
    var durationInMin = 0

    init(stationId:String,
         powerValue:Double,
         chargeType:String,
         plugType:String,
         startTime:String,
         endTime:String,
         durationInMin:Int) {
        
        self.stationId = stationId
        self.powerValue = powerValue
        self.chargeType = chargeType
        self.plugType = plugType
        self.startTime = startTime
        self.endTime = endTime
        self.durationInMin = durationInMin
    }
    
}
