//
//  VehicleModel.swift
//  ElectricCharge
//
//  Created by Raghavendra on 01/05/23.
//

import Foundation

class VehicleModel {
    var id : String?
    var name : String?
    var type : String?
    var manufacturerId : String?
    var maxPower : Double?
    var powerType : [PowerType?]?
    var supportedPorts : [String?]?
    var supportedPortTypes : [PortTypesModel]?
    
    init(vehicleModelResponse:VehicleModelsQuery.Data.VehicleModel.Doc) {
        if let idValue = vehicleModelResponse.id {
            self.id = idValue
        }
        if let nameValue = vehicleModelResponse.name {
            self.name = nameValue
        }
        if let typeValue = vehicleModelResponse.type {
            self.type = typeValue
        }
        if let manufacturerIdValue = vehicleModelResponse.manufacturerId {
            self.manufacturerId = manufacturerIdValue
        }
        if let maxPowerValue = vehicleModelResponse.maxPower {
            self.maxPower = maxPowerValue
        }
        if let powerTypeValue = vehicleModelResponse.powerType {
            self.powerType = powerTypeValue
        }
        if let supportedPortsValue = vehicleModelResponse.supportedPorts {
            self.supportedPorts = supportedPortsValue
        }
        
        if let supportedPortTypesValue = vehicleModelResponse.supportedPortTypes {
            for eachSupportedPortType in supportedPortTypesValue {
                if let eachSupportedPortTypeValue = eachSupportedPortType {
                    self.supportedPortTypes?.append(PortTypesModel(vehicleModelPortTypesResponse: eachSupportedPortTypeValue))
                }
            }
        }
    }
}
