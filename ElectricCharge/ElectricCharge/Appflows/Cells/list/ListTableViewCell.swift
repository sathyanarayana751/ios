//
//  ListTableViewCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 28/11/21.
//

import UIKit
import SDWebImage
import UIView_Shimmer

class ListTableViewCell: UITableViewCell, ShimmeringViewProtocol {
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var stationImageView: UIImageView!
    
    @IBOutlet weak var stationNameLabel: UILabel!
    
    @IBOutlet weak var perUnitLabel: UILabel!
    
    @IBOutlet weak var perUnitValueLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var plugTypeLabel: UILabel!
    
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var availableButton: UIButton!
    
    let locationManager = LocationManager.shared
    
    var shimmeringAnimatedItems: [UIView] {
        [
            stationImageView,
            stationNameLabel,
            perUnitLabel,
            perUnitValueLabel,
            distanceLabel,
            plugTypeLabel,
            availableButton
        ]
    }
    var favSelectClosure: (()->())?

    //    var stationDetails:SearchQuery.Data.Search.Doc = SearchQuery.Data.Search.Doc()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func favouriteButtonAction(_ sender: Any) {
        self.favSelectClosure?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showStationDetails(stationData:StationDetailsModel) {
                
        self.stationNameLabel.text = stationData.name
        var chargeType = "DC"
        if let selectedChargeType = stationData.selectedChargeType {
            chargeType = selectedChargeType
        }
        
        if chargeType == "AC" {
            if let perUnitACCharge = stationData.perUnitACCharge {
                self.perUnitValueLabel.text = "₹\(Int(perUnitACCharge))"
            } else {
                self.perUnitValueLabel.text = "₹10"
            }
        } else {
            if let perUnitDCCharge = stationData.perUnitDCCharge {
                self.perUnitValueLabel.text = "₹\(Int(perUnitDCCharge))"
            } else {
                self.perUnitValueLabel.text = "₹10"
            }
        }
        
        self.perUnitLabel.text = "/ unit"
                
        var plugType = "CCS-2"
        if let selectedPlugType = stationData.selectedPlugType {
            plugType = selectedPlugType
        }
        self.plugTypeLabel.text = plugType
        
        if let arrayOfImages = stationData.images, arrayOfImages.count > 0, let firstImageURL = arrayOfImages.first as? String {
            self.stationImageView.sd_setImage(with: URL(string: firstImageURL), placeholderImage: nil)
        }
        
        var stationAvailable = false
        if let status = stationData.status {
            if status == .active {
                stationAvailable = true
            }
        }
        
        if stationAvailable {
            availableButton.backgroundColor = UIColor(named: "2ECE54")
            availableButton.borderWidth = 0
            availableButton.borderColor = UIColor.clear
            availableButton.setTitleColor(UIColor.white, for: .normal)
            availableButton.setTitle("Available", for: .normal)
        } else {
            availableButton.backgroundColor = UIColor.clear
            availableButton.borderWidth = 1
            availableButton.borderColor = UIColor(named: "2A382C")
            availableButton.setTitleColor(UIColor(named: "2A382C"), for: .normal)
            availableButton.setTitle("Not Available", for: .normal)
        }
        
        availableButton.isHidden = true
        
        if let coordinates = stationData.locationCoordinates, coordinates.count == 2, let longitude = coordinates[0], let latitude = coordinates[1], let searchLatitude = stationData.searchLatitude, let searchLongitude = stationData.searchLongitude {
            
            let distance = locationManager.calculateDistance(loc1Latitude: searchLatitude,
                                                             loc1Longitude: searchLongitude,
                                                             loc2Latitude: latitude,
                                                             loc2Longitude: longitude)
            
            self.distanceLabel.text = String(format: "%.01f kms away", distance)
        }
    }
}
