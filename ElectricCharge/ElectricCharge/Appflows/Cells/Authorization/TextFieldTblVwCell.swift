//
//  TextFieldTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit
enum SigninEnum: Int {
    case email =  0
    case password =  1
}

enum SignupEnum: Int {
    case name =  0
    case email =  1
    case phone = 2
    case password = 3
}

enum Corporate: Int {
    case companyID =  0
    case employeeID =  1
    case password = 2
}

class TextFieldTblVwCell: UITableViewCell {
    @IBOutlet weak var vwTextfield: UIView!
    @IBOutlet weak var textField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheSigninFields(index: Int) {
        let type = SigninEnum(rawValue: index) ?? .email
        switch type {
        case .email:
            self.textField.placeholder = "Email / Phone"
        case .password:
            self.textField.placeholder = "Password"
        }
    }
    
    func updateTheSignUpFields(index: Int) {
        let type = SignupEnum(rawValue: index) ?? .name
        switch type {
        case .name:
            self.textField.placeholder = "Name"
        case .email:
            self.textField.placeholder = "Email"
        case .phone:
            self.textField.placeholder = "Phone"
        case .password:
            self.textField.placeholder = "Password"
        }
    }
    
    func updateTheCorporateFields(index: Int) {
        let type = Corporate(rawValue: index) ?? .companyID
        switch type {
        case .companyID:
            self.textField.placeholder = "Company ID"
        case .employeeID:
            self.textField.placeholder = "Employee ID"
        case .password:
            self.textField.placeholder = "Password"
        }
    }
}
