//
//  ViewTitleTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit

class ViewTitleTblVwCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var titleView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
