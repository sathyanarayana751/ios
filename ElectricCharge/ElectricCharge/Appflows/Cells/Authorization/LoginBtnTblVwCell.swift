//
//  LoginBtnTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit

class LoginBtnTblVwCell: UITableViewCell {

    @IBOutlet weak var loginBtn: UIButton!
    override func awakeFromNib() {
        loginBtn.addTheCornerRadius(radius: 5)
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
