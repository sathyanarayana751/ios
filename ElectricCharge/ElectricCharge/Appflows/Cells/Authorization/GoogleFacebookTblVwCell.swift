//
//  GoogleFacebookTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit

class GoogleFacebookTblVwCell: UITableViewCell {
    @IBOutlet weak var googleVw: UIView!
    @IBOutlet weak var facebookVw: UIView!
    
    @IBOutlet weak var lblGoogle: UILabel!
    @IBOutlet weak var lblFacebook: UILabel!
    
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        googleVw.addBorderColor(radius: 10, color: UIColor(named: "EAEAEA")!)
        facebookVw.addBorderColor(radius: 10, color: UIColor(named: "EAEAEA")!)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
