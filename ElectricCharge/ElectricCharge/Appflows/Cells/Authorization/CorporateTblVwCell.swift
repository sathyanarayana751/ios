//
//  CorporateTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 23/10/21.
//

import UIKit

class CorporateTblVwCell: UITableViewCell {
    @IBOutlet weak var lblCorporateTitle: UILabel!
    @IBOutlet weak var lblForgotPasswordTitle: UILabel!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var btnCorporate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hideTheCorporate(hide: Bool) {
        self.btnCorporate.isHidden = hide
        self.lblCorporateTitle.isHidden = hide
    }
}
