//
//  PlugTypeColViewCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 16/11/21.
//

import UIKit
import UIView_Shimmer

class PlugTypeColViewCell: UICollectionViewCell,ShimmeringViewProtocol {
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var plugTypeImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    var shimmeringAnimatedItems: [UIView] {
        [
            bgView,
            plugTypeImg,
            lblType,
            radioButton
        ]
    }
}
