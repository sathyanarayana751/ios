//
//  UICollectionViewShadowCell.swift
//  ElectricCharge
//
//  Created by Raghavendra on 28/03/22.
//

import UIKit

class UICollectionViewShadowCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 3.0
    }
    
    override func layoutSubviews() {
        
    }

}
