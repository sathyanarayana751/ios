//
//  UpcomingBookingCollectionVwCell.swift
//  ElectricCharge
//
//  Created by Raghavendra on 28/03/22.
//

import UIKit
import UIView_Shimmer

class UpcomingBookingCollectionVwCell: UICollectionViewShadowCell, ShimmeringViewProtocol {
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var stationImageView: UIImageView!
    
    @IBOutlet weak var stationNameLabel: UILabel!
    
    @IBOutlet weak var availableButton: UIButton!
    
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var selectedSlotsTitleLabel: UILabel!
    @IBOutlet weak var selectedSlotsLabel: UILabel!
        
    @IBOutlet weak var bookedDateTitleLabel: UILabel!
    @IBOutlet weak var bookedDateLabel: UILabel!
    
    @IBOutlet weak var eachBookingIdentifier: UIButton!
    
    var selectedDateFormatter = DateFormatter()
    var slotsTimeFormatter = DateFormatter()

    var shimmeringAnimatedItems: [UIView] {
        [
            stationImageView,
            stationNameLabel,
            availableButton,
            favouriteButton,
            selectedSlotsTitleLabel,
            selectedSlotsLabel,
            bookedDateTitleLabel,
            bookedDateLabel
        ]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "MMM dd"
        
        slotsTimeFormatter.locale = Locale.current
        slotsTimeFormatter.dateFormat = "h:mm a"
    }
    
    override func layoutSubviews() {
        self.topView.layer.masksToBounds = true
    }
    
    func showReservationDetails(reservationData:ReservationsListQuery.Data.Reservation.Doc) {
        
        if let stationData = reservationData.station {
            
            self.stationNameLabel.text = stationData.name
            
//            self.favouriteButton.setImage(UIImage(named: "heart-outline"), for: .normal)
            
            if let arrayOfImages = stationData.images, arrayOfImages.count > 0, let firstImageURL = arrayOfImages.first as? String {
                self.stationImageView.sd_setImage(with: URL(string: firstImageURL), placeholderImage: nil)
            }
            
            /*var stationAvailable = false
            if let status = stationData.status {
                if status == .active {
                    stationAvailable = true
                }
            }*/
            
            availableButton.borderWidth = 0
            availableButton.borderColor = UIColor.clear
            availableButton.setTitleColor(UIColor.white, for: .normal)
            switch reservationData.status {
                case .hold:
                availableButton.backgroundColor = UIColor.lightGray
                    availableButton.setTitle(ReservationStatus.hold.rawValue, for: .normal)
                    break
                case .reserved:
                    availableButton.backgroundColor = UIColor(named: "FFAE42")
                    availableButton.setTitle(ReservationStatus.reserved.rawValue, for: .normal)
                    break
                case .running:
                    availableButton.backgroundColor = UIColor(named: "2ECE54")
                    availableButton.setTitle(ReservationStatus.running.rawValue, for: .normal)
                    break
                case .completed:
                    availableButton.backgroundColor = UIColor(named: "2ECE54")
                    availableButton.setTitle(ReservationStatus.completed.rawValue, for: .normal)
                    break
                case .canceled:
                    availableButton.backgroundColor = UIColor(named: "FF3B30")
                    availableButton.setTitle(ReservationStatus.canceled.rawValue, for: .normal)
                    break
                case .none,.some(.__unknown(_)):
                    break
            }
        }
        
        if let startDateAndTime = reservationData.startTime, let endDateAndTime = reservationData.endTime {
            
            let reservationStartDate = Date(timeIntervalSince1970:TimeInterval(startDateAndTime)!/1000)
            let reservationEndDate = Date(timeIntervalSince1970:TimeInterval(endDateAndTime)!/1000)
                
            self.bookedDateLabel.text = selectedDateFormatter.string(from: reservationStartDate)
            
            self.selectedSlotsLabel.text = "\(slotsTimeFormatter.string(from: reservationStartDate)) - \(slotsTimeFormatter.string(from: reservationEndDate))"
        }
    }
}
