//
//  BookingHistoryFilterTblCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit
import UIView_Shimmer

class BookingHistoryFilterTblCell: UITableViewShadowCell, ShimmeringViewProtocol {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var stationImageView: UIImageView!
    
    @IBOutlet weak var stationNameLabel: UILabel!
    
    @IBOutlet weak var selectedSlotsTitleLabel: UILabel!
    @IBOutlet weak var selectedSlotsLabel: UILabel!
        
    @IBOutlet weak var bookedDateTitleLabel: UILabel!
    @IBOutlet weak var bookedDateLabel: UILabel!
    @IBOutlet weak var eachBookingIdentifier: UIButton!

    @IBOutlet weak var bookingUnavailableView: UIView!
    
    @IBOutlet weak var bookingStatusLabel: UILabel!
    
    
    var arrayOfReservations:[ReservationsListQuery.Data.Reservation.Doc?] = []
    var selectedDateFormatter = DateFormatter()
    var slotsTimeFormatter = DateFormatter()
    
    var shimmeringAnimatedItems: [UIView] {
        [
            stationImageView,
            stationNameLabel,
            selectedSlotsTitleLabel,
            selectedSlotsLabel,
            bookedDateTitleLabel,
            bookedDateLabel,
            bookingStatusLabel
        ]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.layer.masksToBounds = true
        selectedDateFormatter.locale = Locale.current
        selectedDateFormatter.dateFormat = "MMM dd"
        
        slotsTimeFormatter.locale = Locale.current
        slotsTimeFormatter.dateFormat = "h:mm a"
    }
    
    override func layoutSubviews() {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showReservationDetails(reservationData:ReservationsListQuery.Data.Reservation.Doc) {
        
        if let stationData = reservationData.station {
            
            self.stationNameLabel.text = stationData.name
                        
            if let arrayOfImages = stationData.images, arrayOfImages.count > 0, let firstImageURL = arrayOfImages.first as? String {
                self.stationImageView.sd_setImage(with: URL(string: firstImageURL), placeholderImage: nil)
            }
        }
        
        if let startDateAndTime = reservationData.startTime, let endDateAndTime = reservationData.endTime {
            
            let reservationStartDate = Date(timeIntervalSince1970:TimeInterval(startDateAndTime)!/1000)
            let reservationEndDate = Date(timeIntervalSince1970:TimeInterval(endDateAndTime)!/1000)
                
            self.bookedDateLabel.text = selectedDateFormatter.string(from: reservationStartDate)
            
            self.selectedSlotsLabel.text = "\(slotsTimeFormatter.string(from: reservationStartDate)) - \(slotsTimeFormatter.string(from: reservationEndDate))"
        }
        
        switch reservationData.status {
            case .hold:
                bookingStatusLabel.textColor = UIColor.lightGray
                bookingStatusLabel.text = ReservationStatus.hold.rawValue
                break
            case .reserved:
                bookingStatusLabel.textColor = UIColor(named: "FFAE42")
                bookingStatusLabel.text = ReservationStatus.reserved.rawValue
                break
            case .running:
                bookingStatusLabel.textColor = UIColor(named: "2ECE54")
                bookingStatusLabel.text = ReservationStatus.running.rawValue
                break
            case .completed:
                bookingStatusLabel.textColor = UIColor(named: "2ECE54")
                bookingStatusLabel.text = ReservationStatus.completed.rawValue
                break
            case .canceled:
                bookingStatusLabel.textColor = UIColor(named: "FF3B30")
                bookingStatusLabel.text = ReservationStatus.canceled.rawValue
                break
            case .none,.some(.__unknown(_)):
                break
        }
    }

}
