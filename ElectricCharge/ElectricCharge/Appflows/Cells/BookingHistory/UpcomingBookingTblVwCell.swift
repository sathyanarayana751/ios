//
//  UpcomingBookingTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class UpcomingBookingTblVwCell: UITableViewCell {

    @IBOutlet weak var upcomingBookingsCollectionView: UICollectionView!
    
    @IBOutlet weak var upcomingBookingsUnavailableView: UIView!
    
    var isLoading = false
    var arrayOfReservations:[ReservationsListQuery.Data.Reservation.Doc?] = []
    var favouriteStations:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCollectionViewInitialProperties()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewInitialProperties() {
        
        let cellWidth = (0.85) * UIScreen.main.bounds.width
        let sectionSpacing = (1/13) * UIScreen.main.bounds.width
        let cellSpacing = (1/34) * UIScreen.main.bounds.width
        let cellHeight = upcomingBookingsCollectionView.bounds.height
        
        let layout = PagingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: sectionSpacing, bottom: 0, right: sectionSpacing)
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.minimumLineSpacing = cellSpacing
        upcomingBookingsCollectionView.setCollectionViewLayout(layout, animated: false)
        upcomingBookingsCollectionView.decelerationRate = .fast
    }
}

extension UpcomingBookingTblVwCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isLoading {
            return 5
        } else {
            if arrayOfReservations.count > 0 {
                upcomingBookingsUnavailableView.isHidden = true
                return arrayOfReservations.count
            } else {
                upcomingBookingsUnavailableView.isHidden = false
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpcomingBookingCollectionVwCell", for: indexPath) as? UpcomingBookingCollectionVwCell

        cell?.eachBookingIdentifier.tag = indexPath.row
        if !isLoading {
            if let particularReservationData = arrayOfReservations[indexPath.row] {
                cell?.showReservationDetails(reservationData: particularReservationData)
                cell?.favouriteButton.isUserInteractionEnabled = false
                if self.favouriteStations.contains((particularReservationData.station?.id)!) {
                    cell?.favouriteButton.setImage(UIImage(named: "heart-outline-red"), for: .normal)
                } else {
                    cell?.favouriteButton.setImage(UIImage(named: "heart-outline"), for: .normal)
                }
            }
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.setTemplateWithSubviews(isLoading, animate: true, viewBackgroundColor: .systemBackground)
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//
//        cell.setTemplateWithSubviews(viewModel.isLoading, animate: true, viewBackgroundColor: .systemBackground)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if let particularStationData = viewModel.arrayOfStations[indexPath.row], let stationData = particularStationData.station {
//            moveToScheduleChargingView(stationDetails: stationData)
//        }
    }
}

