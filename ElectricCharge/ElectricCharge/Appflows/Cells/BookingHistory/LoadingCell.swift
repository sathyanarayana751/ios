//
//  LoadingCell.swift
//  ElectricCharge
//
//  Created by Raghavendra on 28/06/22.
//

import Foundation
import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
