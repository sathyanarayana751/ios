//
//  CheckAvailabilityTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class CheckAvailabilityTblVwCell: UITableViewCell {

    @IBOutlet weak var startTimeButton: UIButton!
    @IBOutlet weak var endTimeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
