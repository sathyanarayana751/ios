//
//  SelectDateTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class SelectDateTblVwCell: UITableViewCell {

    @IBOutlet weak var dateColVw2: UICollectionView!
    var arrayOfDates:[Date] = []
    var selectedRowIndex = 0
    var monthFormatter = DateFormatter()
    var dayFormatter = DateFormatter()
    let calendar = Calendar(identifier: .gregorian)

    var dateChangeClosure: (()->())?
    
    var selectedDate:Date = Date() {
        didSet {
            self.dateChangeClosure?()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        monthFormatter.locale = Locale.current
        dayFormatter.locale = Locale.current
        monthFormatter.dateFormat = "MMM"
        dayFormatter.dateFormat = "d"
        let startDate = getCurrentDate()
        var dateComponent = DateComponents()
        arrayOfDates.append(startDate)
        for i in 1...6
        {
            dateComponent.day = i
            if let newDate = calendar.date(byAdding: dateComponent, to: startDate) {
                print("\(newDate)")
                arrayOfDates.append(newDate)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getCurrentDate() -> Date {
        var calendar = Calendar.current
        calendar.locale = Locale.current

        let startDate = Date().dateWithInterval(interval: (15.0 * 60.0), minimumInterval: (5.0 * 60.0))
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate)

        components.hour = 0
        components.minute = 0
        components.second = 0

        return calendar.date(from: components)!
    }

}

extension  SelectDateTblVwCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfDates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DateColVwCell.identifier, for: indexPath) as? DateColVwCell
        if indexPath.row  ==  selectedRowIndex {
            cell?.lblDate.textColor = UIColor.white
            cell?.lblMonth.textColor = UIColor.white
            cell?.bgView.backgroundColor = UIColor(named: "2ECE54")
            self.selectedDate = arrayOfDates[indexPath.row]
        } else {
            cell?.lblDate.textColor = UIColor(named: "2A382C")
            cell?.lblMonth.textColor = UIColor(named: "2A382C")
            cell?.bgView.backgroundColor = UIColor(named: "F4F4F4")
        }
        
        let date = arrayOfDates[indexPath.row]
        cell?.lblMonth.text = monthFormatter.string(from: date)
        cell?.lblDate.text = dayFormatter.string(from: date)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 63, height: 71)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedRowIndex = indexPath.row
        //selectedDate = arrayOfDates[selectedRowIndex]
        self.dateColVw2.reloadData()
    }
}
