//
//  ChargeDurationTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class ChargeDurationTblVwCell: UITableViewCell {

    @IBOutlet weak var durationHourButton: UIButton!
    @IBOutlet weak var durationMinuteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
