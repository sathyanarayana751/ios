//
//  DateColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 30/01/22.
//

import UIKit

class DateColVwCell: UICollectionViewCell {
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var bgView: UIView!
}
