//
//  MachineDetailsTblVWCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class MachineDetailsTblVWCell: UITableViewCell {

    @IBOutlet weak var lblMachineDetails: UILabel!
    @IBOutlet weak var imgVwMachine: UIImageView!
    @IBOutlet weak var lblMachineNname: UILabel!
    @IBOutlet weak var lblMachineNameValue: UILabel!
    @IBOutlet weak var lblPlug: UILabel!
    @IBOutlet weak var lblPlugName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
