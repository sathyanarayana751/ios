//
//  TimeSlotColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import UIView_Shimmer

class TimeSlotColVwCell: UICollectionViewCell, ShimmeringViewProtocol {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var vwTime: UIView!
    
    var shimmeringAnimatedItems: [UIView] {
        [
            vwTime
        ]
    }
}
