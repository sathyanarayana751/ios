//
//  StationImageColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class StationImageColVwCell: UICollectionViewCell {
    @IBOutlet weak var imgVwStationImage: UIImageView!
    
}
