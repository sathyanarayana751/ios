//
//  PlugTypeColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit

class PlugTypeColVwCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var radioBtnn: UIButton!
    @IBOutlet weak var lblPower: UILabel!
    @IBOutlet weak var imgVwPlugType: UIImageView!
}
