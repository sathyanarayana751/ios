//
//  WalletHistoryTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import UIView_Shimmer

class WalletHistoryTblVwCell: UITableViewCell,ShimmeringViewProtocol {
    @IBOutlet weak var lblAmountID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var vwBackground: UIView!
    
    var shimmeringAnimatedItems: [UIView] {
        [
            lblAmountID,
            lblDate,
            lblAmount,
            vwBackground
        ]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
