//
//  WalletDetailsView.swift
//  ElectricCharge
//
//  Created by Raghavendra on 07/09/22.
//

import UIKit
import UIView_Shimmer

class WalletDetailsCollectionViewCell:UICollectionViewCell, ShimmeringViewProtocol {
        
    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var lblWalletBalance: UILabel!
    
    @IBOutlet weak var reservedAmountTitleLabel: UILabel!
    @IBOutlet weak var reservedAmountValueLabel: UILabel!
    @IBOutlet weak var availableAmountTitleLabel: UILabel!
    @IBOutlet weak var availableAmountValueLabel: UILabel!
    
    var shimmeringAnimatedItems: [UIView] {
        [
            lblWalletAmount,
            lblWalletBalance,
            reservedAmountTitleLabel,
            reservedAmountValueLabel,
            availableAmountTitleLabel,
            availableAmountValueLabel
        ]
    }    
}
