//
//  AddMoneyTblVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import UIView_Shimmer

class AddMoneyTblVwCell: UITableViewCell, ShimmeringViewProtocol {

    @IBOutlet weak var colVwAmounts: UICollectionView!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var btnAddAmount: LoadingButton!
    
    @IBOutlet weak var textFieldBackView: UIView!
    
    var selectedIndex = 0
    let arrayOfAmounts = ["500","1000","1500"]
    
    var sourceVC:WalletVC?
    
    var shimmeringAnimatedItems: [UIView] {
        [
            colVwAmounts,
            tfAmount,
            btnAddAmount,
            textFieldBackView
        ]
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTextFieldValue(){
        self.tfAmount.text = "\(self.arrayOfAmounts[selectedIndex])"
    }

    @IBAction func addAmountTap(_ sender: Any) {
        
        if (tfAmount.text?.isEmpty)! {
            self.sourceVC?.displayMessageAlert(withMessage: Constants.InputFields.MissingAmount)
        } else if Int(tfAmount.text!) ?? 0 < 500 {
            self.sourceVC?.displayMessageAlert(withMessage: Constants.InputFields.MissingMinimumAmount)
        } else {
            self.sourceVC?.callTopupWalletAPI(Int(tfAmount.text!) ?? 0)
        }
    }
}

extension AddMoneyTblVwCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfAmounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeSlotColVwCell", for: indexPath) as? TimeSlotColVwCell
        cell?.lblTime.text = "₹\(arrayOfAmounts[indexPath.row])"
        if indexPath.row == selectedIndex {
            cell?.lblTime.textColor = UIColor(hexString: "#FFFFFF")
            cell?.vwTime.backgroundColor = UIColor(hexString: "#2ECE54")
        } else {
            cell?.lblTime.textColor = UIColor(hexString: "#2ECE54")
            cell?.vwTime.backgroundColor = .white
            cell?.vwTime.borderColor =  UIColor(hexString: "#2ECE54")
            cell?.vwTime.borderWidth = 1
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        colVwAmounts.reloadData()
        updateTextFieldValue()
    }
}

