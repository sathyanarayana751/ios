//
//  SegmentHeaderColVwCell.swift
//  ElectricCharge
//
//  Created by Vengababu Maparthi on 22/01/22.
//

import UIKit
import UIView_Shimmer

class SegmentHeaderColVwCell: UICollectionViewCell, ShimmeringViewProtocol {
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var vwBottom: UIView!
    
    
    var shimmeringAnimatedItems: [UIView] {
        [
            lblHeader,
            vwBottom
        ]
    }
}
